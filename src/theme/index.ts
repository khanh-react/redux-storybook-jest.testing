import './customHue';

import { colors, createMuiTheme, responsiveFontSizes, ThemeOptions } from '@material-ui/core';
import { mergeAll } from 'ramda';

import typography from './typography';

const baseConfig: ThemeOptions = {
  direction: 'ltr',
  typography,
  overrides: {
    MuiLinearProgress: {
      root: {
        borderRadius: 3,
        overflow: 'hidden',
      },
    },
    MuiListItemIcon: {
      root: {
        minWidth: 32,
      },
    },
    MuiChip: {
      root: {
        backgroundColor: 'rgba(0,0,0,0.075)',
      },
    },
  },
};

export const themeConfig: ThemeOptions = {
  palette: {
    type: 'light',
    background: {
      default: colors.common.white,
      paper: '#f6f7f8',
    },
    primary: {
      light: '#ffeb98',
      main: '#ffe063',
      dark: '#e7cb59',
    },
    secondary: {
      light: '#86868E',
      main: '#2f2f32',
      dark: '#09090a',
      '100': '#007BFF', // Local Styles => Blue 100
    },
    error: {
      light: '#e87878',
      main: '#e54545',
      dark: '#c03535',
      '100': '#fa3434', // Local Styles => Red 100
    },
    info: {
      light: '#559eee',
      main: '#097af4',
      dark: '#0066d3',
      '100': '#6979F8', // Local Styles => Blue/Accent/ 1
    },
    success: {
      light: '#6ad7bb',
      main: '#00c692',
      dark: '#00b27d',
    },
    warning: {
      light: '#f8b267',
      main: '#ff9521',
      dark: '#e67a05',
      '100': '#FF9017', // Local Styles => Orange 100
    },
  },
};

export function createTheme() {
  const themeSetting: ThemeOptions = mergeAll([{}, baseConfig, themeConfig]);
  const theme = responsiveFontSizes(createMuiTheme(themeSetting));

  return theme;
}
