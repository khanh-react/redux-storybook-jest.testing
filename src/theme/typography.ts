import { TypographyOptions } from '@material-ui/core/styles/createTypography';

const typography: TypographyOptions = {
  fontFamily: [
    'Source Sans Pro',
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    'Roboto',
    '"Helvetica Neue"',
    'Arial',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
  ].join(','),

  h1: {
    fontWeight: 600,
    fontSize: 36,
    letterSpacing: '-0.24px',
  },
  h2: {
    fontWeight: 600,
    fontSize: 28,
    letterSpacing: '-0.24px',
  },
  h3: {
    fontWeight: 600,
    fontSize: 24,
    letterSpacing: '-0.06px',
  },
  h4: {
    fontWeight: 600,
    fontSize: 22,
    letterSpacing: '-0.06px',
  },
  h5: {
    fontWeight: 600,
    fontSize: 18,
    letterSpacing: '-0.05px',
  },
  h6: {
    fontWeight: 400,
    fontSize: 18,
    letterSpacing: '-0.05px',
  },
  overline: {
    fontWeight: 600,
  },
  subtitle1: {
    fontSize: 12,
    lineHeight: '15px',
    color: '#848489',
  },
  body1: {
    color: '#263238',
  },
  // subtitle2: {
  //   fontSize: 12,
  // }
};

export default typography;
