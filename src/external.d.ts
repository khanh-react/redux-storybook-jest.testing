declare module 'react-country-flag' {
  import * as React from 'react';

  export interface ReactCountryFlagProps<T> extends React.DetailedHTMLProps<React.LabelHTMLAttributes<T>, T> {
    cdnSuffix?: string;
    cdnUrl?: string;
    countryCode: string;
    svg?: boolean;
    style?: React.CSSProperties;
  }
  const ReactCountryFlag: React.FC<ReactCountryFlagProps<HTMLSpanElement | HTMLImageElement>> = ({
    /* eslint-disable @typescript-eslint/no-unused-vars */
    cdnSuffix = 'svg',
    cdnUrl = 'https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.4.3/flags/4x3/',
    svg = false,
    style = {},
    countryCode,
  }) => React.ReactNode; // return should be HTMLImageElement or HTMLSpanElement but close enough for me.
  export default ReactCountryFlag;
}

// Type definitions for react-mic 12.4
// Project: https://hackingbeauty.github.io/react-mic
// Definitions by: mikaello <https://github.com/mikaello>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/**
 * Record a user's voice and display as an oscillation (or frequency bars).
 */

declare module '@cleandersonlobo/react-mic' {
  export class ReactMic extends React.PureComponent<ReactMicProps> {}

  /**
   * The object sent when the recording stops
   */
  export interface ReactMicStopEvent {
    blob: Blob;
    startTime: number;
    stopTime: number;
    option: {
      audioBitsPerSecond: number;
      mimeType: string;
    };
    blobURL: string;
  }

  export interface ReactMicProps {
    /** Set to true to begin recording */
    record?: boolean;

    /** Available in React-Mic-Plus upgrade only */
    pause?: boolean;

    visualSetting?: 'sinewave' | 'frequencyBars';

    className?: string;

    /** Callback that is executed when audio stops recording */
    onStop?: (recordedData: ReactMicStopEvent) => void;

    /** Callback that is executed when chunk of audio is available */
    onData?: (recordedData: Blob) => void;

    /** defaults -> "audio/webm".  Set to "audio/wav" for WAV or "audio/mp3" for MP3 audio format (available in React-Mic-Gold) */
    mimeType?: 'audio/webm' | 'audio/wav' | 'audio/mp3' | 'audio/ogg';

    /** Sound wave color */
    strokeColor?: string;

    /** Background color */
    backgroundColor?: string;

    /** Specify 1 for mono, defaults -> 2 (stereo) */
    channelCount?: 1 | 2;

    /** Enables/disables echo cancellation, defaults -> false */
    echoCancellation?: boolean;

    /** Enables/disables auto gain control, defaults -> false */
    autoGainControl?: boolean;

    /** Enables/disables background noise suppression, defaults -> false */
    noiseSuppression?: boolean;
  }
}

declare module 'react-highlight-words-ref' {
  export interface FindChunks {
    autoEscape?: boolean;
    caseSensitive?: boolean;
    sanitize?: (text: string) => string;
    searchWords: string[];
    textToHighlight: string;
  }

  export interface Chunk {
    start: number;
    end: number;
  }
  export interface HighlighterProps {
    /** The class name to be applied to an active match. Use along with activeIndex */
    activeClassName?: string;
    /** Specify the match index that should be actively highlighted. Use along with activeClassName */
    activeIndex?: number;
    /** The inline style to be applied to an active match. Use along with activeIndex */
    activeStyle?: React.CSSProperties;
    /** Escape characters in searchWords which are meaningful in regular expressions */
    autoEscape?: boolean;
    /** CSS class name applied to the outer/wrapper <span> */
    className?: string;
    /** Search should be case sensitive; defaults to false */
    caseSensitive?: boolean;
    /**
     * Use a custom function to search for matching chunks. This makes it possible to use arbitrary logic
     * when looking for matches. See the default findChunks function in highlight-words-core for signature.
     * Have a look at the custom findChunks example on how to use it.
     */
    findChunks?: (options: FindChunks) => Chunk[];
    /** CSS class name applied to highlighted text */
    highlightClassName?: string;
    /** Inline styles applied to highlighted text */
    highlightStyle?: React.CSSProperties;
    /**
     * Type of tag to wrap around highlighted matches; defaults to mark but can also be a React element
     * (class or functional)
     */
    highlightTag?: string | React.ComponentType<unknown>;
    /**
     * Process each search word and text to highlight before comparing (eg remove accents); signature
     * (text: string): string
     */
    sanitize?: (text: string) => string;
    /** Array of search words. The search terms are treated as RegExps unless autoEscape is set. */
    searchWords: string[];
    /** Text to highlight matches in */
    textToHighlight: string;
    /** CSS class name applied to unhighlighted text */
    unhighlightClassName?: string;
    /** Inline styles applied to unhighlighted text */
    unhighlightStyle?: React.CSSProperties;
    /** Allows to pass through any parameter to wrapped component */
    [index: string]: unknown;
  }
  // eslint-disable-next-line react/prefer-stateless-function

  const Highlighter: React.FC<HighlighterProps<HTMLSpanElement | HTMLImageElement>> = ({
    /* eslint-disable @typescript-eslint/no-unused-vars */
    cdnSuffix = 'svg',
    cdnUrl = 'https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.4.3/flags/4x3/',
    svg = false,
    style = {},
    countryCode,
  }) => React.ReactNode; // return should be HTMLImageElement or HTMLSpanElement but close enough for me.
  export default Highlighter;
}
