export const validateFileImage = (file: File) => {
  const validTypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'image/x-icon'];
  if (validTypes.indexOf(file.type) === -1) {
    return false;
  }
  return true;
};

export const validateFileAudio = (file: File) => {
  const validTypes = ['audio/mpeg', 'audio/wav', 'audio/ogg'];
  if (validTypes.indexOf(file.type) === -1) {
    return false;
  }
  return true;
};

export const validateFileVideo = (file: File) => {
  const validTypes = ['video/mp4'];
  if (validTypes.indexOf(file.type) === -1) {
    return false;
  }
  return true;
};

export const isExcelFile = (file_name: string) => {
  if (
    ['lsm', 'xls', 'ltx', 'xlt', 'lsx', 'lsb', 'csv'].indexOf(
      file_name.substring(file_name.length - 3, file_name.length),
    ) !== -1
  ) {
    return true;
  }
  return false;
};

export const isWordFile = (file_name: string) => {
  if (
    ['doc', 'ocm', 'ocx', 'txt', 'xml', 'dotx'].indexOf(file_name.substring(file_name.length - 3, file_name.length)) !==
    -1
  ) {
    return true;
  }
  return false;
};

export const isZipFile = (file_name: string) => {
  if (['zip', 'rar'].indexOf(file_name.substring(file_name.length - 3, file_name.length)) !== -1) {
    return true;
  }
  return false;
};

export const isPdfFile = (file_name: string) => {
  if (file_name.substring(file_name.length - 3, file_name.length) === 'pdf') {
    return true;
  }
  return false;
};

export const isPowerPointFile = (file_name: string) => {
  if (
    ['ptx', 'ptm', 'ptm', 'pdf', 'otx', 'otm', 'psx'].indexOf(
      file_name.substring(file_name.length - 3, file_name.length),
    ) !== -1
  ) {
    return true;
  }
  return false;
};

export const getType = (text: string) => {
  const fourDigit = text.substring(text.length - 4, text.length);
  if (fourDigit[0] !== '.') {
    return fourDigit;
  }

  return fourDigit.substring(fourDigit.length - 3, fourDigit.length);
};
