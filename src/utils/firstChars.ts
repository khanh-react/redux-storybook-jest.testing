export default function firstChars(str: string) {
  // if (!str) {
  //   return 'https://via.placeholder.com/150/858585/FFFFFF/?text=IB';
  // }

  const regex = /(?!\bm[rs]\.?\b|\bmiss\b|\bmrs\b)((?<=\s)|(?<=^))(\b[a-z])/gi;
  const matches = str.match(regex);
  let initials = '';
  if (matches) {
    initials = [...matches][0].toUpperCase();
    if (matches.length > 1) {
      initials += matches[matches.length - 1].substring(0, 1).toUpperCase();
    }
  }
  return `https://via.placeholder.com/150/858585/FFFFFF/?text=${initials}`;
}
