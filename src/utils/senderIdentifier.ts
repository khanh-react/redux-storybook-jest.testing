import { getTime } from 'date-fns';
import { uuid } from 'uuidv4';

export const identifierCode = (userId: number) => {
  return `${userId}-${getTime(new Date())}-${uuid()}`;
};
