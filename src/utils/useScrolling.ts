import { RefObject, useEffect, useState } from 'react';

export const useScrolling = (ref: RefObject<HTMLElement>): boolean => {
  const [scrolling, setScrolling] = useState<boolean>(false);
  useEffect(() => {
    if (ref.current) {
      let scrollingTimeout: ReturnType<typeof setTimeout>;

      const handleScrollEnd = () => {
        setScrolling(false);
      };

      const handleScroll = () => {
        setScrolling(true);
        clearTimeout(scrollingTimeout);
        scrollingTimeout = setTimeout(() => handleScrollEnd(), 3000);
      };

      ref.current.addEventListener('scroll', handleScroll, false);
      return () => {
        if (ref.current) {
          // eslint-disable-next-line react-hooks/exhaustive-deps
          ref.current.removeEventListener('scroll', handleScroll, false);
        }
      };
    }
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    return () => {};
  }, [ref]);

  return scrolling;
};
