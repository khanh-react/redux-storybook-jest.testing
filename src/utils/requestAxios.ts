import axios from 'axios';
import { readRecord } from './localStorageService';

const BaseAPI = process.env.REACT_APP_API_URL;
const service = axios.create({
  baseURL: BaseAPI,
  timeout: 30000,
});

service.interceptors.request.use(
  (config) => {
    const token = readRecord('accessToken');
    const subdomainId = readRecord('subdomainId');
    if (typeof token !== 'undefined') {
      // eslint-disable-next-line no-param-reassign
      config.headers.Authorization = `Bearer ${token}`;
      // eslint-disable-next-line no-param-reassign
      config.headers['x-imbee-subdomain-id'] = subdomainId;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

service.interceptors.response.use(
  (response) => response.data,
  (error) => {
    return Promise.reject(error);
  },
);

export const sendFilesAxios = (
  payload: {
    chatId: string;
    document: File;
    identifier: string;
    fileCaption: string;
    type: string;
    quoted_message_id?: string;
  },
  onProgress: (loaded: number) => void,
) => {
  const formData = new FormData();
  formData.append('file', payload.document);
  formData.append('data[chat_id]', payload.chatId);
  formData.append('data[sender_identifier]', payload.identifier);
  formData.append('data[from_type]', 'Customer Service');
  formData.append('data[message_type]', payload.type);
  formData.append('data[file_caption]', payload.fileCaption);
  payload.quoted_message_id && formData.append('data[quoted_message_id]', payload.quoted_message_id);
  return service({
    url: '/messages',
    method: 'POST',
    data: formData,
    onUploadProgress(progressEvent) {
      const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
      onProgress(percentCompleted);
    },
  });
};
