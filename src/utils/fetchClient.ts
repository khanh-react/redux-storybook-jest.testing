/* eslint-disable no-console */
import fetch from 'node-fetch';

import { history } from './history';
import { readRecord } from './localStorageService';

const authHeaders = () => {
  const token = readRecord('accessToken');
  const id = readRecord('subdomainId');
  if (token && id) {
    return { 'x-imbee-subdomain-id': id, Authorization: `Bearer ${token}` };
  }
  return null;
};
// const baseURL = 'http://localhost';
const BASE = process.env.REACT_APP_API_URL;
// ? process.env.REACT_APP_API_URL : baseURL;

function formatUrl(path: string, query: string, filters?: string) {
  return `${BASE}${path}${query}${filters}`;
}
const snakeCase = (str: string) => {
  return str
    .replace(/\W+/g, ' ')
    .split(/ |\B(?=[A-Z])/)
    .map((word) => word.toLowerCase())
    .join('_');
};

export async function createRequest<RequestBody>(
  path: string,
  config?: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    query?: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    filters?: any;
    method?: 'POST' | 'GET' | 'DELETE' | 'PUT';
    headers?: RequestInit['headers'];
    data?: RequestBody;
    type?: 'form' | 'json';
  },
) {
  let resStatus = 0;
  const query: string =
    (config?.query &&
      `?${Object.keys(config.query)
        .map((key) => `${snakeCase(key)}=${encodeURIComponent(config.query?.[key] ?? '')}`)
        .join('&')}`) ||
    '';
  const filters: string =
    (config?.filters &&
      `&${Object.keys(config.filters)
        .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(config.filters?.[key] ?? '')}`)
        .join('&')}`) ||
    '';
  const response = await fetch(formatUrl(path, query, filters), {
    method: config?.method ?? 'GET',
    headers: {
      ...(config?.type === 'form' ? null : { 'Content-Type': 'application/json' }),
      ...authHeaders(),
      ...config?.headers,
    } as { [key: string]: string },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    body: config?.type === 'form' ? (config.data as any) : config?.data && JSON.stringify(config?.data),
  });
  resStatus = response.status;
  switch (resStatus) {
    case 201:
      console.log('success');
      break;
    case 401:
      history.push('/login');
      break;
    case 500:
      console.log('server error, try again');
      break;
    default:
      console.log('unhandled');
      break;
  }
  return response.json();
}

export async function createDownloadRequest<RequestBody>(
  path: string,
  config?: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    query?: any;
    method?: 'POST' | 'GET' | 'DELETE' | 'PUT';
    headers?: RequestInit['headers'];
    data?: RequestBody;
  },
) {
  const query: string =
    (config?.query &&
      `?${Object.keys(config.query)
        .map((key) => `${snakeCase(key)}=${encodeURIComponent(config.query?.[key] ?? '')}`)
        .join('&')}`) ||
    '';

  const response = await fetch(formatUrl(path, query), {
    method: config?.method ?? 'GET',
    headers: {
      ...authHeaders(),
    } as { [key: string]: string },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  });

  if (response.status === 401) history.push('/login');

  return response.blob();
}
