import { makeStyles, Typography } from '@material-ui/core';
import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import {
  createFaq,
  fetchInternalMessageAct,
  getCateFaq,
  getFaq,
  getStarredMessagesAct,
  renewPubsub,
  searchAcross,
  searchMessage,
  selectChat,
  SelectRoomSearchMessage,
  updateCaseLeftNav,
  updateCaseRightNav,
  updateChatsQuery,
  searchAcrossSuccess,
} from '../../redux/action/chat';
import { listMessage, MessageType } from '../../redux/services/chat';
import AddIndividual from '../../components/chat/Container/AddIndividual';
import AddQuickReply from '../../components/chat/AddQuickReply';
import Carousel from '../../components/chat/PreviewImage/Carousel';
import { ChatList } from '../../components/chat/Container/ChatList';
import { ChatRoom } from '../../components/chat/Container/ChatRoom';
import ChatRoomFilters from '../../components/chat/Container/ChatRoomFilters';
import CreateGroup from '../../components/chat/Container/CreateGroup';
import FetchQuickReplies from '../../components/chat/FetchQuickReplies';
import MainNav from '../../components/chat/LeftNav/MainNav';
import RightNavChat from '../../components/chat/Container/RightNavChat';
import RightSection from '../../components/chat/Container/RightSection';
import RsAllFiles from '../../components/chat/Container/RsAllFiles';
import RsGroupChat from '../../components/chat/Container/RsGroupChat';
import RsSearchMessage from '../../components/chat/Container/RsSearchMessage';
import SearchBar from '../../components/chat/FilterChat/SearchBar';
import { ChatSearchResult } from '../../components/chat/SearchInRoom/SearchMessage';
import StarredMessageNav from '../../components/chat/Starred/StarredMessageNav';
import TeamChat, { MessageProps } from '../../components/chat/Container/TeamChat';
import { Flex } from '../../components/common/Flex';
import { AppState } from '../../redux/store';
import { readRecord } from '../../utils/localStorageService';
import firstChars from '../../utils/firstChars';
import LoadingMessage from '../../components/common/LoadingMessage';

const useStyles = makeStyles((theme) => ({
  navBar: {
    color: '#98999F',
    '&:hover': {
      borderBottom: '2px solid #FFDB36',
      color: '#FFDB36',
    },
  },
  cursorPointer: {
    cursor: 'pointer',
  },
  titleStyle: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 15,
    lineHeight: '19px',
    letterSpacing: 0.09375,
    textTransform: 'uppercase',
    color: '#BBBBC5',
    margin: '21px 17px 10px',
  },
}));

export const HoverSearch = styled.div`
  cursor: pointer;
  transition: 0.3s ease-out;
  &:hover {
    background: rgba(255, 224, 99, 0.15);
  }
`;

export function Chat() {
  const classes = useStyles();
  const allRooms = useSelector((s: AppState) => (s.chat.chats === undefined ? undefined : s.chat.chats));
  const allChatId = useSelector((s: AppState) => s.chat.chatList);
  const query = useSelector((s: AppState) => s.chat.query);
  const caseLeftNav = useSelector((s: AppState) => s.chat.caseLeftNav);
  const caseRightNav = useSelector((s: AppState) => s.chat.caseRightNav);
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const staredMessages = useSelector((state: AppState) => state.chat.starredMessages);
  const [caseFilter, setCaseFilter] = React.useState('all');
  const [listUnread, setListUnread] = React.useState<string[]>([]);
  const [listUnassign, setListUnassign] = React.useState<string[]>([]);
  const [listMe, setListMe] = React.useState<string[]>([]);
  const [flagSearch, setFlagSearch] = React.useState(false);
  const [textSearch, setTextSearch] = React.useState('');
  const dataSearch = useSelector((s: AppState) => s.chat.searchAcrossResult);
  const messageInternal = useSelector((s: AppState) => s.chat.messagesInternal);
  const currentRomInterNal = useSelector((s: AppState) => s.chat.currentInternalChat);
  const userId = useSelector((s: AppState) => s.auth.userId);
  const subDomainId = useSelector((s: AppState) => (s.auth.subdomainId === undefined ? undefined : s.auth.subdomainId));
  const [flagCarousel, setFlagCarousel] = React.useState(false);
  const [urlCarousel, setUrlCarousel] = React.useState('');
  const currentChatType = useSelector(
    (s: AppState) => currentChatId && s.chat.chats[currentChatId] && s.chat.chats[currentChatId].type,
  );
  const allFaq = useSelector((s: AppState) => s.chat.faq);
  const allCateFaq = useSelector((s: AppState) => s.chat.cateFaq);

  const [flagAddQuickReply, setFlagAddQuickReply] = React.useState(false);

  const currentContactId = useSelector(
    (s: AppState) => currentChatId && s.chat.chats[currentChatId] && s.chat.chats[currentChatId].contact?.id,
  );

  const LoadingSearchAcross = useSelector((s: AppState) => s.chat.flagSearchAcross);

  const dispatch = useDispatch();
  useEffect(() => {
    if (!allFaq) {
      dispatch(getCateFaq());
      dispatch(getFaq({}));
    }
  }, [dispatch, allFaq]);
  const dropdownCateFaq = useMemo(() => {
    return (allCateFaq ?? []).map((item) => {
      return { name: item.name, id: item.id };
    });
  }, [allCateFaq]);

  const categories = useMemo(() => {
    return (allFaq ?? []).map((item) => {
      return {
        id: item.id,
        shortcut: item.shortcut,
        question: item.question,
        answer: item.answer,
        faqCategory: item.faqCategory.id,
        ordering: item.ordering,
      };
    });
  }, [allFaq]);

  const onSubmitCreateQuickReply = async (shortcut: string, category: string, message: string) => {
    const reg = new RegExp(
      '^(?=.{1,32}$)(?![0-9$&+,:;=?@#|<>.^*()%!-])[a-zA-Z0-9$&+,:;=?@#|<>.^*()%!-]+(?<![$&+,:;=?@#|<>.^*()%!-])$',
    );
    if (reg.test(shortcut) && message !== '') {
      dispatch(createFaq(shortcut, category, message));
      setFlagAddQuickReply(false);
    }
  };

  const zoomOutImage = (url: string) => {
    setFlagCarousel(true);
    setUrlCarousel(url);
  };

  useEffect(() => {
    if (!query) {
      dispatch(updateChatsQuery({ order_by: 'timestamp' }));
    }
  }, [dispatch, query]);

  useEffect(() => {
    dispatch(renewPubsub());
  }, [dispatch]);

  useEffect(() => {
    if (!staredMessages) {
      dispatch(getStarredMessagesAct());
    }
  }, [dispatch, staredMessages]);

  const handleSetCase = (value: string) => {
    setCaseFilter(value);
  };
  useEffect(() => {
    if (allRooms) {
      const keys = Object.keys(allRooms);
      const countUnread: string[] = [];
      const countAssign: string[] = [];
      const countMe: string[] = [];

      keys.forEach((key) => {
        if (allRooms[key].read === 0) {
          countUnread.push(allRooms[key].id);
        }
        if (allRooms[key].assignedUser) {
          countAssign.push(allRooms[key].id);
        }
        if (allRooms[key].assignedUser?.id === userId) {
          countMe.push(allRooms[key].id);
        }
      });

      setListUnread(countUnread);
      setListUnassign(countAssign);
      setListMe(countMe);
    }
  }, [allRooms, userId]);
  useEffect(() => {
    const saveNav = readRecord('caseLeftNav');
    if (saveNav) {
      dispatch(updateCaseLeftNav(saveNav));
    }
  }, [dispatch]);

  const onSelectSearchRoom = (id: string) => {
    dispatch(selectChat(id));
  };

  const onSelectSearchMessage = async (room: string, id: string, time: number) => {
    const boxMessage = document.querySelector('#current-chat');

    try {
      const jsonRes = await listMessage({
        chat_id: `${room}`,
        limit: 15,
        up_to_id: id,
        up_to_timestamp: time,
      });
      dispatch(SelectRoomSearchMessage(`${room}`));
      if (jsonRes.data.data.length > 12) {
        dispatch(searchMessage(jsonRes, `${room}`));
        if (boxMessage) {
          const allInnerMess = Array.from(
            document.getElementsByClassName('inner-message') as HTMLCollectionOf<HTMLElement>,
          );
          if (allInnerMess) {
            allInnerMess.forEach((e) => {
              e.style.border = 'none';
            });
          }
          setTimeout(() => {
            const element = document.getElementById(`${id}`);
            if (element) {
              const innermess = Array.from(
                element.getElementsByClassName('inner-message') as HTMLCollectionOf<HTMLElement>,
              );
              if (innermess) {
                innermess.forEach((e) => {
                  e.style.border = '1px solid gray';
                });
              }
            }
            boxMessage.scrollTop = 0;
          }, 100);
        }
      } else {
        const jsonResFull = await listMessage({
          chat_id: `${room}`,
          limit: 12,
        });
        dispatch(searchMessage(jsonResFull, `${room}`));
        if (boxMessage) {
          boxMessage.scrollTop = 5;
          const allInnerMess = Array.from(
            document.getElementsByClassName('inner-message') as HTMLCollectionOf<HTMLElement>,
          );
          if (allInnerMess) {
            allInnerMess.forEach((e) => {
              e.style.border = 'none';
            });
          }
          setTimeout(() => {
            const element = document.getElementById(`${id}`);
            if (element) {
              element.scrollIntoView({ block: 'center', inline: 'center' });
              const innermess = Array.from(
                element.getElementsByClassName('inner-message') as HTMLCollectionOf<HTMLElement>,
              );
              if (innermess) {
                innermess.forEach((e) => {
                  e.style.border = '1px solid gray';
                });
              }
            }
          }, 100);
        }
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  useEffect(() => {
    currentChatType && currentChatType === 'Group' && dispatch(updateCaseRightNav('rs-group'));
    currentChatType && currentChatType === 'Individual' && dispatch(updateCaseRightNav('user'));
  }, [currentChatType, dispatch, currentChatId]);

  const handleClickRightNav = (index: string) => {
    switch (index) {
      case 'user':
        if (caseRightNav === 'user' || caseRightNav === 'rs-group') {
          dispatch(updateCaseRightNav(''));
        } else if (currentChatType === 'Individual') {
          dispatch(updateCaseRightNav(index));
        } else {
          dispatch(updateCaseRightNav('rs-group'));
        }
        break;
      case 'team-chat':
        if (index !== caseRightNav) {
          if (currentRomInterNal !== 0) {
            dispatch(fetchInternalMessageAct());
            dispatch(updateCaseRightNav(index));
          }
        } else {
          dispatch(updateCaseRightNav(''));
        }

        break;
      case 'calendar':
        if (index !== caseRightNav) {
          dispatch(updateCaseRightNav(index));
        } else {
          dispatch(updateCaseRightNav(''));
        }

        break;
      case 'message':
        if (index !== caseRightNav) {
          dispatch(updateCaseRightNav(index));
        } else {
          dispatch(updateCaseRightNav(''));
        }
        break;
      case 'note':
        if (index !== caseRightNav) {
          dispatch(updateCaseRightNav(index));
        } else {
          dispatch(updateCaseRightNav(''));
        }
        break;
      default:
        break;
    }
  };

  const handleChangeValueSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTextSearch(e.target.value);
    if (e.target.value !== '') {
      dispatch(searchAcross(e.target.value));
    } else {
      dispatch(searchAcrossSuccess({ messages: [], chats: [] }));
    }
  };
  useEffect(() => {
    if (textSearch) {
      setFlagSearch(true);
    } else {
      setFlagSearch(false);
    }
  }, [textSearch]);
  const messageInternalMemo = useMemo(
    () =>
      messageInternal &&
      (messageInternal ?? []).map(
        (mess): MessageProps => ({
          type: mess.message_type,
          outgoing: userId === mess.senderUser.id,
          text: mess.text,
          thumbnailUrl:
            mess.message_type !== MessageType.Text
              ? `${process.env.REACT_APP_API_URL}/internal-chats/media/${mess.id}?access-token=${readRecord(
                  'accessToken',
                )}&subdomain-id=${subDomainId}`
              : undefined,
          createdAt: new Date(`${mess.create_date} +0000`),
          sender: mess.senderUser.name,
          avatar: firstChars(mess.senderUser.name),
          zoomOutImage,
          file_name: mess.file_name,
          id: mess.id,
          senderId: mess.senderUser.id,
        }),
      ),
    [messageInternal, userId, subDomainId],
  );

  return (
    <Flex bgcolor="white" width="100%" height="calc(100vh - 64px)">
      {flagCarousel && <Carousel images={urlCarousel} onClose={() => setFlagCarousel(false)} />}
      <Flex flexDirection="column" width={348} height="100%">
        {caseLeftNav === 'new-group' && <CreateGroup />}
        {caseLeftNav === 'new-individual' && <AddIndividual />}

        {(caseLeftNav === 'message' || caseLeftNav === undefined) && (
          <>
            <MainNav
              caseFilter={caseFilter}
              setCaseFilter={handleSetCase}
              listUnread={listUnread.length}
              listUnassign={listUnassign.length}
              closedSearch={() => setFlagSearch(false)}
            />
            <SearchBar handleText={handleChangeValueSearch} textSearch={textSearch} />
            <Flex
              flex={1}
              width="100%"
              height="calc(100% - 120px)"
              style={{ borderRight: '1px solid rgb(224 224 231)' }}
              flexDirection="column"
              overflow="auto"
              position="relative"
            >
              {LoadingSearchAcross && <LoadingMessage top={30} />}
              {!flagSearch ? (
                <ChatList
                  height="100%"
                  chatIds={
                    // eslint-disable-next-line no-nested-ternary
                    caseFilter === 'all'
                      ? allChatId ?? []
                      : // eslint-disable-next-line no-nested-ternary
                      caseFilter === 'news'
                      ? listUnread
                      : caseFilter === 'unassigned'
                      ? listUnassign
                      : listMe
                  }
                />
              ) : (
                <>
                  {dataSearch?.messages.length !== 0 && <Typography className={classes.titleStyle}>MESSAGE</Typography>}

                  {(dataSearch?.messages ?? []).map((item) => (
                    <HoverSearch
                      onClick={() => onSelectSearchMessage(item.chat.id, item.id, item.timestamp)}
                      key={item.id}
                    >
                      <ChatSearchResult
                        key={item.id}
                        id={item.id}
                        name={item.contact?.name ?? item.user.name}
                        // description={item.contact?.name ?? ''}
                        lastMessage={item.text ?? item.file_name}
                        highlightText={textSearch}
                        updatedAt={new Date(item.timestamp)}
                      />
                    </HoverSearch>
                  ))}

                  {dataSearch?.messages.length !== 0 && <Typography className={classes.titleStyle}>CHAT</Typography>}

                  {(dataSearch?.chats ?? []).map((item) => (
                    <HoverSearch onClick={() => onSelectSearchRoom(item.id)} key={item.id}>
                      <ChatSearchResult
                        key={item.id}
                        id={item.id}
                        name={item.name}
                        // description={item.contact?.name ?? ''}
                        lastMessage={item.lastMessage?.text ?? item.lastMessage?.file_name ?? ''}
                        highlightText={textSearch}
                        updatedAt={new Date(item.timestamp)}
                      />
                    </HoverSearch>
                  ))}
                </>
              )}
            </Flex>
          </>
        )}
        {caseLeftNav === 'starred' && <StarredMessageNav />}
      </Flex>

      {currentChatId && (
        <Flex flex={1}>
          <ChatRoom />
        </Flex>
      )}
      {currentChatId && currentContactId && caseRightNav === 'user' && <RightSection contactId={currentContactId} />}
      {caseRightNav === 'rs-all-file' && <RsAllFiles />}
      {caseRightNav === 'rs-search-message' && <RsSearchMessage onSelectSearchMessage={onSelectSearchMessage} />}
      {caseRightNav === 'chat-room-filter' && <ChatRoomFilters />}
      {caseRightNav === 'team-chat' && <TeamChat messages={messageInternalMemo ?? []} />}
      {caseRightNav === 'calendar' && (
        <FetchQuickReplies
          dropdown={dropdownCateFaq}
          categories={categories}
          handleClickAdd={() => setFlagAddQuickReply(true)}
        />
      )}
      {flagAddQuickReply && (
        <AddQuickReply
          categories={dropdownCateFaq}
          handleClosed={() => setFlagAddQuickReply(false)}
          onSubmitCreate={onSubmitCreateQuickReply}
        />
      )}
      {currentChatType === 'Group' && caseRightNav === 'rs-group' && <RsGroupChat />}
      {currentChatId && <RightNavChat active={caseRightNav || ''} handleClick={handleClickRightNav} />}
    </Flex>
  );
}
