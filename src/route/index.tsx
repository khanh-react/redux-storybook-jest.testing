/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/no-array-index-key */
import React, { Fragment, lazy, Suspense } from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';

import ContactDetail from '../components/contacts/containers/ContactDetail';
import Contacts from '../components/contacts/containers/Contacts';
import GuestGuard from '../components/guard/GuestGuard';
import Loader from '../components/loading';
import QuickReplySettings from '../components/settings/QuickReplySettings';
import CMSLayout from '../layout/CMSLayout';
import DefaultLayout from '../layout/DefaultLayout';
import { AppState } from '../redux/store';
import { Chat } from './chat';

/* Master routes config */
/* This will contain the login, register and first level definition only */
/* Other routes should define and group in another file */

type RouteConfig = {
  exact?: boolean;
  path: string;
  guard?: React.ElementType;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  layout?: (props: { children: React.ReactNode }) => JSX.Element;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  component: ((props: any) => JSX.Element) | React.LazyExoticComponent<(props: any) => JSX.Element>;
  routes?: RouteConfig[];
};

const routesConfig: RouteConfig[] = [
  {
    exact: true,
    path: '/',
    component: () => <Redirect to="/login" />,
  },
  {
    exact: true,
    path: '/login',
    layout: DefaultLayout,
    // guard: GuestGuard,
    component: lazy(() => import('../views/auth/LoginView')),
  },
  {
    exact: true,
    path: '/cms',
    layout: CMSLayout,
    guard: GuestGuard,
    component: () => <div>Content loading...</div>,
  },
  {
    exact: true,
    path: '/cms/contacts',
    layout: CMSLayout,
    component: Contacts,
    guard: GuestGuard,
  },
  {
    exact: true,
    path: '/cms/chat',
    layout: CMSLayout,
    component: Chat,
    guard: GuestGuard,
  },
  {
    exact: true,
    path: '/cms/setting/quick-reply',
    layout: CMSLayout,
    component: QuickReplySettings,
    guard: GuestGuard,
  },
  {
    exact: true,
    path: '/cms/contacts/contact-detail/:id',
    layout: CMSLayout,
    component: ContactDetail,
    guard: GuestGuard,
  },
  {
    path: '*',
    layout: DefaultLayout,
    component: () => <>404 Not found</>,
  },
];

const RenderRoutes = (routes: RouteConfig[]) => {
  const statusLoading = useSelector((s: AppState) => s.util.loading);

  return routes ? (
    <Suspense fallback={<div>Loading...</div>}>
      <Loader loadingStatus={statusLoading} />
      <Switch>
        {routes.map((route, i) => {
          const Guard = route.guard || Fragment;
          const Layout = route.layout || Fragment;
          const Component = route.component;

          return (
            <Route
              key={i}
              path={route.path}
              exact={route.exact}
              render={(props) => (
                <Guard>
                  <Layout>{route.routes ? RenderRoutes(route.routes) : <Component {...props} />}</Layout>
                </Guard>
              )}
            />
          );
        })}
      </Switch>
    </Suspense>
  ) : null;
};

const Routes = () => RenderRoutes(routesConfig);

export default Routes;
