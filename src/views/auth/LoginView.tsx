import { Box, Container } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import jwtDecode, { JwtPayload } from 'jwt-decode';
import { Link as RouterLink, Redirect } from 'react-router-dom';
import styled from 'styled-components';
import { readRecord } from '../../utils/localStorageService';
import { loginSuccess } from '../../redux/action/login';
import Logo from '../../components/common/Logo';
import PageWrapper from '../../components/common/PageWrapper';
import LoginCard from '../../components/login/LoginCard';
import { setLoading } from '../../redux/action/util';

const StyledContainer = styled(Container)`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const StyledLogoLink = styled(RouterLink)`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const LoginView = () => {
  const dispatch = useDispatch();
  const [isAuthenticated, setIsAuthenticated] = React.useState(false);
  // const history = useHistory();

  // const handleSubmitSuccess = () => {
  //   history.push('/cms');
  // };
  useEffect(() => {
    dispatch(setLoading(false));
  }, [dispatch]);

  useEffect(() => {
    const token = readRecord('accessToken');
    const subdomainId = readRecord('subdomainId');
    if (token && subdomainId) {
      const tokenExpiration = jwtDecode<JwtPayload>(token).exp;
      const dateNow = new Date();
      if (tokenExpiration && tokenExpiration < dateNow.getTime() / 1000) {
        setIsAuthenticated(false);
      } else {
        setIsAuthenticated(true);
        dispatch(loginSuccess(token, parseInt(subdomainId, 10)));
      }
    } else {
      setIsAuthenticated(false);
    }
  }, [isAuthenticated, dispatch]);

  if (isAuthenticated) {
    return <Redirect to="/cms" />;
  }
  return (
    <PageWrapper title="Login">
      <StyledContainer>
        <Box my={8} display="flex" alignItems="center">
          <StyledLogoLink to="/">
            <Logo />
          </StyledLogoLink>
        </Box>
        <LoginCard />
      </StyledContainer>
    </PageWrapper>
  );
};

export default LoginView;
