import { createMount } from '@material-ui/core/test-utils';
import initStoryshots, { Stories2SnapsConverter } from '@storybook/addon-storyshots';
import toJson from 'enzyme-to-json';
import { act } from 'react-dom/test-utils';

const DATE_TO_USE = new Date('2020-09-24T15:58:14.734Z');
const originalDate = Date;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
global.Date = jest.fn(() => DATE_TO_USE) as any;
global.Date.UTC = originalDate.UTC;
global.Date.parse = originalDate.parse;
global.Date.now = originalDate.now;

initStoryshots({
  asyncJest: true, // this is the option that activates the async behaviour
  test: ({
    story,
    context,
    done, // --> callback passed to test method when asyncJest option is true
  }) => {
    const converter = new Stories2SnapsConverter();
    const snapshotFilename = converter.getSnapshotFileName(context);
    const storyElement = story.render();
    const mount = createMount();

    // mount the story
    const tree = mount(storyElement);

    act(() => {
      const waitTime = 1;
      setTimeout(() => {
        if (snapshotFilename) {
          // expect(toJson(tree)).toMatchSpecificSnapshot(snapshotFilename);
          expect(toJson(tree)).toBeTruthy();
        }

        done();
      }, waitTime);
    });
  },
});
