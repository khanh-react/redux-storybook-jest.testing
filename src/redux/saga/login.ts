import delay from '@redux-saga/delay-p';
import { call, put, takeEvery, throttle } from 'redux-saga/effects';

import { storeToLocalStorage } from '../../utils/localStorageService';
import { Unpacked } from '../../utils/types';
import { renewPubsub } from '../action/chat';
import {
  loginFailure,
  loginSuccess,
  LoginSuccessAction,
  setSubdomainChannel,
  setSubdomainChannelSuccess,
  setSubdomainConfig,
  setSubdomainConfigSuccess,
  setSubdomainSetting,
  setSubdomainSettingSuccess,
  setSubdomainUser,
  setSubdomainUserSuccess,
} from '../action/login';
import {
  getSubdomainChannel,
  getSubdomainConfig,
  getSubdomainSetting,
  getSubdomainUser,
  login,
} from '../services/login';
import {
  FETCH_SUB_DOMAIN_CHANNEL,
  FETCH_SUB_DOMAIN_CONFIG,
  FETCH_SUB_DOMAIN_SETTING,
  FETCH_SUB_DOMAIN_USER,
  LOGIN_SUCCESS,
  LoginRequestAction,
  SetSubdomainChannel,
  SetSubdomainConfig,
  SetSubdomainSetting,
  SetSubdomainUser,
} from '../type/login/types';

function updateToken(res: { token: string; subdomain: { id: number; name: string }[] }) {
  storeToLocalStorage('accessToken', res.token);
  storeToLocalStorage('subdomainId', `${res.subdomain[0].id}`);
}

export function* loginFlow(action: LoginRequestAction) {
  try {
    const { payload } = action;
    const response: Unpacked<ReturnType<typeof login>> = yield call(login, payload.email, payload.password);
    yield put(loginSuccess(response.data.token, response.data.subdomain[0].id));
    yield call(updateToken, response.data);
    yield put(renewPubsub());
  } catch (error) {
    yield put(loginFailure(error.message));
  }
}

export function* fetchSubdomainConfig(action: SetSubdomainConfig) {
  const domain = action.payload;
  const configSubdomain = yield call(getSubdomainConfig, domain);
  yield put(setSubdomainConfigSuccess(configSubdomain.data.data));
}
export function* fetchSubdomainSetting(action: SetSubdomainSetting) {
  const domain = action.payload;
  const configSubdomain = yield call(getSubdomainSetting, domain);
  yield put(setSubdomainSettingSuccess(configSubdomain.data));
}

export function* fetchSubdomainChannel(action: SetSubdomainChannel) {
  const domain = action.payload;
  const configSubdomain = yield call(getSubdomainChannel, domain);
  yield put(setSubdomainChannelSuccess(configSubdomain.data.data));
}

export function* fetchSubdomainUser(action: SetSubdomainUser) {
  const domain = action.payload;
  const configSubdomain = yield call(getSubdomainUser, domain);
  yield put(setSubdomainUserSuccess(configSubdomain.data.data));
}

export function* fetchAllSubdomain(action: LoginSuccessAction) {
  yield delay(500);
  yield put(setSubdomainConfig(action.payload.subdomainId));
  yield put(setSubdomainSetting(action.payload.subdomainId));
  yield put(setSubdomainChannel(action.payload.subdomainId));
  yield put(setSubdomainUser(action.payload.subdomainId));
}

function* authWatcher() {
  yield throttle(1000, 'LOGIN_REQUEST', loginFlow);
  yield takeEvery(FETCH_SUB_DOMAIN_CONFIG, fetchSubdomainConfig);
  yield takeEvery(FETCH_SUB_DOMAIN_SETTING, fetchSubdomainSetting);
  yield takeEvery(FETCH_SUB_DOMAIN_CHANNEL, fetchSubdomainChannel);
  yield takeEvery(FETCH_SUB_DOMAIN_USER, fetchSubdomainUser);
  yield takeEvery(LOGIN_SUCCESS, fetchAllSubdomain);
}

export default authWatcher;
