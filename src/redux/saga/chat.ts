import delay from '@redux-saga/delay-p';
import { Client, IMessage, StompSubscription } from '@stomp/stompjs';
import { get } from 'lodash';
import { omit } from 'ramda';
import { eventChannel } from 'redux-saga';
import { call, debounce, fork, put, select, take, takeEvery, throttle } from 'redux-saga/effects';

import { deleteRecord, readRecord } from '../../utils/localStorageService';
import { sendFilesAxios } from '../../utils/requestAxios';
import { identifierCode } from '../../utils/senderIdentifier';
import { Unpacked } from '../../utils/types';
import {
  ADD_PARTICIPANTS,
  ADD_PARTICIPANTS_FAILED,
  ADD_PARTICIPANTS_SUCCESS,
  AddParticipantsAction,
  ASSIGN_TAG,
  ASSIGN_USER,
  ASSIGN_USER_FAILED,
  ASSIGN_USER_SUCCESS,
  AssignTagTicked,
  assignTagTicketSuccess,
  AssignUserAction,
  chatFetchFailed,
  chatFetchSuccess,
  chatUpdated,
  CLEAR_CHAT,
  ClearChatAction,
  clearChatSuccess,
  clearMessageInternalAct,
  CLOSE_CHAT_TICKET,
  CLOSE_CHAT_TICKET_FAILED,
  CLOSE_CHAT_TICKET_SUCCESS,
  CloseChatTicketAction,
  CREATE_CATE_FAQ,
  CREATE_FAQ,
  CREATE_NEW_CHAT,
  CREATE_TAG,
  CreateCateFaq,
  createCateFaqSuccess,
  CreateFaq,
  createFaqSuccess,
  CreateNewChatAction,
  CreateTag,
  createTagSuccess,
  DELETE_CATE_FAQ,
  DELETE_CHAT,
  DELETE_FAQ,
  DELETE_TAG,
  DeleteCateFaq,
  deleteCateFaqSuccess,
  DeleteChatAction,
  DeleteFaq,
  deleteFaqSuccess,
  deleteMessageErrorAct,
  deleteMessageInternalAct,
  deleteRoomSuccess,
  DeleteTag,
  deleteTagSuccess,
  EXPORT_PARTICIPANTS,
  EXPORT_PARTICIPANTS_FAILED,
  EXPORT_PARTICIPANTS_SUCCESS,
  ExportParticipants,
  FETCH_INTERNAL_MESSAGE,
  fetchMessageInternalSuccessAct,
  GET_CATE_FAQ,
  GET_FAQ,
  GET_STARRED_MESSAGE,
  GET_TAG,
  GetCateFaq,
  getCateFaqSuccess,
  GetFaq,
  getFaqSuccess,
  getStarredMessagesAct,
  getStarredMessagesSuccess,
  getTagSuccess,
  LOAD_MORE,
  LOAD_MORE_ROOM_SCROLL,
  LOAD_MORE_TEAM_CHAT,
  loadingMessageInternalAct,
  loadingSendFileInternalAct,
  LoadMore,
  LoadMoreRoomScrollTeamChat,
  MARK_READ_CHAT,
  MARK_UN_READ_CHAT,
  MarkReadChat,
  markReadChatSuccess,
  messagesFetchSuccess,
  messageUnread,
  messageUpdated,
  newMessage,
  OPEN_CHAT_TICKET,
  OPEN_CHAT_TICKET_FAILED,
  OPEN_CHAT_TICKET_SUCCESS,
  OpenChatTicket,
  progressUpload,
  pushNewRoomCreate,
  putNewMessageInternalAct,
  REFRESH_PARTICIPANTS,
  REFRESH_PARTICIPANTS_FAILED,
  REFRESH_PARTICIPANTS_SUCCESS,
  REMOVE_PARTICIPANTS,
  REMOVE_PARTICIPANTS_FAILED,
  REMOVE_PARTICIPANTS_SUCCESS,
  RemoveParticipantsAction,
  RESEND_MESSAGE,
  RESEND_MESSAGE_DISCONNECT,
  ResendMessage,
  ResendMessageDisconnect,
  resendMessageSuccessAct,
  ResetMessageAction,
  SEARCH_ACROSS,
  SEARCH_MESSAGE_IN_ROOM,
  SearchAcross,
  searchAcrossSuccess,
  SearchMessInRoom,
  searchMessInRoomSuccess,
  SELECT_CHAT,
  selectChat,
  SelectChatAction,
  selectInternalRoomAct,
  SEND_DOCUMENT_TEMP_MESSAGE,
  SEND_MESSAGE,
  SEND_TEXT_INTERNAl_CHAT,
  SendDocumentTempMessage,
  sendDocumentTempMessageSuccess,
  SendMessageAction,
  SendTextInternalChat,
  sendTextTempMessage,
  setFlagLoadMore,
  setTimeLoadingFile,
  UNASSIGN_USER,
  UNASSIGN_USER_FAILED,
  UNASSIGN_USER_SUCCESS,
  UnAssignUserAction,
  UPDATE_CATE_FAQ,
  UPDATE_CHATS_QUERY,
  UPDATE_FAQ,
  UPDATE_STARRED_MESS,
  UPDATE_TAG,
  updateCaseLeftNav,
  updateCaseRightNav,
  UpdateCateFaq,
  updateCateFaqSuccess,
  updateChatInput,
  UpdateChatsQueryAction,
  UpdateFaq,
  updateFaqSuccess,
  UpdateStarredMess,
  UpdateTag,
  updateTagSuccess,
  UPLOAD_INTERNAL_FILE,
  UploadFileInternal,
  flagSearchAcross,
  flagSearchInRoom,
  CHANGE_NAME_CHAT_ROOM,
  ChangeNameChatRoom,
} from '../action/chat';
import { setLoading } from '../action/util';
import { ChatState } from '../reducer/chat';
import {
  addMembersGCApi,
  assignChat,
  assignUserApi,
  clearChat,
  closeTicketApi,
  createCateFaq,
  createChat,
  createdTag,
  createFaq,
  createTicket,
  deleteCateFaq,
  deleteChat,
  deleteFaq,
  deleteTag,
  exportMembersGCApi,
  getAllFaq,
  getAllTagList,
  getCateFaq,
  getInternalChatId,
  getMessageInternal,
  getStarredMessages,
  listChats,
  listMessage,
  markReadChat,
  markUnReadChat,
  Message,
  MessageType,
  refreshMembersGCApi,
  removeMembersGCApi,
  resendMessage,
  searchAcross,
  searchInRoom,
  sendText,
  sendTextInternal,
  starredMessage,
  unAssignUserApi,
  updateCateFaq,
  updateFaq,
  updateTag,
  uploadFileInternal,
  updateChat,
} from '../services/chat';
import { renewPubsub } from '../services/pubsub';
import { AppState } from '../store';
import { LOGIN_SUCCESS } from '../type/login/types';

export function scrollToLastMessage(smooth = true, delayTime = 100, div: string) {
  setTimeout(() => {
    const currentChat = document.querySelector(`#${div}`);
    if (currentChat && currentChat.lastElementChild) {
      currentChat.lastElementChild.scrollIntoView(smooth ? { behavior: 'smooth' } : undefined);
    }
  }, delayTime);
}

function* fetchChats(action: UpdateChatsQueryAction) {
  yield put(setLoading(true));
  try {
    const query = yield select((state: AppState) => state.chat.query);
    const jsonRes: Unpacked<ReturnType<typeof listChats>> = yield call(listChats, {
      limit: 12,
      ...query,
      ...action.payload,
    });
    yield put(chatFetchSuccess(jsonRes));
    const currentChatId = yield select((state: AppState) => state.chat.currentChatId);
    const lastIdRoomChat = readRecord('lastIdRoomChat');
    const checkHasLastRoom = jsonRes.data.data.every((room) => room.id !== lastIdRoomChat);
    if (!currentChatId && !checkHasLastRoom) {
      if (lastIdRoomChat) {
        yield put(selectChat(lastIdRoomChat));
      }
    }
    yield put(setLoading(false));
  } catch (e) {
    yield put(chatFetchFailed());
    yield put(setLoading(false));
  }
}

function* fetchCurrentChatMessages(action: SelectChatAction) {
  yield put(searchMessInRoomSuccess([]));
  const divScroll = document.getElementById('current-chat') as HTMLDivElement;
  if (divScroll) {
    divScroll.style.opacity = '0%';
  }

  yield put(ResetMessageAction());

  try {
    const currentChatId = yield select((state: AppState) => state.chat.currentChatId);
    const messages: Message[] = yield select(
      (state: AppState) => currentChatId && state.chat.messages?.[currentChatId],
    );

    if (currentChatId !== action.payload || !messages?.length) {
      const jsonRes: Unpacked<ReturnType<typeof listMessage>> = yield call(listMessage, {
        chat_id: action.payload,
        limit: 20,
      });
      yield put(messagesFetchSuccess(jsonRes, action.payload));
      setTimeout(() => {
        if (divScroll) {
          divScroll.style.opacity = '100%';
        }
      }, 100);
    }
    yield call(scrollToLastMessage, false, 0, 'current-chat');
    const fetchInternalRoom = yield call(getInternalChatId, action.payload);
    if (fetchInternalRoom.code !== -1) {
      yield put(selectInternalRoomAct(fetchInternalRoom.data.data.id));
    } else {
      yield put(selectInternalRoomAct(0));
    }
  } catch (e) {
    // yield put(chatFetchFailed());
  }
}

function* sendMessages(action: SendMessageAction) {
  try {
    const currentChatId = yield select((state: AppState) => state.chat.currentChatId);
    const replyMessage = yield select((state: AppState) => state.chat.onReplyMessage?.message_id) ?? 0;
    yield put(ResetMessageAction());
    switch (action.message.type) {
      case MessageType.Text:
        {
          const text = yield select((state: AppState) => state.chat.inputs?.[currentChatId]);
          if (text !== '') {
            yield put(updateChatInput(currentChatId, ''));
            const userId = yield select((state: AppState) => state.auth.userId);
            const name = yield select((state: AppState) => state.subdomain.subdomainConfig?.name);
            const pass = identifierCode(userId);
            yield put(sendTextTempMessage(currentChatId, name, text, pass, replyMessage));
            yield call(scrollToLastMessage, true, 100, 'current-chat');
            yield call(sendText, currentChatId, text, pass, replyMessage);
          }
        }
        break;

      default:
    }
  } catch (e) {
    // yield put(chatFetchFailed());
  }
}

async function createStompClient() {
  try {
    const data = await renewPubsub();
    const { webConnectionString, username, password, channels } = data.data.pubsub;
    const client = new Client({
      brokerURL: webConnectionString,
      connectHeaders: {
        login: username,
        passcode: password,
      },
      // debug(str) {
      //   console.log(str);
      // },
      reconnectDelay: 3000,
      heartbeatIncoming: 4000,
      heartbeatOutgoing: 4000,
    });
    return { client, channels };
  } catch (error) {
    throw new Error(error);
  }
}

function createEventChannel(client: Client, channels: string[]) {
  return eventChannel((emitter) => {
    const onMessage = (m: IMessage) => {
      // console.log(m);
      try {
        emitter({ type: 'CHAT_EVENT', payload: JSON.parse(m.body) });
      } catch (e) {
        // console.error(e);
      }
    };
    let subscriptions: StompSubscription[];
    // eslint-disable-next-line no-param-reassign
    client.onConnect = () => {
      emitter({ type: 'SOCKET', message: 'connected' });
      subscriptions = channels.map((c) => client.subscribe(`/topic/${c}`, onMessage));
    };

    const unsubscribe = () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };

    client.activate();
    return unsubscribe;
  });
}

// eslint-disable-next-line
let stompChannel: any = null;

function* subscribeChatEvents() {
  yield delay(1000);
  while (true) {
    try {
      // An error from socketChannel will cause the saga jump to the catch block
      const action = yield take(stompChannel);
      if (action.type === 'CHAT_EVENT')
        switch (action.payload.type) {
          case 'create': {
            if (action.payload.namespace === 'internal-chat-message') {
              const internalId = yield select((state: AppState) => state.chat.currentInternalChat);

              // eslint-disable-next-line eqeqeq
              if (action.payload.message.teamInternalChat.id == internalId) {
                yield put(putNewMessageInternalAct(action.payload.message));
                yield call(scrollToLastMessage, true, 200, 'team-chat');
              }
            } else {
              yield put(
                chatUpdated({ ...action.payload.message.chat, lastMessage: omit(['chat'], action.payload.message) }),
              );
              const chatId = action.payload.message.chat.id;
              const messages: ChatState['messages'] = yield select((state: AppState) => state.chat.messages);
              if (!messages?.[action.payload.message.id]) yield put(newMessage(action.payload.message));
              const currentChatId = yield select((state: AppState) => state.chat.currentChatId);
              if (currentChatId === chatId) {
                yield call(scrollToLastMessage, true, 100, 'current-chat');
              }
            }

            break;
          }
          case 'update': {
            yield put(messageUpdated(action.payload.message));
            break;
          }
          case 'unread': {
            yield put(messageUnread(action.payload.chat.id, action.payload.chat.read, action.payload.unread_count));
            break;
          }
          case 'revoke': {
            if (action.payload.namespace === 'internal-chat-message') {
              yield put(deleteMessageInternalAct(action.payload.message.id));
            }
            break;
          }
          case 'clear': {
            if (action.payload.namespace === 'internal-chat') {
              yield put(clearMessageInternalAct(action.payload.teamInternalChat.id));
            }
            break;
          }
          default:
        }
    } catch (err) {
      // console.error('socket error:', err);
      // socketChannel is still open in catch block
      // if we want end the socketChannel, we need close it explicitly
      // socketChannel.close()
    }
  }
}
function* refreshAndRegister() {
  while (true) {
    const data = yield call(createStompClient);
    const { client, channels } = data;
    stompChannel = yield call(createEventChannel, client, channels);

    yield call(subscribeChatEvents);
    yield delay(1200000);
  }
}
function* createNewChat(action: CreateNewChatAction) {
  const data = action.newChat;
  const listRoom = yield select((state: AppState) => state.chat.chatList);
  try {
    yield put(setLoading(true));
    const result = yield call(createChat, data);
    if (result.data) {
      if (listRoom.every((item: string) => item !== result.data.chat.id)) {
        yield put(pushNewRoomCreate(result.data.chat));
      }
      yield put(selectChat(result.data.chat.id));
      yield put(updateCaseLeftNav('message'));
      if (action.newChat.type === 'Individual') {
        yield put(updateCaseRightNav('user'));
      } else {
        yield put(updateCaseRightNav('rs-group'));
      }
    }
    yield put(setLoading(false));
  } catch (error) {
    // console.log(error)
    yield put(setLoading(false));
  }
}

function* deleteChatFunction(action: DeleteChatAction) {
  const currentId = action.chat_id ? action.chat_id : yield select((state: AppState) => state.chat.currentChatId);
  try {
    const result = yield call(deleteChat, currentId);
    if (result.data) {
      yield put(deleteRoomSuccess(currentId));
      yield put(selectChat(''));
      deleteRecord('lastIdRoomChat');
    }
  } catch (error) {
    throw new Error(error);
  }
}

function* clearChatFunction(action: ClearChatAction) {
  const currentId = action.chat_id ? action.chat_id : yield select((state: AppState) => state.chat.currentChatId);
  try {
    yield put(setLoading(true));
    const result = yield call(clearChat, currentId);
    if (result.data) {
      yield put(clearChatSuccess(currentId));
      yield put(setLoading(false));
    }
  } catch (error) {
    yield put(setLoading(false));
    throw new Error(error);
  }
}

function* getStaredMessagesFunction() {
  try {
    const result = yield call(getStarredMessages);
    if (result.data) {
      yield put(getStarredMessagesSuccess(result.data.data));
    }
  } catch (error) {
    // console.log(error)
  }
}

function* markReadChatFunction(action: MarkReadChat) {
  const chatId = action.chat_id;
  const currentRoom = yield select((state: AppState) => state.chat.chats);
  if (get(currentRoom, `[${chatId}].read`, 0) !== 0) {
    return;
  }
  try {
    const result = yield call(markReadChat, chatId);
    if (result.data.result === 'Success') {
      yield put(markReadChatSuccess(chatId));
    }
  } catch (error) {
    // console.log(error)
  }
}

function* markUnReadChatFunction() {
  const chatId = yield select((state: AppState) => state.chat.currentChatId);
  const currentRoom = yield select((state: AppState) => state.chat.chats);
  if (get(currentRoom, `[${chatId}].read`, 0) === 0) {
    return;
  }
  try {
    yield call(markUnReadChat, chatId);
  } catch (error) {
    // console.log(error)
  }
}

function* assignTagFunction(action: AssignTagTicked) {
  try {
    const data = yield call(
      assignChat,
      action.payload.chatId,
      action.payload.listTags.map((item) => item.id),
    );
    if (data.code === 0) {
      yield put(assignTagTicketSuccess(action.payload.chatId, action.payload.listTags));
    }
    yield put(setLoading(false));
  } catch (error) {
    yield put(setLoading(false));
    throw new Error(error);
  }
}

function* openChatTicketFunction(action: OpenChatTicket) {
  try {
    const response = yield call(createTicket, action.chatId);
    yield put({ type: OPEN_CHAT_TICKET_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: OPEN_CHAT_TICKET_FAILED, payload: error.message });
  }
}

// eslint-disable-next-line
let emit: any;
const chan = eventChannel((emitter) => {
  emit = emitter;
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  return () => {};
});

function* sendDocumentTempFunction(action: SendDocumentTempMessage) {
  const userId = yield select((state: AppState) => state.auth.userId);
  const replyMessage = yield select((state: AppState) => state.chat.onReplyMessage?.message_id) ?? 0;
  const pass = identifierCode(userId);
  yield put(
    sendDocumentTempMessageSuccess(
      action.payload.currentId,
      action.payload.file.name,
      action.payload.name,
      pass,
      action.payload.type,
      action.payload.file.size,
      action.payload.file,
      replyMessage,
    ),
  );
  yield call(scrollToLastMessage, true, 100, 'current-chat');
  const sizeFile = action.payload.file.size;
  const time = 50 + Math.round((sizeFile / 500000) * 70);
  yield put(setTimeLoadingFile(pass, time));

  try {
    sendFilesAxios(
      {
        chatId: action.payload.currentId,
        document: action.payload.file,
        identifier: pass,
        fileCaption: action.payload.caption,
        type: action.payload.type,
        quoted_message_id: replyMessage !== 0 ? replyMessage : null,
      },
      (progressEvent: number) => {
        emit({ pass, progressEvent });
      },
    );
  } catch (error) {
    throw new Error(error);
  }
}

function* watchOnProgress() {
  while (true) {
    const data = yield take(chan);
    yield put(progressUpload(data.pass, data.progressEvent));
  }
}

function* loadMoreMessageFunction(action: LoadMore) {
  const currentRoom = yield select((state: AppState) => state.chat.currentChatId);
  const ListMessageId = yield select((state: AppState) => state.chat.messageList);
  const ListMessage = yield select((state: AppState) => state.chat.messages);
  const firstMessageId = ListMessageId?.[currentRoom][0];
  const boxMessage = document.querySelector('#current-chat');
  if (yield select((state: AppState) => state.chat.statusLoadMessage?.[currentRoom])) {
    return;
  }
  yield put(setFlagLoadMore(currentRoom, true));
  try {
    const jsonRes: Unpacked<ReturnType<typeof listMessage>> = yield call(listMessage, {
      chat_id: currentRoom,
      limit: 20,
      before_id: firstMessageId,
      before_timestamp: ListMessage?.[firstMessageId].timestamp,
    });
    if (jsonRes.data.data.length !== 0) {
      yield put(messagesFetchSuccess(jsonRes, currentRoom));
      if (boxMessage) {
        boxMessage.scrollTop = boxMessage.scrollHeight - action.position;
      }
    }
    yield put(setFlagLoadMore(currentRoom, false));
  } catch (error) {
    yield put(setFlagLoadMore(currentRoom, false));
    throw new Error(error);
  }
}

function* loadMoreMessageTeamChatFunction(action: LoadMoreRoomScrollTeamChat) {
  yield put(loadingMessageInternalAct(true));
  const messageFirst = yield select((state: AppState) => state.chat.messagesInternal?.[0]);
  const boxMessage = document.querySelector('#team-chat');
  try {
    const result = yield call(getMessageInternal, messageFirst.teamInternalChat.id, {
      limit: 10,
      before_id: messageFirst.id,
      before_at: messageFirst.create_date,
    });
    if (result.code !== -1 && result.data.data.length > 0) {
      yield put(fetchMessageInternalSuccessAct(result.data.data, true));
      setTimeout(() => {
        if (boxMessage) {
          boxMessage.scrollTop = boxMessage.scrollHeight - action.position;
        }
      }, 100);
    }
    yield put(loadingMessageInternalAct(false));
  } catch (error) {
    yield put(loadingMessageInternalAct(false));
  }
}

function* assignUserFunction(action: AssignUserAction) {
  try {
    const response = yield call(assignUserApi, action.payload.userId, action.payload.chatTicketId);
    if (response.code === 0) {
      yield put({ type: ASSIGN_USER_SUCCESS, payload: response.data });
    }
  } catch (error) {
    yield put({ type: ASSIGN_USER_FAILED, payload: error.message });
  }
}
function* unAssignUserFunction(action: UnAssignUserAction) {
  try {
    const response = yield call(unAssignUserApi, action.payload.chatTicketId);
    if (response.code === 0) {
      yield put({ type: UNASSIGN_USER_SUCCESS, payload: response.data });
    }
  } catch (error) {
    yield put({ type: UNASSIGN_USER_FAILED, payload: error.message });
  }
}

function* resendMessageFunction(action: ResendMessage) {
  try {
    yield call(resendMessage, action.id);
    yield put(resendMessageSuccessAct(action.id));
  } catch (error) {
    throw new Error(error);
  }
}

function* fetchInternalMessageFunction() {
  const currentInternalChat = yield select((state: AppState) => state.chat.currentInternalChat);
  try {
    yield put(loadingMessageInternalAct(true));
    const result = yield call(getMessageInternal, currentInternalChat, { limit: 12 });
    if (result.code !== -1) {
      yield put(fetchMessageInternalSuccessAct(result.data.data));
    }
    yield call(scrollToLastMessage, false, 0, 'team-chat');
    yield put(loadingMessageInternalAct(false));
  } catch (error) {
    yield put(loadingMessageInternalAct(false));
    throw new Error(error);
  }
}
function* resendMessageDisconnectFunction(action: ResendMessageDisconnect) {
  const messages = yield select((state: AppState) => state.chat.messages);
  const mess = messages?.[action.id];
  const userId = yield select((state: AppState) => state.auth.userId);
  const pass = identifierCode(userId);
  yield put(deleteMessageErrorAct(action.id, mess.chat.id));
  if (mess.message_type === MessageType.Text) {
    try {
      yield call(sendText, mess.chat.id, mess.text, mess.id, mess.quotedMessage.id);
    } catch (error) {
      throw new Error(error);
    }
  } else {
    try {
      sendFilesAxios(
        {
          chatId: mess.chat.id,
          document: mess.file,
          identifier: mess.id,
          fileCaption: mess.text,
          type: mess.message_type,
        },
        (progressEvent: number) => {
          emit({ pass, progressEvent });
        },
      );
    } catch (error) {
      throw new Error(error);
    }
  }
}

function* closeChatTicketFunction(action: CloseChatTicketAction) {
  try {
    yield put(setLoading(true));
    const response = yield call(closeTicketApi, action.payload.chatTicketId);
    if (response.code === 0) {
      yield put(setLoading(false));
      yield put({ type: CLOSE_CHAT_TICKET_SUCCESS, payload: response.data });
    } else {
      yield put(setLoading(false));
    }
  } catch (error) {
    yield put({ type: CLOSE_CHAT_TICKET_FAILED, payload: error.message });
  }
}

function* exportParticipantsFunction(action: ExportParticipants) {
  try {
    const blob = yield call(exportMembersGCApi, Number(action.chatId));
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'participants.xlsx';
    a.click();
    yield put({ type: EXPORT_PARTICIPANTS_SUCCESS, payload: url });
  } catch (error) {
    yield put({ type: EXPORT_PARTICIPANTS_FAILED, payload: error.message });
  }
}
function* refreshParticipantsFunction(action: ExportParticipants) {
  try {
    const response = yield call(refreshMembersGCApi, Number(action.chatId));
    yield put({ type: REFRESH_PARTICIPANTS_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: REFRESH_PARTICIPANTS_FAILED, payload: error.message });
  }
}
function* addParticipantsFunction(action: AddParticipantsAction) {
  try {
    const response = yield call(addMembersGCApi, action.payload.chatId, action.payload.phone_list);

    yield put({ type: ADD_PARTICIPANTS_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: ADD_PARTICIPANTS_FAILED, payload: error.message });
  }
}
function* removeParticipantsFunction(action: RemoveParticipantsAction) {
  try {
    const response = yield call(removeMembersGCApi, action.payload.chatId, action.payload.phone_list);

    yield put({ type: REMOVE_PARTICIPANTS_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: REMOVE_PARTICIPANTS_FAILED, payload: error.message });
  }
}

function* loadMoreRoomScrollFunction() {
  const list = yield select((state: AppState) => state.chat.chatList);
  const offset_id = list[list.length - 1];
  const listDetailRoom = yield select((state: AppState) => state.chat.chats);
  const offset_timestamp = listDetailRoom?.[offset_id].timestamp;
  try {
    const result = yield call(listChats, { order_by: 'timestamp', offset_timestamp, offset_id, limit: 5 });
    if (result.code !== -1 && result.data.data.length > 0) {
      yield put(chatFetchSuccess(result, true));
    }
  } catch (error) {
    throw new Error(error);
  }
}

function* getCateFaqFunction(action: GetCateFaq) {
  try {
    const response = yield call(getCateFaq, action.filter);
    yield put(getCateFaqSuccess(response.data.data));
  } catch (error) {
    throw new Error(error);
  }
}

function* getFaqFunction(action: GetFaq) {
  try {
    const response = yield call(getAllFaq, action.filterValue);
    yield put(getFaqSuccess(response.data.data));
  } catch (error) {
    throw new Error(error);
  }
}

function* getTagFunction() {
  try {
    const response = yield call(getAllTagList);
    if (response.code !== -1) {
      yield put(getTagSuccess(response.data.tag));
    }
  } catch (error) {
    throw new Error(error);
  }
}

function* createFaqFunction(action: CreateFaq) {
  try {
    const response = yield call(createFaq, action.payload.shortcut, action.payload.category, action.payload.message);
    if (response.code !== -1) {
      yield put(createFaqSuccess(response.data.data));
    }
  } catch (error) {
    throw new Error(error);
  }
}

function* createTagFunction(action: CreateTag) {
  try {
    const response = yield call(createdTag, action.name);
    if (response.code !== -1) {
      yield put(createTagSuccess(response.data.tag));
    }
  } catch (error) {
    throw new Error(error);
  }
}

function* deleteFaqFunction(action: DeleteFaq) {
  try {
    yield call(deleteFaq, action.id);
    yield put(deleteFaqSuccess(action.id));
  } catch (error) {
    throw new Error(error);
  }
}

function* deleteTagFunction(action: DeleteTag) {
  try {
    yield call(deleteTag, action.id);
    yield put(deleteTagSuccess(action.id));
  } catch (error) {
    throw new Error(error);
  }
}

function* createCateFaqFunction(action: CreateCateFaq) {
  try {
    const response = yield call(createCateFaq, action.name);
    yield put(createCateFaqSuccess(response.data.data));
  } catch (error) {
    throw new Error(error);
  }
}

function* deleteCateFaqFunction(action: DeleteCateFaq) {
  try {
    yield call(deleteCateFaq, action.id);
    yield put(deleteCateFaqSuccess(action.id));
  } catch (error) {
    throw new Error(error);
  }
}

function* updateFaqFunction(action: UpdateFaq) {
  try {
    const response = yield call(updateFaq, action.payload);
    if (response.code !== -1) {
      yield put(updateFaqSuccess(response.data.data));
    }
  } catch (error) {
    throw new Error(error);
  }
}

function* updateTagFunction(action: UpdateTag) {
  try {
    const response = yield call(updateTag, action.name, action.id);
    if (response.code !== -1) {
      yield put(updateTagSuccess(response.data.tag));
    }
  } catch (error) {
    throw new Error(error);
  }
}

function* updateCateFaqFunction(action: UpdateCateFaq) {
  try {
    const response = yield call(updateCateFaq, action.payload.name, action.payload.id);
    if (response.code !== -1) {
      yield put(updateCateFaqSuccess(response.data.data));
    }
  } catch (error) {
    throw new Error(error);
  }
}

function* updateStarredMessFunction(action: UpdateStarredMess) {
  try {
    const response = yield call(starredMessage, action.name, action.id);
    if (response.code !== -1) {
      yield put(getStarredMessagesAct());
    }
  } catch (error) {
    throw new Error(error);
  }
}

function* searchMessInRoomFunction(action: SearchMessInRoom) {
  yield put(flagSearchInRoom(true));
  try {
    const result = yield call(searchInRoom, action.id, action.text);
    if (result !== -1) {
      yield put(searchMessInRoomSuccess(result.data.data));
    } else {
      yield put(searchMessInRoomSuccess([]));
    }
    yield put(flagSearchInRoom(false));
  } catch (error) {
    yield put(flagSearchInRoom(false));
    throw new Error(error);
  }
}

function* searchAcrossFunction(action: SearchAcross) {
  yield put(flagSearchAcross(true));
  try {
    const result = yield call(searchAcross, action.text);
    if (result !== -1) {
      yield put(searchAcrossSuccess(result.data));
    } else {
      yield put(searchAcrossSuccess({ messages: [], chats: [] }));
    }
    yield put(flagSearchAcross(false));
  } catch (error) {
    yield put(flagSearchAcross(false));
    throw new Error(error);
  }
}

function* uploadFileInternalFunction(action: UploadFileInternal) {
  yield put(loadingSendFileInternalAct(true));
  try {
    yield call(
      uploadFileInternal,
      action.data.chatId,
      action.data.document,
      action.data.type,
      action.data.fileCaption,
      action.data.replyId ?? undefined,
    );
    yield put(loadingSendFileInternalAct(false));
  } catch (error) {
    yield put(loadingSendFileInternalAct(false));
    throw new Error(error);
  }
}

function* sendTextInternalChatFunction(action: SendTextInternalChat) {
  if (action.text === '') {
    return;
  }
  try {
    yield call(sendTextInternal, action.room, action.text);
  } catch (error) {
    throw new Error(error);
  }
}

function* changeNameChatRoomFunction(action: ChangeNameChatRoom) {
  try {
    yield call(updateChat, action.payload.roomId, action.payload.name);
  } catch (error) {
    throw new Error(error);
  }
}

export function* chatSaga() {
  yield debounce(500, UPDATE_CHATS_QUERY, fetchChats);
  yield throttle(1000, SELECT_CHAT, fetchCurrentChatMessages);
  yield takeEvery(SEND_MESSAGE, sendMessages);
  yield takeEvery(LOGIN_SUCCESS, refreshAndRegister);
  // yield fork(subscribeChatEvents);

  // yield [takeEvery(RENEW_PUBSUB, refreshAndRegister), takeEvery(RENEW_PUBSUB, subscribeChatEvents)];
  yield takeEvery(CREATE_NEW_CHAT, createNewChat);
  yield takeEvery(DELETE_CHAT, deleteChatFunction);
  yield takeEvery(GET_STARRED_MESSAGE, getStaredMessagesFunction);
  yield takeEvery(MARK_READ_CHAT, markReadChatFunction);
  yield takeEvery(MARK_UN_READ_CHAT, markUnReadChatFunction);
  yield takeEvery(CLEAR_CHAT, clearChatFunction);
  yield takeEvery(ASSIGN_TAG, assignTagFunction);
  yield takeEvery(OPEN_CHAT_TICKET, openChatTicketFunction);
  yield takeEvery(SEND_DOCUMENT_TEMP_MESSAGE, sendDocumentTempFunction);
  yield takeEvery(ASSIGN_USER, assignUserFunction);
  yield takeEvery(UNASSIGN_USER, unAssignUserFunction);
  yield takeEvery(CLOSE_CHAT_TICKET, closeChatTicketFunction);
  yield fork(watchOnProgress);
  yield takeEvery(LOAD_MORE, loadMoreMessageFunction);
  yield takeEvery(RESEND_MESSAGE, resendMessageFunction);
  yield takeEvery(FETCH_INTERNAL_MESSAGE, fetchInternalMessageFunction);
  yield takeEvery(EXPORT_PARTICIPANTS, exportParticipantsFunction);
  yield takeEvery(REFRESH_PARTICIPANTS, refreshParticipantsFunction);
  yield takeEvery(ADD_PARTICIPANTS, addParticipantsFunction);
  yield takeEvery(REMOVE_PARTICIPANTS, removeParticipantsFunction);
  yield takeEvery(RESEND_MESSAGE_DISCONNECT, resendMessageDisconnectFunction);
  yield takeEvery(LOAD_MORE_ROOM_SCROLL, loadMoreRoomScrollFunction);
  yield takeEvery(GET_CATE_FAQ, getCateFaqFunction);
  yield takeEvery(GET_FAQ, getFaqFunction);
  yield takeEvery(CREATE_FAQ, createFaqFunction);
  yield takeEvery(DELETE_FAQ, deleteFaqFunction);
  yield takeEvery(CREATE_CATE_FAQ, createCateFaqFunction);
  yield takeEvery(DELETE_CATE_FAQ, deleteCateFaqFunction);
  yield takeEvery(UPDATE_FAQ, updateFaqFunction);
  yield debounce(500, UPDATE_CATE_FAQ, updateCateFaqFunction);
  yield takeEvery(LOAD_MORE_TEAM_CHAT, loadMoreMessageTeamChatFunction);
  yield takeEvery(GET_TAG, getTagFunction);
  yield takeEvery(DELETE_TAG, deleteTagFunction);
  yield takeEvery(CREATE_TAG, createTagFunction);
  yield takeEvery(UPDATE_TAG, updateTagFunction);
  yield takeEvery(UPDATE_STARRED_MESS, updateStarredMessFunction);
  yield debounce(300, SEARCH_MESSAGE_IN_ROOM, searchMessInRoomFunction);
  yield debounce(300, SEARCH_ACROSS, searchAcrossFunction);
  yield takeEvery(UPLOAD_INTERNAL_FILE, uploadFileInternalFunction);
  yield takeEvery(SEND_TEXT_INTERNAl_CHAT, sendTextInternalChatFunction);
  yield takeEvery(CHANGE_NAME_CHAT_ROOM, changeNameChatRoomFunction);
}
