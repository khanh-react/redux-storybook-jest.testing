import omit from 'lodash/omit';
import { call, put, select, throttle } from 'redux-saga/effects';

import { AppState } from '../store';
import {
  addContactApi,
  bulkDeleteContactsApi,
  deleteSingleContactApi,
  getContactDetailApi,
  listContacts,
  updateContactDetailApi,
} from '../services/contact';
import {
  ADD_CONTACT,
  ADD_CONTACT_FAILED,
  ADD_CONTACT_SUCCESS,
  AddContactAction,
  BULK_DELETE_REQUEST,
  BULK_DELETE_REQUEST_FAILED,
  BULK_DELETE_REQUEST_SUCCESS,
  BulkDeleteRequestAction,
  CONTACTS_FETCH_FAILED,
  CONTACTS_FETCH_SUCCESS,
  DELETE_SINGLE_CONTACT_REQUEST,
  DELETE_SINGLE_CONTACT_REQUEST_FAILED,
  DELETE_SINGLE_CONTACT_REQUEST_SUCCESS,
  DeleteSingleContactAction,
  GET_CONTACT_DETAIL,
  GET_CONTACT_DETAIL_FAILED,
  GET_CONTACT_DETAIL_SUCCESS,
  GetContactDetailAction,
  UPDATE_CONTACT_DETAIL,
  UPDATE_CONTACT_DETAIL_FAILED,
  UPDATE_CONTACT_DETAIL_SUCCESS,
  UPDATE_CONTACT_QUERY,
} from '../type/contact/actions';
import { ContactQuery } from '../type/contact/state';

function* fetchContacts() {
  try {
    const query: ContactQuery = yield select((state: AppState) => state.contacts.query);
    if (query) {
      const jsonRes = yield call(listContacts, omit(query, 'filters'), query.filters);
      yield put({ type: CONTACTS_FETCH_SUCCESS, payload: jsonRes.data });
    }
  } catch (e) {
    yield put({ type: CONTACTS_FETCH_FAILED, message: e.message });
  }
}
function* deleteContacts(delList: BulkDeleteRequestAction) {
  const { payload } = delList;
  try {
    if (delList) {
      const response = yield call(bulkDeleteContactsApi, payload);
      yield put({ type: BULK_DELETE_REQUEST_SUCCESS, payload: response.data.result });
    }
  } catch (e) {
    yield put({ type: BULK_DELETE_REQUEST_FAILED, message: e.message });
  }
}

function* addContact(data: AddContactAction) {
  const { payload } = data;
  try {
    const response = yield call(addContactApi, payload);
    yield put({ type: ADD_CONTACT_SUCCESS, payload: response.data.result });
  } catch (e) {
    yield put({ type: ADD_CONTACT_FAILED, message: e.message });
  }
}

function* delSingleContact(id: DeleteSingleContactAction) {
  const { payload } = id;
  try {
    const response = yield call(deleteSingleContactApi, payload);
    yield put({ type: DELETE_SINGLE_CONTACT_REQUEST_SUCCESS, payload: response.data });
  } catch (e) {
    yield put({ type: DELETE_SINGLE_CONTACT_REQUEST_FAILED, message: e.message });
  }
}
function* getContactDetail(id: GetContactDetailAction) {
  const { payload } = id;
  try {
    const response = yield call(getContactDetailApi, payload);
    yield put({ type: GET_CONTACT_DETAIL_SUCCESS, payload: response.data });
  } catch (e) {
    yield put({ type: GET_CONTACT_DETAIL_FAILED, message: e.message });
  }
}

function* updateContactDetail() {
  try {
    const data = yield select((state: AppState) => state.contacts.contactData);
    const response = yield call(updateContactDetailApi, data);
    yield put({ type: UPDATE_CONTACT_DETAIL_SUCCESS, payload: response.data });
  } catch (e) {
    yield put({ type: UPDATE_CONTACT_DETAIL_FAILED, message: e.message });
  }
}

export function* contactSaga() {
  yield throttle(1000, UPDATE_CONTACT_QUERY, fetchContacts);
  yield throttle(1000, BULK_DELETE_REQUEST, deleteContacts);
  yield throttle(1000, ADD_CONTACT, addContact);
  yield throttle(1000, DELETE_SINGLE_CONTACT_REQUEST, delSingleContact);
  yield throttle(1000, GET_CONTACT_DETAIL, getContactDetail);
  yield throttle(1000, UPDATE_CONTACT_DETAIL, updateContactDetail);
}
