import { AnyAction } from 'redux';

type InitialState = {
  test: string;
};

const initialState: InitialState = {
  test: 'dummy',
};

const dummyReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case 'HELLO':
      return {
        ...state,
        test: 'hello',
      };
    default:
      return state;
  }
};

export default dummyReducer;
