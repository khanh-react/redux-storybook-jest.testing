/* eslint-disable @typescript-eslint/no-extra-non-null-assertion */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { getTime } from 'date-fns';
import { findIndex, get, set, sortBy, uniq } from 'lodash';
import { fromPairs } from 'ramda';
import { scrollToLastMessage } from '../saga/chat';
import { storeToLocalStorage } from '../../utils/localStorageService';
import {
  ASSIGN_TAG_SUCCESS,
  ASSIGN_USER_SUCCESS,
  CHAT_UPDATED,
  ChatActionTypes,
  CHATS_FETCH_SUCCESS,
  CLEAR_ALL_MESSAGE_INTERNAL,
  CLEAR_CHAT_SUCCESS,
  CLOSE_CHAT_TICKET_SUCCESS,
  DELETE_INTERNAL_MESSAGE,
  DELETE_MESSAGE,
  DELETE_MESSAGE_ERROR,
  EDIT_TAG_SUCCESS,
  FETCH_MESSAGE_INTERNAL_SUCCESS,
  GET_STARRED_MESSAGES_SUCCESS,
  LOADING_MESSAGE_INTERNAL,
  LOADING_SEND_FILE_INTERNAL,
  MARK_READ_CHAT_SUCCESS,
  MARK_UNREAD,
  MESSAGE_SEARCH,
  MESSAGE_UPDATED,
  MESSAGES_FETCH_SUCCESS,
  NEW_MESSAGE,
  NEW_MESSAGE_INTERNAL,
  ON_REPLY_MESSAGE,
  OPEN_CHAT_TICKET_SUCCESS,
  PROGRESS_UPLOAD,
  RESEND_MESSAGE_SUCCESS,
  RESET_REPLY_MESSAGE,
  SELECT_CHAT,
  SELECT_INTERNAL_ROOM,
  SELECT_ROOM_MESSAGE_SEARCH,
  SEND_DOCUMENT_TEMP_MESSAGE_SUCCESS,
  SEND_MESSAGE_SUCCESS,
  SET_FLAG_LOAD_MESSAGE,
  SET_TIME_LOADING_FILE,
  UNASSIGN_USER_SUCCESS,
  UPDATE_CASE_LEFT_NAV,
  UPDATE_CASE_RIGHT_NAV,
  UPDATE_CHAT_INPUT,
  UPDATE_CHATS_QUERY,
  UPDATE_EMOJI_INPUT,
  UPDATE_STARRED_MESSAGE,
  CHATS_FILTER_SUCCESS,
  SEND_TEXT_TEMP_MESSAGE,
  PUSH_NEW_ROOM_CREATE,
  DELETE_ROOM_SUCCESS,
  GET_CATE_FAQ_SUCCESS,
  GET_FAQ_SUCCESS,
  CREATE_FAQ_SUCCESS,
  DELETE_FAQ_SUCCESS,
  CREATE_CATE_FAQ_SUCCESS,
  DELETE_CATE_FAQ_SUCCESS,
  UPDATE_FAQ_SUCCESS,
  UPDATE_CATE_FAQ_SUCCESS,
  GET_TAG_SUCCESS,
  DELETE_TAG_SUCCESS,
  CREATE_TAG_SUCCESS,
  UPDATE_TAG_SUCCESS,
  SEARCH_MESSAGE_IN_ROOM_SUCCESS,
  SEARCH_ACROSS_SUCCESS,
  FLAG_SEARCH_ACROSS,
  FLAG_SEARCH_IN_ROOM,
  CHANGE_NAME_CHAT_ROOM,
} from '../action/chat';
import {
  AssignResponse,
  Chat,
  ListChatsQuery,
  Message,
  MessageInternal,
  MessageType,
  CateFaq,
  Faq,
  Tag,
} from '../services/chat';

export interface ChatState {
  currentChatId?: Chat['id'];
  query?: ListChatsQuery;
  chatList?: Chat['id'][];
  chats: Record<Chat['id'], Chat>;
  messageList?: Record<Chat['id'], Message['id'][]>;
  messages?: Record<Message['id'], Message>;
  inputs?: Record<Chat['id'], string>;
  caseLeftNav?: string;
  caseRightNav?: string;
  starredMessages?: Message[];
  onReplyMessage?: {
    user: string;
    text?: string;
    message_id: string;
    messageType: MessageType;
    imageUrl?: string;
    fileName?: string;
  };
  fileProgress?: { [x: string]: { time: number; run: boolean } };
  statusLoadMessage?: { [x: string]: boolean };
  currentInternalChat?: number;
  messagesInternal?: MessageInternal[];
  statusLoadingFileInternal?: boolean;
  loadingInternal?: boolean;
  chatTicket?: Record<AssignResponse['data']['chatTicket']['chat_id'], AssignResponse['data']['chatTicket']>;
  faq?: Faq[];
  cateFaq?: CateFaq[];
  tag?: Tag[];
  searchMessageResult?: Message[];
  searchAcrossResult?: { chats: Chat[]; messages: Message[] };
  flagSearchAcross?: boolean;
  flagSearchInRoom?: boolean;
}
export const INITIAL_CHAT_STATE: ChatState = { chats: {} };

export const DEFAULT_QUERY: ListChatsQuery = {};

export const chatReducer = (state: ChatState = INITIAL_CHAT_STATE, action: ChatActionTypes): ChatState => {
  switch (action.type) {
    case UPDATE_CHATS_QUERY:
      return { ...state, query: action.payload };
    case CHATS_FETCH_SUCCESS: {
      const ids = action.payload.data.data.map((c) => c.id);
      return {
        ...state,
        chatList: action.isFetchMore ? uniq([...(state.chatList ?? []), ...ids]) : ids,
        chats: {
          ...state.chats,
          ...fromPairs(action.payload.data.data.map((c) => [c.id, c])),
        },
        chatTicket: { ...state.chatTicket, ...fromPairs(action.payload.data.data.map((c) => [c.id, c.chatTicket!])) },
      };
    }
    case CHATS_FILTER_SUCCESS: {
      const ids = action.payload.data.data.map((c) => c.id);
      return {
        ...state,
        chatList: ids,
        chats: fromPairs(action.payload.data.data.map((c) => [c.id, c])),
      };
    }
    case SELECT_CHAT: {
      storeToLocalStorage('lastIdRoomChat', action.payload);
      const tempMessage = { ...state.fileProgress };
      const keys = Object.keys(tempMessage);
      keys.forEach((key) => {
        tempMessage[key].run = false;
      });
      return {
        ...state,
        currentChatId: action.payload,
        caseRightNav: state.caseRightNav === 'team-chat' ? '' : state.caseRightNav,
        messagesInternal: [],
      };
    }
    case MESSAGES_FETCH_SUCCESS: {
      return {
        ...state,
        messageList: {
          ...state.messageList,
          [action.chatId]: sortBy(
            uniq([...(state.messageList?.[action.chatId] ?? []), ...action.payload.data.data.map((m) => m.id)]),
            (o) => o,
          ),
        },
        messages: {
          ...state.messages,
          ...fromPairs(action.payload.data.data.map((m) => [m.id, m])),
        },
      };
    }
    case CHAT_UPDATED: {
      if (!state.chats[action.chat.id!]) return state;

      return {
        ...state,
        chats: {
          ...state.chats,
          [action.chat.id!]: {
            ...state.chats[action.chat.id!],
            ...action.chat,
          },
        },
      };
    }

    case NEW_MESSAGE: {
      if (!action.message.chat.id || !state.messageList?.[action.message.chat.id]) return state;
      const chatId = action.message.chat.id;
      const listChats = [...(state.chatList ?? [])];
      const indexChat = findIndex(listChats, (o) => o === chatId);
      if (indexChat !== -1 && indexChat !== 0) {
        const item = listChats.splice(indexChat, 1);
        listChats.splice(0, 0, item[0]);
      }
      const messageIdListTemp = { ...state.messageList };
      const tempListId = messageIdListTemp?.[chatId];
      const indexRemove = findIndex(tempListId, (item) => item === action.message.sender_identifier);
      if (indexRemove !== -1 && action.message.from_type !== 'Other') {
        tempListId[indexRemove] = action.message.id;
        return {
          ...state,
          messageList: {
            ...state.messageList,
            [chatId]: tempListId,
          },
          messages: {
            ...state.messages,
            [action.message.id]: action.message,
          },
        };
      }
      return {
        ...state,
        messageList: {
          ...state.messageList,
          [chatId]: [...(state.messageList?.[chatId] ?? []), action.message.id],
        },
        messages: {
          ...state.messages,
          [action.message.id]: action.message,
        },
        chatList: listChats,
      };
    }

    case MESSAGE_UPDATED: {
      const currentMessage = state.messages?.[action.message.id!];
      if (!currentMessage) return state;
      return {
        ...state,
        messages: {
          ...state.messages,
          [action.message.id!]: {
            ...currentMessage,
            ...action.message,
            status: Number(action.message.status ?? currentMessage.status),
          },
        },
      };
    }

    case SEND_MESSAGE_SUCCESS: {
      const chatId = action.message.chat.id;

      return {
        ...state,
        messageList: {
          ...state.messageList,
          [chatId]: [...(state.messageList?.[chatId] ?? []), action.message.id],
        },
        messages: {
          ...state.messages,
          [action.message.id]: action.message,
        },
      };
    }
    case UPDATE_CHAT_INPUT:
      return {
        ...state,
        inputs: {
          ...state.inputs,
          [action.chatId]: action.text,
        },
      };

    case UPDATE_EMOJI_INPUT: {
      return {
        ...state,
        inputs: {
          ...state.inputs,
          [action.chatId]: `${state.inputs ? state.inputs[action.chatId] : ''}${action.text}`,
        },
      };
    }

    case UPDATE_CASE_LEFT_NAV: {
      storeToLocalStorage('caseLeftNav', action.onCase);
      return {
        ...state,
        caseLeftNav: action.onCase,
      };
    }
    case UPDATE_CASE_RIGHT_NAV: {
      return {
        ...state,
        caseRightNav: action.onCase,
      };
    }
    case GET_STARRED_MESSAGES_SUCCESS: {
      return { ...state, starredMessages: action.data };
    }

    case UPDATE_STARRED_MESSAGE: {
      return { ...state, starredMessages: [] };
    }
    case DELETE_MESSAGE: {
      const currentRoom = state.currentChatId || '';
      const temp = { ...state.chats?.[currentRoom] };
      if (action.message_id === get(state, `chats.${currentRoom}.lastMessage.id`, '')) {
        set(temp, 'lastMessage.revoke', 1);
        return { ...state, chats: { ...state.chats, [currentRoom]: temp } };
      }
      return state;
    }

    case ON_REPLY_MESSAGE: {
      return { ...state, onReplyMessage: action.message };
    }

    case RESET_REPLY_MESSAGE: {
      return { ...state, onReplyMessage: undefined };
    }

    case MARK_READ_CHAT_SUCCESS: {
      const currentRoom = action.chat_id;
      const temp = { ...state.chats?.[currentRoom] };
      set(temp, 'read', 1);
      set(temp, 'unread_count', 0);
      return { ...state, chats: { ...state.chats, [currentRoom]: temp } };
    }

    case MARK_UNREAD: {
      const currentRoom = action.id;
      const temp = { ...state.chats?.[currentRoom] };
      set(temp, 'read', action.read);
      set(temp, 'unread_count', action.unread_count);
      return { ...state, chats: { ...state.chats, [currentRoom]: temp } };
    }

    case CLEAR_CHAT_SUCCESS: {
      const temp = { ...state.chats?.[action.chat_id] };
      set(temp, 'lastMessage', null);
      return {
        ...state,
        messageList: {
          ...state.messageList,
          [action.chat_id]: [],
        },
        chats: { ...state.chats, [action.chat_id]: temp },
      };
    }
    case ASSIGN_TAG_SUCCESS: {
      const temp = { ...state.chats?.[action.payload.chatId] };
      set(temp, 'tagList', action.payload.listTags);
      return { ...state, chats: { ...state.chats, [action.payload.chatId]: temp } };
    }
    case SEND_DOCUMENT_TEMP_MESSAGE_SUCCESS: {
      const tempReplyMess = state.messages?.[action.payload.reply] ?? undefined;
      const chatId = action.payload.currentId;
      const listChats = [...(state.chatList ?? [])];
      const indexChat = findIndex(listChats, (o) => o === chatId);
      if (indexChat !== -1 && indexChat !== 0) {
        const item = listChats.splice(indexChat, 1);
        listChats.splice(0, 0, item[0]);
      }
      const { fileName } = action.payload;
      const tempMess: Message = {
        id: action.payload.id,
        status: 9,
        revoke: 0,
        from_type: 'Customer Service',
        message_type:
          // eslint-disable-next-line no-nested-ternary
          action.payload.type === 'Document'
            ? MessageType.Document
            : action.payload.type === 'Voice'
            ? MessageType.Voice
            : MessageType.Media,
        text: '',
        file_name: fileName,
        file_caption: '',
        bookmark: 1,
        read: 1,
        tel: '0888222293',
        timestamp: getTime(new Date()),
        chat: {
          id: chatId,
        },
        user: {
          id: action.payload.size / 1048576,
          name: action.payload.name,
        },
        uuid: '12233',
        api_message_id: '123',
        api_error_message: '123',
        file: action.payload.file,
        quotedMessage: tempReplyMess,
        file_mime_type: action.payload.file.type,
      };
      return {
        ...state,
        messages: { ...state.messages, [tempMess.id]: tempMess },
        messageList: {
          ...state.messageList,
          [chatId]: [...(state.messageList?.[chatId] ?? []), tempMess.id],
        },
        chatList: listChats,
      };
    }

    case SEND_TEXT_TEMP_MESSAGE: {
      const chatId = action.payload.currentId;
      const listChats = [...(state.chatList ?? [])];
      const indexChat = findIndex(listChats, (o) => o === chatId);
      if (indexChat !== -1 && indexChat !== 0) {
        const item = listChats.splice(indexChat, 1);
        listChats.splice(0, 0, item[0]);
      }
      const tempReplyMess = state.messages?.[action.payload.reply] ?? undefined;
      const tempMess: Message = {
        id: action.payload.pass,
        status: 0,
        revoke: 0,
        from_type: 'Customer Service',
        message_type: MessageType.Text,
        text: action.payload.text,
        file_caption: '',
        bookmark: 1,
        read: 1,
        tel: '0888222293',
        timestamp: getTime(new Date()),
        chat: {
          id: chatId,
        },
        user: {
          id: 123,
          name: action.payload.name,
        },
        uuid: '12233',
        api_message_id: '123',
        api_error_message: '123',
        quotedMessage: tempReplyMess,
      };
      return {
        ...state,
        messages: { ...state.messages, [tempMess.id]: tempMess },
        messageList: {
          ...state.messageList,
          [chatId]: [...(state.messageList?.[chatId] ?? []), tempMess.id],
        },
        chatList: listChats,
      };
    }

    case EDIT_TAG_SUCCESS: {
      const temp = { ...state.chats?.[action.payload.chatId] };
      const tempTag = temp.tagList;
      const indexTag = findIndex(tempTag, (item) => item.id === action.payload.tag.id);
      tempTag[indexTag].name = action.payload.tag.name;
      set(temp, 'tagList', tempTag);
      return { ...state, chats: { ...state.chats, [action.payload.chatId]: temp } };
    }
    case SET_TIME_LOADING_FILE: {
      return {
        ...state,
        fileProgress: {
          ...state.fileProgress,
          [action.payload.messageId]: { time: action.payload.time, run: true },
        },
      };
    }
    case PROGRESS_UPLOAD: {
      // const tempRoom = [...(state.messageList?.[action.payload.roomId] ?? [])];
      // const index = findIndex(tempRoom, (item) => item === action.payload.chatId);
      // if (index !== -1) {
      //   tempRoom.splice(index, 1);
      // }
      // const tempMess = { ...state.messages };
      // unset(tempMess, action.payload.chatId);
      return { ...state };
    }

    case SET_FLAG_LOAD_MESSAGE: {
      return { ...state, statusLoadMessage: { ...state.statusLoadMessage, [action.roomId]: action.status } };
    }
    case MESSAGE_SEARCH: {
      return {
        ...state,
        messageList: {
          ...state.messageList,
          [action.chatId]: action.payload.data.data.map((m) => m.id),
        },
        messages: {
          ...state.messages,
          ...fromPairs(action.payload.data.data.map((m) => [m.id, m])),
        },
      };
    }

    case SELECT_ROOM_MESSAGE_SEARCH: {
      storeToLocalStorage('lastIdRoomChat', action.chatId);
      return {
        ...state,
        currentChatId: action.chatId,
      };
    }
    case RESEND_MESSAGE_SUCCESS: {
      const idMessage = action.id;
      const tempMessage = { ...state.messages?.[idMessage] };
      const currentRoom = state.currentChatId || '';
      const tempRoom = { ...state.chats?.[currentRoom] };

      set(tempMessage, 'status', 0);
      set(tempRoom, 'lastMessage.status', 0);

      return {
        ...state,
        chats: { ...state.chats, [currentRoom]: tempRoom },
        messages: { ...state.messages, [idMessage]: tempMessage as Message },
      };
    }
    case SELECT_INTERNAL_ROOM: {
      return { ...state, currentInternalChat: action.id };
    }
    case FETCH_MESSAGE_INTERNAL_SUCCESS: {
      const divScroll = document.getElementById('team-chat') as HTMLDivElement;
      if (divScroll) {
        divScroll.style.overflow = 'hidden auto';
        scrollToLastMessage(false, 0, 'team-chat');
      }
      return {
        ...state,
        messagesInternal: action.loadMoreFlag
          ? [...action.messageList, ...(state.messagesInternal ?? [])]
          : action.messageList,
      };
    }

    case NEW_MESSAGE_INTERNAL: {
      return { ...state, messagesInternal: [...(state.messagesInternal ?? []), action.message] };
    }

    case DELETE_INTERNAL_MESSAGE: {
      const listInternalMess = [...(state.messagesInternal ?? [])];
      const index = findIndex(listInternalMess, (o) => o.id === action.messageId);
      if (index !== -1) {
        listInternalMess.splice(index, 1);
        return { ...state, messagesInternal: listInternalMess };
      }
      return state;
    }
    case CLEAR_ALL_MESSAGE_INTERNAL: {
      const currentRoom = state.currentInternalChat;
      if (currentRoom === action.id) {
        return { ...state, messagesInternal: [] };
      }
      return state;
    }

    case LOADING_SEND_FILE_INTERNAL: {
      return { ...state, statusLoadingFileInternal: action.status };
    }

    case DELETE_MESSAGE_ERROR: {
      const message = { ...state.messages?.[action.id] };
      set(message, 'status', 0);
      return { ...state, messages: { ...state.messages, [action.id]: message as Message } };
    }

    case LOADING_MESSAGE_INTERNAL: {
      return { ...state, loadingInternal: action.status };
    }

    case ASSIGN_USER_SUCCESS: {
      const chatId = state.currentChatId || '';
      return {
        ...state,
        chatTicket: {
          ...state.chatTicket,
          [chatId]: action.payload.chatTicket,
        },
      };
    }
    case UNASSIGN_USER_SUCCESS: {
      const chatId = state.currentChatId || '';
      return {
        ...state,
        chatTicket: {
          ...state.chatTicket,
          [chatId]: action.payload.chatTicket,
        },
      };
    }
    case CLOSE_CHAT_TICKET_SUCCESS: {
      const chatId = state.currentChatId || '';
      return {
        ...state,
        chatTicket: {
          ...state.chatTicket,
          [chatId]: action.payload.chatTicket,
        },
      };
    }
    case OPEN_CHAT_TICKET_SUCCESS: {
      const chatId = state.currentChatId || '';
      return {
        ...state,
        chatTicket: {
          ...state.chatTicket,
          [chatId]: action.payload.data.chatTicket,
        },
      };
    }
    case PUSH_NEW_ROOM_CREATE: {
      const idRoom = action.payload.room.id;

      return {
        ...state,
        chatList: [idRoom, ...(state.chatList ?? [])],
        chats: {
          ...state.chats,
          [idRoom]: action.payload.room,
        },
      };
    }

    case DELETE_ROOM_SUCCESS: {
      const idRoom = action.payload.room;
      const chatListRoom = [...(state.chatList ?? [])];
      const index = findIndex(chatListRoom, (o) => o === idRoom);
      chatListRoom.splice(index, 1);

      return {
        ...state,
        chatList: chatListRoom,
      };
    }

    case GET_CATE_FAQ_SUCCESS: {
      return { ...state, cateFaq: action.cate };
    }
    case GET_FAQ_SUCCESS: {
      return { ...state, faq: action.faq };
    }

    case GET_TAG_SUCCESS: {
      return { ...state, tag: action.tag };
    }

    case CREATE_FAQ_SUCCESS: {
      return { ...state, faq: [...(state.faq ?? []), action.faq] };
    }

    case CREATE_TAG_SUCCESS: {
      return { ...state, tag: [...(state.tag ?? []), action.tag] };
    }

    case DELETE_FAQ_SUCCESS: {
      const index = findIndex(state.faq, (o) => o.id === action.id);
      const temp = [...(state.faq ?? [])];
      temp.splice(index, 1);
      return { ...state, faq: temp };
    }

    case DELETE_TAG_SUCCESS: {
      const index = findIndex(state.tag, (o) => o.id === action.id);
      const temp = [...(state.tag ?? [])];
      temp.splice(index, 1);
      return { ...state, tag: temp };
    }

    case CREATE_CATE_FAQ_SUCCESS: {
      return { ...state, cateFaq: [...(state.cateFaq ?? []), action.cateFaq] };
    }

    case DELETE_CATE_FAQ_SUCCESS: {
      const index = findIndex(state.cateFaq, (o) => o.id === action.id);
      const temp = [...(state.cateFaq ?? [])];
      temp.splice(index, 1);
      return { ...state, cateFaq: temp };
    }

    case UPDATE_FAQ_SUCCESS: {
      const index = findIndex(state.faq, (o) => o.id === action.faq.id);
      const temp = [...(state.faq ?? [])];
      temp[index] = action.faq;
      return { ...state, faq: temp };
    }

    case UPDATE_TAG_SUCCESS: {
      const index = findIndex(state.tag, (o) => o.id === action.tag.id);
      const temp = [...(state.tag ?? [])];
      temp[index] = action.tag;
      return { ...state, tag: temp };
    }

    case UPDATE_CATE_FAQ_SUCCESS: {
      const index = findIndex(state.cateFaq, (o) => o.id === action.cateFaq.id);
      const temp = [...(state.cateFaq ?? [])];
      temp[index] = action.cateFaq;
      return { ...state, cateFaq: temp };
    }

    case SEARCH_MESSAGE_IN_ROOM_SUCCESS: {
      return { ...state, searchMessageResult: action.mess };
    }
    case SEARCH_ACROSS_SUCCESS: {
      return { ...state, searchAcrossResult: action.data };
    }

    case FLAG_SEARCH_ACROSS: {
      return { ...state, flagSearchAcross: action.status };
    }

    case FLAG_SEARCH_IN_ROOM: {
      return { ...state, flagSearchInRoom: action.status };
    }

    case CHANGE_NAME_CHAT_ROOM: {
      if (!state.chats[action.payload.roomId!]) return state;
      return {
        ...state,
        chats: {
          ...state.chats,
          [action.payload.roomId]: {
            ...state.chats[action.payload.roomId],
            name: action.payload.name,
            whatsapp_name: action.payload.name,
          },
        },
      };
    }

    default:
      return state;
  }
};
