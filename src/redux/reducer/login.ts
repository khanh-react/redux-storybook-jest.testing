import jwtDecode from 'jwt-decode';
import { get } from 'lodash';

import {
  AuthState,
  FETCH_SUB_DOMAIN_CHANNEL_SUCCESS,
  FETCH_SUB_DOMAIN_CONFIG_SUCCESS,
  FETCH_SUB_DOMAIN_SETTING_SUCCESS,
  FETCH_SUB_DOMAIN_USER_SUCCESS,
  INITIAL_AUTH_STATE,
  INITIAL_SUBDOMAIN_STATE,
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LoginActionTypes,
  SubdomainState,
} from '../type/login/types';

const authReducer = (state: AuthState = INITIAL_AUTH_STATE, action: LoginActionTypes): AuthState => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: '',
        token: '',
      };
    case LOGIN_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        error: '',
        token: action.payload.token,
        userId: get(jwtDecode(action.payload.token), 'user', 0),
        subdomainId: action.payload.subdomainId,
      };
    }

    case LOGIN_FAILURE:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.payload,
        token: '',
      };

    default:
      return state;
  }
};
export const subDomainReducer = (
  state: SubdomainState = INITIAL_SUBDOMAIN_STATE,
  action: LoginActionTypes,
): SubdomainState => {
  switch (action.type) {
    case FETCH_SUB_DOMAIN_CONFIG_SUCCESS: {
      return {
        ...state,
        subdomainConfig: action.payload,
      };
    }

    case FETCH_SUB_DOMAIN_SETTING_SUCCESS: {
      return {
        ...state,
        subdomainSetting: action.payload,
      };
    }

    case FETCH_SUB_DOMAIN_CHANNEL_SUCCESS: {
      return {
        ...state,
        subdomainChannel: action.payload,
      };
    }

    case FETCH_SUB_DOMAIN_USER_SUCCESS: {
      return {
        ...state,
        subdomainUser: action.payload,
      };
    }

    default:
      return state;
  }
};
export default authReducer;
