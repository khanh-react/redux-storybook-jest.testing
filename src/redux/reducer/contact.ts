import {
  BULK_DELETE_REQUEST_SUCCESS,
  ContactActionTypes,
  CONTACTS_FETCH_SUCCESS,
  DELETE_SINGLE_CONTACT_REQUEST_SUCCESS,
  EDIT_CONTACT,
  GET_CONTACT_DETAIL_SUCCESS,
  UPDATE_CONTACT_DATA_QUERY,
  UPDATE_CONTACT_DETAIL_SUCCESS,
  UPDATE_CONTACT_QUERY,
  UPDATE_FILTERS,
} from '../type/contact/actions';
import { ContactState, DEFAULT_QUERY, INITIAL_CONTACT_STATE } from '../type/contact/state';

export const contactReducer = (
  state: ContactState = INITIAL_CONTACT_STATE,
  action: ContactActionTypes,
): ContactState => {
  switch (action.type) {
    case UPDATE_CONTACT_QUERY:
      return {
        ...state,
        query: { ...DEFAULT_QUERY, ...state.query, ...action.payload },
      };
    case CONTACTS_FETCH_SUCCESS:
      return {
        ...state,
        currentPage: action.payload,
        fields: action.payload.fieldHeaders,
        selectedFields: state.selectedFields || action.payload.fieldHeaders.map((f) => f.name),
      };
    case BULK_DELETE_REQUEST_SUCCESS:
      return {
        ...state,
        delResult: {
          ...action.payload,
        },
      };
    case EDIT_CONTACT:
      return {
        ...state,
        fields: [...action.payload],
      };
    case GET_CONTACT_DETAIL_SUCCESS:
      return {
        ...state,
        contactDetail: action.payload,
      };
    case UPDATE_CONTACT_DATA_QUERY:
      return {
        ...state,
        contactData: action.payload,
      };
    case DELETE_SINGLE_CONTACT_REQUEST_SUCCESS:
      return {
        ...state,
        delSingleResult: action.payload,
      };
    case UPDATE_CONTACT_DETAIL_SUCCESS:
      return {
        ...state,
        updateContactResult: action.payload,
      };
    case UPDATE_FILTERS:
      return {
        ...state,
        filters: [...action.filters],
      };

    default:
      return state;
  }
};
