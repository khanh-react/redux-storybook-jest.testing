import { combineReducers } from 'redux';

import dummyReducer from './dummyReducer';

// TODO: prepare reducers in duck style
export default combineReducers({
  dummyReducer,
});
