/* eslint-disable @typescript-eslint/no-extra-non-null-assertion */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { UtilActionTypes, SET_LEFT_NAV_ROUTE } from '../action/util';

export interface UtilState {
  loading: boolean;
  leftNavRoute: string;
}

export const INITIAL_UTIL_STATE: UtilState = { loading: false, leftNavRoute: '' };

export const utilReducer = (state: UtilState = INITIAL_UTIL_STATE, action: UtilActionTypes) => {
  switch (action.type) {
    case 'SET_LOADING': {
      return { ...state, loading: action.status };
    }
    case SET_LEFT_NAV_ROUTE: {
      return { ...state, leftNavRoute: action.status };
    }

    default:
      return state;
  }
};
