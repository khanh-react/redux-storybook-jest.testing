import { createRequest } from '../../utils/fetchClient';

export const login = (
  email: string,
  password: string,
): Promise<{ data: { token: string; subdomain: { id: number; name: string }[] } }> =>
  createRequest('/user/login', { method: 'POST', data: { email, password } });

export interface SubdomainConfig {
  id: number;
  domain: string;
  status: string;
  name: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  params: any | null;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  earliestUnpaidInvoice: any | null;
}

export const getSubdomainConfig = (subdomain_id: number): Promise<{ data: SubdomainConfig; code: number }> => {
  return createRequest(`/subdomains/${subdomain_id}`, {
    method: 'GET',
  });
};

export interface SubdomainUser {
  id: number;
  subdomain_id: string;
  user_id: number;
  name: string;
  email: string;
  tel: string;
  status: string;
  type: string;
  service_status: string;
  assign_all_category: number;
  assign_all_number: number;
  assign_all_contact: number;
  assign_all_chat: number;
}

export const getSubdomainUser = (subdomain_id: number): Promise<{ data: { data: SubdomainUser }; code: number }> => {
  return createRequest(`/subdomain/${subdomain_id}/users`, {
    method: 'GET',
  });
};

export interface SubdomainChannel {
  id: string;
  subdomain_id: string;
  user_id: number;
  name: string;
  email: string;
  tel: string;
  status: string;
  type: string;
  service_status: string;
  assign_all_category: number;
  assign_all_number: number;
  assign_all_contact: number;
  assign_all_chat: number;
  platform: string;
  owner: string;
}

export const getSubdomainChannel = (
  subdomain_id: number,
): Promise<{ data: { data: SubdomainChannel[] }; code: number }> => {
  return createRequest(`/subdomain/${subdomain_id}/channels`, {
    method: 'GET',
  });
};

export interface SubdomainSetting {
  tag: Tag[];
  ticketStatus: TicketStatus[];
  accessibleTagAttributeKeys: AccessibleTagAttributeKey[];
  contactField: ContactField[];
}
export interface AccessibleTagAttributeKey {
  key: string;
  label: string;
}

export interface ContactField {
  name: string;
  label: string;
  type: string;
  ordering: number;
  required: number;
  permission: number;
  enabled: number;
  editable_label: number;
  editable_required: number;
  editable_type: number;
  editable_enabled: number;
  option_list: string[];
}

export interface Tag {
  id: number;
  subdomain_id: string;
  name: string;
  tagAttributes: TagAttribute[];
}

export interface TagAttribute {
  key: string;
  value: null;
}

export interface TicketStatus {
  id: number;
  subdomain_id: number;
  name: string;
  type: string;
  ordering: number;
}
export const getSubdomainSetting = (subdomain_id: number): Promise<{ data: SubdomainSetting; code: number }> => {
  return createRequest(`/subdomain/${subdomain_id}/domain-settings`, {
    method: 'GET',
  });
};
