import { createRequest } from '../../utils/fetchClient';
import { Contact, ContactQuery } from '../type/contact/state';

export const listContacts = (query: ContactQuery, filters?: { [name: string]: string }) => {
  return createRequest(`/contacts`, { query, filters });
};

export const bulkDeleteContactsApi = (
  data: number[],
): Promise<{ data: { result: { deleted: number[]; failed: number[] }[] } }> =>
  createRequest('/contacts/bulk-delete', { method: 'POST', data: { data } });

export const addContactApi = (data: {
  name: string;
  fields: Record<string, unknown>;
}): Promise<{ data: { result: Record<string, unknown>; id: string } }> =>
  createRequest('/contacts', { method: 'POST', data: { data } });

export const deleteSingleContactApi = (id: string): Promise<{ data: { result: boolean } }> =>
  createRequest(`/contacts/${id}`, { method: 'DELETE' });

export const getContactDetailApi = (id: string) => createRequest(`/contacts/${id}`, { method: 'GET' });

export const updateContactDetailApi = (data: Partial<Contact>): Promise<{ data: { result: { id: string } } }> =>
  createRequest(`/contacts/${data.id}`, { method: 'PUT', data: { data } });
