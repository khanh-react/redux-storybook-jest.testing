/* eslint-disable no-restricted-syntax */
/* eslint-disable no-shadow */
import { createDownloadRequest, createRequest } from '../../utils/fetchClient';

interface ApiKey {
  id: string;
  name: string;
}
export enum NumberBoolean {
  True = 1,
  False = 0,
}

export enum MessageStatus {
  Failed = -1, // Failed;
  Newly = 0, // Newly created;
  Sent = 1, // Sent (API acknowledge);
  Received = 2, // Received (End user acknowledge);
  Read = 3, // Read (End user read)
}

export enum MessageRevoke {
  No = 0,
  Deleting = 1,
  Deleted = 2,
}

// interface User {
//   id: number;
//   name: string;
// }

interface Contact {
  id: string;
  name: string;
  whatsapp_name?: string;
  fields: Record<string, string>;
  tel?: string;
}

export enum MessageType {
  Text = 'Text',
  Media = 'Media',
  Document = 'Document',
  Voice = 'Voice',
  Link = 'Link',
  Sticker = 'Sticker',
  Event = 'Event',
  Vcard = 'Vcard',
  Image = 'Image',
  Video = 'Video',
}
export interface BasicMessage {
  id: string;
  status: MessageStatus;
  revoke: number;
  from_type: 'Other' | 'Customer Service' | 'System' | 'Web' | 'Phone';
  message_type: MessageType;
  text: string;
  file_name?: string;
  file_url?: string;
  secure_download_url?: string;
  file_preview_url?: string;
  file_caption?: string;
  link?: string;
  link_title?: string;
  link_description?: string;
  timestamp: number;
  chat: { id: string };
  user: { id: number; name: string };
  contact?: Contact;
}
export interface BasicUser {
  id: number;
  name: string;
}

export interface Message extends BasicMessage {
  uuid: string;
  read: NumberBoolean;
  readUser?: BasicUser;
  bookmark: NumberBoolean;
  api_message_id: string;
  api_error_message: string;
  quotedMessage?: Message;
  chatCategory?: { id: number; name: string };
  tel: string;
  sender_identifier?: string;
  file?: File;
  file_mime_type?: string;
}

export enum ChatPlatform {
  'WhatsApp',
  'Line',
  'Facebook',
  'Twilio',
  'Wechat',
}

export interface ChatTicket {
  id: number;
  subdomain_chat_ticket_no: number;
  open_at: number;
}
export interface ChatContactList {
  id: string;
  is_admin: number | null;
  is_super_admin: number | null;
  contact: ChatContactListContact;
}

export interface ChatContactListContact {
  id: number;
  tel: string;
  name: string;
}
export interface Chat {
  id: string;
  name: string;
  whatsapp_name?: string;
  to: string;
  type: 'Individual' | 'Group';
  api_key_id: number;
  platform: ChatPlatform;
  owner: string;
  timestamp: number;
  read: number;
  unread_count: number;
  status?: string;
  chat_ticket_status_id?: number;
  unread_message_at?: string;
  enable_send: NumberBoolean;
  fields: Record<string, string>;
  contact?: Contact;
  contactList?: Record<string, string>[];
  lastMessage?: BasicMessage;
  readUser?: string;
  chatTicket?: ChatTicketResponse;
  tag?: string;
  tagList: { id: string; name: string }[];
  assignedUser: BasicUser;
  apiKey: ApiKey;
  chatContactList: ChatContactList[];
}

export interface ListChatsResponse {
  code: number;
  data: { data: Chat[] };
}

export interface ListChatsQuery {
  filter?: string; // The search keyword
  order_by?: 'timestamp' | 'unread_timestamp' | 'unread_count';
  limit?: number; // Page limit, default to 50
  offset_timestamp?: number; // For pagination, use together with "last-message-date-desc" mode
  offset_id?: number;
  offset_unread_count?: number; // For pagination, used in order_by mode "unread_count"
  // should always pass the "unread_count" of the last ChatModel from the previous result
}

export const listChats = (query?: ListChatsQuery): Promise<ListChatsResponse> => createRequest('/chat', { query });

export interface ListMessagesResponse {
  data: {
    _link: Record<string, string>;
    _meta: Record<string, string>;
    data: Message[];
  };
}

export interface ListMessagesQuery {
  chat_id: Chat['id'];

  limit?: number;
  // Optional the page size, default 50

  start_mode?: 'latest' | 'oldest' | 'oldest unread';
  // Optional the loading mechanism, used only if before_timestamp/before_id/up_to_timestamp/up_to_id are not used

  before_id?: number;
  // Optional for paging purpose, load messages that are before this message id, used tgt with before_timestamp

  before_timestamp?: number;
  // Optional for paging purpose, load messages that are before this timestamp, used tgt with before_id

  up_to_id?: string;
  // Optional for paging purpose, load messages that are after this message id, used tgt with up_to_timestamp

  up_to_timestamp?: number;
  // Optional for paging purpose, load messages that are after this timestamp, used tgt with up_to_id
}

export const listMessage = (query?: ListMessagesQuery): Promise<ListMessagesResponse> =>
  createRequest('/message', { query });

export const sendText = (
  chatId: string,
  text: string,
  sender_identifier: string,
  reply?: string,
): Promise<{ data: Message; code: number }> => {
  // const formData = new FormData();
  // formData.append('to', to);
  // formData.append('text', text);
  // formData.append('type', 'Text');
  return createRequest('/messages', {
    method: 'POST',
    data: {
      data: {
        chat_id: chatId,
        text,
        message_type: 'Text',
        from_type: 'Customer Service',
        sender_identifier,
        quoted_message_id: reply || null,
      },
    },
  });
};

export interface NewChat {
  to: string | string[];
  name: string;
  type: string;
  api_key_id: number;
  timestamp: number;
}

export const createChat = ({
  to,
  name,
  type,
  api_key_id,
  timestamp,
}: NewChat): Promise<{ data: Chat; code: number }> => {
  return createRequest('/chats', {
    method: 'POST',
    data:
      type === 'Individual'
        ? { data: { to, name, type, api_key_id, timestamp } }
        : { data: { to_list: to, name, type, api_key_id, timestamp } },
  });
};

export interface DeleteChat {
  result: boolean;
}

export const deleteChat = (idRoom: string): Promise<{ data: DeleteChat; code: number }> => {
  return createRequest(`/chats/${idRoom}`, {
    method: 'DELETE',
  });
};

export interface StarredChat {
  result: string;
}

export const starredMessage = (message_id: string, toggle: number): Promise<{ data: StarredChat; code: number }> => {
  return createRequest(`/message/${message_id}/bookmark`, {
    method: 'POST',
    data: { toggle },
  });
};

export interface GetStarredChat {
  data: Message[];
}

export const getStarredMessages = (): Promise<{ data: GetStarredChat; code: number }> => {
  return createRequest(`/bookmark`, {
    method: 'GET',
  });
};

export interface DeleteMessage {
  result: string;
}

export const deleteMessage = (message_id: string): Promise<{ data: DeleteMessage; code: number }> => {
  return createRequest(`/message/${message_id}/revoke`, {
    method: 'POST',
  });
};

// export const uploadFile = (
//   chatId: string,
//   document: File,
//   identifier: string,
//   fileCaption: string,
//   type: string,
//   quoted_message_id?: string,
// ): Promise<{ data: Message; code: number }> => {
//   const formData = new FormData();
//   formData.append('file', document);
//   formData.append('data[chat_id]', chatId);
//   formData.append('data[sender_identifier]', identifier);
//   formData.append('data[from_type]', 'Customer Service');
//   formData.append('data[message_type]', type);
//   formData.append('data[file_caption]', fileCaption);
//   quoted_message_id && formData.append('data[quoted_message_id]', quoted_message_id);
//   return createRequest('/messages', {
//     type: 'form',
//     method: 'POST',
//     data: formData,
//   });
// };

export interface MarkReadChat {
  result: string;
}

export const markReadChat = (chat_id: string): Promise<{ data: MarkReadChat; code: number }> => {
  return createRequest(`/chats/${chat_id}/mark-read`, {
    method: 'POST',
  });
};

export interface MarkUnReadChat {
  result: string;
}

export const markUnReadChat = (chat_id: string): Promise<{ data: MarkUnReadChat; code: number }> => {
  return createRequest(`/chats/${chat_id}/mark-unread`, {
    method: 'POST',
  });
};

export interface ClearChat {
  result: string;
}

export const clearChat = (idRoom: string): Promise<{ data: ClearChat; code: number }> => {
  return createRequest(`/chats/${idRoom}/clear`, {
    method: 'DELETE',
  });
};

export interface Tag {
  id: string;
  subdomain_id: number;
  name: string;
}

export interface Tags {
  tag: Tag[];
}

export const getAllTagList = (): Promise<{ data: Tags; code: number }> => {
  return createRequest(`/tags`, {
    method: 'GET',
  });
};

export const createdTag = (name: string): Promise<{ data: { tag: Tag }; code: number }> => {
  return createRequest(`/tags`, {
    method: 'POST',
    data: { data: { name } },
  });
};

export const updateTag = (name: string, id: string): Promise<{ data: { tag: Tag }; code: number }> => {
  return createRequest(`/tags/${id}`, {
    method: 'PUT',
    data: { data: { name } },
  });
};

export const deleteTag = (id: string): Promise<{ data: { tag: Tag }; code: number }> => {
  return createRequest(`/tags/${id}`, {
    method: 'DELETE',
  });
};

interface UpdateChat {
  result: {
    id: string;
  };
}
export const updateChat = (idRoom: string, name: string): Promise<{ data: UpdateChat; code: number }> => {
  return createRequest(`/chats/${idRoom}`, {
    method: 'PUT',
    data: {
      data: { name },
    },
  });
};

export interface CreatedTicket {
  result: string;
  data: Chat;
}

export const createTicket = (chat_id: string): Promise<{ data: CreatedTicket; code: number }> => {
  return createRequest(`/chats/${chat_id}/new-ticket`, {
    method: 'POST',
  });
};

interface AssignChat {
  data: {
    result: string;
    data: {
      tag_id_list: string[];
    };
  };
}

export const assignChat = (idRoom: string, tag_id_list: string[]): Promise<{ data: AssignChat; code: number }> => {
  return createRequest(`/chats/${idRoom}/assign-tag`, {
    method: 'POST',
    data: {
      data: { tag_id_list },
    },
  });
};

interface ResendMessage {
  data: {
    status: number;
    api_error_message: string;
  };
}

export const resendMessage = (idMessage: string): Promise<{ data: ResendMessage; code: number }> => {
  return createRequest(`/message/${idMessage}/resend`, {
    method: 'POST',
  });
};

interface SearchAcross {
  chats: Chat[];
  messages: Message[];
}

export const searchAcross = (
  keyword: string,
  offset?: number,
  limit?: number,
): Promise<{ data: SearchAcross; code: number }> => {
  const offsetTemp = offset || '';
  const limitTemp = limit || '';

  return createRequest(`/search/inbox?keyword=${keyword}&offset=${offsetTemp}&limit=${limitTemp}`, {
    method: 'GET',
  });
};

export interface ChatTicketResponse extends ChatTicket {
  subdomain_id: number;
  chat_id: number;
  status: string;
  assign_status: string;
  assigned_user_id: number;
  tagList: { id: string; name: string }[];
  assignedUser: BasicUser;
}
export interface AssignResponse {
  data: {
    result: string;
    chatTicket: ChatTicketResponse;
  };
}

export const assignUserApi = (
  userId: number,
  chatTicketId: number,
): Promise<{ data: AssignResponse; code: number }> => {
  return createRequest(`/chat-tickets/${chatTicketId}/assigned-user`, {
    method: 'PUT',
    data: {
      data: { userId },
    },
  });
};

export const unAssignUserApi = (chatTicketId: number): Promise<{ data: AssignResponse; code: number }> => {
  return createRequest(`/chat-tickets/${chatTicketId}/assigned-user`, {
    method: 'PUT',
    data: {},
  });
};

interface SearchInRoom {
  data: Message[];
}

export const searchInRoom = (
  roomId: string,
  keyword: string,
  offset?: number,
  limit?: number,
): Promise<{ data: SearchInRoom; code: number }> => {
  const offsetTemp = offset || '';
  const limitTemp = limit || '';

  return createRequest(`/chats/${roomId}/search?keyword=${keyword}&offset=${offsetTemp}&limit=${limitTemp}`, {
    method: 'GET',
  });
};

// For Close Ticket
export const closeTicketApi = (chatTicketId: number): Promise<{ data: AssignResponse; code: number }> => {
  return createRequest(`/chat-tickets/${chatTicketId}/close`, {
    method: 'PUT',
    data: {},
  });
};
// Submit Agent response for chat ticket reservation
export const submitAgentResponseApi = (
  chatTicketReservationId: number,
  res: string,
): Promise<{ data: { result: string } }> => {
  return createRequest(`/chat-ticket-reservations/${chatTicketReservationId}/response`, {
    method: 'POST',
    data: { data: { response: res } },
  });
};
// Export member groupChat => excel
export const exportMembersGCApi = (chatId: number) => {
  return createDownloadRequest(`/chats/${chatId}/members`, {
    method: 'GET',
  });
};
// Refresh members from channel
export const refreshMembersGCApi = (chatId: number): Promise<{ data: { result: string } }> => {
  return createRequest(`/chats/${chatId}/refresh-members`, {
    method: 'POST',
  });
};
// Add member to groupChat
export const addMembersGCApi = (chatId: number, phone_list: string[]): Promise<{ data: { result: string } }> => {
  return createRequest(`/chats/${chatId}/add-members`, {
    method: 'POST',
    data: {
      phone_list,
    },
  });
};
// Remove member to groupChat
export const removeMembersGCApi = (chatId: number, phone_list: string[]): Promise<{ data: { result: string } }> => {
  return createRequest(`/chats/${chatId}/remove-members`, {
    method: 'POST',
    data: {
      phone_list,
    },
  });
};
interface GetInternalChatId {
  data: {
    id: number;
    subdomain_id: number;
    contact_id: number;
    chat_id: number;
  };
}

export const getInternalChatId = (roomId: string): Promise<{ data: GetInternalChatId; code: number }> => {
  return createRequest(`/chats/${roomId}/internal-chat`, {
    method: 'GET',
  });
};

export interface ListInternalChatsQuery {
  limit?: number;
  message_type?: 'Document' | 'Image' | 'Video';
  before_id?: string;
  before_at?: string;
}

export interface MessageInternal {
  id: string;
  message_type: MessageType;
  text?: string;
  file_name?: string;
  file_mime_type?: string;
  create_date: Date;
  teamInternalChat: {
    id: number;
  };
  senderUser: {
    id: number;
    name: string;
  };
  quotedInternalChatMessage?: {
    id: string;
    message_type: MessageType;
    text?: string;
    file_name?: string;
    file_mime_type?: string;
    create_date: string;
    teamInternalChat: {
      id: number;
    };
    senderUser: {
      id: number;
      name: string;
    };
  };
}

export const getMessageInternal = (
  idRoom: number,
  query?: ListInternalChatsQuery,
): Promise<{ data: MessageInternal; code: number }> => createRequest(`/internal-chats/${idRoom}/messages`, { query });

interface SendTextInternal {
  data: MessageInternal;
}

export const sendTextInternal = (roomId: number, text: string): Promise<{ data: SendTextInternal; code: number }> => {
  return createRequest(`/internal-chats/${roomId}/messages`, {
    method: 'POST',
    data: {
      data: { message_type: 'Text', text },
    },
  });
};

export const uploadFileInternal = (
  chatId: number,
  document: File,
  type: string,
  fileCaption: string,
  replyId?: string,
): Promise<{ data: Message; code: number }> => {
  const formData = new FormData();
  formData.append('file', document);
  formData.append('data[message_type]', type);
  formData.append('data[text]', fileCaption);
  replyId && formData.append('data[quoted_internal_chat_message_id]', replyId);
  return createRequest(`/internal-chats/${chatId}/messages`, {
    type: 'form',
    method: 'POST',
    data: formData,
  });
};

interface DeleteInternalMessage {
  result: string;
}

export const deleteInternalMessage = (
  roomId: number,
  messId: string,
): Promise<{ data: DeleteInternalMessage; code: number }> => {
  return createRequest(`/internal-chats/${roomId}/messages/${messId}`, {
    method: 'DELETE',
  });
};

export const clearInternalMessage = (roomId: number): Promise<{ data: DeleteInternalMessage; code: number }> => {
  return createRequest(`/internal-chats/${roomId}/clear-messages`, {
    method: 'POST',
  });
};

export const listChatFilter = (
  name: string,
  assigned_user_id: number[],
  tag_id: number[],
  chat_ticket_status_id: number[],
  limit?: number,
): Promise<ListChatsResponse> => {
  let assigned = '';
  assigned_user_id.forEach((item) => {
    assigned += `&filters[assigned_user_id]=${item}`;
  });

  let tagId = '';
  tag_id.forEach((item) => {
    tagId += `&filters[tag_id]=${item}`;
  });

  let status = '';
  chat_ticket_status_id.forEach((item) => {
    status += `&filters[chat_ticket_status_id]=${item}`;
  });

  return createRequest(
    `/chat?order_by=timestamp${limit ? `&limit=${limit}` : ''}${name !== '' ? `&filter=${name}` : ''}${
      assigned !== '' ? assigned : ''
    }${tagId !== '' ? tagId : ''}${status !== '' ? status : ''}`,
    {
      method: 'GET',
    },
  );
};

export interface CateFaq {
  id: string;
  subdomain_id: number;
  name: string;
  ordering: number;
}

export interface CateFaqData {
  data: CateFaq[];
}

export const getCateFaq = (filter?: string): Promise<{ data: CateFaqData; code: number }> => {
  return createRequest(`/faq-categories${filter ? `?filter=${filter}` : ''}`, {
    method: 'GET',
  });
};

export interface Faq {
  id: string;
  subdomain_id: number;
  shortcut: string;
  question: string;
  answer: string;
  ordering: number;
  faqCategory: CateFaq;
}

interface FaqData {
  data: Faq;
}

export const createFaq = (
  shortcut: string,
  faq_category_id: string,
  question: string,
): Promise<{ data: FaqData; code: number }> => {
  return createRequest(`/faqs`, {
    method: 'POST',
    data: {
      data: {
        shortcut,
        faq_category_id,
        question,
        answer: 'null',
      },
    },
  });
};

interface FaqArray {
  data: Faq[];
}

export interface PropsQueryFaq {
  shortcut?: string;
  question?: string;
  answer?: string;
  faq_category_id?: string;
}

export const getAllFaq = (query: PropsQueryFaq): Promise<{ data: FaqArray; code: number }> => {
  let str = '';

  Object.keys(query).forEach((key) => {
    if (str !== '') {
      str += '&';
    }
    /* eslint-disable @typescript-eslint/no-explicit-any */
    str += `filters[${key}]=${encodeURIComponent((query as any)[key])}`;
  });

  return createRequest(`/faqs?${str}`, {
    method: 'GET',
  });
};

export const updateFaq = ({
  shortcut,
  faq_category_id,
  question,
  id,
  answer,
  ordering,
}: {
  shortcut?: string;
  faq_category_id?: string;
  question?: string;
  id: string;
  answer?: string;
  ordering?: number;
}): Promise<{ data: Faq; code: number }> => {
  return createRequest(`/faqs/${id}`, {
    method: 'PUT',
    data: {
      data: {
        shortcut,
        faq_category_id,
        question,
        answer,
        ordering,
      },
    },
  });
};

export interface CateFaqOne {
  data: CateFaq;
}

export const deleteFaq = (id: string): Promise<{ data: { result: string }; code: number }> => {
  return createRequest(`/faqs/${id}`, {
    method: 'DELETE',
  });
};

export const deleteCateFaq = (id: string): Promise<{ data: { result: string }; code: number }> => {
  return createRequest(`/faq-categories/${id}`, {
    method: 'DELETE',
  });
};

export const createCateFaq = (name: string): Promise<{ data: CateFaqOne; code: number }> => {
  return createRequest(`/faq-categories`, {
    method: 'POST',
    data: {
      data: {
        name,
      },
    },
  });
};

export const updateCateFaq = (name: string, id: string): Promise<{ data: CateFaqOne; code: number }> => {
  return createRequest(`/faq-categories/${id}`, {
    method: 'PUT',
    data: {
      data: {
        name,
      },
    },
  });
};
