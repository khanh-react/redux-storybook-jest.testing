import { createRequest } from '../../utils/fetchClient';

export interface Pubsub {
  webConnectionString: string;
  enabled: boolean;
  channel: string;
  channels: string[];
  username: string;
  password: string;
  renewOnFail: boolean;
}

// /user/renew-pubsub
export const renewPubsub = (): Promise<{
  code: number;
  data: {
    pubsub: Pubsub;
  };
}> => createRequest('/pubsub/renew-pubsub', {});
