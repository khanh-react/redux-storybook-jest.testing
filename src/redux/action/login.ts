import {
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  FETCH_SUB_DOMAIN_CONFIG,
  FETCH_SUB_DOMAIN_CONFIG_SUCCESS,
  FETCH_SUB_DOMAIN_SETTING,
  FETCH_SUB_DOMAIN_SETTING_SUCCESS,
  FETCH_SUB_DOMAIN_CHANNEL,
  FETCH_SUB_DOMAIN_CHANNEL_SUCCESS,
  FETCH_SUB_DOMAIN_USER,
  FETCH_SUB_DOMAIN_USER_SUCCESS,
} from '../type/login/types';
import { SubdomainConfig, SubdomainSetting, SubdomainChannel, SubdomainUser } from '../services/login';

export function loginRequest(payload: { email: string; password: string }) {
  return {
    type: LOGIN_REQUEST,
    payload,
  };
}

export interface LoginSuccessAction {
  type: typeof LOGIN_SUCCESS;
  payload: { token: string; subdomainId: number };
}

export function loginSuccess(token: string, subdomainId: number) {
  return {
    type: LOGIN_SUCCESS,
    payload: { token, subdomainId },
  };
}
export function loginFailure(error: string) {
  return {
    type: LOGIN_FAILURE,
    payload: error,
  };
}

export function setSubdomainConfig(subdomain_id: number) {
  return {
    type: FETCH_SUB_DOMAIN_CONFIG,
    payload: subdomain_id,
  };
}

export function setSubdomainConfigSuccess(data: SubdomainConfig) {
  return {
    type: FETCH_SUB_DOMAIN_CONFIG_SUCCESS,
    payload: data,
  };
}

export function setSubdomainSetting(subdomain_id: number) {
  return {
    type: FETCH_SUB_DOMAIN_SETTING,
    payload: subdomain_id,
  };
}

export function setSubdomainSettingSuccess(data: SubdomainSetting) {
  return {
    type: FETCH_SUB_DOMAIN_SETTING_SUCCESS,
    payload: data,
  };
}

export function setSubdomainChannel(subdomain_id: number) {
  return {
    type: FETCH_SUB_DOMAIN_CHANNEL,
    payload: subdomain_id,
  };
}

export function setSubdomainChannelSuccess(data: SubdomainChannel) {
  return {
    type: FETCH_SUB_DOMAIN_CHANNEL_SUCCESS,
    payload: data,
  };
}

export function setSubdomainUser(subdomain_id: number) {
  return {
    type: FETCH_SUB_DOMAIN_USER,
    payload: subdomain_id,
  };
}

export function setSubdomainUserSuccess(data: SubdomainUser) {
  return {
    type: FETCH_SUB_DOMAIN_USER_SUCCESS,
    payload: data,
  };
}
