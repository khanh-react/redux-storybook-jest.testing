export const SET_LOADING = 'SET_LOADING';

export interface UtilActions {
  type: typeof SET_LOADING;
  status: boolean;
}

export const setLoading = (status: boolean): UtilActions => {
  return {
    type: 'SET_LOADING',
    status,
  };
};

export const SET_LEFT_NAV_ROUTE = 'SET_LEFT_NAV_ROUTE';

export interface LeftNavRightRoute {
  type: typeof SET_LEFT_NAV_ROUTE;
  status: string;
}

export const setLeftNavRoute = (status: string): LeftNavRightRoute => {
  return {
    type: 'SET_LEFT_NAV_ROUTE',
    status,
  };
};

export type UtilActionTypes = UtilActions | LeftNavRightRoute;
