import {
  ADD_CONTACT,
  BULK_DELETE_REQUEST,
  DELETE_SINGLE_CONTACT_REQUEST,
  EDIT_CONTACT,
  GET_CONTACT_DETAIL,
  UPDATE_CONTACT_DATA_QUERY,
  UPDATE_CONTACT_DETAIL,
  UPDATE_CONTACT_QUERY,
} from '../type/contact/actions';
import { Contact, ContactQuery, FieldHeader } from '../type/contact/state';

export const updateContactQuery = (query: Partial<ContactQuery>) => ({ type: UPDATE_CONTACT_QUERY, payload: query });
export const bulkDelete = (payload: number[]) => ({ type: BULK_DELETE_REQUEST, payload });
export const addContact = (payload: Record<string, unknown>) => ({ type: ADD_CONTACT, payload });
export const editContact = (payload: Record<string, unknown>[]) => ({ type: EDIT_CONTACT, payload });
export const deleteSingleContact = (payload: string) => ({ type: DELETE_SINGLE_CONTACT_REQUEST, payload });
export const getContactDetail = (payload: string) => ({ type: GET_CONTACT_DETAIL, payload });
export const updateContactDataQuery = (payload: Partial<Contact>) => ({ type: UPDATE_CONTACT_DATA_QUERY, payload });
export const updateContactDetail = () => ({ type: UPDATE_CONTACT_DETAIL });
export const updateFilters = (filters: Partial<FieldHeader>[]) => ({
  type: 'UPDATE_FILTERS',
  filters,
});
