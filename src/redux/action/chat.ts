import {
  AssignResponse,
  Chat,
  CreatedTicket,
  ListChatsQuery,
  ListChatsResponse,
  ListMessagesResponse,
  Message,
  MessageInternal,
  MessageType,
  NewChat,
  CateFaq,
  Faq,
  PropsQueryFaq,
  Tag,
} from '../services/chat';

// import { Pubsub } from './pubsub';

export const CHATS_FETCH_SUCCESS = 'CHATS_FETCH_SUCCESS';
interface ChatsFetchSuccessAction {
  type: typeof CHATS_FETCH_SUCCESS;
  isFetchMore?: boolean;
  payload: ListChatsResponse;
}
export const chatFetchSuccess = (payload: ListChatsResponse, isFetchMore = false): ChatsFetchSuccessAction => ({
  type: 'CHATS_FETCH_SUCCESS',
  isFetchMore,
  payload,
});

export const CHATS_FETCH_FAILED = 'CHATS_FETCH_FAILED';
interface ChatsFetchFailedAction {
  type: typeof CHATS_FETCH_FAILED;
}

export const chatFetchFailed = (): ChatsFetchFailedAction => ({
  type: 'CHATS_FETCH_FAILED',
});

export const UPDATE_CHATS_QUERY = 'UPDATE_CHATS_QUERY';
export interface UpdateChatsQueryAction {
  type: typeof UPDATE_CHATS_QUERY;
  payload: Partial<ListChatsQuery>;
}
export const updateChatsQuery = (query: ListChatsQuery): UpdateChatsQueryAction => ({
  type: UPDATE_CHATS_QUERY,
  payload: query,
});

export const SELECT_CHAT = 'SELECT_CHAT';
export interface SelectChatAction {
  type: typeof SELECT_CHAT;
  payload: Chat['id'];
}
export const selectChat = (chatId: Chat['id']): SelectChatAction => ({
  type: SELECT_CHAT,
  payload: chatId,
});

export const MESSAGES_FETCH_SUCCESS = 'MESSAGES_FETCH_SUCCESS';
interface MessagesFetchSuccessAction {
  type: typeof MESSAGES_FETCH_SUCCESS;
  chatId: Chat['id'];
  mode: 'fresh' | 'new' | 'old';
  payload: ListMessagesResponse;
}
export const messagesFetchSuccess = (
  payload: ListMessagesResponse,
  chatId: Chat['id'],
  mode: MessagesFetchSuccessAction['mode'] = 'fresh',
): MessagesFetchSuccessAction => ({
  type: 'MESSAGES_FETCH_SUCCESS',
  chatId,
  payload,
  mode,
});

export const UPDATE_CHAT_INPUT = 'UPDATE_CHAT_INPUT';
interface UpdateChatInputAction {
  type: typeof UPDATE_CHAT_INPUT;
  chatId: Chat['id'];
  text: string;
}
export const updateChatInput = (chatId: Chat['id'], text: string): UpdateChatInputAction => ({
  type: 'UPDATE_CHAT_INPUT',
  chatId,
  text,
});

export const UPDATE_EMOJI_INPUT = 'UPDATE_EMOJI_INPUT';
interface UpdateEmojiInputAction {
  type: typeof UPDATE_EMOJI_INPUT;
  chatId: Chat['id'];
  text: string;
}
export const updateEmojiInput = (chatId: Chat['id'], text: string): UpdateEmojiInputAction => ({
  type: 'UPDATE_EMOJI_INPUT',
  chatId,
  text,
});

export const SEND_MESSAGE = 'SEND_MESSAGE';
export interface SendMessageAction {
  type: typeof SEND_MESSAGE;
  message: { type: MessageType };
}

export const sendMessage = (message: SendMessageAction['message']): SendMessageAction => ({
  type: 'SEND_MESSAGE',
  message,
});

export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS';
export interface SendMessageSuccessAction {
  type: typeof SEND_MESSAGE_SUCCESS;
  message: Message;
}

export const sendMessageSuccess = (message: Message): SendMessageSuccessAction => ({
  type: 'SEND_MESSAGE_SUCCESS',
  message,
});

export const CHAT_UPDATED = 'CHAT_UPDATED';
export interface ChatUpdatedAction {
  type: typeof CHAT_UPDATED;
  chat: Partial<Chat>;
}

export const chatUpdated = (chat: Partial<Chat>): ChatUpdatedAction => ({
  type: 'CHAT_UPDATED',
  chat,
});

export const MESSAGE_UPDATED = 'MESSAGE_UPDATED';
export interface MessageUpdatedAction {
  type: typeof MESSAGE_UPDATED;
  message: Partial<Message>;
}

export const messageUpdated = (message: Partial<Message>): MessageUpdatedAction => ({
  type: 'MESSAGE_UPDATED',
  message,
});

export const NEW_MESSAGE = 'NEW_MESSAGE';
export interface NewMessageAction {
  type: typeof NEW_MESSAGE;
  message: Message;
}

export const newMessage = (message: Message): NewMessageAction => ({
  type: 'NEW_MESSAGE',
  message,
});

export const CREATE_NEW_CHAT = 'CREATE_NEW_CHAT';

export interface CreateNewChatAction {
  type: typeof CREATE_NEW_CHAT;
  newChat: NewChat;
}

export const createNewChat = (newChat: NewChat): CreateNewChatAction => ({
  type: 'CREATE_NEW_CHAT',
  newChat,
});

export const UPDATE_CASE_LEFT_NAV = 'UPDATE_CASE_LEFT_NAV';

export interface UpdateCaseLeftNav {
  type: typeof UPDATE_CASE_LEFT_NAV;
  onCase: string;
}

export const updateCaseLeftNav = (onCase: string): UpdateCaseLeftNav => ({
  type: 'UPDATE_CASE_LEFT_NAV',
  onCase,
});
export const UPDATE_CASE_RIGHT_NAV = 'UPDATE_CASE_RIGHT_NAV';

export interface UpdateCaseRightNav {
  type: typeof UPDATE_CASE_RIGHT_NAV;
  onCase: string;
}

export const updateCaseRightNav = (onCase: string): UpdateCaseRightNav => ({
  type: 'UPDATE_CASE_RIGHT_NAV',
  onCase,
});
export const DELETE_CHAT = 'DELETE_CHAT';

export interface DeleteChatAction {
  type: typeof DELETE_CHAT;
  chat_id?: string;
}

export const deleteChat = (chat_id?: string): DeleteChatAction => ({
  type: 'DELETE_CHAT',
  chat_id,
});

export const CLEAR_CHAT = 'CLEAR_CHAT';

export interface ClearChatAction {
  type: typeof CLEAR_CHAT;
  chat_id?: string;
}

export const clearChat = (chat_id?: string): ClearChatAction => ({
  type: 'CLEAR_CHAT',
  chat_id,
});

export const CLEAR_CHAT_SUCCESS = 'CLEAR_CHAT_SUCCESS';

export interface ClearChatSuccessAction {
  type: typeof CLEAR_CHAT_SUCCESS;
  chat_id: string;
}

export const clearChatSuccess = (chat_id: string): ClearChatSuccessAction => ({
  type: 'CLEAR_CHAT_SUCCESS',
  chat_id,
});

export const GET_STARRED_MESSAGES_SUCCESS = 'GET_STARRED_MESSAGES_SUCCESS';

export interface getStarredMessagesSuccessAction {
  type: typeof GET_STARRED_MESSAGES_SUCCESS;
  data: Message[];
}

export const getStarredMessagesSuccess = (data: Message[]): getStarredMessagesSuccessAction => ({
  type: 'GET_STARRED_MESSAGES_SUCCESS',
  data,
});

export const UPDATE_STARRED_MESSAGE = 'UPDATE_STARRED_MESSAGE';

export interface updateStarredMessagesAction {
  type: typeof UPDATE_STARRED_MESSAGE;
}

export const updateStarredMessages = (): updateStarredMessagesAction => ({
  type: 'UPDATE_STARRED_MESSAGE',
});

export const GET_STARRED_MESSAGE = 'GET_STARRED_MESSAGE';

export interface getStarredMessagesAction {
  type: typeof GET_STARRED_MESSAGE;
}

export const getStarredMessagesAct = (): getStarredMessagesAction => ({
  type: 'GET_STARRED_MESSAGE',
});

export const DELETE_MESSAGE = 'DELETE_MESSAGE';

export interface DeleteMessageAction {
  type: typeof DELETE_MESSAGE;
  message_id: string;
}

export const deleteMessageAction = (message_id: string): DeleteMessageAction => ({
  type: 'DELETE_MESSAGE',
  message_id,
});

export const ON_REPLY_MESSAGE = 'ON_REPLY_MESSAGE';

export interface ReplyMessageAction {
  type: typeof ON_REPLY_MESSAGE;
  message: {
    user: string;
    text?: string;
    message_id: string;
    messageType: MessageType;
    imageUrl?: string;
    fileName?: string;
  };
}

export const replyMessage = (message: {
  user: string;
  text?: string;
  message_id: string;
  messageType: MessageType;
  imageUrl?: string;
  fileName?: string;
}): ReplyMessageAction => ({
  type: 'ON_REPLY_MESSAGE',
  message,
});

export const RESET_REPLY_MESSAGE = 'RESET_REPLY_MESSAGE';

export interface ResetMessageAction {
  type: typeof RESET_REPLY_MESSAGE;
}

export const ResetMessageAction = (): ResetMessageAction => ({
  type: 'RESET_REPLY_MESSAGE',
});

export const MARK_READ_CHAT = 'MARK_READ_CHAT';

export interface MarkReadChat {
  type: typeof MARK_READ_CHAT;
  chat_id: string;
}

export const markReadChat = (chat_id: string): MarkReadChat => ({
  type: 'MARK_READ_CHAT',
  chat_id,
});

export const MARK_UN_READ_CHAT = 'MARK_UN_READ_CHAT';

export interface MarkUnReadChat {
  type: typeof MARK_UN_READ_CHAT;
}

export const markUnReadChat = (): MarkUnReadChat => ({
  type: 'MARK_UN_READ_CHAT',
});

export const MARK_READ_CHAT_SUCCESS = 'MARK_READ_CHAT_SUCCESS';

export interface MarkReadChatSuccess {
  type: typeof MARK_READ_CHAT_SUCCESS;
  chat_id: string;
}

export const markReadChatSuccess = (chat_id: string): MarkReadChatSuccess => ({
  type: 'MARK_READ_CHAT_SUCCESS',
  chat_id,
});

export const RENEW_PUBSUB = 'RENEW_PUBSUB';

export interface RenewPubsub {
  type: typeof RENEW_PUBSUB;
}

export const renewPubsub = () => {
  return {
    type: RENEW_PUBSUB,
  };
};

export const ASSIGN_TAG = 'ASSIGN_TAG';

export interface AssignTagTicked {
  type: typeof ASSIGN_TAG;
  payload: {
    chatId: string;
    listTags: { id: string; name: string }[];
  };
}

export const assignTagTicket = (chatId: string, listTags: { id: string; name: string }[]): AssignTagTicked => {
  return {
    type: ASSIGN_TAG,
    payload: {
      chatId,
      listTags,
    },
  };
};

export const ASSIGN_TAG_SUCCESS = 'ASSIGN_TAG_SUCCESS';

export interface RemoveTagTicketSuccess {
  type: typeof ASSIGN_TAG_SUCCESS;
  payload: {
    chatId: string;
    listTags: { id: string; name: string }[];
  };
}

export const assignTagTicketSuccess = (
  chatId: string,
  listTags: { id: string; name: string }[],
): RemoveTagTicketSuccess => {
  return {
    type: ASSIGN_TAG_SUCCESS,
    payload: {
      chatId,
      listTags,
    },
  };
};

export const OPEN_CHAT_TICKET = 'OPEN_CHAT_TICKET';

export interface OpenChatTicket {
  type: typeof OPEN_CHAT_TICKET;
  chatId: string;
}

export const openChatTicket = (chatId: string) => {
  return {
    type: OPEN_CHAT_TICKET,
    chatId,
  };
};
export const OPEN_CHAT_TICKET_FAILED = 'OPEN_CHAT_TICKET_FAILED';
export const OPEN_CHAT_TICKET_SUCCESS = 'OPEN_CHAT_TICKET_SUCCESS';
export interface OpenChatTicketSuccessAction {
  type: typeof OPEN_CHAT_TICKET_SUCCESS;
  payload: CreatedTicket;
}

export const SEND_DOCUMENT_TEMP_MESSAGE = 'SEND_DOCUMENT_TEMP_MESSAGE';

export interface SendDocumentTempMessage {
  type: typeof SEND_DOCUMENT_TEMP_MESSAGE;
  payload: { currentId: string; file: File; name: string; type: string; caption: string };
}

export const sendDocumentTempMessage = (currentId: string, file: File, name: string, type: string, caption: string) => {
  return {
    type: SEND_DOCUMENT_TEMP_MESSAGE,
    payload: { currentId, file, name, type, caption },
  };
};

export const SEND_DOCUMENT_TEMP_MESSAGE_SUCCESS = 'SEND_DOCUMENT_TEMP_MESSAGE_SUCCESS';

export interface SendDocumentTempMessageSuccess {
  type: typeof SEND_DOCUMENT_TEMP_MESSAGE_SUCCESS;
  payload: {
    currentId: string;
    fileName: string;
    name: string;
    id: string;
    type: string;
    size: number;
    file: File;
    reply: string;
  };
}

export const sendDocumentTempMessageSuccess = (
  currentId: string,
  fileName: string,
  name: string,
  id: string,
  type: string,
  size: number,
  file: File,
  reply: string,
) => {
  return {
    type: SEND_DOCUMENT_TEMP_MESSAGE_SUCCESS,
    payload: { currentId, fileName, name, id, type, size, file, reply },
  };
};

export const SET_TIME_LOADING_FILE = 'SET_TIME_LOADING_FILE';

export interface SetTimeLoadingFile {
  type: typeof SET_TIME_LOADING_FILE;
  payload: { messageId: string; time: number };
}

export const setTimeLoadingFile = (messageId: string, time: number) => {
  return {
    type: SET_TIME_LOADING_FILE,
    payload: { messageId, time },
  };
};

export const EDIT_TAG_SUCCESS = 'EDIT_TAG_SUCCESS';

export interface EditTagSuccess {
  type: typeof EDIT_TAG_SUCCESS;
  payload: {
    chatId: string;
    tag: { id: string; name: string };
  };
}

export const editTagSuccess = (chatId: string, tag: { id: string; name: string }): EditTagSuccess => {
  return {
    type: EDIT_TAG_SUCCESS,
    payload: {
      chatId,
      tag,
    },
  };
};

export const PROGRESS_UPLOAD = 'PROGRESS_UPLOAD';

export interface ProgressUpload {
  type: typeof PROGRESS_UPLOAD;
  payload: {
    chatId: string;
    percent: number;
  };
}

export const progressUpload = (chatId: string, percent: number): ProgressUpload => {
  return {
    type: PROGRESS_UPLOAD,
    payload: {
      chatId,
      percent,
    },
  };
};

export const MARK_UNREAD = 'MARK_UNREAD';

export interface MessageUnread {
  type: typeof MARK_UNREAD;
  id: string;
  read: number;
  unread_count: number;
}

export const messageUnread = (id: string, read: number, unread_count: number): MessageUnread => {
  return {
    type: MARK_UNREAD,
    id,
    read,
    unread_count,
  };
};

export const LOAD_MORE = 'LOAD_MORE';
export interface LoadMore {
  type: typeof LOAD_MORE;
  status: string;
  position: number;
}

export const loadMore = (status: string, position: number): LoadMore => {
  return {
    type: LOAD_MORE,
    status,
    position,
  };
};

export const SET_FLAG_LOAD_MESSAGE = 'SET_FLAG_LOAD_MESSAGE';
export interface SetFlagLoadMore {
  type: typeof SET_FLAG_LOAD_MESSAGE;
  roomId: string;
  status: boolean;
}

export const setFlagLoadMore = (roomId: string, status: boolean): SetFlagLoadMore => {
  return {
    type: SET_FLAG_LOAD_MESSAGE,
    status,
    roomId,
  };
};

export const ASSIGN_USER = 'ASSIGN_USER';

export interface AssignUserAction {
  type: typeof ASSIGN_USER;
  payload: {
    userId: number;
    chatTicketId: number;
  };
}
export const assignUser = (userId: number, chatTicketId: number) => ({
  type: ASSIGN_USER,
  payload: {
    userId,
    chatTicketId,
  },
});
export const ASSIGN_USER_FAILED = 'ASSIGN_USER_FAILED';
export const ASSIGN_USER_SUCCESS = 'ASSIGN_USER_SUCCESS';
export interface AssignUserSuccessAction {
  type: typeof ASSIGN_USER_SUCCESS;
  payload: AssignResponse['data'];
}

export const UNASSIGN_USER = 'UNASSIGN_USER';
export interface UnAssignUserAction {
  type: typeof UNASSIGN_USER;
  payload: {
    chatTicketId: number;
  };
}
export const unAssignUser = (chatTicketId: number) => ({
  type: UNASSIGN_USER,
  payload: {
    chatTicketId,
  },
});
export const UNASSIGN_USER_FAILED = 'UNASSIGN_USER_FAILED';
export const UNASSIGN_USER_SUCCESS = 'UNASSIGN_USER_SUCCESS';
export interface UnAssignUserSuccessAction {
  type: typeof UNASSIGN_USER_SUCCESS;
  payload: AssignResponse['data'];
}
export const MESSAGE_SEARCH = 'MESSAGE_SEARCH';
interface MessageSearch {
  type: typeof MESSAGE_SEARCH;
  payload: ListMessagesResponse;
  chatId: Chat['id'];
}
export const searchMessage = (payload: ListMessagesResponse, chatId: Chat['id']): MessageSearch => ({
  type: 'MESSAGE_SEARCH',
  payload,
  chatId,
});

export const SELECT_ROOM_MESSAGE_SEARCH = 'SELECT_ROOM_MESSAGE_SEARCH';
interface SelectRoomMessageSearch {
  type: typeof SELECT_ROOM_MESSAGE_SEARCH;
  chatId: Chat['id'];
}
export const SelectRoomSearchMessage = (chatId: Chat['id']): SelectRoomMessageSearch => ({
  type: 'SELECT_ROOM_MESSAGE_SEARCH',
  chatId,
});

export const RESEND_MESSAGE = 'RESEND_MESSAGE';
export interface ResendMessage {
  type: typeof RESEND_MESSAGE;
  id: string;
}
export const resendMessageAct = (id: string): ResendMessage => ({
  type: 'RESEND_MESSAGE',
  id,
});

export const RESEND_MESSAGE_SUCCESS = 'RESEND_MESSAGE_SUCCESS';
interface ResendMessageSuccess {
  type: typeof RESEND_MESSAGE_SUCCESS;
  id: string;
}
export const resendMessageSuccessAct = (id: string): ResendMessageSuccess => ({
  type: 'RESEND_MESSAGE_SUCCESS',
  id,
});

export const SELECT_INTERNAL_ROOM = 'SELECT_INTERNAL_ROOM';
interface SelectInternalRoom {
  type: typeof SELECT_INTERNAL_ROOM;
  id: number;
}
export const selectInternalRoomAct = (id: number): SelectInternalRoom => ({
  type: 'SELECT_INTERNAL_ROOM',
  id,
});

export const FETCH_INTERNAL_MESSAGE = 'FETCH_INTERNAL_MESSAGE';
interface FetchInternalMessage {
  type: typeof FETCH_INTERNAL_MESSAGE;
}
export const fetchInternalMessageAct = (): FetchInternalMessage => ({
  type: 'FETCH_INTERNAL_MESSAGE',
});

export const FETCH_MESSAGE_INTERNAL_SUCCESS = 'FETCH_MESSAGE_INTERNAL_SUCCESS';
interface FetchMessageInternalSuccess {
  type: typeof FETCH_MESSAGE_INTERNAL_SUCCESS;
  messageList: MessageInternal[];
  loadMoreFlag: boolean;
}
export const fetchMessageInternalSuccessAct = (
  messageList: MessageInternal[],
  loadMoreFlag = false,
): FetchMessageInternalSuccess => ({
  type: 'FETCH_MESSAGE_INTERNAL_SUCCESS',
  messageList,
  loadMoreFlag,
});

export const NEW_MESSAGE_INTERNAL = 'NEW_MESSAGE_INTERNAL';
interface PutNewMessageInternal {
  type: typeof NEW_MESSAGE_INTERNAL;
  message: MessageInternal;
}
export const putNewMessageInternalAct = (message: MessageInternal): PutNewMessageInternal => ({
  type: 'NEW_MESSAGE_INTERNAL',
  message,
});

export const DELETE_INTERNAL_MESSAGE = 'DELETE_INTERNAL_MESSAGE';
interface DeleteMessageInternalAct {
  type: typeof DELETE_INTERNAL_MESSAGE;
  messageId: string;
}

export const deleteMessageInternalAct = (messageId: string): DeleteMessageInternalAct => ({
  type: DELETE_INTERNAL_MESSAGE,
  messageId,
});

export const CLEAR_ALL_MESSAGE_INTERNAL = 'CLEAR_ALL_MESSAGE_INTERNAL';
interface ClearMessageInternal {
  type: typeof CLEAR_ALL_MESSAGE_INTERNAL;
  id: number;
}

export const clearMessageInternalAct = (id: number): ClearMessageInternal => ({
  type: CLEAR_ALL_MESSAGE_INTERNAL,
  id,
});

export const LOADING_SEND_FILE_INTERNAL = 'LOADING_SEND_FILE_INTERNAL';
interface LoadingSendFileInternal {
  type: typeof LOADING_SEND_FILE_INTERNAL;
  status: boolean;
}

export const loadingSendFileInternalAct = (status: boolean): LoadingSendFileInternal => ({
  type: LOADING_SEND_FILE_INTERNAL,
  status,
});

export const RESEND_MESSAGE_DISCONNECT = 'RESEND_MESSAGE_DISCONNECT';
export interface ResendMessageDisconnect {
  type: typeof RESEND_MESSAGE_DISCONNECT;
  id: string;
}

export const resendMessageDisconnectAct = (id: string): ResendMessageDisconnect => ({
  type: RESEND_MESSAGE_DISCONNECT,
  id,
});

export const DELETE_MESSAGE_ERROR = 'DELETE_MESSAGE_ERROR';
export interface DeleteMessageError {
  type: typeof DELETE_MESSAGE_ERROR;
  id: string;
  chatCurrent: string;
}

export const deleteMessageErrorAct = (id: string, chatCurrent: string): DeleteMessageError => ({
  type: DELETE_MESSAGE_ERROR,
  id,
  chatCurrent,
});

export const LOADING_MESSAGE_INTERNAL = 'LOADING_MESSAGE_INTERNAL';
export interface LoadingMessageInternal {
  type: typeof LOADING_MESSAGE_INTERNAL;
  status: boolean;
}

export const loadingMessageInternalAct = (status: boolean): LoadingMessageInternal => ({
  type: LOADING_MESSAGE_INTERNAL,
  status,
});
export const CLOSE_CHAT_TICKET = 'CLOSE_CHAT_TICKET';
export interface CloseChatTicketAction {
  type: typeof CLOSE_CHAT_TICKET;
  payload: {
    chatTicketId: number;
  };
}
export const closeChatTicket = (chatTicketId: number) => ({
  type: CLOSE_CHAT_TICKET,
  payload: {
    chatTicketId,
  },
});
export const CLOSE_CHAT_TICKET_FAILED = 'CLOSE_CHAT_TICKET_FAILED';
export const CLOSE_CHAT_TICKET_SUCCESS = 'CLOSE_CHAT_TICKET_SUCCESS';
export interface CloseChatTicketSuccessAction {
  type: typeof CLOSE_CHAT_TICKET_SUCCESS;
  payload: AssignResponse['data'];
}

export const EXPORT_PARTICIPANTS = 'EXPORT_PARTICIPANTS';

export interface ExportParticipants {
  type: typeof EXPORT_PARTICIPANTS;
  chatId: string;
}

export const exportParticipants = (chatId: string) => {
  return {
    type: EXPORT_PARTICIPANTS,
    chatId,
  };
};
export const EXPORT_PARTICIPANTS_FAILED = 'EXPORT_PARTICIPANTS_FAILED';
export const EXPORT_PARTICIPANTS_SUCCESS = 'EXPORT_PARTICIPANTS_SUCCESS';

export const REFRESH_PARTICIPANTS = 'REFRESH_PARTICIPANTS';

export interface RefreshParticipants {
  type: typeof REFRESH_PARTICIPANTS;
  chatId: string;
}

export const refreshParticipants = (chatId: string) => {
  return {
    type: REFRESH_PARTICIPANTS,
    chatId,
  };
};
export const REFRESH_PARTICIPANTS_FAILED = 'REFRESH_PARTICIPANTS_FAILED';
export const REFRESH_PARTICIPANTS_SUCCESS = 'REFRESH_PARTICIPANTS_SUCCESS';

export const ADD_PARTICIPANTS = 'ADD_PARTICIPANTS';

export interface AddParticipantsAction {
  type: typeof ADD_PARTICIPANTS;
  payload: {
    phone_list: string[];
    chatId: number;
  };
}
export const addParticipants = (chatId: number, phone_list: string[]) => ({
  type: ADD_PARTICIPANTS,
  payload: {
    phone_list,
    chatId,
  },
});
export const ADD_PARTICIPANTS_FAILED = 'ADD_PARTICIPANTS_FAILED';
export const ADD_PARTICIPANTS_SUCCESS = 'ADD_PARTICIPANTS_SUCCESS';

export const REMOVE_PARTICIPANTS = 'REMOVE_PARTICIPANTS';

export interface RemoveParticipantsAction {
  type: typeof REMOVE_PARTICIPANTS;
  payload: {
    phone_list: string[];
    chatId: number;
  };
}
export const removeParticipants = (chatId: number, phone_list: string[]) => ({
  type: REMOVE_PARTICIPANTS,
  payload: {
    phone_list,
    chatId,
  },
});
export const REMOVE_PARTICIPANTS_FAILED = 'REMOVE_PARTICIPANTS_FAILED';
export const REMOVE_PARTICIPANTS_SUCCESS = 'REMOVE_PARTICIPANTS_SUCCESS';

export const CHATS_FILTER_SUCCESS = 'CHATS_FILTER_SUCCESS';
interface ChatsFilterSuccessAction {
  type: typeof CHATS_FILTER_SUCCESS;
  payload: ListChatsResponse;
}
export const chatFilterSuccess = (payload: ListChatsResponse): ChatsFilterSuccessAction => ({
  type: 'CHATS_FILTER_SUCCESS',
  payload,
});

export const SEND_TEXT_TEMP_MESSAGE = 'SEND_TEXT_TEMP_MESSAGE';

export interface SendTextTempMessage {
  type: typeof SEND_TEXT_TEMP_MESSAGE;
  payload: { currentId: string; name: string; text: string; pass: string; reply: string };
}

export const sendTextTempMessage = (currentId: string, name: string, text: string, pass: string, reply: string) => {
  return {
    type: SEND_TEXT_TEMP_MESSAGE,
    payload: { currentId, name, text, pass, reply },
  };
};

export const PUSH_NEW_ROOM_CREATE = 'PUSH_NEW_ROOM_CREATE';

export interface PushNewRoomCreate {
  type: typeof PUSH_NEW_ROOM_CREATE;
  payload: { room: Chat };
}

export const pushNewRoomCreate = (room: Chat): PushNewRoomCreate => {
  return {
    type: PUSH_NEW_ROOM_CREATE,
    payload: { room },
  };
};

export const DELETE_ROOM_SUCCESS = 'DELETE_ROOM_SUCCESS';

export interface DeleteRoomSuccess {
  type: typeof DELETE_ROOM_SUCCESS;
  payload: { room: string };
}

export const deleteRoomSuccess = (room: string): DeleteRoomSuccess => {
  return {
    type: DELETE_ROOM_SUCCESS,
    payload: { room },
  };
};

export const LOAD_MORE_ROOM_SCROLL = 'LOAD_MORE_ROOM_SCROLL';
export interface LoadMoreRoomScroll {
  type: typeof LOAD_MORE_ROOM_SCROLL;
  position: number;
}

export const loadMoreRoomScroll = (position: number): LoadMoreRoomScroll => {
  return {
    type: LOAD_MORE_ROOM_SCROLL,
    position,
  };
};

export const GET_CATE_FAQ = 'GET_CATE_FAQ';
export interface GetCateFaq {
  type: typeof GET_CATE_FAQ;
  filter: string | undefined;
}

export const getCateFaq = (filter?: string): GetCateFaq => {
  return {
    type: GET_CATE_FAQ,
    filter,
  };
};

export const GET_CATE_FAQ_SUCCESS = 'GET_CATE_FAQ_SUCCESS';
export interface GetCateFaqSuccess {
  type: typeof GET_CATE_FAQ_SUCCESS;
  cate: CateFaq[];
}

export const getCateFaqSuccess = (cate: CateFaq[]): GetCateFaqSuccess => {
  return {
    type: GET_CATE_FAQ_SUCCESS,
    cate,
  };
};

export const GET_FAQ = 'GET_FAQ';
export interface GetFaq {
  type: typeof GET_FAQ;
  filterValue: PropsQueryFaq;
}

export const getFaq = (filterValue: PropsQueryFaq): GetFaq => {
  return {
    type: GET_FAQ,
    filterValue,
  };
};

export const GET_FAQ_SUCCESS = 'GET_FAQ_SUCCESS';
export interface GetFaqSuccess {
  type: typeof GET_FAQ_SUCCESS;
  faq: Faq[];
}

export const getFaqSuccess = (faq: Faq[]): GetFaqSuccess => {
  return {
    type: GET_FAQ_SUCCESS,
    faq,
  };
};

export const CREATE_FAQ = 'CREATE_FAQ';
export interface CreateFaq {
  type: typeof CREATE_FAQ;
  payload: {
    shortcut: string;
    category: string;
    message: string;
  };
}

export const createFaq = (shortcut: string, category: string, message: string): CreateFaq => {
  return {
    type: CREATE_FAQ,
    payload: {
      shortcut,
      category,
      message,
    },
  };
};

export const CREATE_FAQ_SUCCESS = 'CREATE_FAQ_SUCCESS';
export interface CreateFaqSuccess {
  type: typeof CREATE_FAQ_SUCCESS;
  faq: Faq;
}

export const createFaqSuccess = (faq: Faq): CreateFaqSuccess => {
  return {
    type: CREATE_FAQ_SUCCESS,
    faq,
  };
};

export const DELETE_FAQ = 'DELETE_FAQ';
export interface DeleteFaq {
  type: typeof DELETE_FAQ;
  id: string;
}

export const deleteFaq = (id: string): DeleteFaq => {
  return {
    type: DELETE_FAQ,
    id,
  };
};

export const DELETE_FAQ_SUCCESS = 'DELETE_FAQ_SUCCESS';
export interface DeleteFaqSuccess {
  type: typeof DELETE_FAQ_SUCCESS;
  id: string;
}

export const deleteFaqSuccess = (id: string): DeleteFaqSuccess => {
  return {
    type: DELETE_FAQ_SUCCESS,
    id,
  };
};

export const CREATE_CATE_FAQ = 'CREATE_CATE_FAQ';
export interface CreateCateFaq {
  type: typeof CREATE_CATE_FAQ;
  name: string;
}

export const createCateFaq = (name: string): CreateCateFaq => {
  return {
    type: CREATE_CATE_FAQ,
    name,
  };
};

export const CREATE_CATE_FAQ_SUCCESS = 'CREATE_CATE_FAQ_SUCCESS';
export interface CreateCateFaqSuccess {
  type: typeof CREATE_CATE_FAQ_SUCCESS;
  cateFaq: CateFaq;
}

export const createCateFaqSuccess = (cateFaq: CateFaq): CreateCateFaqSuccess => {
  return {
    type: CREATE_CATE_FAQ_SUCCESS,
    cateFaq,
  };
};

export const DELETE_CATE_FAQ = 'DELETE_CATE_FAQ';
export interface DeleteCateFaq {
  type: typeof DELETE_CATE_FAQ;
  id: string;
}

export const deleteCateFaq = (id: string): DeleteCateFaq => {
  return {
    type: DELETE_CATE_FAQ,
    id,
  };
};

export const DELETE_CATE_FAQ_SUCCESS = 'DELETE_CATE_FAQ_SUCCESS';
export interface DeleteCateFaqSuccess {
  type: typeof DELETE_CATE_FAQ_SUCCESS;
  id: string;
}

export const deleteCateFaqSuccess = (id: string): DeleteCateFaqSuccess => {
  return {
    type: DELETE_CATE_FAQ_SUCCESS,
    id,
  };
};

export const UPDATE_FAQ = 'UPDATE_FAQ';
export interface UpdateFaq {
  type: typeof UPDATE_FAQ;
  payload: {
    shortcut?: string;
    faq_category_id?: string;
    question?: string;
    id: string;
    answer?: string;
    ordering?: number;
  };
}

export const updateFaq = ({
  shortcut,
  faq_category_id,
  question,
  id,
  answer,
  ordering,
}: {
  shortcut?: string;
  faq_category_id?: string;
  question?: string;
  id: string;
  answer?: string;
  ordering?: number;
}): UpdateFaq => {
  return {
    type: UPDATE_FAQ,
    payload: {
      shortcut,
      faq_category_id,
      question,
      id,
      answer,
      ordering,
    },
  };
};

export const UPDATE_FAQ_SUCCESS = 'UPDATE_FAQ_SUCCESS';
export interface UpdateFaqSuccess {
  type: typeof UPDATE_FAQ_SUCCESS;
  faq: Faq;
}

export const updateFaqSuccess = (faq: Faq): UpdateFaqSuccess => {
  return {
    type: UPDATE_FAQ_SUCCESS,
    faq,
  };
};

export const UPDATE_CATE_FAQ = 'UPDATE_CATE_FAQ';
export interface UpdateCateFaq {
  type: typeof UPDATE_CATE_FAQ;
  payload: {
    name: string;
    id: string;
  };
}

export const updateCateFaq = ({ name, id }: { name: string; id: string }): UpdateCateFaq => {
  return {
    type: UPDATE_CATE_FAQ,
    payload: {
      name,
      id,
    },
  };
};

export const UPDATE_CATE_FAQ_SUCCESS = 'UPDATE_CATE_FAQ_SUCCESS';
export interface UpdateCateFaqSuccess {
  type: typeof UPDATE_CATE_FAQ_SUCCESS;
  cateFaq: CateFaq;
}

export const updateCateFaqSuccess = (cateFaq: CateFaq): UpdateCateFaqSuccess => {
  return {
    type: UPDATE_CATE_FAQ_SUCCESS,
    cateFaq,
  };
};

export const LOAD_MORE_TEAM_CHAT = 'LOAD_MORE_TEAM_CHAT';
export interface LoadMoreRoomScrollTeamChat {
  type: typeof LOAD_MORE_TEAM_CHAT;
  position: number;
}

export const loadMoreRoomScrollTeamChat = (position: number): LoadMoreRoomScrollTeamChat => {
  return {
    type: LOAD_MORE_TEAM_CHAT,
    position,
  };
};

export const GET_TAG = 'GET_TAG';
export interface GetTag {
  type: typeof GET_TAG;
}

export const getTag = (): GetTag => {
  return {
    type: GET_TAG,
  };
};

export const GET_TAG_SUCCESS = 'GET_TAG_SUCCESS';
export interface GetTagSuccess {
  type: typeof GET_TAG_SUCCESS;
  tag: Tag[];
}

export const getTagSuccess = (tag: Tag[]): GetTagSuccess => {
  return {
    type: GET_TAG_SUCCESS,
    tag,
  };
};

export const DELETE_TAG = 'DELETE_TAG';
export interface DeleteTag {
  type: typeof DELETE_TAG;
  id: string;
}

export const deleteTag = (id: string): DeleteTag => {
  return {
    type: DELETE_TAG,
    id,
  };
};

export const DELETE_TAG_SUCCESS = 'DELETE_TAG_SUCCESS';
export interface DeleteTagSuccess {
  type: typeof DELETE_TAG_SUCCESS;
  id: string;
}

export const deleteTagSuccess = (id: string): DeleteTagSuccess => {
  return {
    type: DELETE_TAG_SUCCESS,
    id,
  };
};

export const CREATE_TAG = 'CREATE_TAG';
export interface CreateTag {
  type: typeof CREATE_TAG;
  name: string;
}

export const createTag = (name: string): CreateTag => {
  return {
    type: CREATE_TAG,
    name,
  };
};

export const CREATE_TAG_SUCCESS = 'CREATE_TAG_SUCCESS';
export interface CreateTagSuccess {
  type: typeof CREATE_TAG_SUCCESS;
  tag: Tag;
}

export const createTagSuccess = (tag: Tag): CreateTagSuccess => {
  return {
    type: CREATE_TAG_SUCCESS,
    tag,
  };
};

export const UPDATE_TAG = 'UPDATE_TAG';
export interface UpdateTag {
  type: typeof UPDATE_TAG;
  name: string;
  id: string;
}

export const updateTag = (name: string, id: string): UpdateTag => {
  return {
    type: UPDATE_TAG,
    name,
    id,
  };
};

export const UPDATE_TAG_SUCCESS = 'UPDATE_TAG_SUCCESS';
export interface UpdateTagSuccess {
  type: typeof UPDATE_TAG_SUCCESS;
  tag: Tag;
}

export const updateTagSuccess = (tag: Tag): UpdateTagSuccess => {
  return {
    type: UPDATE_TAG_SUCCESS,
    tag,
  };
};

export const UPDATE_STARRED_MESS = 'UPDATE_STARRED_MESS';
export interface UpdateStarredMess {
  type: typeof UPDATE_STARRED_MESS;
  name: string;
  id: number;
}

export const updateStarredMess = (name: string, id: number): UpdateStarredMess => {
  return {
    type: UPDATE_STARRED_MESS,
    name,
    id,
  };
};

export const SEARCH_MESSAGE_IN_ROOM = 'SEARCH_MESSAGE_IN_ROOM';
export interface SearchMessInRoom {
  type: typeof SEARCH_MESSAGE_IN_ROOM;
  text: string;
  id: string;
}

export const searchMessInRoom = (id: string, text: string): SearchMessInRoom => {
  return {
    type: SEARCH_MESSAGE_IN_ROOM,
    text,
    id,
  };
};

export const SEARCH_MESSAGE_IN_ROOM_SUCCESS = 'SEARCH_MESSAGE_IN_ROOM_SUCCESS';
export interface SearchMessInRoomSuccess {
  type: typeof SEARCH_MESSAGE_IN_ROOM_SUCCESS;
  mess: Message[];
}

export const searchMessInRoomSuccess = (mess: Message[]): SearchMessInRoomSuccess => {
  return {
    type: SEARCH_MESSAGE_IN_ROOM_SUCCESS,
    mess,
  };
};

export const SEARCH_ACROSS = 'SEARCH_ACROSS';
export interface SearchAcross {
  type: typeof SEARCH_ACROSS;
  text: string;
}

export const searchAcross = (text: string): SearchAcross => {
  return {
    type: SEARCH_ACROSS,
    text,
  };
};

export const SEARCH_ACROSS_SUCCESS = 'SEARCH_ACROSS_SUCCESS';
export interface SearchAcrossSuccess {
  type: typeof SEARCH_ACROSS_SUCCESS;
  data: { chats: Chat[]; messages: Message[] };
}

export const searchAcrossSuccess = (data: { chats: Chat[]; messages: Message[] }): SearchAcrossSuccess => {
  return {
    type: SEARCH_ACROSS_SUCCESS,
    data,
  };
};

export const UPLOAD_INTERNAL_FILE = 'UPLOAD_INTERNAL_FILE';
export interface UploadFileInternal {
  type: typeof UPLOAD_INTERNAL_FILE;
  data: { chatId: number; document: File; type: string; fileCaption: string; replyId?: string };
}

export const uploadFileInternalAct = (data: {
  chatId: number;
  document: File;
  type: string;
  fileCaption: string;
  replyId?: string;
}): UploadFileInternal => {
  return {
    type: UPLOAD_INTERNAL_FILE,
    data,
  };
};

export const SEND_TEXT_INTERNAl_CHAT = 'SEND_TEXT_INTERNAl_CHAT';
export interface SendTextInternalChat {
  type: typeof SEND_TEXT_INTERNAl_CHAT;
  room: number;
  text: string;
}

export const sendTextInternalChat = (room: number, text: string): SendTextInternalChat => {
  return {
    type: SEND_TEXT_INTERNAl_CHAT,
    room,
    text,
  };
};

export const FLAG_SEARCH_ACROSS = 'FLAG_SEARCH_ACROSS';

interface FlagSearchAcross {
  type: typeof FLAG_SEARCH_ACROSS;
  status: boolean;
}

export const flagSearchAcross = (status: boolean): FlagSearchAcross => {
  return {
    type: FLAG_SEARCH_ACROSS,
    status,
  };
};

export const FLAG_SEARCH_IN_ROOM = 'FLAG_SEARCH_IN_ROOM';

interface FlagSearchInRoom {
  type: typeof FLAG_SEARCH_IN_ROOM;
  status: boolean;
}

export const flagSearchInRoom = (status: boolean): FlagSearchInRoom => {
  return {
    type: FLAG_SEARCH_IN_ROOM,
    status,
  };
};

export const CHANGE_NAME_CHAT_ROOM = 'CHANGE_NAME_CHAT_ROOM';

export interface ChangeNameChatRoom {
  type: typeof CHANGE_NAME_CHAT_ROOM;
  payload: {
    roomId: string;
    name: string;
  };
}

export const changeNameChatRoom = (roomId: string, name: string): ChangeNameChatRoom => {
  return {
    type: CHANGE_NAME_CHAT_ROOM,
    payload: {
      roomId,
      name,
    },
  };
};

export type ChatActionTypes =
  | UpdateChatsQueryAction
  | ChatsFetchSuccessAction
  | ChatsFetchFailedAction
  | SelectChatAction
  | MessagesFetchSuccessAction
  | UpdateChatInputAction
  | SendMessageAction
  | SendMessageSuccessAction
  | ChatUpdatedAction
  | NewMessageAction
  | MessageUpdatedAction
  | CreateNewChatAction
  | UpdateCaseLeftNav
  | DeleteChatAction
  | UpdateEmojiInputAction
  | getStarredMessagesSuccessAction
  | updateStarredMessagesAction
  | getStarredMessagesAction
  | DeleteMessageAction
  | ReplyMessageAction
  | ResetMessageAction
  | MarkReadChat
  | MarkReadChatSuccess
  | ClearChatAction
  | ClearChatSuccessAction
  | MarkUnReadChat
  | RemoveTagTicketSuccess
  | SendDocumentTempMessageSuccess
  | EditTagSuccess
  | SetTimeLoadingFile
  | ProgressUpload
  | MessageUnread
  | UpdateCaseRightNav
  | SetFlagLoadMore
  | UpdateCaseRightNav
  | AssignUserAction
  | UnAssignUserAction
  | MessageSearch
  | SelectRoomMessageSearch
  | ResendMessageSuccess
  | SelectInternalRoom
  | FetchMessageInternalSuccess
  | PutNewMessageInternal
  | DeleteMessageInternalAct
  | ClearMessageInternal
  | LoadingSendFileInternal
  | DeleteMessageError
  | LoadingMessageInternal
  | AssignUserSuccessAction
  | UnAssignUserSuccessAction
  | CloseChatTicketSuccessAction
  | OpenChatTicketSuccessAction
  | ChatsFilterSuccessAction
  | SendTextTempMessage
  | PushNewRoomCreate
  | LoadMoreRoomScroll
  | DeleteRoomSuccess
  | GetCateFaqSuccess
  | GetFaqSuccess
  | CreateFaqSuccess
  | DeleteFaqSuccess
  | CreateCateFaqSuccess
  | DeleteCateFaqSuccess
  | UpdateFaqSuccess
  | UpdateCateFaqSuccess
  | GetTagSuccess
  | DeleteTagSuccess
  | CreateTagSuccess
  | UpdateTagSuccess
  | SearchMessInRoomSuccess
  | SearchAcrossSuccess
  | FlagSearchAcross
  | FlagSearchInRoom
  | ChangeNameChatRoom;
