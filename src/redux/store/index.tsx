/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-underscore-dangle */
import { applyMiddleware, combineReducers, compose, createStore, StoreEnhancer } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { spawn } from 'redux-saga/effects';
import { chatReducer, ChatState, INITIAL_CHAT_STATE } from '../reducer/chat';
import { chatSaga } from '../saga/chat';
import { contactReducer } from '../reducer/contact';
import { contactSaga } from '../saga/contact';
import { ContactState, INITIAL_CONTACT_STATE } from '../type/contact/state';
import authReducer, { subDomainReducer } from '../reducer/login';
import authWatcher from '../saga/login';
import { AuthState, INITIAL_AUTH_STATE, INITIAL_SUBDOMAIN_STATE, SubdomainState } from '../type/login/types';
import { INITIAL_UTIL_STATE, utilReducer, UtilState } from '../reducer/utils';

export interface AppState {
  contacts: ContactState;
  auth: AuthState;
  chat: ChatState;
  util: UtilState;
  subdomain: SubdomainState;
}

const INITIAL_STATE: AppState = {
  contacts: INITIAL_CONTACT_STATE,
  auth: INITIAL_AUTH_STATE,
  chat: INITIAL_CHAT_STATE,
  util: INITIAL_UTIL_STATE,
  subdomain: INITIAL_SUBDOMAIN_STATE,
};

const reducers = combineReducers({
  contacts: contactReducer,
  auth: authReducer,
  chat: chatReducer,
  util: utilReducer,
  subdomain: subDomainReducer,
});

function* rootSaga() {
  yield spawn(authWatcher);
  yield spawn(contactSaga);
  yield spawn(chatSaga);
}
export function configureStore(preloadedState = INITIAL_STATE) {
  const sagaMiddleware = createSagaMiddleware();

  const middlewares = process.env.NODE_ENV === 'test' ? [] : [sagaMiddleware];

  const middlewareEnhancer = composeWithDevTools(applyMiddleware(...middlewares));

  const enhancers: StoreEnhancer[] = [middlewareEnhancer];
  const composedEnhancers: StoreEnhancer = compose(...enhancers);

  const store = createStore(reducers, preloadedState, composedEnhancers);

  if (process.env.NODE_ENV !== 'test') {
    sagaMiddleware.run(rootSaga);
  }

  return store;
}
