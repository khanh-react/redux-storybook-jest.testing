import { SubdomainChannel, SubdomainConfig, SubdomainSetting, SubdomainUser } from '../../services/login';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';

export interface LoginRequestAction {
  type: typeof LOGIN_REQUEST;
  payload: {
    email: string;
    password: string;
  };
}
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
interface LoginSuccessAction {
  type: typeof LOGIN_SUCCESS;
  payload: { token: string; subdomainId: number };
}
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
interface LoginFailureAction {
  type: typeof LOGIN_FAILURE;
  payload: string;
}

export const FETCH_SUB_DOMAIN_CONFIG_SUCCESS = 'FETCH_SUB_DOMAIN_CONFIG_SUCCESS';
export const FETCH_SUB_DOMAIN_CONFIG = 'FETCH_SUB_DOMAIN_CONFIG';

interface SetSubdomainConfigSuccess {
  type: typeof FETCH_SUB_DOMAIN_CONFIG_SUCCESS;
  payload: SubdomainConfig;
}

export interface SetSubdomainConfig {
  type: typeof FETCH_SUB_DOMAIN_CONFIG;
  payload: number;
}

export const FETCH_SUB_DOMAIN_SETTING_SUCCESS = 'FETCH_SUB_DOMAIN_SETTING_SUCCESS';
export const FETCH_SUB_DOMAIN_SETTING = 'FETCH_SUB_DOMAIN_SETTING';

interface SetSubdomainSettingSuccess {
  type: typeof FETCH_SUB_DOMAIN_SETTING_SUCCESS;
  payload: SubdomainSetting;
}

export interface SetSubdomainSetting {
  type: typeof FETCH_SUB_DOMAIN_SETTING;
  payload: number;
}

export const FETCH_SUB_DOMAIN_CHANNEL_SUCCESS = 'FETCH_SUB_DOMAIN_CHANNEL_SUCCESS';
export const FETCH_SUB_DOMAIN_CHANNEL = 'FETCH_SUB_DOMAIN_CHANNEL';

interface SetSubdomainChannelSuccess {
  type: typeof FETCH_SUB_DOMAIN_CHANNEL_SUCCESS;
  payload: SubdomainChannel[];
}

export interface SetSubdomainChannel {
  type: typeof FETCH_SUB_DOMAIN_CHANNEL;
  payload: number;
}

export const FETCH_SUB_DOMAIN_USER_SUCCESS = 'FETCH_SUB_DOMAIN_USER_SUCCESS';
export const FETCH_SUB_DOMAIN_USER = 'FETCH_SUB_DOMAIN_USER';

interface SetSubdomainUserSuccess {
  type: typeof FETCH_SUB_DOMAIN_USER_SUCCESS;
  payload: SubdomainUser[];
}

export interface SetSubdomainUser {
  type: typeof FETCH_SUB_DOMAIN_USER;
  payload: number;
}

export interface AuthState {
  userId?: number;
  loading?: boolean;
  loaded?: boolean;
  error?: string;
  token?: string;
  subdomainId?: number;
}
export interface SubdomainState {
  subdomainConfig?: SubdomainConfig;
  subdomainSetting?: SubdomainSetting;
  subdomainChannel?: SubdomainChannel[];
  subdomainUser?: SubdomainUser[];
}
export const INITIAL_AUTH_STATE: AuthState = {};
export const INITIAL_SUBDOMAIN_STATE: SubdomainState = {};

export type LoginActionTypes =
  | LoginRequestAction
  | LoginSuccessAction
  | LoginFailureAction
  | SetSubdomainConfigSuccess
  | SetSubdomainSettingSuccess
  | SetSubdomainChannelSuccess
  | SetSubdomainUserSuccess;
