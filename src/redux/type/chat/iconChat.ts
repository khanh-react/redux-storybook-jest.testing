import { IconName } from '../../../components/common/Icon';
import { ChatPlatform, MessageStatus } from '../../services/chat';

export const MESSAGE_STATUS_TO_ICON_NAME: Record<MessageStatus, IconName | null> = {
  [MessageStatus.Failed]: 'error',
  [MessageStatus.Newly]: 'pending',
  [MessageStatus.Read]: 'seen',
  [MessageStatus.Received]: 'received',
  [MessageStatus.Sent]: 'sent',
};

export const CHAT_PLATFORM_TO_ICON_NAME: Record<ChatPlatform, IconName | null> = {
  [ChatPlatform.WhatsApp]: 'whatsapp',
  [ChatPlatform.Line]: 'line',
  [ChatPlatform.Facebook]: 'fb',
  [ChatPlatform.Twilio]: null,
  [ChatPlatform.Wechat]: null,
};
