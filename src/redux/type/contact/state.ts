export interface FieldHeader {
  name: string;
  label: string;
  type: 'text' | 'date' | 'select' | 'user' | 'checkbox' | 'radio' | 'textarea' | 'time';
  ordering: number;
  option_list?: string[];
}

export interface Contact {
  id: string;
  name: string;
  tel: string;
  fields: { [field: string]: string | { [field: string]: number } };
  remark: string;
  param_assign_all_number: string;
  // param_assign_number_list?: never[];
}

export interface Page {
  total: number;
  page: number;
  fieldHeaders: FieldHeader[];
  data: Contact[];
}
export interface ContactQuery {
  sortOrder: 'asc' | 'desc';
  sortKey: string;
  filter?: string;
  page: number;
  pageSize: number;
  filters?: { [name: string]: string };
}

export interface DeleteResult {
  deleted: number[];
  failed: number[];
}
export const DEFAULT_PAGE_SIZE = 10;

export interface ContactState {
  query?: ContactQuery;
  fields?: FieldHeader[];
  selectedFields?: string[];
  currentPage?: Page;
  delResult?: DeleteResult;
  delSingleResult?: { id: string; result: boolean };
  contactDetail?: ContactDetail;
  contactData?: Partial<Contact>;
  updateContactResult?: { result: { id: string } };
  filters?: Partial<FieldHeader>[];
}

export const DEFAULT_QUERY: ContactQuery = {
  sortOrder: 'asc',
  sortKey: 'id',
  page: 1,
  pageSize: DEFAULT_PAGE_SIZE,
};

export interface ContactDetail {
  fieldHeaders: FieldHeader[];
  contact: Contact;
  // user:         any[];
}

export const INITIAL_CONTACT_STATE: ContactState = {};
