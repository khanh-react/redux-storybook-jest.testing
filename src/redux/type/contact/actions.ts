import { Contact, ContactDetail, ContactQuery, DeleteResult, FieldHeader, Page } from './state';

export const CONTACTS_FETCH_SUCCESS = 'CONTACTS_FETCH_SUCCESS';
interface ContactsFetchSuccessAction {
  type: typeof CONTACTS_FETCH_SUCCESS;
  payload: Page;
}

export const CONTACTS_FETCH_FAILED = 'CONTACTS_FETCH_FAILED';
interface ContactsFetchFailedAction {
  type: typeof CONTACTS_FETCH_FAILED;
  payload: Page;
}

export const UPDATE_CONTACT_QUERY = 'UPDATE_CONTACT_QUERY';
export interface UpdateContactsQueryAction {
  type: typeof UPDATE_CONTACT_QUERY;
  payload: Partial<ContactQuery>;
}

export const BULK_DELETE_REQUEST = 'BULK_DELETE_REQUEST';
export interface BulkDeleteRequestAction {
  type: typeof BULK_DELETE_REQUEST;
  payload: number[];
}
export const BULK_DELETE_REQUEST_FAILED = 'BULK_DELETE_REQUEST_FAILED';
export const BULK_DELETE_REQUEST_SUCCESS = 'BULK_DELETE_REQUEST_SUCCESS';
export interface BulkDeleteRequestSuccessAction {
  type: typeof BULK_DELETE_REQUEST_SUCCESS;
  payload: DeleteResult;
}

export const ADD_CONTACT = 'ADD_CONTACT';

export interface AddContactAction {
  type: typeof ADD_CONTACT;
  payload: {
    name: string;
    fields: Record<string, unknown>;
  };
}
export const ADD_CONTACT_FAILED = 'ADD_CONTACT_FAILED';
export const ADD_CONTACT_SUCCESS = 'ADD_CONTACT_SUCCESS';
export const EDIT_CONTACT = 'EDIT_CONTACT';

export interface EditContactAction {
  type: typeof EDIT_CONTACT;
  payload: FieldHeader[];
}

export const DELETE_SINGLE_CONTACT_REQUEST = 'DELETE_SINGLE_CONTACT_REQUEST';
export interface DeleteSingleContactAction {
  type: typeof DELETE_SINGLE_CONTACT_REQUEST;
  payload: string;
}
export const DELETE_SINGLE_CONTACT_REQUEST_FAILED = 'DELETE_SINGLE_CONTACT_REQUEST_FAILED';
export const DELETE_SINGLE_CONTACT_REQUEST_SUCCESS = 'DELETE_SINGLE_CONTACT_REQUEST_SUCCESS';
export interface DeleteSingleContactRequestSuccessAction {
  type: typeof DELETE_SINGLE_CONTACT_REQUEST_SUCCESS;
  payload: { id: string; result: boolean };
}

export const GET_CONTACT_DETAIL = 'GET_CONTACT_DETAIL';
export interface GetContactDetailAction {
  type: typeof GET_CONTACT_DETAIL;
  payload: string;
}
export const GET_CONTACT_DETAIL_SUCCESS = 'GET_CONTACT_DETAIL_SUCCESS';
export interface GetContactDetailSuccessAction {
  type: typeof GET_CONTACT_DETAIL_SUCCESS;
  payload: ContactDetail;
}
export const GET_CONTACT_DETAIL_FAILED = 'GET_CONTACT_DETAIL_FAILED';

export const UPDATE_CONTACT_DATA_QUERY = 'UPDATE_CONTACT_DATA_QUERY';
export interface UpdateContactDataQueryAction {
  type: typeof UPDATE_CONTACT_DATA_QUERY;
  payload: Partial<Contact>;
}

export const UPDATE_CONTACT_DETAIL = 'UPDATE_CONTACT_DETAIL';
export interface UpdateContactDetailAction {
  type: typeof UPDATE_CONTACT_DETAIL;
}
export const UPDATE_CONTACT_DETAIL_SUCCESS = 'UPDATE_CONTACT_DETAIL_SUCCESS';

export interface UpdateContactDetailSuccessAction {
  type: typeof UPDATE_CONTACT_DETAIL_SUCCESS;
  payload: { result: { id: string } };
}
export const UPDATE_CONTACT_DETAIL_FAILED = 'UPDATE_CONTACT_DETAIL_FAILED';
export const UPDATE_FILTERS = 'UPDATE_FILTERS';
export interface UpdateFiltersAction {
  type: typeof UPDATE_FILTERS;
  filters: Partial<FieldHeader>[];
}

export type ContactActionTypes =
  | UpdateContactsQueryAction
  | ContactsFetchSuccessAction
  | ContactsFetchFailedAction
  | BulkDeleteRequestAction
  | BulkDeleteRequestSuccessAction
  | EditContactAction
  | GetContactDetailSuccessAction
  | UpdateContactDataQueryAction
  | DeleteSingleContactRequestSuccessAction
  | UpdateContactDetailSuccessAction
  | UpdateFiltersAction;
