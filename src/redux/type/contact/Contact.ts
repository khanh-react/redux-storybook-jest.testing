export type Profile = {
  name: string;
  avatar?: string;
  role?: string;
};

export type Row = Record<string, string>;

export interface ContactField<D extends Row> {
  type: 'text' | 'date' | 'select' | 'user' | 'checkbox' | 'radio' | 'textarea' | 'id' | 'time';
  key: keyof D;
  title: string;
}

export interface ContactRow extends Row {
  // profile: string;
  phone: string;
  gender: string;
  carbs: string;
  degree: string;
  s: string;
  ss: string;
  sss: string;
}

export const ALL_COLUMNS: ContactField<ContactRow>[] = [
  { type: 'id', key: 'id', title: 'Select' },
  { type: 'user', key: 'field_owner', title: 'Owner' },
  { type: 'user', key: 'name', title: 'Name' },
  { type: 'select', key: 'field_32', title: 'Gender' },
  { type: 'date', key: 'field_33', title: 'Birthday' },
  { type: 'select', key: 'field_34', title: 'opt-in' },
  { type: 'text', key: 'field_35', title: 'Region' },
  { type: 'date', key: 'field_36', title: 'Creation date' },
];
export type Order = 'asc' | 'desc';

export type VisibleColumns<D extends Row> = Record<keyof D, boolean>;

export const ALWAYS_VISIBLE_COLUMNS: VisibleColumns<ContactRow> = {
  id: true,
  field_owner: false,
  name: false,
  field_32: false,
  field_33: false,
  field_34: false,
  field_35: false,
  field_36: true,
};
export const DEFAULT_VISIBLE_COLUMNS: VisibleColumns<ContactRow> = {
  id: true,
  field_owner: true,
  name: true,
  field_32: true,
  field_33: true,
  field_34: true,
  field_35: true,
  field_36: true,
};
