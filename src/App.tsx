import { MuiThemeProvider, StylesProvider } from '@material-ui/core';
import React from 'react';
import { Router } from 'react-router';
import { ThemeProvider } from 'styled-components';

// import PrivateRoute from './components/login/AuthVerify';
import Routes from './route';
import { createTheme } from './theme';
import { history } from './utils/history';

// import { QuickReplyCategories } from './components/settings/QuickReplyCategories';
// import { createData } from './components/settings/QuickReplyCategories/types';
// import { QuickReplyAddCategory } from './components/settings/QuickReplyAddCategory';
// import { AddQuickReply } from './components/chat/components/AddQuickReply';
// import { QuickReplySettings } from './components/settings/QuickReplySettings';
// import { createData } from './components/settings/QuickReplySettings/types';
// import { BroadcastList } from './components/settings/BroadCastList';
// import { createData } from './components/settings/BroadCastList/types';
// import { BroadcastCreate } from './components/settings/BroadCastCreate';
// import { BroadCastEdit } from './components/settings/BroadCastEdit';
// import { FetchQuickReplies } from './components/chat/components/FetchQuickReplies';
import { QuickReplyCreate } from './components/settings/QuickReplyCreate';

const theme = createTheme();

// const mockdata = [
//   createData('/welcome', 'A short welcome message for customers 1', 'James Corden', '2 days ago'),
//   createData('/payment', 'A short welcome message for customers 2', 'James Corden', '1 week ago'),
//   createData('/thankyou', 'A short welcome message for customers 3', 'James Corden', '13/02/2020'),
//   createData('/shiping', 'A short welcome message for customers 4', 'James Corden', '04/02/2020'),
//   createData('/teams', 'A short welcome message for customers 5', 'James Corden', '21/12/2019'),
//   createData('/welcome1', 'A short welcome message for customers 6', 'James Corden', '2 days ago'),
//   createData('/payment1', 'A short welcome message for customers 7', 'James Corden', '1 week ago'),
//   createData('/thankyou1', 'A short welcome message for customers 8', 'James Corden', '13/02/2020'),
//   createData('/shiping1', 'A short welcome message for customers 9', 'James Corden', '04/02/2020'),
//   createData('/teams1', 'A short welcome message for customers 10', 'James Corden', '21/12/2019'),
//   createData('/welcome2', 'A short welcome message for customers 11', 'James Corden', '2 days ago'),
//   createData('/payment2', 'A short welcome message for customers 12', 'James Corden', '1 week ago'),
//   createData('/thankyou2', 'A short welcome message for customers 13', 'James Corden', '13/02/2020'),
//   createData('/shiping2', 'A short welcome message for customers 14', 'James Corden', '04/02/2020'),
//   createData('/teams2', 'A short welcome message for customers 15', 'James Corden', '21/12/2019'),
// ];

// const broadcastData = [
//   createData('01', '/New product launch 2020', '13/02/2020  01:30PM', 20, 'James Corden', '1 days ago'),
//   createData('02', '/New product launch 2021', '13/02/2020  01:31PM', 21, 'James Corden', '2 days ago'),
//   createData('03', '/New product launch 2022', '13/02/2020  01:32PM', 22, 'James Corden', '3 days ago'),
//   createData('04', '/New product launch 2023', '13/02/2020  01:33PM', 23, 'James Corden', '4 days ago'),
//   createData('05', '/New product launch 2024', '13/02/2020  01:34PM', 24, 'James Corden', '5 days ago'),
//   createData('06', '/New product launch 2025', '13/02/2020  01:35PM', 25, 'James Corden', '6 days ago'),
//   createData('07', '/New product launch 2026', '13/02/2020  01:36PM', 26, 'James Corden', '7 days ago'),
//   createData('08', '/New product launch 2027', '13/02/2020  01:37PM', 27, 'James Corden', '8 days ago'),
//   createData('09', '/New product launch 2028', '13/02/2020  01:38PM', 28, 'James Corden', '9 days ago'),
//   createData('10', '/New product launch 2029', '13/02/2020  01:39PM', 29, 'James Corden', '10 days ago'),
// ];

// const dropdown = [
//   {
//     id: 'all',
//     name: 'All categories',
//   },
//   {
//     id: 'thankyou',
//     name: 'Thank You',
//   },
//   {
//     id: 'welcome',
//     name: 'Wellcome',
//   },
//   {
//     id: 'team',
//     name: 'Teams',
//   },
// ];

// const categories = [
//   {
//     id: 'team',
//     shortcut: '/work-hours',
//     items: [
//       {
//         id: 1,
//         content: ['Our Office'],
//       },
//       {
//         id: 2,
//         content: ['Monday to Sunday', '9:00 am - 2:00 pm', '2:00 am - 5:00 pm'],
//       },
//       {
//         id: 3,
//         content: ['Lauch Hour', '1:00 pm - 2:00 pm'],
//       },
//       {
//         id: 4,
//         content: ['Saturday, Sunday and Public Hodidays', 'Closed'],
//       },
//     ],
//   },
//   {
//     id: 'team',
//     shortcut: '/payment-1',
//     items: [
//       {
//         id: 1,
//         content: [
//           ' Hello <b style="font-weight: 500">{chat_name}</b>, This is a confirmation that we have just received your secure online payment. Thank you for your trust in <b style="font-weight: 500">{company_name}</b> .',
//         ],
//       },
//     ],
//   },
//   {
//     id: 'thankyou',
//     shortcut: '/thankyou-1',
//     items: [
//       {
//         id: 1,
//         content: [
//           'Thank you for your response, <b style="font-weight: 500">{chat_name}</b>.😊  We appreciate the attention you are giving to this issue. We will follow up after we investigate a few more possible causes of the problem.',
//         ],
//       },
//     ],
//   },
//   {
//     id: 'welcome',
//     shortcut: '/welcome-1',
//     items: [
//       {
//         id: 1,
//         content: [
//           'Hi <b style="font-weight: 500">{chat_name}</b>. Welcome to the team! We are thrilled to have you at our office. You’re going to be a valuable asset to our company and we can’t wait to see all that you accomplish.',
//         ],
//       },
//     ],
//   },
//   {
//     id: 'welcome',
//     shortcut: '/welcome-2',
//     items: [
//       {
//         id: 1,
//         content: [
//           'Hi👋🏻 <b style="font-weight: 500">{chat_name}</b>. A big congratulations on your new role. On behalf of the members and supervisors, we would like to welcome you to the team. We are extremely happy and excited about working as a team starting from today.',
//         ],
//       },
//     ],
//   },
// ];

const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <ThemeProvider theme={theme}>
        <StylesProvider injectFirst>
          <Router history={history}>
            {/* <AddQuickReply /> */}
            {/* <FetchQuickReplies dropdown={dropdown} categories={categories} /> */}
            {/* <BroadcastCreate /> */}
            {/* <BroadCastEdit /> */}
            {/* <BroadcastList data={broadcastData} /> */}
            {/* <ListBroadCasts data={mockdata} /> */}
            {/* <QuickReplyCategories data={mockdata} /> */}
            {/* <QuickReplyAddCategory /> */}
            {/* <AddQuickReply /> */}
            <QuickReplyCreate />
            {/* <PrivateRoute path="/cms" /> */}
            {/* <Routes /> */}
          </Router>
        </StylesProvider>
      </ThemeProvider>
    </MuiThemeProvider>
  );
};

export default App;
