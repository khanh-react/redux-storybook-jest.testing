import PropTypes from 'prop-types';
import jwtDecode, { JwtPayload } from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { readRecord } from '../../utils/localStorageService';
import { loginSuccess } from '../../redux/action/login';

// import { Redirect } from 'react-router'

const GuestGuard = ({ children }: { children: React.ReactElement }) => {
  const dispatch = useDispatch();
  const [isAuthenticated, setIsAuthenticated] = useState(true);
  useEffect(() => {
    const token = readRecord('accessToken');
    const subdomainId = readRecord('subdomainId');
    if (token && subdomainId) {
      const tokenExpiration = jwtDecode<JwtPayload>(token).exp;
      const dateNow = new Date();
      if (tokenExpiration && tokenExpiration < dateNow.getTime() / 1000) {
        setIsAuthenticated(false);
      } else {
        setIsAuthenticated(true);
        dispatch(loginSuccess(token, parseInt(subdomainId, 10)));
      }
    } else {
      setIsAuthenticated(false);
    }
  }, [isAuthenticated, dispatch]);

  return isAuthenticated ? children : <Redirect to="/login" />;
};

GuestGuard.prototype = {
  children: PropTypes.any,
};

export default GuestGuard;
