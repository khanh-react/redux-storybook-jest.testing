/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useCallback } from 'react';
import { useDrop } from 'react-dnd';
import styled from 'styled-components';

export interface TargetBoxProps {
  onDrop: (item: any) => void;
  lastDroppedColor?: string;
}

const TargetBox: React.FC<TargetBoxProps> = ({ onDrop, lastDroppedColor }) => {
  const [{ isOver, canDrop }, drop] = useDrop({
    accept: ['yellow', 'blue'],
    drop(item: { type: string }) {
      onDrop(item.type);
      return undefined;
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
      draggingColor: monitor.getItemType() as string,
    }),
  });

  const Wrapper = styled.div`
    border: 1px solid gray,
    height: 15rem,
    width: 15rem,
    padding: 2rem,
    textAlign: center,
    backgroundColor: #fff,
    opacity: ${isOver ? 1 : 0.7},
  `;
  return (
    <Wrapper ref={drop}>
      <p>Drop here.</p>

      {!canDrop && lastDroppedColor && <p>Last dropped: {lastDroppedColor}</p>}
    </Wrapper>
  );
};

export interface StatefulTargetBoxState {
  lastDroppedColor: string | null;
}

export const StatefulTargetBox: React.FC = (props) => {
  const [lastDroppedColor, setLastDroppedColor] = useState<string | null>(null);
  const handleDrop = useCallback((color: string) => setLastDroppedColor(color), []);

  return <TargetBox {...props} lastDroppedColor={lastDroppedColor as string} onDrop={handleDrop} />;
};
