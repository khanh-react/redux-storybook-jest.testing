import React from 'react';

import InputTag, { InputTagProps } from './InputTag';

export default {
  title: 'Components/InputTag',
  component: InputTag,
  argsTypes: {
    onClose: { action: 'close' },
  },
};

export const Primary = (props: InputTagProps) => {
  return <InputTag {...props} />;
};
Primary.args = {
  hasCloseIcon: true,
  label: 'Alex Ho',
};
