import { MenuItem, Paper, TextField, TextFieldProps } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';

export interface InputFieldProps {
  required?: boolean;
  error?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  label?: string;
  helper?: string;
  select?: boolean;
  placeholder?: string;
}
export const StyledTextField = styled(TextField)`
  ${({ theme }) => `
width: 100%;
.MuiInputLabel-outlined {
  font-family: ${theme.typography};
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.4px;
}
.MuiInputBase-input {
  font-family: ${theme.typography};
  font-size: 14px;
  color: #2e2e32
}
.MuiFormHelperText-root {
  font-family: ${theme.typography};
  font-size: 14px;
  color: #E75264;
}
.MuiFormLabel-asterisk.MuiInputLabel-asterisk {
  color:#E75264
}
.MuiInputLabel-outlined.MuiInputLabel-shrink {
  transform: translate(14px, -6px) scale(0.78571428571);
}
.MuiOutlinedInput-input {
  padding: 17px 14px;
}
`}
`;

export const StyledMenuItem = styled(MenuItem)`
  background-color: white;
  &.Mui-selected {
    background-color: white;
  }
  &.Mui-selected:hover {
    background-color: #fff8da;
  }
  &:hover {
    background-color: #fff8da;
  }
`;
export const StyledPaper = styled(Paper)`
  background-color: white;
`;
export const IbTextField = styled((props: InputFieldProps & TextFieldProps) => (
  <StyledTextField
    variant={props.variant || 'outlined'}
    helperText={props.helper}
    {...props}
    SelectProps={{
      MenuProps: {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'left',
        },
        transformOrigin: {
          vertical: 'top',
          horizontal: 'left',
        },
        getContentAnchorEl: null,
        disableAutoFocusItem: true,
      },
    }}
  />
))`
  .MuiOutlinedInput-root {
    border-radius: 8px;
    width: 100%;
    height: ${(props) => (props.multiline ? 'auto' : '50px')};
    padding: ${(props) => props.multiline && 0};
    &.Mui-focused fieldset {
      ${(props) =>
        props.error
          ? `
          border-color: #E65063
      `
          : `border-color: #2E2E32;`}
    }
  }
  .MuiFormLabel-root {
    font-size: 14px;
    ${(props) =>
      props.error
        ? `
        color: #E65063
    `
        : `color:#98999F;`}
    ${(props) => props.disabled && ` color: #A2A3A9`}
  }
  .Mui-focused.MuiFormLabel-root {
    ${(props) =>
      props.error
        ? `
        color: #E65063
    `
        : `color:  #2E2E32;`}
  }

  .MuiOutlinedInput-notchedOutline {
    ${(props) =>
      props.error
        ? `
        border-color: #E75264
    `
        : `border-color: #C4C4C4;`}
    ${(props) => props.disabled && `border-color: #DCDCE4`}
  }
  .MuiInputLabel-shrink {
    ${(props) =>
      props.error
        ? `
        color: #E65063
    `
        : `color: #98999F;`}
    ${(props) =>
      props.disabled
        ? `
                color: #A2A3A9
            `
        : `color:#98999F;`}
  }
  & > :hover .MuiOutlinedInput-notchedOutline {
    ${(props) =>
      props.error
        ? `
        border-color: #E75264
    `
        : `border-color:  #2E2E32;`}
    ${(props) => props.disabled && `border-color: #DCDCE4`}
  }
  & > :hover .MuiFormLabel-root {
    ${(props) =>
      props.error
        ? `
        color: #E65063
    `
        : `color:  #2E2E32;`}
  }
  .MuiInputBase-input {
    ${(props) => props.disabled && `color:#98999F;`}
  }
`;
