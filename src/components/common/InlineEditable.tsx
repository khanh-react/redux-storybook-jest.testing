import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

const StyledSection = styled.section`
  font-size: 14px;
  color: #3c3d42;
  width: 220px;
  height: 14px;
  cursor: pointer;
  width: 220px;
  height: 14px;
  text-align: center;
  .inline-text-input {
    background: none;
    border: none;
    outline: none;
    width: 220px;
    height: 14px;
    text-align: center;
    border-bottom: 1px solid #dcdce4;
  }
`;
const InlineEditable = ({
  text,
  placeholder,
  onSetText,
  ...props
}: {
  text: string;
  placeholder?: string;
  onSetText: (val: string) => void;
}) => {
  const [isEditing, setEditing] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);
  useEffect(() => {
    if (inputRef && inputRef.current && isEditing === true) {
      inputRef.current.focus();
    }
  }, [isEditing, inputRef]);

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    const { key } = event;
    (key === 'Enter' || key === 'Escape') && setEditing(false);
  };

  return (
    <StyledSection {...props}>
      {isEditing ? (
        <div onBlur={() => setEditing(false)} onKeyDown={(e) => handleKeyDown(e)}>
          <input
            className="inline-text-input"
            ref={inputRef}
            type="text"
            placeholder={placeholder}
            value={text}
            onChange={(e) => onSetText(e.target.value)}
          />
        </div>
      ) : (
        <div onClick={() => setEditing(true)}>
          <span id="inline-text">{text || placeholder}</span>
        </div>
      )}
    </StyledSection>
  );
};

export default InlineEditable;
