import React, { useState, useCallback } from 'react';
import { useDrag, DragSourceMonitor } from 'react-dnd';
import styled from 'styled-components';

export interface SourceBoxProps {
  color?: string;
  onToggleForbidDrag?: () => void;
}

export const SourceBox: React.FC<SourceBoxProps> = ({ color, children }) => {
  const [forbidDrag, setForbidDrag] = useState(false);
  const [{ isDragging }, drag] = useDrag({
    item: { type: `${color}` },
    canDrag: !forbidDrag,
    collect: (monitor: DragSourceMonitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const onToggleForbidDrag = useCallback(() => {
    setForbidDrag(!forbidDrag);
  }, [forbidDrag]);

  const Wrapper = styled.div`
    border: 1px dashed gray,
    padding: 0.5rem,
    margin: 0.5rem,
    opacity: ${isDragging ? 0.4 : 1},
    cursor: ${forbidDrag ? 'default' : 'move'},
  `;

  return (
    <Wrapper ref={drag}>
      <input type="checkbox" checked={forbidDrag} onChange={onToggleForbidDrag} />
      <small>Forbid drag</small>
      {children}
    </Wrapper>
  );
};
