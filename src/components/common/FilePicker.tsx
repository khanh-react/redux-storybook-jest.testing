import React from 'react';
import styled from 'styled-components';
import { Flex } from './Flex';

const Wrapper = styled(Flex)`
  min-height: 35px;
`;

type Props = {
  className?: string;
  children?: React.ReactNode;
  onChange: (e: unknown) => void;
};

export const FilePicker = ({ className, children, onChange }: Props) => {
  const fileRef = React.useRef<HTMLInputElement>(null);

  const handleChooseFile = () => {
    fileRef.current && fileRef.current.click();
  };

  const handleFileChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const imageSelect = e.target.files[0];
      onChange(imageSelect);
    }
  };

  return (
    <Wrapper className={className} alignItems="center" onClick={handleChooseFile}>
      {children}
      <input
        type="file"
        accept="image/*"
        id="file"
        ref={fileRef}
        style={{ display: 'none' }}
        onChange={handleFileChanged}
      />
    </Wrapper>
  );
};

export default FilePicker;
