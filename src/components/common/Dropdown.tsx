import React, { useState, useRef } from 'react';
import styled from 'styled-components';
import { Flex } from './Flex';
import { Text } from './Text';
import useOnClickOutside from './useOnClickOutside';

const Wrapper = styled(Flex)`
  padding: 12px;
  &:hover {
    cursor: pointer;
  }
  img {
    width: 14px;
    height: 14px;
    margin-left: 5px;
    margin-top: 3px;
  }
  .items {
    position: absolute;
    top: 0 !important;
  }
  .dropdown {
    position: relative;
  }
`;

const DropdownListWrapper = styled(Flex)`
  min-width: 143px;
  padding: 12px 0px;
  border-radius: 8px;
  box-shadow: 0 2px 10px 0 rgba(175, 175, 175, 0.5);
  background-color: #fff;
  position: absolute;
  top: 10px;
  left: -70px;
  .item {
    width: 100%;
    height: 32px;
    margin-bottom: 3px;
    padding: 0px 21px;
    font-size: 14px;
    &:hover {
      background-color: #fff8da;
      cursor: pointer;
      font-weight: 500;
    }
  }
`;

type Props = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  items: any[];
  keyValue: string;
  isHidden?: boolean;
  // eslint-disable-next-line react/no-unused-prop-types
  className?: string;
  onIndexChanged?: (index: number) => void;
};

const DropdownList = ({ isHidden, items, keyValue, onIndexChanged }: Props) => {
  if (isHidden) return null;
  return (
    <DropdownListWrapper flexDirection="column">
      {items.map((item, index) => {
        return (
          <Flex
            // eslint-disable-next-line react/no-array-index-key
            key={`drop-item-${index}`}
            className="item"
            alignItems="center"
            onClick={() => onIndexChanged && onIndexChanged(index)}
          >
            {item[keyValue]}
          </Flex>
        );
      })}
    </DropdownListWrapper>
  );
};

export const Dropdown = (props: Props) => {
  const dropListRef = useRef(null);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [itemSelected, setItemSelected] = useState<any>(props.items[0]);
  const [hidden, setHidden] = useState(true);
  const handleClick = () => setHidden(false);
  const handleIndexChanged = (index: number) => {
    setItemSelected(props.items[index]);
    setHidden(true);
    props.onIndexChanged && props.onIndexChanged(index);
  };
  useOnClickOutside(dropListRef, () => setHidden(true));

  return (
    <Wrapper className={props.className} alignItems="center" justifyContent="center">
      <Flex alignItems="center" justifyContent="center" onClick={handleClick}>
        <Text value={itemSelected[props.keyValue]} />
        <img color="red" alt="" src="/static/arrow-d.svg" />
      </Flex>
      <div className="dropdown" ref={dropListRef}>
        <DropdownList {...props} isHidden={hidden} onIndexChanged={handleIndexChanged} />
      </div>
    </Wrapper>
  );
};

export default Dropdown;
