import { Input } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

export const BareInput = withStyles(() => ({
  root: {
    '&:before': {
      content: 'none',
      borderBottom: 'unset',
    },
    '&:after': {
      content: 'none',
      borderBottom: 'unset',
    },
  },
}))(Input);
