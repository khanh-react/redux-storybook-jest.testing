import React from 'react';

import { Flex } from './Flex';
import { IbTextField, InputFieldProps, StyledMenuItem } from './InputField';

export default {
  title: 'Components/InputField',
  component: IbTextField,
};

export const Default = (props: InputFieldProps) => {
  return (
    <Flex width={340} height="80vh">
      <IbTextField {...props} defaultValue="your name" />
    </Flex>
  );
};

Default.args = {
  required: false,
  error: false,
  disabled: false,
  readOnly: false,
  label: 'Text',
  helper: '',
};

export const Required = (props: InputFieldProps) => {
  return (
    <Flex width={340} height="80vh">
      <IbTextField {...props} defaultValue="your name" />
    </Flex>
  );
};

Required.args = {
  required: true,
  error: false,
  disabled: false,
  readOnly: false,
  label: 'Required',
};

export const Error = (props: InputFieldProps) => {
  return (
    <Flex width={340} height="80vh">
      <IbTextField {...props} defaultValue="your name" />
    </Flex>
  );
};

Error.args = {
  required: false,
  error: true,
  disabled: false,
  readOnly: false,
  label: 'Error',
  helper: 'Error Message - a way out for the problem',
};

export const Disabled = (props: InputFieldProps) => {
  return (
    <Flex width={340} height="80vh">
      <IbTextField {...props} defaultValue="your name" />
    </Flex>
  );
};

Disabled.args = {
  required: false,
  error: false,
  disabled: true,
  readOnly: false,
  label: 'Disabled',
  placeholder: 'your name',
};

export const ReadOnly = (props: InputFieldProps) => {
  return (
    <Flex width={340} height="80vh">
      <IbTextField
        {...props}
        defaultValue="your name"
        InputProps={{
          readOnly: props.readOnly,
        }}
      />
    </Flex>
  );
};

ReadOnly.args = {
  required: false,
  error: false,
  disabled: false,
  readOnly: true,
  label: 'Read Only',
  placeholder: 'your name',
};

export const Selected = (props: InputFieldProps) => {
  const [value, setValue] = React.useState('');
  const handleChangeValue = (event: React.ChangeEvent<{ value: unknown }>) => {
    setValue(event.target.value as string);
  };

  return (
    <Flex width={340} height="80vh">
      <Flex flex={1}>
        <IbTextField {...props} onChange={handleChangeValue} value={value}>
          <StyledMenuItem value="value">Value1</StyledMenuItem>
          <StyledMenuItem value="value2">Value2</StyledMenuItem>
        </IbTextField>
      </Flex>
    </Flex>
  );
};

Selected.args = {
  required: false,
  error: false,
  disabled: false,
  readOnly: false,
  select: true,
  label: 'Select',
  placeholder: 'your name',
};
