import React from 'react';

import InlineEditable from './InlineEditable';

export default {
  title: 'Components/InlineEditable',
  component: InlineEditable,
};

export const Default = () => {
  const [value, setValue] = React.useState('');

  return (
    <div>
      <InlineEditable text={value} placeholder="Click here to edit!" onSetText={(text: string) => setValue(text)} />
    </div>
  );
};
