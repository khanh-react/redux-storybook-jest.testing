import { makeStyles } from '@material-ui/core/styles';
import React from 'react';

export interface TextProps {
  value?: string;
  size?: 'xl' | 'lg' | 'md' | 'sm' | 'xsm';
  color?: string;
  weight?: 'bold' | 'italic';
  icon?: React.ReactNode;
  className?: string;
  innerHTML?: boolean;
  children?: React.ReactNode;
}

const useStyle = makeStyles({
  root: (props: TextProps) => ({
    lineHeight: 1.17,
    letterSpacing: -0.06,
    color: props.color || '#263238',
    '&.xsm, &.xsm svg, &.xsm img': {
      fontSize: '12px',
    },
    '&.sm, &.sm svg, &.sm img': {
      fontSize: '14px',
    },
    '&.md, &.md svg, &.md img': {
      fontSize: '16px',
    },
    '&.lg, &.lg svg, &.lg img': {
      fontSize: '18px',
    },
    '&.xl, &.xl svg, &.xl img': {
      fontSize: '22px',
    },
    '&.text--icon': {
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'row',
      '& .text': {
        marginLeft: '8px',
      },
    },
    '&.bold': {
      fontWeight: 'bold',
    },
  }),
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function createMarkup(html: any) {
  return { __html: html };
}

export const Text = (props: TextProps) => {
  const { root } = useStyle(props);
  const { className, value, size, weight, icon, children } = props;

  if (!icon) {
    if (!props.innerHTML) {
      return <span className={`text ${className} ${root} ${size} ${weight}`}>{value || children}</span>;
    }
    return (
      <span
        className={`text ${className} ${root} ${size} ${weight}`}
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={createMarkup(value || children)}
      />
    );
  }

  return (
    <div className={`text--icon ${className} ${root} ${size} ${weight}`}>
      {icon}
      {props.innerHTML ? (
        // eslint-disable-next-line react/no-danger
        <span className="text innerhtml" dangerouslySetInnerHTML={createMarkup(value || children)} />
      ) : (
        <span className="text normal">{value || children}</span>
      )}
    </div>
  );
};

export default Text;
