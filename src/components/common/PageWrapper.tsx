import React, { forwardRef, useCallback, useEffect } from 'react';

// import { Helmet } from 'react-helmet';
// import track from 'src/utils/analytics';

type Props = {
  children: React.ReactNode;
  title?: string;
};

const PageWrapper = forwardRef<HTMLDivElement, Props>(({ children, ...rest }, ref) => {
  // const location = useLocation();

  const sendPageViewEvent = useCallback(
    () => {
      // track.pageview({
      //   page_path: location.pathname
      // });
      // console.log('TODO: track page view');
    },
    [
      /* location */
    ],
  );

  useEffect(() => {
    sendPageViewEvent();
  }, [sendPageViewEvent]);

  return (
    <div ref={ref} {...rest}>
      {/* <Helmet>
        <title>{title}</title>
      </Helmet> */}
      {children}
    </div>
  );
});

export default PageWrapper;
