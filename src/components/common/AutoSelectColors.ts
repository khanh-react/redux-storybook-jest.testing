const COLORS = [
  {
    textColor: '#626264',
    backgroundColor: '#D4D4D4',
  },
  {
    textColor: '#C073FD',
    backgroundColor: '#EFE3F4',
  },
  {
    textColor: '#7596FF',
    backgroundColor: '#E8EAFB',
  },
  {
    textColor: '#009FFF',
    backgroundColor: '#DBECFC',
  },
  {
    textColor: '#4FCF9F',
    backgroundColor: '#DCF4ED',
  },
  {
    textColor: '#FFD857',
    backgroundColor: '#FFF9E9',
  },
  {
    textColor: '#FBB475',
    backgroundColor: '#FEEBDE',
  },
  {
    textColor: '#F67E8E',
    backgroundColor: '#FBE8EB',
  },
];

export const autoColors = (index: number) => {
  if (index < COLORS.length) {
    return COLORS[index];
  }
  const repeat = Math.floor(index / COLORS.length);
  return COLORS[index - repeat * COLORS.length];
};
