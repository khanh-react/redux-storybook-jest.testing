import React from 'react';
import clsx from 'clsx';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      width: '100%',
      '&.small': {
        '& .MuiInputBase-root': {
          width: '100%',
          height: 36,
        },
      },
      '&.medium': {
        '& .MuiInputBase-root': {
          width: 386,
          height: 36,
        },
      },
      '&.large': {
        '& .MuiInputBase-root': {
          width: 480,
          height: 50,
          borderRadius: 8,
        },
      },
      '& .MuiInputBase-root': {
        '&.Mui-focused fieldset': {
          borderWidth: 1,
          borderColor: '#000',
        },
      },
    },
    margin: {
      marginBottom: 3,
    },
    withoutLabel: {
      marginTop: theme.spacing(3),
    },
    textField: {
      width: '25ch',
      '& .MuiSvgIcon-root': {
        opacity: 0.5,
      },
      '& .Mui-focused': {
        color: '#000',
      },
    },
    textarea: {
      '& .Mui-focused fieldset': {
        borderWidth: '1px !important',
        borderColor: '#000 !important',
      },
    },
    textBold: {
      '& .MuiInputBase-root': {
        '& input': {
          fontWeight: 600,
        },
      },
    },
    datetime: {
      minWidth: 230,
      '& .MuiInputBase-root': {
        borderRadius: 8,
        '&.Mui-focused fieldset': {
          borderWidth: 1,
          borderColor: '#000',
        },
      },
      '& label.Mui-focused': {
        color: '#000',
        fontWeight: 600,
      },
    },
  }),
);

type Props = {
  value: string;
  type?: 'default' | 'date' | 'time' | 'datetime-local' | 'textarea';
  placeholder?: string;
  size?: 'small' | 'medium' | 'large';
  label?: string;
  className?: string;
  rows?: number;
  isBold?: boolean;
  icon?: React.ReactNode;
};

export default function ITextField({
  value,
  type = 'default',
  label,
  placeholder,
  icon,
  className,
  rows = 4,
  size = 'medium',
  isBold,
}: Props) {
  const classes = useStyles();
  if (type === 'textarea') {
    return (
      <TextField
        id="outlined-multiline-static"
        className={clsx(classes.textarea, className, isBold && classes.textBold)}
        multiline
        rows={rows}
        defaultValue={value}
        variant="outlined"
        placeholder={placeholder}
      />
    );
  }
  if (type !== 'default') {
    return (
      <TextField
        id={type}
        label={label}
        type={type}
        defaultValue={value}
        className={clsx(classes.datetime, size, className, isBold && classes.textBold)}
        InputLabelProps={{
          shrink: true,
        }}
        variant="outlined"
      />
    );
  }
  return (
    <div className={clsx(classes.root, className, size)}>
      {icon ? (
        <TextField
          id="outlined-start-adornment"
          value={value}
          placeholder={placeholder}
          label={label}
          className={clsx(classes.margin, classes.textField, className, isBold && classes.textBold)}
          InputProps={{
            startAdornment: <InputAdornment position="start">{icon}</InputAdornment>,
          }}
          variant="outlined"
        />
      ) : (
        <TextField
          id="outlined-start-adornment"
          value={value}
          placeholder={placeholder}
          label={label}
          className={clsx(classes.margin, classes.textField, className, isBold && classes.textBold)}
          variant="outlined"
        />
      )}
    </div>
  );
}
