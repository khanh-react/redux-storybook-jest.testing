import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';
import React from 'react';

import useOnClickOutside from './useOnClickOutside';

const useStyle = makeStyles({
  root: () => ({
    width: '100%',
    height: '100vh',
    position: 'fixed',
    top: 0,
    right: 0,
    zIndex: 999,
    '& .wrapper': {
      height: '100% !important',
      border: '1px solid #eeeeee',
      alignItems: 'center',
      position: 'relative',
      background: '#ffffff',
      float: 'right',
      zIndex: 1,
      '&.default': {
        width: 670,
      },
      '&.medium': {
        width: 562,
      },
      '&.fullscreen': {
        width: '100%',
        height: '100%',
      },
    },
    '& .overlay': {
      width: '100%',
      height: '100vh',
      background: '#000000',
      opacity: 0.3,
    },
  }),
});

type FilterModalProps = {
  className?: string;
  size?: 'small' | 'medium' | 'large' | 'default' | 'fullscreen';
  children?: React.ReactNode;
  handleCancel?: () => void;
};

export default (props: FilterModalProps) => {
  const { root } = useStyle();
  const { className, size = 'default', children, handleCancel } = props;
  const ref1 = React.useRef(null);

  useOnClickOutside(ref1, () => {
    handleCancel && handleCancel();
  });
  return (
    <div id="filter-modal" className={`${root} ${className}`}>
      <div className={clsx('wrapper', size)} ref={ref1}>
        {children}
      </div>
      <div className="overlay" />
    </div>
  );
};
