import { fireEvent } from '@testing-library/dom';
import { act, renderHook } from '@testing-library/react-hooks';

import useKeyPress from './useKeypress';

describe('useKeyPress', () => {
  it('should render the hook', () => {
    const { result } = renderHook(() => useKeyPress('Escape'));
    expect(result.current).toBe(false);
  });

  it('should return false when a different key is pressed', () => {
    const { result } = renderHook(() => useKeyPress('Escape'));

    act(() => {
      fireEvent(
        window,
        new KeyboardEvent('keydown', {
          key: 'Enter',
        }),
      );
    });

    expect(result.current).toBe(false);
  });

  it('should return true when the expected key is pressed', () => {
    const { result } = renderHook(() => useKeyPress('Escape'));

    act(() => {
      fireEvent(
        window,
        new KeyboardEvent('keydown', {
          key: 'Escape',
        }),
      );
    });

    expect(result.current).toBe(true);
  });

  it('should return true when the expected key is pressed and false when unpressed', () => {
    const { result } = renderHook(() => useKeyPress('Escape'));

    act(() => {
      fireEvent(
        window,
        new KeyboardEvent('keydown', {
          key: 'Escape',
        }),
      );
    });

    expect(result.current).toBe(true);

    act(() => {
      fireEvent(
        window,
        new KeyboardEvent('keyup', {
          key: 'Escape',
        }),
      );
    });

    expect(result.current).toBe(false);
  });

  it('should handle multiple keys', () => {
    const { result } = renderHook(() => useKeyPress('Escape'));

    act(() => {
      fireEvent(
        window,
        new KeyboardEvent('keydown', {
          key: 'Enter',
        }),
      );
    });

    expect(result.current).toBe(false);

    act(() => {
      fireEvent(
        window,
        new KeyboardEvent('keydown', {
          key: 'Escape',
        }),
      );
    });

    expect(result.current).toBe(true);

    act(() => {
      fireEvent(
        window,
        new KeyboardEvent('keyup', {
          key: 'Enter',
        }),
      );
    });

    expect(result.current).toBe(true);

    act(() => {
      fireEvent(
        window,
        new KeyboardEvent('keyup', {
          key: 'Escape',
        }),
      );
    });

    expect(result.current).toBe(false);
  });
});
