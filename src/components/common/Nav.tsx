import { Box, Theme, useTheme } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import React from 'react';

import { Flex, PointerFlex } from './Flex';
import Icon from './Icon';
import { IbMenu, IbMenuItem, useIbMenu } from './Menu';

export const Nav = (
  props: React.PropsWithChildren<{
    backButton?: boolean;
    title?: string;
    onBack?: () => void;
    actions?: { key: string; title: string }[];
    onAction?: (key: string) => void;
    drawer?: boolean;
  }>,
) => {
  const theme = useTheme<Theme>();

  const { anchorEl, handleClick, handleClose } = useIbMenu();

  const handleAction = (action: string) => () => {
    if (props.onAction) {
      props.onAction(action);
    }
    handleClose();
  };

  return (
    <Flex alignItems="center" height={props.drawer ? 72 : 68} borderBottom="1px solid #EEEEEE" px="10px">
      {props.backButton && (
        <PointerFlex onClick={props.onBack}>
          <Icon name="back" />
        </PointerFlex>
      )}
      <Flex alignItems="center" justifyContent="space-between" flex={1}>
        <Flex
          fontFamily={theme.typography.fontFamily}
          fontWeight="bold"
          fontSize={16}
          lineHeight="20px"
          ml="19px"
          color="0F0F10"
          flex={1}
        >
          {props.title}
        </Flex>

        <Flex>
          {props.children}
          {props.actions && (
            <PointerFlex position="relative" top={-4}>
              <Box
                aria-controls="customized-menu"
                aria-haspopup="true"
                onClick={handleClick}
                mb="-10px"
                color="#2E2E32"
              >
                <MoreVertIcon />
              </Box>
              <IbMenu id="filter-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
                {props.actions.map(({ key, title }) => (
                  <IbMenuItem key={key} onClick={handleAction(key)}>
                    {title}
                  </IbMenuItem>
                ))}
              </IbMenu>
            </PointerFlex>
          )}
        </Flex>
      </Flex>
    </Flex>
  );
};
