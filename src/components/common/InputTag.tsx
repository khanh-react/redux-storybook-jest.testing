import { Box, IconButton, makeStyles, withStyles } from '@material-ui/core';
import CancelRoundedIcon from '@material-ui/icons/CancelRounded';
import React from 'react';

export interface InputTagProps {
  label: string;
  hasCloseIcon: boolean;
  onClose?: (id: string) => void;
  id?: string;
}

export const StyledBox = withStyles((theme) => ({
  root: {
    display: 'inline-flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: '30px',
    backgroundColor: '#FFF8DA',
    height: '32px',
    boxSizing: 'border-box',
    MozBoxSizing: 'border-box',
    WebkitBoxSizing: 'border-box',
    '&:hover': {
      backgroundColor: '#FFF2BC',
    },
    '& span': {
      paddingLeft: 10,
      fontFamily: theme.typography.fontFamily,
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: 13,
      letterSpacing: '0.073125px',
    },
  },
}))(Box);

const useStyles = makeStyles(() => ({
  CancelRoundedIcon1: {
    '& svg': {
      fontSize: 18.8,
    },
    backgroundColor: 'transparent',
    padding: 0,
    margin: 5,
  },
}));
function InputTag(props: InputTagProps) {
  const classes = useStyles();
  return (
    <StyledBox>
      <span>{props.label}</span>
      {props.hasCloseIcon && (
        <IconButton
          onClick={() => {
            props.onClose && props.id && props.onClose(props.id);
          }}
          className={classes.CancelRoundedIcon1}
          id="close-button"
        >
          <CancelRoundedIcon />
        </IconButton>
      )}
    </StyledBox>
  );
}

export default InputTag;
