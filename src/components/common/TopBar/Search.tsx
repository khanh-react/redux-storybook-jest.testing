import React from 'react';
import { Tooltip, IconButton, SvgIcon } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/SearchOutlined';

const Search = () => {
  return (
    <Tooltip title="Search">
      <IconButton color="inherit">
        <SvgIcon fontSize="small">
          <SearchIcon />
        </SvgIcon>
      </IconButton>
    </Tooltip>
  );
};

export default Search;
