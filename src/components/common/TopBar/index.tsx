import React from 'react';

import { Flex } from '../Flex';
import Search from './Search';

const TopBar = () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  return (
    <Flex minHeight={64} width="100%" alignItems="stretch" pl="11px" pr="40px">
      {/* Search */}
      <Flex flexGrow={1} alignItems="center">
        <Search />
      </Flex>
      <Flex alignItems="center">
        <Search />
        <Search />
        <Search />
      </Flex>
    </Flex>
  );
};

export default TopBar;
