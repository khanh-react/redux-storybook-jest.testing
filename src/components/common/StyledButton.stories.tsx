import React from 'react';

import { Flex } from './Flex';
import StyledButton from './StyledButton';

export default {
  title: 'Components/StyledButton',
  component: StyledButton,
};

export const Default = () => {
  return (
    <Flex>
      <Flex color="white">
        <StyledButton bgColor="#f16f63" color="inherit" variant="contained">
          Delete
        </StyledButton>
        <Flex ml="20px">
          <StyledButton variant="outlined">Cancel</StyledButton>
        </Flex>
        <Flex ml="20px">
          <StyledButton bgColor="black" color="inherit" variant="contained" disableElevation>
            Update
          </StyledButton>
        </Flex>
      </Flex>
    </Flex>
  );
};
