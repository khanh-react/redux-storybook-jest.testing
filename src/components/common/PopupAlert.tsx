import React from 'react';
import styled from 'styled-components';
import { Flex } from './Flex';
import Button from './Button';
import Text from './Text';

type Props = {
  onSubmit: () => void;
  onCancel: () => void;
};

const WrapPopUp = styled.div`
  z-index: 10;
  width: 600px;
  height: 205px;
  border-top: 6px solid #ed6e62;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #fff;
  padding: 16px 25px;
  .text-title {
    margin-bottom: 30px;
  }
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
const RgbBackground = styled.div`
  width: 100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.4);
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9;
`;
function PopupAlert({ onSubmit, onCancel }: Props) {
  return (
    <>
      <WrapPopUp>
        <Flex flexDirection="column">
          <Text size="xl" weight="bold" value="Confirmation" className="text-title" />
          <Text size="md" value="Are you sure you want to delete this file? You can’t undo this action." />
        </Flex>
        <Flex justifyContent="flex-end">
          <Flex className="options">
            <Button kind="default" onClick={onCancel}>
              Cancel
            </Button>
            <Button kind="danger" onClick={onSubmit}>
              Confirm
            </Button>
          </Flex>
        </Flex>
      </WrapPopUp>
      <RgbBackground />
    </>
  );
}

export default PopupAlert;
