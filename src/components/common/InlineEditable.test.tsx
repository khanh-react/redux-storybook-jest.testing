import '@testing-library/jest-dom/extend-expect';
import { fireEvent, queryByAttribute, render, screen } from '@testing-library/react';
import React from 'react';

import InlineEditable from './InlineEditable';

const getById = queryByAttribute.bind(null, 'id');
describe('InlineEditable', () => {
  it('It should call onSetText when text change', () => {
    const onSetText = jest.fn();
    const { container } = render(
      <InlineEditable text="Click" placeholder="Click here to edit!" onSetText={onSetText} />,
    );
    const inlineText = getById(container, 'inline-text') as HTMLSpanElement;
    fireEvent.click(inlineText);
    const input = screen.getByPlaceholderText('Click here to edit!') as HTMLInputElement;
    fireEvent.change(input, { target: { value: 'test' } });
    expect(onSetText).toBeCalled();
  });
  it('It should show span with proper keypress', () => {
    const onSetText = jest.fn();
    const { container } = render(
      <InlineEditable text="Click" placeholder="Click here to edit!" onSetText={onSetText} />,
    );
    const inlineText = getById(container, 'inline-text') as HTMLSpanElement;
    fireEvent.click(inlineText);
    const input = screen.getByPlaceholderText('Click here to edit!') as HTMLInputElement;
    fireEvent.keyDown(input, { key: 'Enter', code: 'Enter' });
    expect(screen.getByText('Click')).toBeInTheDocument();
  });
});
