import React, { ImgHTMLAttributes } from 'react';

const MiniLogo = (props: React.PropsWithoutRef<ImgHTMLAttributes<unknown>>) => (
  <img alt="Logo" src="/static/imbee-crm-menu-logo.svg" {...props} />
);

export default MiniLogo;
