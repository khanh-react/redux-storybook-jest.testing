import React from 'react';
import { NoProps } from '../../utils/types';

const Logo = (props: React.PropsWithoutRef<NoProps>) => <img alt="Logo" src="/static/imbee-crm-logo.svg" {...props} />;

export default Logo;
