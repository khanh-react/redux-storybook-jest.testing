import { Tabs } from '@material-ui/core';
import styled from 'styled-components';

export const IbTabs = styled(Tabs)`
  ${({ theme }) => `
  border-bottom: 1px solid #dcdce4;
  margin-top: 15px;
  .MuiTabs-indicator {
    background-color: #ffdb36;
  }
  .MuiTab-root {
    font-family: ${theme.typography.fontFamily};
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    color: #98999f;
    min-width: 100px;
  }
  .Mui-selected {
      color: #FFDB36;
      font-weight: bold;
  }
  `}
`;
