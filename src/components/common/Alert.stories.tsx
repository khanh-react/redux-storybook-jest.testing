/* eslint-disable react/jsx-one-expression-per-line */
import { Box } from '@material-ui/core';
import React from 'react';

import Alert from './Alert';

export const Big = () => (
  <Box display="flex" flexDirection="column" gridRowGap="10px">
    <Alert type="success" message="Hello I am Alert" />
    <Alert type="error" message="Hello I am Alert" />
    <Alert type="warn" message="Hello I am Alert" />
    <Alert type="info" message="Hello I am Alert" />
    <Alert type="success">
      Hello I am Alert, I want to go to
      <a href="https://material-ui.com/" target="blank">
        here
      </a>
    </Alert>
    <Alert type="info">
      Hello I am <b>Alert</b>
    </Alert>
  </Box>
);

export const Small = () => (
  <Box display="flex" flexDirection="column" gridRowGap="10px">
    <Alert type="success" message="Hello I am Alert" variant="small" />
    <Alert type="warn" message="Hello I am Alert" variant="small" />
    <Alert type="error" message="Hello I am Alert" variant="small" />
    <Alert type="info" message="Hello I am Alert" variant="small" />
    <Alert type="success" variant="small">
      Hello I am Alert, I want to go to{' '}
      <a href="https://material-ui.com/" target="blank">
        here
      </a>
    </Alert>
    <Alert type="info" variant="small">
      Ticket <b># 3890</b> Open by Rachel Chan
    </Alert>
  </Box>
);

export default {
  title: 'Components/Alert',
  component: Alert,
};
