import React from 'react';
import styled, { keyframes } from 'styled-components';

type Props = {
  top: number;
};
function LoadingMessage({ top }: Props) {
  const loadingAni = keyframes`
from {
  opacity: 1;
  backface-visibility: hidden;
  transform: translateZ(0) scale(1.5, 1.5);
}
to {
  opacity: 0;
  backface-visibility: hidden;
  transform: translateZ(0) scale(1, 1);
}
`;

  const WrapLoading = styled.div`
    width: 44px;
    height: 44px;
    display: inline-block;
    overflow: hidden;
    background: none;
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    top: ${() => (top ? `${top}px` : 0)};
    z-index: 10;
  `;
  const WrapLoadingSecond = styled.div`
    width: 100%;
    height: 100%;
    position: relative;
    transform: translateZ(0) scale(0.44);
    backface-visibility: hidden;
    transform-origin: 0 0;
    & > * {
      box-sizing: content-box;
    }
    & div > div {
      position: absolute;
      width: 12px;
      height: 12px;
      border-radius: 50%;
      background: #ffd83c;
      animation: ${loadingAni} 0.9900990099009901s linear;
      animation-iteration-count: infinite;
    }
    & div:nth-child(1) > div {
      left: 74px;
      top: 44px;
      animation-delay: -0.8663366336633663s;
    }
    & > div:nth-child(1) {
      transform: rotate(0deg);
      transform-origin: 80px 50px;
    }
    & div:nth-child(2) > div {
      left: 65px;
      top: 65px;
      animation-delay: -0.7425742574257426s;
    }
    & > div:nth-child(2) {
      transform: rotate(45deg);
      transform-origin: 71px 71px;
    }
    & div:nth-child(3) > div {
      left: 44px;
      top: 74px;
      animation-delay: -0.6188118811881188s;
    }
    & > div:nth-child(3) {
      transform: rotate(90deg);
      transform-origin: 50px 80px;
    }
    & div:nth-child(4) > div {
      left: 23px;
      top: 65px;
      animation-delay: -0.49504950495049505s;
    }
    & > div:nth-child(4) {
      transform: rotate(135deg);
      transform-origin: 29px 71px;
    }
    & div:nth-child(5) > div {
      left: 14px;
      top: 44px;
      animation-delay: -0.3712871287128713s;
    }
    & > div:nth-child(5) {
      transform: rotate(180deg);
      transform-origin: 20px 50px;
    }
    & div:nth-child(6) > div {
      left: 23px;
      top: 23px;
      animation-delay: -0.24752475247524752s;
    }
    & > div:nth-child(6) {
      transform: rotate(225deg);
      transform-origin: 29px 29px;
    }
    & div:nth-child(7) > div {
      left: 44px;
      top: 14px;
      animation-delay: -0.12376237623762376s;
    }
    & > div:nth-child(7) {
      transform: rotate(270deg);
      transform-origin: 50px 20px;
    }
    & div:nth-child(8) > div {
      left: 65px;
      top: 23px;
      animation-delay: 0s;
    }
    & > div:nth-child(8) {
      transform: rotate(315deg);
      transform-origin: 71px 29px;
    }
  `;
  return (
    <WrapLoading>
      <WrapLoadingSecond>
        <div>
          <div />
        </div>
        <div>
          <div />
        </div>
        <div>
          <div />
        </div>
        <div>
          <div />
        </div>
        <div>
          <div />
        </div>
        <div>
          <div />
        </div>
        <div>
          <div />
        </div>
        <div>
          <div />
        </div>
      </WrapLoadingSecond>
    </WrapLoading>
  );
}

export default LoadingMessage;
