import { fireEvent, render } from '@testing-library/react';
import React, { useRef } from 'react';

import useOnClickOutside from './useOnClickOutside';

interface Callback<T extends Event = Event> {
  (event: T): void;
}
interface Props {
  callback: Callback;
}

const TestComponent = ({ callback }: Props) => {
  const ref1 = useRef(null);
  useOnClickOutside(ref1, callback);

  return (
    <>
      <div data-testid="ref-1" ref={ref1} />
      <div data-testid="out-1" />
    </>
  );
};
describe('useOnclickOutside', () => {
  Object.defineProperties(window.HTMLHtmlElement.prototype, {
    clientWidth: { value: 100 },
    clientHeight: { value: 100 },
  });

  const setup = () => {
    const cb = jest.fn();
    const { getByTestId } = render(<TestComponent callback={cb} />);

    return { cb, getByTestId };
  };

  it('should not trigger callback when clicks (touches) inside of the targets', () => {
    const { cb, getByTestId } = setup();
    const ref1 = getByTestId('ref-1');

    fireEvent.mouseDown(ref1);
    fireEvent.touchStart(ref1);

    expect(cb).not.toHaveBeenCalled();
  });
  it('should trigger callback when clicks outside (touches) of the targets', () => {
    const { cb, getByTestId } = setup();
    const outside = getByTestId('out-1');
    fireEvent.mouseDown(outside);
    fireEvent.touchStart(outside);
    expect(cb).toHaveBeenCalledTimes(2);
  });
});
