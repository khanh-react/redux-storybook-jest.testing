import Button, { ButtonProps } from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import React from 'react';

export interface ImButtonProps extends ButtonProps {
  kind: 'default' | 'primary' | 'dark' | 'danger' | 'warning' | 'cancel';
  icon?: React.ReactNode;
}

const BootstrapButton = withStyles({
  root: {
    boxShadow: 'none',
    textTransform: 'none',
    padding: '6px 12px',
    border: '1px solid',
    borderRadius: 10,
    height: '40px',
    minWidth: 86,
    fontFamily: 'Source Sans Pro',
    fontStyle: 'normal',
    fontSize: 15,
    lineHeight: 20,
    letterSpacing: 0.3,
    fontStretch: 'normal',
    '&.MuiButtonBase-root': {
      marginRight: 8,
    },
    '&:hover': {
      backgroundColor: '#0069d9',
      borderColor: '#0062cc',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#0062cc',
      borderColor: '#005cbf',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
    '&.primary': {
      backgroundColor: '#097af4',
      color: '#ffffff',
    },
    '&.dark': {
      backgroundColor: '#0F0F10',
      color: '#ffffff',
    },
    '&.danger': {
      backgroundColor: '#f16f63',
      color: '#ffffff',
      '&:hover': {
        borderColor: '#fff',
      },
    },
    '&.warning': {
      backgroundColor: '#e54545',
      color: '#ffffff',
      fontWeight: '600',
      width: '100px',
      height: '36px',
      borderRadius: '8px',
      margin: 0,
    },
    '&.cancel': {
      backgroundColor: '#ffffff',
      color: '#263238',
      border: '1px solid #6C6C72',
      fontWeight: '600',
      width: '100px',
      height: '36px',
      borderRadius: '8px',
      marginRight: 10,
    },
    '&.default': {
      backgroundColor: '#ffffff',
      color: '#263238',
      border: '1px solid #6C6C72',
      fontWeight: 'bolder',
    },
  },
})(Button);

export default ({ kind: type, icon, ...props }: ImButtonProps) => {
  return (
    <BootstrapButton
      className={`${type} ${props.className} `}
      variant="contained"
      color="primary"
      disableRipple
      startIcon={icon}
      {...props}
    />
  );
};
