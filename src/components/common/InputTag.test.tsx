import '@testing-library/jest-dom/extend-expect';

import { fireEvent, queryByAttribute, render } from '@testing-library/react';
import React from 'react';

import InputTag from './InputTag';

const getById = queryByAttribute.bind(null, 'id');

describe('InputTag', () => {
  it('It should call onClose after click close button', () => {
    const onClose = jest.fn();
    const { container } = render(<InputTag label="Test" hasCloseIcon onClose={onClose} id="test" />);
    const closeButton = getById(container, 'close-button') as HTMLElement;
    fireEvent.click(closeButton);
    expect(onClose).toBeCalled();
  });
});
