import { autoColors } from './AutoSelectColors';

describe('AutoSelectColors', () => {
  it('should return result matched provided value', () => {
    const input = 0;
    const output = {
      textColor: '#626264',
      backgroundColor: '#D4D4D4',
    };
    const input1 = 8;
    expect(autoColors(input)).toEqual(output);
    expect(autoColors(input1)).toEqual(output);
  });
});
