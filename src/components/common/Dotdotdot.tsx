import React, { useState, useRef } from 'react';
import styled from 'styled-components';
import VisibilityIcon from '@material-ui/icons/VisibilityOutlined';
import DeleteForeverIcon from '@material-ui/icons/DeleteForeverOutlined';
import { NotInterested, MailOutline } from '@material-ui/icons';
import EditIcon from '@material-ui/icons/Edit';
import { CenterFlex, Flex } from './Flex';
import Text from './Text';
import useOnClickOutside from './useOnClickOutside';

const Wrapper = styled.div`
  width: 20px;
  height: 20px;
  margin: 0 auto;
  position: relative;
  &:hover {
    cursor: pointer;
  }
  .dot {
    height: 3px;
    width: 3px;
    background-color: #000;
    border-radius: 50%;
    margin-bottom: 2px;
  }
  .options {
    position: absolute;
    top: 10;
    left: -90px;
    width: 120px;
    border-radius: 8px;
    padding-top: 5px;
    box-shadow: 0 2px 10px 0 rgba(175, 175, 175, 0.5);
    background-color: #ffffff;
    z-index: 10;
    .option {
      width: 100%;
      margin-bottom: 5px;
      padding: 5px 10px 7px 10px;
      border-radius: 3px;
      &:hover {
        background-color: #fff8da;
      }
    }
    .text {
      margin-left: 8px;
    }
  }
`;

type Props = {
  actions: ('VIEW' | 'EDIT' | 'DELETE' | 'DISABLE' | 'RE-INVITE')[];
  submit?: (value: string) => void;
};

export const Dotdotdot = ({ actions, submit }: Props) => {
  const optionsRef = useRef(null);
  const [isOpened, setOpen] = useState(false);

  useOnClickOutside(optionsRef, () => setOpen(false));
  const onSubmit = (action: string) => {
    setOpen(false);
    submit && submit(action);
  };
  return (
    <>
      <Wrapper>
        <CenterFlex flexDirection="column" onClick={() => setOpen(true)}>
          <div className="dot" />
          <div className="dot" />
          <div className="dot" />
        </CenterFlex>
        {isOpened && (
          <div ref={optionsRef} className="options">
            {actions.map((action) => (
              <Flex onClick={() => onSubmit(action)} key={action}>
                {action === 'VIEW' && (
                  <Flex alignItems="center" className="option">
                    <VisibilityIcon fontSize="small" />
                    <Text value="View" />
                  </Flex>
                )}
                {action === 'EDIT' && (
                  <Flex alignItems="center" className="option">
                    <EditIcon fontSize="small" />
                    <Text value="Edit" />
                  </Flex>
                )}
                {action === 'DELETE' && (
                  <Flex alignItems="center" className="option">
                    <DeleteForeverIcon fontSize="small" />
                    <Text value="Delete" />
                  </Flex>
                )}
                {action === 'DISABLE' && (
                  <Flex alignItems="center" className="option">
                    <NotInterested fontSize="small" />
                    <Text value="Disable" />
                  </Flex>
                )}
                {action === 'RE-INVITE' && (
                  <Flex alignItems="center" className="option">
                    <MailOutline fontSize="small" />
                    <Text value="Re-invite" />
                  </Flex>
                )}
              </Flex>
            ))}
          </div>
        )}
      </Wrapper>
    </>
  );
};

export default Dotdotdot;
