import { Box } from '@material-ui/core';
import { AccessTimeRounded } from '@material-ui/icons';
import React from 'react';

import Text, { TextProps } from './Text';

export default {
  title: 'Components/Text',
  component: Text,
  argTypes: {
    size: {
      control: {
        type: 'inline-radio',
        options: ['xl', 'lg', 'md', 'sm'],
      },
    },
    color: {
      control: {
        type: 'color',
      },
    },
  },
};

export const Primary = (props: Partial<TextProps>) => {
  return (
    <Box display="flex" flexDirection="column" gridColumnGap="10px">
      <Text value="X-Large Text Without Icon" size={props.size || 'xl'} color={props.color} />
      <Text value="Large Text Without Icon" size={props.size || 'lg'} color={props.color} />
      <Text value="Medium Text Without Icon" size={props.size || 'md'} color={props.color} />
      <Text value="Small Text Without Icon" size={props.size || 'sm'} color={props.color} />
      <br />
      <Text value="X-Large Text Within Icon" size="xl" color={props.color} icon={<AccessTimeRounded />} />
      <Text value="Large Text Within Icon" size={props.size || 'lg'} color={props.color} icon={<AccessTimeRounded />} />
      <Text
        value="Medium Text Within Icon"
        size={props.size || 'md'}
        color={props.color}
        icon={<AccessTimeRounded />}
      />
      <Text value="Small Text Within Icon" size={props.size || 'sm'} color={props.color} icon={<AccessTimeRounded />} />
    </Box>
  );
};
