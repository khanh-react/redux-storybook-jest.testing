import React from 'react';

import Button from './Button';

export const Default = () => {
  return (
    <div>
      <Button kind="default">Default Button</Button>
      <Button kind="primary">Primary Button</Button>
      <Button kind="dark">Dark Button</Button>
    </div>
  );
};

export default {
  title: 'Components/Button',
  component: Button,
};
