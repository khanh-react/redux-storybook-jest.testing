import React from 'react';

import MiniLogo from './MiniLogo';

export default {
  title: 'Components/MiniLogo',
  component: MiniLogo,
};

export const Primary = () => <MiniLogo />;
