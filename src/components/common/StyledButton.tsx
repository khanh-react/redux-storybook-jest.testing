import { Button, ButtonProps } from '@material-ui/core';
import { omit } from 'lodash';
import React from 'react';
import styled from 'styled-components';

const StyledButton = styled((props: ButtonProps & { bgColor?: string; width?: string }) => (
  <Button {...omit(props, ['bgColor', 'width'])} />
))`
  .MuiButton-label {
    font-family: ${({ theme }) => theme.typography.fontFamily};
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    text-transform: none;
  }
  &&:hover {
    background-color: ${(props) => props.bgColor} !important;
  }
  border-radius: 8px;
  height: 36px;
  box-shadow: none;
  background: ${(props) => props.bgColor};
  width: ${(props) => props.width};
`;

export default StyledButton;
