import { IconName } from '../Icon';

export type NavConfig = {
  title: string;
  icon?: IconName;
  href?: string;
  items?: NavConfig[];
};

export type NavGroupConfig = {
  subheader: string;
  items: NavConfig[];
};

const navConfig: NavGroupConfig[] = [
  {
    subheader: 'dashboard',
    items: [
      {
        title: 'Dashboard',
        icon: 'nav-dashboard',
        // href: '/cms/reports/dashboard',
      },
    ],
  },
  {
    subheader: 'inbox',
    items: [
      {
        title: 'Inbox',
        icon: 'nav-chat',
        href: '/cms/chat',
      },
    ],
  },
  {
    subheader: 'contacts',
    items: [
      {
        title: 'Contacts',
        icon: 'chat-off',
        href: '/cms/contacts',
      },
    ],
  },
  {
    subheader: 'chat-ticket',
    items: [
      {
        title: 'Chat Ticket',
        icon: 'chat-ticket',
        // href: '/cms/contacts',
      },
    ],
  },
  {
    subheader: 'automatic',
    items: [
      {
        title: 'Automatic',
        icon: 'automatic',
        // href: '/cms/contacts',
        items: [{ title: 'Broadcast' }, { title: 'Chatbox' }],
      },
    ],
  },
  {
    subheader: 'invoicing',
    items: [
      {
        title: 'Invoicing',
        icon: 'nav-billing',
        // href: '/cms/account',
      },
    ],
  },
  {
    subheader: 'coupon',
    items: [
      {
        title: 'Coupon',
        icon: 'coupon',
        // href: '/cms/account',
      },
    ],
  },
  {
    subheader: 'calendar',
    items: [
      {
        title: 'Calendar',
        icon: 'nav-event',
        // href: '/cms/calendar',
      },
    ],
  },
  {
    subheader: 'monitor',
    items: [
      {
        title: 'Monitor',
        icon: 'monitor',
        // href: '/cms/schedule-message',
      },
    ],
  },
  {
    subheader: 'report',
    items: [
      {
        title: 'Report',
        icon: 'report',
        // href: '/cms/schedule-message',
      },
    ],
  },

  {
    subheader: 'account',
    items: [
      {
        title: 'Account',
        icon: 'nav-contact',
        // href: '/cms/contacts',
        items: [{ title: 'Billing' }, { title: 'Subscription' }, { title: 'Referral' }, { title: 'Profile' }],
      },
    ],
  },
  {
    subheader: 'settings',
    items: [
      {
        title: 'Settings',
        icon: 'nav-setting',
        // href: '/cms/settings',
        items: [
          { title: 'Users' },
          { title: 'Teams' },
          { title: 'Roles' },
          { title: 'Skills' },
          { title: 'Triggers' },
          { title: 'Routing' },
          { title: 'Channel' },
          { title: 'Quick Replies', href: '/cms/setting/quick-reply' },
          { title: 'Tags' },
        ],
      },
    ],
  },
];

export default navConfig;
