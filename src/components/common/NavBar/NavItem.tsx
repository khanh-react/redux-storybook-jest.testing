import { Button, Collapse, ListItem } from '@material-ui/core';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import styled from 'styled-components';
import { AppState } from '../../../redux/store';
import { Flex } from '../Flex';
import { setLeftNavRoute } from '../../../redux/action/util';
import Icon, { IconName } from '../Icon';

const StyledItem = styled(ListItem)`
  display: block;
  padding-top: 0;
  padding-bottom: 0;
  width: 100%;
`;

const StyledItemLeaf = styled(ListItem)`
  display: flex;
  align-items: stretch;
  padding-top: 0;
  padding-bottom: 0;
`;

const StyledButton = styled(Button)`
  justify-content: flex-start;
  width: 100%;
  height: 55px;
  letter-spacing: 0;
  text-transform: none;
  font-size: 14px;
  font-weight: 600;
  padding-left: 28px;
  padding-right: 28px;
  border-radius: 0px;
  .MuiButton-label span {
    margin-left: 10px;
  }
  box-sizing: border-box;
  color: #ae9629;
  &.active {
    color: #0f0f10;
  }
`;

type NavItemProps = {
  title: string;
  href?: string;
  depth: number;
  children?: React.ReactNode;
  icon?: IconName;
  className?: string;
  open: boolean;
  info?: () => JSX.Element;
};

const NavItem = (props: NavItemProps) => {
  const leftNavRout = useSelector((s: AppState) => s.util.leftNavRoute);
  const { title, depth, children, icon, className, info: Info, ...rest } = props;
  const dispatch = useDispatch();
  const handleToggle = () => {
    if (props.title === leftNavRout) {
      dispatch(setLeftNavRoute(''));
    } else {
      dispatch(setLeftNavRoute(props.title));
    }
  };
  const isOpenCollaps = useMemo(() => {
    return leftNavRout === props.title;
  }, [leftNavRout, props.title]);
  const history = useHistory();
  const location = useLocation();
  const onClick = React.useCallback(() => {
    if (props.href) history.push(props.href);
  }, [history, props.href]);

  let paddingLeft = 0;

  if (depth > 0) {
    paddingLeft = 32 + 8 * depth;
  }

  const style = paddingLeft ? { paddingLeft } : undefined;
  if (children) {
    return (
      <StyledItem button className={className} disableGutters key={title} {...rest}>
        <StyledButton className={className} onClick={handleToggle} style={style}>
          <Flex justifyContent="space-between" width="100%">
            <Flex>
              {icon && <Icon name={icon} />}
              <span>{title}</span>
            </Flex>
            {isOpenCollaps ? <ExpandLessIcon className="icon-expand" /> : <ExpandMoreIcon className="icon-expand" />}
          </Flex>
        </StyledButton>
        <Collapse in={isOpenCollaps}>{children}</Collapse>
      </StyledItem>
    );
  }
  const active = location.pathname === props.href;

  return (
    <StyledItemLeaf button className={className} disableGutters key={title} {...rest}>
      <StyledButton
        className={(className ?? '') + (active ? 'active' : '')}
        style={style}
        onClick={() => {
          onClick();
          dispatch(setLeftNavRoute(''));
        }}
      >
        {icon && <Icon name={active ? (`${icon}-on` as IconName) : icon} />}
        <span>{title}</span>
        {Info && <Info />}
      </StyledButton>
    </StyledItemLeaf>
  );
};

NavItem.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  depth: PropTypes.number.isRequired,
  href: PropTypes.string,
  icon: PropTypes.any,
  info: PropTypes.any,
  open: PropTypes.bool,
  title: PropTypes.string.isRequired,
};

NavItem.defaultProps = {
  open: false,
};

export default NavItem;
