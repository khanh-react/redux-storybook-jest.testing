import '@testing-library/jest-dom/extend-expect';

import { fireEvent, queryByAttribute, render } from '@testing-library/react';
import React from 'react';

import Tag from './Tag';

const getById = queryByAttribute.bind(null, 'id');

describe('InputTag', () => {
  it('It should change bgcolor when mouse enter/leave', () => {
    const { container } = render(<Tag color="blue" name="Test" outline />);
    const colorTag = getById(container, 'tag') as HTMLElement;
    fireEvent.mouseEnter(colorTag);
    expect(colorTag).toHaveStyle(`background-color: rgb(232, 234, 251)`);
    fireEvent.mouseLeave(colorTag);
    expect(colorTag).toHaveStyle(`background-color: white`);
  });
});
