import { Button } from '@material-ui/core';
import styled from 'styled-components';

export const Touchable = styled(Button)`
  padding: 0px;
  border-radius: 0px;
  width: 100%;
`;
