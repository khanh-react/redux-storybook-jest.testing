import React from 'react';

import ChatsInfo from './ChatsInfo';

export default {
  title: 'Settings/Monitor/Component/ChatsInfo',
  component: ChatsInfo,
};

export const Default = () => {
  return (
    <ChatsInfo
      newChats={56}
      newChatsPercent="+25%"
      assignedChats={32}
      assignedChatsPercent="+33.5%"
      servingChats={24}
      servingChatsPercent="-2.5%"
    />
  );
};
