import React from 'react';

import { Flex } from '../../../common/Flex';
import { Container, StylePercentLess, StyleTitle, StyleValue } from './styles';

export const ChatsInfo = ({
  newChats,
  newChatsPercent,
  assignedChats,
  assignedChatsPercent,
  servingChats,
  servingChatsPercent,
  bgcolor,
}: {
  newChats?: number;
  newChatsPercent?: string;
  assignedChats?: number;
  assignedChatsPercent?: string;
  servingChats?: number;
  servingChatsPercent?: string;
  bgcolor?: string;
}) => {
  return (
    <Container bgcolor={bgcolor}>
      <Flex flexDirection="row" justifyContent="center">
        <Flex flexDirection="column" width="284px" my="15px" borderRight=" 1px solid #eeeeee">
          <StyleTitle>New chats</StyleTitle>

          <Flex justifyContent="center" alignItems="center" mt="15px" ml="45px">
            <StyleValue>{newChats}</StyleValue>
            <StylePercentLess color={newChatsPercent?.charAt(0)}>{newChatsPercent}</StylePercentLess>
          </Flex>
        </Flex>
        <Flex flexDirection="column" width="284px" my="15px" borderRight=" 1px solid #eeeeee">
          <StyleTitle>Assigned Chats</StyleTitle>

          <Flex justifyContent="center" alignItems="center" mt="15px" ml="45px">
            <StyleValue>{assignedChats}</StyleValue>
            <StylePercentLess color={assignedChatsPercent?.charAt(0)}>{assignedChatsPercent}</StylePercentLess>
          </Flex>
        </Flex>
        <Flex flexDirection="column" width="284px" my="15px">
          <StyleTitle>Serving Chats</StyleTitle>

          <Flex justifyContent="center" alignItems="center" mt="15px" ml="45px">
            <StyleValue>{servingChats}</StyleValue>
            <StylePercentLess color={servingChatsPercent?.charAt(0)}>{servingChatsPercent}</StylePercentLess>
          </Flex>
        </Flex>
      </Flex>
    </Container>
  );
};

export default ChatsInfo;
