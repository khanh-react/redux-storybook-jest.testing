import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import React from 'react';
import styled from 'styled-components';

import { Flex, PointerFlex } from '../../../common/Flex';
import { IbMenu, IbMenuItem, useIbMenu } from '../../../common/Menu';
import Text from '../../../common/Text';

export const dumbData: IAgentActivityTable[] = [
  {
    id: 1,
    name: 'William Wong',
    status: 'online',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
  {
    id: 2,
    name: 'Rachel Chan',
    status: 'away',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
  {
    id: 3,
    name: 'Wattana Carlson',
    status: 'invisible',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
  {
    id: 4,
    name: 'James Corden',
    status: 'online',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
  {
    id: 5,
    name: 'Rachel Chan',
    status: 'away',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
  {
    id: 6,
    name: 'James Corden',
    status: 'invisible',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
  {
    id: 7,
    name: 'Wattana Carlson',
    status: 'online',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
  {
    id: 8,
    name: 'James Corden',
    status: 'invisible',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
  {
    id: 9,
    name: 'Wattana Carlson',
    status: 'online',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
  {
    id: 10,
    name: 'Rachel Chan',
    status: 'away',
    assigned: 12,
    inProcess: 12,
    responseTime: '25s',
    chatDuration: '35m12s',
  },
];
const StyledHeader = styled(Text)`
  ${({ theme }) => `
    margin: 14px 0px 14px 20px;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 20px;
    letter-spacing: -0.05px;
    color: ${theme.palette.grey[800]};
  `}
`;

const StyleTableContainer = styled(TableContainer)`
  border-top: 1px solid rgba(224, 224, 224, 1);
`;

const StyledCenter = styled(TableCell)`
  text-align: center;
`;
const StyledName = styled(Text)`
  ${({ theme }) => `
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    letter-spacing: 0.3px;
    color: ${theme.palette.grey[800]};
`}
`;
const StyledFooter = styled(Text)`
  ${({ theme }) => `
    margin: 11px 9px 12px 0px;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 20px;
    letter-spacing: 0.1px;
    color: ${theme.palette.grey[800]};
`}
`;
const Container = styled(Flex)`
  ${({ theme }) => `
    box-shadow: 0 1px 2px 0 rgba(63, 63, 68, 0.15), 0 0 0 1px rgba(63, 63, 68, 0.05);;
    border-radius: 10px;
    background: ${theme.palette.background.default};
  `}
`;
const StyledStatus = styled(Flex)`
  ${({ bgcolor }) => `
  background: ${
    (bgcolor === 'online' && 'rgba(0, 198, 146, 0.12)') ||
    (bgcolor === 'invisible' && 'rgba(0, 0, 0, 0.09)') ||
    (bgcolor === 'away' && 'rgba(255, 149, 33, 0.15)')
  };
  border-radius: 5px;
  font-style: normal;
  font-weight: 600;
  padding: 5px;
  font-size: 12px;
  line-height: 12px;
  height: 20px;
  letter-spacing: 0.83px;
  text-transform: uppercase;
  color: ${
    (bgcolor === 'online' && '#00c692') || (bgcolor === 'invisible' && '#77777a') || (bgcolor === 'away' && '#ff9521')
  };
  display: inline-flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`}
`;
export type IAgentActivityTable = {
  id: number;
  name: string;
  status: string;
  assigned: number;
  inProcess: number;
  responseTime: string;
  chatDuration: string;
};

const TableMenu = () => {
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const handleAction = (action: string) => {
    handleClose();
    // eslint-disable-next-line no-console
    console.log(action);
  };
  return (
    <>
      <PointerFlex alignItems="center" onClick={handleClick}>
        <MoreVertIcon />
      </PointerFlex>
      <IbMenu
        id="filter-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 30,
          horizontal: -120,
        }}
      >
        <IbMenuItem onClick={() => handleAction('Unreplied')}>Unreplied</IbMenuItem>
        <IbMenuItem onClick={() => handleAction('Resolved')}>Resolved</IbMenuItem>
        <IbMenuItem onClick={() => handleAction('Last Response')}>Last Response</IbMenuItem>
        <IbMenuItem onClick={() => handleAction('Last online')}>Last online</IbMenuItem>
      </IbMenu>
    </>
  );
};
export const AgentActivityTable = ({ listData }: { listData?: IAgentActivityTable[] }) => {
  return (
    <Container flexDirection="column">
      <Flex justifyContent="space-between" alignItems="center">
        <StyledHeader>Agent Activity</StyledHeader>
        <Flex flexDirection="column" mr="15px">
          <TableMenu />
        </Flex>
      </Flex>
      <StyleTableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Assigned</TableCell>
              <TableCell>In Process</TableCell>
              <TableCell>Response Time</TableCell>
              <TableCell>Chat Duration</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listData && listData.length > 0 ? (
              listData?.map((item) => {
                return (
                  <TableRow key={item.id}>
                    <TableCell>
                      <StyledName>{item.name}</StyledName>
                    </TableCell>
                    <TableCell>
                      <StyledStatus bgcolor={item.status}>{item.status}</StyledStatus>
                    </TableCell>
                    <TableCell>{item.assigned}</TableCell>
                    <TableCell>{item.inProcess}</TableCell>
                    <TableCell>{item.responseTime}</TableCell>
                    <TableCell>{item.chatDuration}</TableCell>
                  </TableRow>
                );
              })
            ) : (
              <TableRow>
                <StyledCenter colSpan={6}>No data found</StyledCenter>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </StyleTableContainer>
      <PointerFlex justifyContent="flex-end" alignItems="center" mr="18px">
        <StyledFooter>See All</StyledFooter>
        <ArrowForwardIcon fontSize="small" />
      </PointerFlex>
    </Container>
  );
};

export default AgentActivityTable;
