import React from 'react';

import { Flex } from '../../../common/Flex';
import { Container, StylePercentLess, StyleTitle, StyleValue } from './styles';

export const TotalChats = ({
  title,
  value,
  percent,
  bgcolor,
}: {
  title: string;
  value?: number;
  percent?: string;
  bgcolor?: string;
}) => {
  return (
    <Container flexDirection="column" bgcolor={bgcolor}>
      <Flex flexDirection="column" width="420px" my="15px">
        <StyleTitle>{title}</StyleTitle>
        <Flex justifyContent="center" alignItems="center" mt="15px" ml="45px">
          <StyleValue>{value}</StyleValue>
          <StylePercentLess color={percent?.charAt(0)}>{percent}</StylePercentLess>
        </Flex>
      </Flex>
    </Container>
  );
};

export default TotalChats;
