import React from 'react';

import TotalChats from './TotalChats';

export default {
  title: 'Settings/Monitor/Component/TotalChats',
  component: TotalChats,
};

export const Default = () => {
  return <TotalChats title="Total Chats" value={130} percent="+45%" bgcolor="#ffe880" />;
};
