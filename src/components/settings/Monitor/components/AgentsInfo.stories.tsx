import React from 'react';

import AgentsInfo from './AgentsInfo';

export default {
  title: 'Settings/Monitor/Component/AgentsInfo',
  component: AgentsInfo,
};

export const Default = () => {
  return <AgentsInfo loggedUsers={25} onlineUsers={18} awayUsers={5} invisibleUsers={2} />;
};
