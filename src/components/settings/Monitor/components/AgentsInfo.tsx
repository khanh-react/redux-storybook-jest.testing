import React from 'react';

import { Flex } from '../../../common/Flex';
import { Container, StyleTitle, StyleValue } from './styles';

export const AgentsInfo = ({
  loggedUsers,
  onlineUsers,
  awayUsers,
  invisibleUsers,
  bgcolor,
}: {
  loggedUsers?: number;
  onlineUsers?: number;
  awayUsers?: number;
  invisibleUsers?: number;
  bgcolor?: string;
}) => {
  return (
    <Container bgcolor={bgcolor}>
      <Flex
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        my="15px"
        borderRight=" 1px solid #eeeeee"
      >
        <Flex width="163px" height="31px" m="20px 35px 15px 45px" justifyContent="center" alignItems="center">
          <StyleTitle>Agents Logged in </StyleTitle>
        </Flex>
        <Flex mb="30px">
          <StyleValue>{loggedUsers}</StyleValue>
        </Flex>
      </Flex>
      <Flex>
        <Flex flexDirection="column" justifyContent="center" alignItems="center">
          <Flex m="20px 50px 15px 53px" justifyContent="center" alignItems="center" width="100px" height="31px">
            <Flex width="10px" height="10px" borderRadius="50%" bgcolor="#00c692" mr="5px" />
            <StyleTitle>Online</StyleTitle>
          </Flex>
          <Flex mb="30px" ml="15px">
            <StyleValue>{onlineUsers}</StyleValue>
          </Flex>
        </Flex>
        <Flex flexDirection="column">
          <Flex m="20px 50px 15px 0px" justifyContent="center" alignItems="center" width="100px" height="31px">
            <Flex width="10px" height="10px" borderRadius="50%" bgcolor="#ff9521" mr="5px" />
            <StyleTitle>Away</StyleTitle>
          </Flex>
          <Flex mb="30px" ml="45px">
            <StyleValue>{awayUsers}</StyleValue>
          </Flex>
        </Flex>
        <Flex flexDirection="column">
          <Flex m="20px 45px 15px 0px" justifyContent="center" alignItems="center" width="100px" height="31px">
            <Flex width="10px" height="10px" borderRadius="50%" bgcolor="#bbbbc5" mr="5px" />
            <StyleTitle>invisible</StyleTitle>
          </Flex>
          <Flex mb="30px" ml="45px">
            <StyleValue>{invisibleUsers}</StyleValue>
          </Flex>
        </Flex>
      </Flex>
    </Container>
  );
};

export default AgentsInfo;
