import styled from 'styled-components';

import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';

export const StyleTitle = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 31px;
  text-align: center;
  text-transform: uppercase;
  letter-spacing: 0.83px;
  margin-top: 5px;
`;
export const StyleValue = styled(Text)`
  ${({ theme }) => `
  font-style: normal;
  font-weight: 600;
  font-size: 36px;
  line-height: 24px;
  letter-spacing: -0.1px;
  color: ${theme.palette.grey[800]};
`}
`;
export const Container = styled(Flex)`
  ${({ theme, bgcolor }) => `
  box-shadow: 0 1px 2px 0 rgba(63, 63, 68, 0.15), 0 0 0 1px rgba(63, 63, 68, 0.05);
  border-radius: 10px;
  background: ${bgcolor ? `${bgcolor}` : `${theme.palette.background.default}`};
  width: max-content;
  height: 120px;
  text-align: center;
`}
`;
export const StylePercentLess = styled(Text)`
  ${({ theme, color }) => `
  background: ${color === '-' ? '#fdeef0' : 'rgba(0, 198, 146, 0.119154)'};
  border-radius: 5px;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  width: 42px;
  line-height: 18px;
  height: 19px;
  letter-spacing: 0.83px;
  text-transform: uppercase;
  color: ${color === '-' ? theme.palette.error.main : '#00c692'};
  margin-left:10px;
`}
`;

export const LongestTime = styled(Text)`
  ${({ theme }) => `
  font-style: normal;
  font-size: 12px;
  line-height: 31px;
  letter-spacing: 0.83px;
  color: ${theme.palette.error.main};
`}
`;

export const AvgTime = styled(Text)`
  ${({ theme }) => `
  font-style: normal;
  font-size: 12px;
  line-height: 31px;
  letter-spacing: 0.83px;
  color: ${theme.palette.success.main};
`}
`;
