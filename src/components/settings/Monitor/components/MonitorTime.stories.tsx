import React from 'react';

import MonitorTime from './MonitorTime';

export default {
  title: 'Settings/Monitor/Component/MonitorTime',
  component: MonitorTime,
};

export const WaitTime = () => {
  return <MonitorTime title="Wait Time" longTime="2m 12s" averageTime="35s" />;
};

export const ResponseTime = () => {
  return <MonitorTime title="Response Time" longTime="1m 12s" averageTime="20s" />;
};

export const ChatDuration = () => {
  return <MonitorTime title="Chat Duration" longTime="51m 20s" averageTime="20m 12s" />;
};
