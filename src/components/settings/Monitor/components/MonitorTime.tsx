import React from 'react';

import { Flex } from '../../../common/Flex';
import { AvgTime, Container, LongestTime, StyleTitle, StyleValue } from './styles';

export const MonitorTime = ({
  title,
  longTime,
  averageTime,
}: {
  title: string;
  longTime: string;
  averageTime: string;
}) => {
  return (
    <Container flexDirection="column">
      <Flex mt="15px" justifyContent="center" alignItems="center">
        <StyleTitle>{title}</StyleTitle>
      </Flex>

      <Flex flexDirection="row" justifyContent="center">
        <Flex flexDirection="column" width="264px">
          <Flex flexDirection="column">
            <StyleValue>{longTime}</StyleValue>
            <LongestTime>Longest</LongestTime>
          </Flex>
        </Flex>
        <Flex flexDirection="column" width="264px">
          <Flex flexDirection="column">
            <StyleValue>{averageTime}</StyleValue>
            <AvgTime>Average</AvgTime>
          </Flex>
        </Flex>
      </Flex>
    </Container>
  );
};

export default MonitorTime;
