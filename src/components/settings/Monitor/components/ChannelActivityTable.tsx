import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import React from 'react';
import styled from 'styled-components';

import { Flex, PointerFlex } from '../../../common/Flex';
import Icon, { IconName } from '../../../common/Icon';
import { IbMenu, IbMenuItem, useIbMenu } from '../../../common/Menu';
import Text from '../../../common/Text';

export const dumbData: IChannelActivityTable[] = [
  {
    id: 1,
    channel: 'whatsapp',
    open: 20,
    assigned: 10,
    unassigned: 0,
  },
  {
    id: 2,
    channel: 'wechat',
    open: 20,
    assigned: 10,
    unassigned: 0,
  },
  {
    id: 3,
    channel: 'line',
    open: 20,
    assigned: 10,
    unassigned: 0,
  },
  {
    id: 4,
    channel: 'fb',
    open: 20,
    assigned: 10,
    unassigned: 0,
  },
  {
    id: 5,
    channel: 'line',
    open: 20,
    assigned: 10,
    unassigned: 0,
  },
  {
    id: 6,
    channel: 'fb',
    open: 20,
    assigned: 10,
    unassigned: 0,
  },
];
const StyledHeader = styled(Text)`
  ${({ theme }) => `
    margin: 14px 0px 14px 20px;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 20px;
    letter-spacing: -0.05px;
    color: ${theme.palette.grey[800]};
  `}
`;

const StyleTableContainer = styled(TableContainer)`
  border-top: 1px solid rgba(224, 224, 224, 1);
`;

const StyleIcon = styled(Icon)`
  margin-right: 16px;
`;

const StyledCenter = styled(TableCell)`
  text-align: center;
`;

const Container = styled(Flex)`
  ${({ theme }) => `
    box-shadow: 0 1px 2px 0 rgba(63, 63, 68, 0.15), 0 0 0 1px rgba(63, 63, 68, 0.05);;
    border-radius: 10px;
    background: ${theme.palette.background.default};
  `}
`;

export type IChannelActivityTable = {
  id: number;
  channel: IconName;
  open: number;
  assigned: number;
  unassigned: number;
};

const TableMenu = () => {
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const handleAction = (action: string) => {
    handleClose();
    // eslint-disable-next-line no-console
    console.log(action);
  };
  return (
    <>
      <PointerFlex alignItems="center" onClick={handleClick}>
        <MoreVertIcon />
      </PointerFlex>
      <IbMenu
        id="filter-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 30,
          horizontal: -120,
        }}
      >
        <IbMenuItem onClick={() => handleAction('action1')}>action1</IbMenuItem>
        <IbMenuItem onClick={() => handleAction('action2')}>action2</IbMenuItem>
      </IbMenu>
    </>
  );
};
export const ChannelActivityTable = ({ listData }: { listData?: IChannelActivityTable[] }) => {
  return (
    <Container flexDirection="column">
      <Flex justifyContent="space-between" alignItems="center">
        <StyledHeader>Channel Activity</StyledHeader>
        <Flex flexDirection="column" mr="15px">
          <TableMenu />
        </Flex>
      </Flex>
      <StyleTableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell>Open</TableCell>
              <TableCell>Assigned</TableCell>
              <TableCell>Unassigned</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listData && listData.length > 0 ? (
              listData?.map((item) => {
                return (
                  <TableRow key={item.id}>
                    <TableCell>
                      <Flex>
                        <StyleIcon name={item.channel} size={18} />
                        {item.channel === 'fb' ? 'Messenger' : item.channel}
                      </Flex>
                    </TableCell>
                    <TableCell>{item.open}</TableCell>
                    <TableCell>{item.assigned}</TableCell>
                    <TableCell>{item.unassigned}</TableCell>
                  </TableRow>
                );
              })
            ) : (
              <TableRow>
                <StyledCenter colSpan={6}>No data found</StyledCenter>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </StyleTableContainer>
    </Container>
  );
};

export default ChannelActivityTable;
