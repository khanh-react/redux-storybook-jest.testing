import React from 'react';

import ChannelActivityTable, { dumbData } from './ChannelActivityTable';

export default {
  title: 'Settings/Monitor/Component/ChannelActivityTable',
  component: ChannelActivityTable,
};

export const Default = () => {
  return <ChannelActivityTable listData={dumbData} />;
};
