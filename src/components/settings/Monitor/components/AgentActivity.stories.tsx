import React from 'react';

import AgentActivityTable, { dumbData } from './AgentActivityTable';

export default {
  title: 'Settings/Monitor/Component/AgentActivityTable',
  component: AgentActivityTable,
};

export const Default = () => {
  return <AgentActivityTable listData={dumbData} />;
};
