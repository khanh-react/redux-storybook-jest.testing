import React from 'react';

import Monitor, { Monitor2 } from './Monitor';

export default {
  title: 'Settings/Monitor',
  component: Monitor,
};

export const Default = () => {
  return <Monitor />;
};

export const MonitorAll = () => {
  return <Monitor2 />;
};
