import React from 'react';

import { Flex } from '../../common/Flex';
import Text from '../../common/Text';
import AgentActivityTable, { dumbData as dumbData1 } from './components/AgentActivityTable';
import AgentsInfo from './components/AgentsInfo';
import ChannelActivityTable, { dumbData as dumbData2 } from './components/ChannelActivityTable';
import ChatsInfo from './components/ChatsInfo';
import MonitorTime from './components/MonitorTime';
import TotalChats from './components/TotalChats';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
// interface IMonitorProps {}

const Monitor = () => {
  return (
    <Flex width="100%" height="100%" bgcolor=" #f0f0f0" flexDirection="column">
      <Flex flexDirection="column" p="40px">
        <Text size="xl" weight="bold" value="Monitor" />
        <Flex mt="15px">
          <TotalChats title="Total Chats" value={130} percent="+45%" bgcolor="#ffe880" />
          <Flex ml="20px">
            <ChatsInfo
              newChats={56}
              newChatsPercent="+25%"
              assignedChats={32}
              assignedChatsPercent="+33.5%"
              servingChats={24}
              servingChatsPercent="-2.5%"
            />
          </Flex>
        </Flex>
        <Flex mt="20px">
          <Flex flexDirection="column">
            <AgentsInfo loggedUsers={25} onlineUsers={18} awayUsers={5} invisibleUsers={2} />
            <Flex mt="20px" flexDirection="column">
              <AgentActivityTable listData={dumbData1} />
            </Flex>
          </Flex>

          <Flex flexDirection="column" ml="20px">
            <MonitorTime title="Wait Time" longTime="2m 12s" averageTime="35s" />

            <Flex mt="20px" flexDirection="column">
              <MonitorTime title="Response Time" longTime="1m 12s" averageTime="20s" />
            </Flex>
            <Flex mt="20px" flexDirection="column">
              <MonitorTime title="Chat Duration" longTime="51m 20s" averageTime="20m 12s" />
            </Flex>
            <Flex mt="20px" flexDirection="column">
              <ChannelActivityTable listData={dumbData2} />
            </Flex>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};

export const Monitor2 = () => {
  return (
    <Flex width="100%" height="100%" bgcolor=" #f0f0f0" flexDirection="column">
      <Flex flexDirection="column" p="40px">
        <Text size="xl" weight="bold" value="Monitor" />
        <Flex mt="15px">
          <TotalChats title="Total Chats" value={130} percent="+45%" bgcolor="#ffe880" />
          <Flex ml="20px">
            <ChatsInfo
              newChats={56}
              newChatsPercent="+25%"
              assignedChats={32}
              assignedChatsPercent="+33.5%"
              servingChats={24}
              servingChatsPercent="-2.5%"
            />
          </Flex>
        </Flex>

        <Flex mt="20px">
          <AgentsInfo loggedUsers={25} onlineUsers={18} awayUsers={5} invisibleUsers={2} />
          <Flex flexDirection="column" ml="20px">
            <MonitorTime title="Wait Time" longTime="2m 12s" averageTime="35s" />
          </Flex>
        </Flex>

        <Flex>
          <Flex mt="20px" flexDirection="column">
            <MonitorTime title="Response Time" longTime="1m 12s" averageTime="20s" />
            <Flex mt="20px" flexDirection="column">
              <MonitorTime title="Chat Duration" longTime="51m 20s" averageTime="20m 12s" />
            </Flex>
          </Flex>
          <Flex mt="20px" flexDirection="column" ml="20px" width={743}>
            <ChannelActivityTable listData={dumbData2.slice(0, dumbData2.length - 3)} />
          </Flex>
        </Flex>
        <Flex mt="20px" flexDirection="column" width={1286}>
          <AgentActivityTable listData={dumbData1} />
        </Flex>
      </Flex>
    </Flex>
  );
};
export default Monitor;
