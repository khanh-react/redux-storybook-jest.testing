export type Data = {
  id: string;
  category: string;
  shortcut: string;
  question: string;
};

export type TableProps = {
  data: Data[];
  handleSearch: (text: string) => void;
  submitCaseOption: (data: Data, value: string) => void;
  selected: string[];
  setSelected: (data: string[]) => void;
};
