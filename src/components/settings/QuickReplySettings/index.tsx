/* eslint-disable no-nested-ternary */
import React, { useState, useMemo, useEffect } from 'react';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useSelector, useDispatch } from 'react-redux';
import { findIndex } from 'lodash';
import Wrapper from './styles';
import Table from './table';
import { Data as DataFaq } from './types';
import { Data as DataCate } from '../QuickReplyCategories/types';
import { Flex, CenterFlex } from '../../common/Flex';
import Text from '../../common/Text';
import Button from '../../common/Button';
import {
  getCateFaq,
  getFaq,
  createFaq,
  deleteFaq,
  createCateFaq,
  deleteCateFaq,
  updateFaq,
  updateCateFaq,
} from '../../../redux/action/chat';
import QuickReplyCreate from '../QuickReplyCreate';
import QuickReplyCategories from '../QuickReplyCategories';
import QuickReplyAddCategory from '../QuickReplyAddCategory';
import PopupAlert from '../../common/PopupAlert';
import { AppState } from '../../../redux/store';

export const QuickReplySettings = () => {
  const allFaqStore = useSelector((s: AppState) => s.chat.faq);
  const allCateFaqStore = useSelector((s: AppState) => s.chat.cateFaq);
  const [onCaseView, setonCaseView] = useState('faq');
  const [cateSelectIndex, setCateSelectIndex] = React.useState(0);
  const [shortcut, setShortcut] = React.useState('');
  const [message, setMessage] = React.useState('');
  const [onEdit, setonEdit] = React.useState(false);
  const [idEdit, setIdEdit] = React.useState('');
  const [flagCreateCate, setFlagCreateCate] = React.useState(false);
  const [flagPopUpAlert, setFlagPopUpAlert] = React.useState(false);
  const [selected, setSelected] = React.useState<string[]>([]);
  const [nameCate, setNameCate] = React.useState('');
  const dispatch = useDispatch();
  useEffect(() => {
    if (!allFaqStore) {
      dispatch(getCateFaq());
      dispatch(getFaq({}));
    }
  }, [dispatch, allFaqStore]);
  const allFaq = useMemo(() => {
    return (allFaqStore ?? []).map((item) => {
      return {
        id: item.id,
        shortcut: item.shortcut,
        question: item.question,
        category: item.faqCategory.name,
      };
    });
  }, [allFaqStore]);
  const allCateFaq = useMemo(() => {
    return (allCateFaqStore ?? []).map((item) => {
      return {
        name: item.name,
        id: item.id,
      };
    });
  }, [allCateFaqStore]);

  const handleSearch = (value: string) => {
    dispatch(getFaq({ shortcut: value }));
  };
  const onSubmitCreateFaq = () => {
    const patt = new RegExp(
      '^(?=.{1,32}$)(?![0-9$&+,:;=?@#|<>.^*()%!-])[a-zA-Z0-9$&+,:;=?@#|<>.^*()%!-]+(?<![$&+,:;=?@#|<>.^*()%!-])$',
    );
    if (patt.test(shortcut) && message !== '') {
      if (!onEdit) {
        dispatch(createFaq(shortcut, allCateFaq[cateSelectIndex].id, message));
        setonCaseView('faq');
        setShortcut('');
        setMessage('');
        setCateSelectIndex(0);
      } else {
        dispatch(
          updateFaq({
            shortcut,
            faq_category_id: allCateFaq[cateSelectIndex].id,
            question: message,
            id: idEdit,
          }),
        );
        setonCaseView('faq');
        setShortcut('');
        setMessage('');
        setCateSelectIndex(0);
        setonEdit(false);
      }
    }
  };
  const submitCaseOption = (data: DataFaq, onCase: string) => {
    if (onCase === 'VIEW') {
      const indexCate = findIndex(allCateFaq, (o) => o.name === data.category);
      setCateSelectIndex(indexCate);
      setShortcut(data.shortcut);
      setMessage(data.question);
      setonEdit(true);
      setonCaseView('viewFaq');
      setIdEdit(data.id);
    } else {
      setFlagPopUpAlert(true);
      setIdEdit(data.id);
    }
  };

  const handleSearchCate = (value: string) => {
    dispatch(getCateFaq(value));
  };

  const submitCaseOptionCate = (data: DataCate, onCase: string) => {
    if (onCase === 'VIEW') {
      setNameCate(data.name);
      setonEdit(true);
      setIdEdit(data.id);
      setFlagCreateCate(true);
    } else {
      setFlagPopUpAlert(true);
      setIdEdit(data.id);
    }
  };
  const onSubmitDeleteQuick = () => {
    if (onCaseView === 'faq') {
      if (selected.length > 0) {
        selected.forEach((item) => {
          dispatch(deleteFaq(item));
        });
        setSelected([]);
        setFlagPopUpAlert(false);
      } else {
        dispatch(deleteFaq(idEdit));

        setFlagPopUpAlert(false);
      }
    } else if (onCaseView === 'viewCateFaq') {
      if (selected.length > 0) {
        selected.forEach((item) => {
          dispatch(deleteCateFaq(item));
        });
        setSelected([]);
        setFlagPopUpAlert(false);
      } else {
        dispatch(deleteCateFaq(idEdit));
        setFlagPopUpAlert(false);
      }
    }
  };

  const submitCreateCate = () => {
    if (!onEdit) {
      dispatch(createCateFaq(nameCate));
      setFlagCreateCate(false);
      setNameCate('');
    } else {
      dispatch(updateCateFaq({ name: nameCate, id: idEdit }));
      setFlagCreateCate(false);
      setNameCate('');
      setonEdit(false);
    }
  };

  const deleteCate = () => {
    if (onEdit && idEdit !== '') {
      dispatch(deleteCateFaq(idEdit));
      setFlagCreateCate(false);
      setNameCate('');
      setonEdit(false);
    }
  };
  const backToMain = () => {
    setFlagCreateCate(false);
    setonCaseView('faq');
  };

  return (
    <>
      {flagCreateCate && (
        <QuickReplyAddCategory
          closedModal={() => {
            setFlagCreateCate(false);
            setNameCate('');
            setonEdit(false);
          }}
          nameCate={nameCate}
          setNameCate={(value: string) => setNameCate(value)}
          // descriptionCate={descriptionCate}
          // setDescriptionCate={(value: string) => setDescriptionCate(value)}
          submitCreate={submitCreateCate}
          deleteCate={deleteCate}
        />
      )}
      {flagPopUpAlert && <PopupAlert onSubmit={onSubmitDeleteQuick} onCancel={() => setFlagPopUpAlert(false)} />}
      {onCaseView === 'faq' ? (
        <Wrapper>
          <div className="header">
            <Text size="xl" weight="bold" value="Quick Replies" />
            <Flex className="breadcrumb">
              <Text color="#097af4" size="xsm" value="SETTINGS" />
              <Text className="slash" size="xsm" value="/" />
              <Text size="xsm" value="TRIGGERS" />
            </Flex>
            <Flex className="options">
              <Button kind="primary" onClick={() => setonCaseView('viewCateFaq')}>
                Categories
              </Button>
              <Button
                kind="dark"
                onClick={() => {
                  setonCaseView('viewFaq');
                  setShortcut('');
                  setMessage('');
                  setCateSelectIndex(0);
                }}
              >
                <CenterFlex>
                  <AddCircleOutlineIcon fontSize="small" />
                  <Text className="btn-text" color="#FFF" value="Create quick reply" />
                </CenterFlex>
              </Button>
            </Flex>
          </div>
          <Table
            data={allFaq}
            handleSearch={handleSearch}
            submitCaseOption={submitCaseOption}
            setSelected={(data: string[]) => setSelected(data)}
            selected={selected}
          />
        </Wrapper>
      ) : onCaseView === 'viewFaq' ? (
        <QuickReplyCreate
          onCancel={() => setonCaseView('faq')}
          dropdownCateFaq={allCateFaq}
          cateSelectIndex={cateSelectIndex}
          setCateSelectIndex={(index: number) => setCateSelectIndex(index)}
          shortcut={shortcut}
          setShortcut={(value: string) => setShortcut(value)}
          message={message}
          setMessage={(value: string) => setMessage(value)}
          onSubmit={onSubmitCreateFaq}
        />
      ) : (
        <QuickReplyCategories
          data={allCateFaq}
          handleSearch={handleSearchCate}
          submitCaseOption={submitCaseOptionCate}
          clickCreateCate={() => setFlagCreateCate(true)}
          setSelected={(data: string[]) => setSelected(data)}
          selected={selected}
          backToMain={backToMain}
        />
      )}
    </>
  );
};

export default QuickReplySettings;
