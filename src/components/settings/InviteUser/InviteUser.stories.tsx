import React from 'react';

import { InviteUser } from './InviteUser';

export default {
  title: 'Settings/InviteUser',
  component: InviteUser,
};

export const Default = () => {
  return <InviteUser />;
};
