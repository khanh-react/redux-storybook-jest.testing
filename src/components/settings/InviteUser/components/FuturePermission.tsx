import React from 'react';
import styled from 'styled-components';
import { Checkbox, FormControl, FormGroup, FormControlLabel } from '@material-ui/core';
import { ListCheckBoxFuture } from './types';
import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';

const Container = styled(Flex)`
  width: 1070px;
  border-radius: 10px;
  box-shadow: 0 2px 8px 0 rgba(210, 210, 210, 0.5);
  border: solid 1px #dcdcdc;
  background-color: #ffffff;
  margin-top: 20px;
`;

const StyledTitleCard = styled(Flex)`
  height: 55px;
  margin: 0 0 18px;
  padding: 17px 0px 18px 30px;
  border-radius: 4px;
  border-bottom: 1px solid #eeeeee;
`;
const StyledCardEnable = styled(Flex)`
  margin: 25px 0px 10px 30px;
`;

const StyledTitleText = styled(Text)`
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.05px;
  color: #0f0f10;
`;

const StyledFormGroup = styled(FormGroup)`
  width: 100%;
  flex-direction: row;
`;

const StyledFormControlLabel = styled(FormControlLabel)`
  margin-right: 50px;
  width: 220px;
  white-space: nowrap;
`;

export const FuturePermission = ({ disabled }: { disabled?: boolean }) => {
  const [listCheckBox, setListCheckBoxChannel] = React.useState(ListCheckBoxFuture);

  const handleChangeCheckBox = (event: React.ChangeEvent<HTMLInputElement>, id: string) => {
    const newList = listCheckBox.splice(0, listCheckBox.length);
    newList.forEach((e) => {
      if (e.id === id) {
        e.check = event.target.checked;
      }
    });
    setListCheckBoxChannel(newList);
  };

  return (
    <Container flexDirection="column">
      <StyledTitleCard>
        <StyledTitleText value="Feature Permission" />
      </StyledTitleCard>
      <StyledCardEnable flexDirection="column">
        <Flex flexDirection="row" flexWrap="wrap">
          <FormControl component="fieldset">
            <StyledFormGroup>
              {listCheckBox.map((item) => {
                return (
                  <StyledFormControlLabel
                    disabled={disabled}
                    key={item.id}
                    control={
                      <Checkbox
                        checked={item.check}
                        onChange={(event) => handleChangeCheckBox(event, item.id)}
                        name={item.name}
                      />
                    }
                    label={item.name}
                  />
                );
              })}
            </StyledFormGroup>
          </FormControl>
        </Flex>
      </StyledCardEnable>
    </Container>
  );
};

export default FuturePermission;
