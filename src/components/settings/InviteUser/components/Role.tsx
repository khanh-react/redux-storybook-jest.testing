import React, { useState } from 'react';
import styled from 'styled-components';
import { Add, Remove } from '@material-ui/icons';
import { IbTextField, StyledMenuItem } from '../../../common/InputField';
import InputTag from '../../../common/InputTag';
import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';

const Container = styled(Flex)`
  width: 1070px;
  padding: 40px 45px 56px 38px;
  border-radius: 10px;
  box-shadow: 0 2px 8px 0 rgba(210, 210, 210, 0.5);
  border: solid 1px #dcdcdc;
  background-color: #ffffff;
`;

const StyleSelected = styled(IbTextField)`
  width: 470px;
  height: 50px;
  border-radius: 10px;
`;

const StyleChatlimted = styled(Flex)`
  margin-left: 40px;
  width: 470px;
`;

const StyleNumber = styled(Flex)`
  margin-left: auto;
`;
const StyleInputTag = styled(Flex)`
  margin-top: 26px;
  .MuiBox-root-158 {
    margin-right: 10px;
  }
`;

const StyleTextRouting = styled(Text)`
  width: 471px;
  font-size: 12px;
  margin-top: 10px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.3px;
  color: #98999f;
`;

const StyleIconRemove = styled(Remove)`
  height: 27px;
  margin-right: 5px;
`;

const StyleIconAdd = styled(Add)`
  height: 27px;
  margin-left: 5px;
`;

const StyleTextEmail = styled(IbTextField)`
  width: 979px;
  height: 50px;
  border-radius: 8px;
  margin-top: 35px;
`;

const StyleCountNumber = styled(IbTextField)`
  width: 60px;
  height: 30px;
  border-radius: 4px;
  .MuiOutlinedInput-input {
    padding: 4px 22px 6px;
  }
`;

export const Role = () => {
  const [value, setValue] = useState('Agent');
  const [number, setNumber] = useState(10);
  const [listEmail, setListEmail] = useState([{ name: '', id: '' }]);
  let inputEmail: { name: string; id: string } = { name: '', id: '' };
  const [validator, setValidator] = useState({ helperText: '', error: false });
  const handleChangeValue = (event: React.ChangeEvent<{ value: unknown }>) => {
    setValue(event.target.value as string);
  };

  const setEmailInvited = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const nameEmail: string = event.target.value;
    inputEmail = {
      name: nameEmail,
      id: Math.random().toString(36).substring(7),
    };
  };

  const removeTag = (id: string) => {
    let newListEmail = listEmail.splice(0, listEmail.length);
    newListEmail = newListEmail.filter((item) => {
      return item.id !== id;
    });
    setListEmail(newListEmail);
  };

  const calculator = (type: string) => {
    let count: number = number;
    if (type === 'sub') {
      count--;
    } else {
      count++;
    }
    setNumber(count);
  };

  const addEmailContact = (event: React.KeyboardEvent<HTMLDivElement>): void => {
    if (event.key === 'Enter') {
      const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (inputEmail && re.test(inputEmail.name.toLocaleLowerCase())) {
        const newListEmail = listEmail.splice(0, listEmail.length);
        newListEmail.push(inputEmail);
        setListEmail(newListEmail);
        setValidator({ helperText: '', error: false });
      } else {
        setValidator({ helperText: 'Invalid format', error: true });
      }
    }
  };
  return (
    <Container>
      <Flex flexDirection="column">
        <Flex flexDirection="row">
          <StyleSelected select label="Role" onChange={handleChangeValue} value={value}>
            <StyledMenuItem value="Agent">Agent</StyledMenuItem>
            <StyledMenuItem value="admin">admin</StyledMenuItem>
            <StyledMenuItem value="client">client</StyledMenuItem>
          </StyleSelected>
          <StyleChatlimted flexDirection="column">
            <Flex>
              <Text value="Chat Limit" />
              <StyleNumber>
                <StyleIconRemove onClick={() => calculator('sub')} fontSize="small" />
                <StyleCountNumber value={number} />
                <StyleIconAdd onClick={() => calculator('plus')} fontSize="small" />
              </StyleNumber>
            </Flex>
            <StyleTextRouting value="Number of chats thr agent is allowed to take. Edit other Chat Routing setting under" />
            <Text value="Routing > Setting." />
          </StyleChatlimted>
        </Flex>
        <StyleTextEmail
          onKeyDown={addEmailContact}
          onChange={setEmailInvited}
          error={validator.error}
          helperText={validator.helperText}
          placeholder="Add people by email address"
          label="Emails"
        />
        <StyleInputTag flexDirection="row" flexWrap="wrap">
          {listEmail &&
            listEmail
              .filter(function remove(item) {
                return item.name !== '';
              })
              .map((item) => {
                return (
                  <Flex key={item.id} mr="10px">
                    <InputTag id={item.id} onClose={(id) => removeTag(id)} hasCloseIcon label={item.name} />
                  </Flex>
                );
              })}
        </StyleInputTag>
      </Flex>
    </Container>
  );
};

export default Role;
