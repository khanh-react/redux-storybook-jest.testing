import React from 'react';

import { ContactPermission } from './ContactPermission';

export default {
  title: 'Settings/Component/ContactPermission',
  component: ContactPermission,
};

export const Default = () => {
  return <ContactPermission />;
};
