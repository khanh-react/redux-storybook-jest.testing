import React from 'react';

import { Role } from './Role';

export default {
  title: 'Settings/Component/Role',
  component: Role,
};

export const Default = () => {
  return <Role />;
};
