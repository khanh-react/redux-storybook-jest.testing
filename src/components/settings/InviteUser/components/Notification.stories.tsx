import React from 'react';

import { Notification } from './Notification';

export default {
  title: 'Settings/Component/Notification',
  component: Notification,
};

export const Default = () => {
  return <Notification />;
};
