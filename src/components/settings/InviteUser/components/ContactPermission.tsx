import React from 'react';
import styled from 'styled-components';
import { Radio, Checkbox, FormControl, FormGroup, FormControlLabel, RadioGroup } from '@material-ui/core';
import { CheckboxChannel } from './types';
import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';

const Container = styled(Flex)`
  width: 1070px;
  border-radius: 10px;
  box-shadow: 0 2px 8px 0 rgba(210, 210, 210, 0.5);
  border: solid 1px #dcdcdc;
  background-color: #ffffff;
  margin-top: 20px;
`;

const StyledTitleCard = styled(Flex)`
  height: 55px;
  margin: 0 0 18px;
  padding: 17px 0px 18px 30px;
  border-radius: 4px;
  border-bottom: 1px solid #eeeeee;
`;
const StyledCardEnable = styled(Flex)`
  margin: 25px 0px 10px 30px;
`;

const StyledCardChannel = styled(Flex)`
  padding: 0px 0px 0px 30px;
`;

const StyledTitleText = styled(Text)`
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.05px;
  color: #0f0f10;
`;

const StyledTextRadio = styled(Text)`
  font-size: 15px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.71;
  letter-spacing: normal;
  color: #2e2e32;
`;

const StyledFormGroup = styled(FormGroup)`
  width: 586px;
  flex-direction: row;
`;

const StyledFormControlLabel = styled(FormControlLabel)`
  margin-right: 50px;
  width: 250px;
`;

const StyledRadioGroup = styled(RadioGroup)`
  flex-direction: row !important;
`;

export const ContactPermission = ({ disabled }: { disabled?: boolean }) => {
  const [selectedValue, setSelectedValue] = React.useState('a');
  const [listCheckBoxChannel, setListCheckBoxChannel] = React.useState(CheckboxChannel);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedValue(event.target.value);
  };

  const handleChangeCheckBox = (event: React.ChangeEvent<HTMLInputElement>, id: string) => {
    const newList = listCheckBoxChannel.splice(0, listCheckBoxChannel.length);
    newList.forEach((e) => {
      if (e.id === id) {
        e.check = event.target.checked;
      }
    });
    setListCheckBoxChannel(newList);
  };

  return (
    <Container flexDirection="column">
      <StyledTitleCard>
        <StyledTitleText value="Contact Permission" />
      </StyledTitleCard>
      <StyledCardChannel flexDirection="column">
        <StyledTextRadio value="Assigned All Channels" />
        <Flex flexDirection="row">
          <StyledRadioGroup value={selectedValue} onChange={handleChange}>
            <FormControlLabel disabled={disabled} value="a" control={<Radio />} label="Yes" />
            <FormControlLabel disabled={disabled} value="b" control={<Radio />} label="No" />
          </StyledRadioGroup>
        </Flex>
      </StyledCardChannel>
      <StyledCardEnable flexDirection="column">
        <StyledTextRadio value="Enabled Channels" />
        <Flex flexDirection="row" flexWrap="wrap">
          <FormControl component="fieldset">
            <StyledFormGroup>
              {listCheckBoxChannel.map((item) => {
                return (
                  <StyledFormControlLabel
                    key={item.id}
                    disabled={disabled}
                    control={
                      <Checkbox
                        checked={item.check}
                        onChange={(event) => handleChangeCheckBox(event, item.id)}
                        name={item.name}
                      />
                    }
                    label={item.name}
                  />
                );
              })}
            </StyledFormGroup>
          </FormControl>
        </Flex>
      </StyledCardEnable>
    </Container>
  );
};

export default ContactPermission;
