export const CheckboxChannel = [
  {
    id: Math.random().toString(36).substring(7),
    name: 'Whatsapp (85264676423)',
    check: true,
  },

  {
    id: Math.random().toString(36).substring(7),
    name: 'Twilio - SMS (+14153048772)',
    check: false,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Line (JG Shop)',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Facebook Messenger (JG Shop)',
    check: true,
  },
];

export const ListCheckBoxFuture = [
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage Numbers/Channels',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage Billing',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage Chat Ticket',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage FAQ',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Calendar Event',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage Chat Permission',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'View Dashboard',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage Report',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage Coupon',
    check: false,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage Coupon',
    check: false,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'View Referral',
    check: false,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage List',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage Message Category',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage User',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Schedule Message',
    check: false,
  },

  {
    id: Math.random().toString(36).substring(7),
    name: 'Edit Timezone',
    check: false,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Team Internal Chat',
    check: false,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Manage Bot',
    check: false,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: ' Manage Client Invoice',
    check: false,
  },
];

export const listCheckBoxNotification = [
  {
    id: Math.random().toString(36).substring(7),
    name: 'Status Update Email',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Broadcast Not Delivered WhatsApp',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Status Update WhatsApp',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Ticket Not Assigned Email',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Message Not Delivered Email',
    check: false,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Ticket Not Assigned WhatsApp',
    check: false,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Message Not Delivered WhatsApp',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Ticket Assigned Email',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Broadcast Not Delivered Email',
    check: true,
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Ticket Assigned WhatsApp',
    check: true,
  },
];

export const TagSkill = [
  {
    id: Math.random().toString(36).substring(7),
    name: 'Customer Service',
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Analytical',
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Problem solving',
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Customer Support',
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'English',
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Chinese',
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Buyer-Seller Agreement',
  },
  {
    id: Math.random().toString(36).substring(7),
    name: 'Communication',
  },
];
