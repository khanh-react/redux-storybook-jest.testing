import React from 'react';

import { FuturePermission } from './FuturePermission';

export default {
  title: 'Settings/Component/FuturePermission',
  component: FuturePermission,
};

export const Default = () => {
  return <FuturePermission />;
};
