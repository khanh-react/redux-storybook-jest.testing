import React from 'react';
import Button from '../../common/Button';
import { Flex } from '../../common/Flex';
import Text from '../../common/Text';
import Wrapper from '../components/styles';
import Role from './components/Role';
import ContactPermission from './components/ContactPermission';
import FuturePermission from './components/FuturePermission';
import Notification from './components/Notification';

export const InviteUser = () => {
  return (
    <Wrapper>
      <div className="header">
        <Text size="xl" weight="bold" value="Add User" />
        <Flex className="breadcrumb">
          <Text color="#097af4" size="xsm" value="SETTINGS" />
          <Text className="slash" size="xsm" value="/" />
          <Text color="#097af4" size="xsm" value="USERS" />
          <Text className="slash" size="xsm" value="/" />
          <Text size="xsm" value="EDIT" />
        </Flex>
        <Flex className="options">
          <Button kind="default">
            <Text className="btn-text" color="#2e2e32" value="Cancel" />
          </Button>
          <Button kind="dark">
            <Text className="btn-text" color="#fff" value="Invite" />
          </Button>
        </Flex>
      </div>
      <Role />
      <ContactPermission />
      <FuturePermission />
      <Notification />
    </Wrapper>
  );
};

export default InviteUser;
