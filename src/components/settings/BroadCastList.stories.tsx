import React from 'react';

import { BroadcastList } from './BroadCastList';
import { createData } from './BroadCastList/types';

export default {
  title: 'Settings/Broadcast/BroadCastList',
  component: BroadcastList,
};

const mockdata = [
  createData('01', '/New product launch 2020', '13/02/2020  01:30PM', 20, 'James Corden', '1 days ago'),
  createData('02', '/New product launch 2021', '13/02/2020  01:31PM', 21, 'James Corden', '2 days ago'),
  createData('03', '/New product launch 2022', '13/02/2020  01:32PM', 22, 'James Corden', '3 days ago'),
  createData('04', '/New product launch 2023', '13/02/2020  01:33PM', 23, 'James Corden', '4 days ago'),
  createData('05', '/New product launch 2024', '13/02/2020  01:34PM', 24, 'James Corden', '5 days ago'),
  createData('06', '/New product launch 2025', '13/02/2020  01:35PM', 25, 'James Corden', '6 days ago'),
  createData('07', '/New product launch 2026', '13/02/2020  01:36PM', 26, 'James Corden', '7 days ago'),
  createData('08', '/New product launch 2027', '13/02/2020  01:37PM', 27, 'James Corden', '8 days ago'),
  createData('09', '/New product launch 2028', '13/02/2020  01:38PM', 28, 'James Corden', '9 days ago'),
  createData('10', '/New product launch 2029', '13/02/2020  01:39PM', 29, 'James Corden', '10 days ago'),
];

export const Default = () => {
  return <BroadcastList data={mockdata} />;
};
