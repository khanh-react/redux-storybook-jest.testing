import React from 'react';
import TelegramIcon from '@material-ui/icons/Telegram';
import Wrapper from './styles';
import { Flex } from '../../../common/Flex';
import { Text } from '../../../common/Text';

type Props = {
  isVisible?: boolean;
};

export const Preview = ({ isVisible }: Props) => {
  if (!isVisible) return null;
  return (
    <Wrapper justifyContent="center">
      <Flex className="container" flexDirection="column">
        <Flex className="head" flexDirection="column" justifyContent="center">
          <Flex className="big-line" />
          <Flex className="small-line" />
        </Flex>
        <Flex className="body" flexDirection="column">
          <Flex>
            <Flex className="brand" flexDirection="column">
              <Text weight="bold" value="Imbee - SaaS platform" />
              <Text
                color="#6c6c72"
                size="sm"
                value="Build the best customer relationship through popular messaging apps"
              />
            </Flex>
            <Flex className="thumb" justifyContent="center" alignItems="center">
              <img width="80%" alt="" src="static/imbee-crm-menu-logo.svg" />
            </Flex>
          </Flex>
          <Text className="mrt10" value="Please click this link for more details! :)" />
        </Flex>
        <Flex justifyContent="flex-end">
          <Text className="txt" size="sm" color="#b5b6bb" value="1 min ago" />
          <Text className="txt" size="sm" color="#b5b6bb">
            Sent from
            <Text color="#000" weight="bold" value=" You" />
          </Text>
        </Flex>
        <Flex className="foot" alignItems="center" justifyContent="center">
          <Flex className="comment" />
          <TelegramIcon color="disabled" />
        </Flex>
      </Flex>
    </Wrapper>
  );
};

export default Preview;
