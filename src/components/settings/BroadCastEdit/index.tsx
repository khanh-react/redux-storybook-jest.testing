import DeleteIcon from '@material-ui/icons/DeleteOutline';
import React, { useState } from 'react';

import Button from '../../common/Button';
import { Flex } from '../../common/Flex';
import { IbTextField, StyledMenuItem } from '../../common/InputField';
import Text from '../../common/Text';
import TextField from '../../common/TextField';
import { Preview } from './Preview';
import Wrapper from './styles';
import { Tabs } from './Tabs';
import { Placeholder } from './types';

export const BroadCastEdit = () => {
  const [currentTab, setTab] = useState(0);
  const placeholders: Placeholder[] = [
    {
      key: '{username}',
      value: '客戶名稱',
    },
    {
      key: '{Chat_name}',
      value: 'Chat ID (e.g. Phone No, Group No)',
    },
    {
      key: '{appointment_time}',
      value: '預約時間',
    },
    {
      key: '{appointment_date}',
      value: '預約日期',
    },
    {
      key: '{field_437}',
      value: '客戶公司名',
    },
    {
      key: '{field_460}',
      value: '客戶公司網站',
    },
    {
      key: '{field_439}',
      value: '公司電郵',
    },
    {
      key: '{field_Owner}',
      value: 'Owner',
    },
    {
      key: '{field_621}',
      value: '客戶計劃',
    },
  ];
  const currentChannel = 'WhatsApp';
  const channels = ['WhatsApp', 'Messenger', 'Zalo'];
  const messageTitle = '';
  const currentDate = '';
  const currentTime = '';
  const currentContactGroup = '';
  const contactGroups = ['Hong Kong Vip Customers', 'USA Vip Customers'];
  const [contactGroup, setContactGroup] = React.useState('');
  const handleChangeContactGroup = (event: React.ChangeEvent<{ value: unknown }>) => {
    setContactGroup(event.target.value as string);
  };
  const [channel, setChannel] = React.useState('');
  const handleChangeChannel = (event: React.ChangeEvent<{ value: unknown }>) => {
    setChannel(event.target.value as string);
  };
  return (
    <Wrapper>
      <Flex className="container" flexDirection="column">
        {/* HEADER  */}
        <Flex className="header" flexDirection="column">
          <Text size="xl" weight="bold" value="Edit Broadcast" />
          <Flex className="breadcrumb">
            <Text color="#097af4" size="xsm" value="SETTINGS" />
            <Text className="slash" size="xsm" value="/" />
            <Text size="xsm" value="ROUTING" />
          </Flex>
          <Flex className="options">
            <Button kind="danger" icon={<DeleteIcon />}>
              Delete
            </Button>
            <Button kind="default">Cancel</Button>
            <Button kind="dark">Save</Button>
          </Flex>
        </Flex>

        {/* BODY */}
        <Flex className="body" flexDirection="column">
          <Flex>
            <Flex className="settings" flexDirection="column">
              <Flex className="setting">
                <Text size="xl" weight="bold" value="Broadcast Setting" />
              </Flex>
              <Flex className="setting" flexDirection="column">
                <IbTextField
                  error={false}
                  disabled={false}
                  readOnly={false}
                  required={false}
                  select
                  defaultValue={currentChannel}
                  label="Channel"
                  variant="outlined"
                  placeholder="Select a Channel"
                  value={channel}
                  onChange={handleChangeChannel}
                >
                  {channels.map((c, index) => {
                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <StyledMenuItem key={`channel-${index}`} value={c}>
                        {c}
                      </StyledMenuItem>
                    );
                  })}
                </IbTextField>
              </Flex>
              <Flex className="setting" flexDirection="row">
                <IbTextField label="Message Title" value={messageTitle} />
              </Flex>
              <Flex className="setting" flexDirection="row">
                <TextField className="date" type="date" label="Date*" value={currentDate} />
                <TextField className="time" type="time" label="Time*" value={currentTime} />
              </Flex>
              <Flex className="setting" flexDirection="column">
                <IbTextField
                  error={false}
                  disabled={false}
                  readOnly={false}
                  required={false}
                  select
                  defaultValue={currentContactGroup}
                  label="Contact Group"
                  variant="outlined"
                  placeholder="Select a contact group"
                  value={contactGroup}
                  onChange={handleChangeContactGroup}
                >
                  {contactGroups.map((group, index) => {
                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <StyledMenuItem key={`group-${index}`} value={group}>
                        {group}
                      </StyledMenuItem>
                    );
                  })}
                </IbTextField>
              </Flex>
            </Flex>
            <Flex className="placeholder" flexDirection="column">
              <Text size="md" weight="bold" value="Placeholder" />
              {placeholders.map((p, index) => {
                return (
                  // eslint-disable-next-line react/no-array-index-key
                  <Flex className="p-row" key={`${p.key}_${index}`}>
                    <Text className="p-txt-left" size="sm" value={p.key} />
                    <Text className="p-txt-right" size="sm" value={p.value} color="#98999f" />
                  </Flex>
                );
              })}
            </Flex>
          </Flex>
          <Flex width="100%">
            <Tabs onChange={(value) => setTab(value)} />
            <Preview isVisible={currentTab === 2} />
          </Flex>
        </Flex>
      </Flex>
    </Wrapper>
  );
};

export default BroadCastEdit;
