import BackupIcon from '@material-ui/icons/Backup';
import React, { useState } from 'react';

import Button from '../../../../common/Button';
import { FilePicker } from '../../../../common/FilePicker';
import { Text } from '../../../../common/Text';
import TextField from '../../../../common/TextField';
import { TabProps as Props } from '../types';
import Wrapper from './styles';

export const Message = ({ isVisible }: Props) => {
  const [, setFile] = useState(null);
  if (!isVisible) return null;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleFileChanged = (e: any) => {
    e && setFile(e);
  };

  return (
    <Wrapper flexDirection="column">
      <Text className="row-item mrt20" value="Image Source" weight="bold" />
      <TextField size="large" value="" placeholder="Paste image url" />
      <Text className="row-item" value="or" weight="bold" />
      <FilePicker className="file" onChange={handleFileChanged}>
        <Button kind="primary" icon={<BackupIcon />}>
          <Text color="#FFF" value="Upload image " />
        </Button>
      </FilePicker>
      <Text className="row-item" value="Message" weight="bold" />
      <TextField type="textarea" className="row-item" value="" placeholder="Text..." />
    </Wrapper>
  );
};

export default Message;
