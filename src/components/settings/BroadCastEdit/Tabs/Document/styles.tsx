import styled from 'styled-components';
import { Flex } from '../../../../common/Flex';

export default styled(Flex)`
  .row-item {
    margin-top: 10px;
  }

  .mrt20 {
    margin-top: 20px;
  }
`;
