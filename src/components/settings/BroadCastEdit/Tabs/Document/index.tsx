import BackupIcon from '@material-ui/icons/Backup';
import React, { useState } from 'react';

import Button from '../../../../common/Button';
import { FilePicker } from '../../../../common/FilePicker';
import { Text } from '../../../../common/Text';
import TextField from '../../../../common/TextField';
import { TabProps as Props } from '../types';
import Wrapper from './styles';

export const Document = ({ isVisible }: Props) => {
  const [, setFile] = useState(null);
  if (!isVisible) return null;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleFileChanged = (e: any) => {
    e && setFile(e);
  };

  return (
    <Wrapper flexDirection="column">
      <Text className="row-item mrt20" value="File Source" weight="bold" />
      <TextField size="large" value="" placeholder="Paste image url" />
      <Text className="row-item" value="or" weight="bold" />
      <FilePicker className="file" onChange={handleFileChanged}>
        <Button kind="primary" icon={<BackupIcon />}>
          <Text color="#FFF" value="Upload image " />
        </Button>
      </FilePicker>
      <TextField className="row-item" size="large" value="ProductA_09/12/2020" label="Filename*" />
      <Text className="row-item" value="Message" weight="bold" />
      <TextField className="row-item" type="textarea" value="" placeholder="Text..." />
    </Wrapper>
  );
};

export default Document;
