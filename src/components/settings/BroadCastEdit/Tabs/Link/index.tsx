import BackupIcon from '@material-ui/icons/Backup';
import React, { useState } from 'react';

import Button from '../../../../common/Button';
import { FilePicker } from '../../../../common/FilePicker';
import { Flex } from '../../../../common/Flex';
import { Text } from '../../../../common/Text';
import TextField from '../../../../common/TextField';
import { TabProps as Props } from '../types';

export const Link = ({ isVisible }: Props) => {
  const [, setFile] = useState(null);
  if (!isVisible) return null;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleFileChanged = (e: any) => {
    e && setFile(e);
  };

  return (
    <Flex flexDirection="column">
      <TextField className="row-item mgt-15" size="large" value="http://imbee.io" label="Link URL*" />
      <TextField className="row-item" size="large" value="Imbee - Saas platform" label="Link Title*" />
      <TextField
        size="large"
        className="row-item"
        value="Build the best customer relationship through popular messaging apps"
        label="Link Description"
      />
      <Text className="row-item" value="File Thumnail" weight="bold" />
      <TextField size="large" className="row-item" value="" placeholder="Paste file URL" />
      <Text className="row-item" value="or" weight="bold" />
      <FilePicker className="row-item file" onChange={handleFileChanged}>
        <Button kind="primary" icon={<BackupIcon />}>
          <Text color="#FFF" value="Upload image " />
        </Button>
      </FilePicker>
      <Text className="row-item" value="Message" weight="bold" />
      <TextField className="row-item" type="textarea" value="" placeholder="Text..." />
    </Flex>
  );
};

export default Link;
