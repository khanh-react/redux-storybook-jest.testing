import React from 'react';

import { QuickReplyCreate } from './QuickReplyCreate';

export default {
  title: 'Settings/QuickReplyCreate',
  component: QuickReplyCreate,
};

export const Default = () => {
  return (
    <QuickReplyCreate
      onCancel={() => null}
      dropdownCateFaq={[{ name: 'hihi', id: 'haha' }]}
      cateSelectIndex={0}
      setCateSelectIndex={() => null}
      shortcut="string"
      setShortcut={() => null}
      message="string"
      setMessage={() => null}
      onSubmit={() => null}
    />
  );
};
