import React from 'react';

import { BroadcastCreate } from './BroadCastCreate';

export default {
  title: 'Settings/Broadcast/BroadcastCreate',
  component: BroadcastCreate,
};

export const Default = () => {
  return <BroadcastCreate />;
};
