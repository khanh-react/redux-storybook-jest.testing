import React, { useState } from 'react';
import styled from 'styled-components';
import { AddCircleOutline } from '@material-ui/icons';
import { Tab } from '@material-ui/core';
import Wrapper from '../components/styles';
import { Flex, CenterFlex } from '../../common/Flex';
import { IbTabs } from '../../common/Tabs';
import ChannelTable from './table';
import { mockdataChannel, DataChannel } from '../components/types';
import Text from '../../common/Text';
import Button from '../../common/Button';

const StyleTab = styled(IbTabs)`
  width: 1289px;
  height: 50px;
  margin: -10px 0px 15px;
  border-radius: 4px;
  background-color: #ffffff;
  .MuiTab-textColorInherit {
    opacity: 0.9;
  }
  .MuiTab-wrapper {
    color: #6c6c72;
    font-weight: bold;
  }
  .Mui-selected {
    .MuiTab-wrapper {
      color: #ffdb36 !important;
      font-weight: bold;
    }
  }
`;

const StyledTextTab = styled(Tab)`
  text-transform: uppercase;
  width: 215px;
  height: 50px;
  padding: 13px 56px 12px 59px;
`;

const StyledPaper = styled.span`
  line-height: 31px;
`;

const StyledTextGetting = styled(Text)`
  margin-bottom: 30px;
`;

export const Home = ({ initialTab }: { initialTab: number }) => {
  const [selectedTab, setSelectedTab] = useState(initialTab);
  const [listData, setListData] = useState(mockdataChannel);
  const [flag, setFlag] = useState(false);
  const handleChange = (event: React.ChangeEvent<unknown>, newValue: number) => {
    setSelectedTab(newValue);
    let newListChannel: DataChannel[] = mockdataChannel;
    if (newValue === 2) {
      newListChannel = mockdataChannel.filter((item) => {
        return item.channel !== 'disable';
      });
    } else if (newValue === 1) {
      newListChannel = mockdataChannel.filter((item) => {
        return item.channel === 'whatsapp';
      });
    }
    setListData(newListChannel);
  };
  return (
    <Wrapper>
      <div className="header">
        <Text size="xl" weight="bold" value="Channels" />
        <Flex className="options">
          <Button kind="dark">
            <CenterFlex>
              <AddCircleOutline fontSize="small" />
              <Text className="btn-text" color="#FFF" value="New Product" />
            </CenterFlex>
          </Button>
        </Flex>
      </div>
      <Flex justifyContent="center" flex={1} flexDirection="column" width="1289px">
        <StyleTab value={selectedTab} onChange={handleChange}>
          {['all', 'whatsapp', 'wechat', 'line', 'facebook', 'twillo'].map((value) => {
            return <StyledTextTab key={value} label={value} />;
          })}
        </StyleTab>
        {flag ? (
          <ChannelTable data={listData} />
        ) : (
          <Flex justifyContent="center" marginTop="200px" flexDirection="row">
            {/* eslint-disable jsx-a11y/alt-text */}
            <img src="static/channel-list-nodata.svg" />
            <Flex flexDirection="column" width="470px" color="#3b3b3b">
              <StyledTextGetting size="xl" weight="bold" value="Getting started with messagaing channels" />
              <StyledPaper>
                1. <strong>Create</strong> a WhatsApp Business channel.
              </StyledPaper>
              <StyledPaper>
                2. <strong>Select our demo number</strong> to start testing and see how it works (do not communicate).
              </StyledPaper>
              <StyledPaper>
                3. <strong>Add phone numbers</strong> to this test channel to receive WhatsApp messages from the added
                number(s).
              </StyledPaper>
              <StyledPaper>
                4. <strong>Send a WhatsApp</strong> message to the demo/testing number.
              </StyledPaper>
              <Flex width="139px" marginTop="30px">
                <Button kind="dark" onClick={() => setFlag(!flag)}>
                  <CenterFlex>
                    <AddCircleOutline fontSize="small" />
                    <Text className="btn-text" color="#FFF" value="Add Channel" />
                  </CenterFlex>
                </Button>
              </Flex>
            </Flex>
          </Flex>
        )}
      </Flex>
    </Wrapper>
  );
};

export default Home;
