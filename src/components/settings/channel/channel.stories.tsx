import React from 'react';

import { Home } from './channel';

export default {
  title: 'Settings/Channel',
  component: Home,
};

export const Default = () => {
  return <Home initialTab={0} />;
};
