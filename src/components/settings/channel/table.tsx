import React from 'react';
import styled from 'styled-components';
import { Box, TablePagination, TableContainer, TableCell, TableBody, TableHead, TableRow } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import { TableChannelProps as Props } from '../components/types';
import Text from '../../common/Text';
import { Dotdotdot } from '../../common/Dotdotdot';
import { Flex } from '../../common/Flex';

const TableWrapper = styled.div`
  background-color: #fff;
  border-radius: 10px;
  width: 1289px;
  margin: 0px 0px 10px;
`;

const StyledStatus = styled(Box)`
  margin: auto;
  width: 80px;
  height: 19px;
  border-radius: 4px;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 19px;
  letter-spacing: 0.83px;
  text-transform: uppercase;
  text-align: center;
`;

const StyledTablePagination = styled(Flex)`
  float: right;
  p {
    font-size: 13px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 0.3px;
    text-align: right;
    color: #6c6c72;
  }
`;
const StyledImg = styled.img`
  margin-left: 10px;
`;

const StyledTextCell = styled(Text)`
  font-size: 14px;
  font-weight: 600 !important;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  color: #98999f;
`;

const StyledCell = styled(TableCell)`
  width: 40px;
`;

const StyledDotOption = styled(Flex)`
  .options {
    width: 143px;
    heigth: 78px;
    left: -105px;
  }
`;

export default function EnhancedTable({ data }: Props) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

  return (
    <TableWrapper>
      <TableContainer>
        <Table aria-labelledby="tableTitle" aria-label="enhanced table">
          <TableHead>
            <TableRow>
              <TableCell align="center">
                <StyledTextCell color="#98999f" weight="bold" value="NAME" />
              </TableCell>
              <TableCell align="center">
                <StyledTextCell color="#98999f" weight="bold" value="CHANNEL" />
              </TableCell>
              <TableCell align="center">
                <StyledTextCell color="#98999f" weight="bold" value="OWNER" />
              </TableCell>
              <TableCell align="center">
                <StyledTextCell color="#98999f" weight="bold" value="API" />
              </TableCell>
              <TableCell align="center">
                <StyledTextCell color="#98999f" weight="bold" value="STATUS" />
              </TableCell>
              <StyledCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                  <TableCell align="center">
                    <Text className="cell-text" value={row.nameChannel} />
                  </TableCell>
                  <TableCell align="center">
                    <Text className="cell-text" value={row.channel} />
                  </TableCell>
                  <TableCell align="center">
                    <Text className="cell-text" value={row.owner} />
                  </TableCell>
                  <TableCell align="center">
                    <Text className="cell-text" value={row.api} />
                    <StyledImg src="static/channel-table-icon-copy.svg" />
                  </TableCell>
                  <TableCell align="center">
                    <StyledStatus
                      bgcolor={row.status === 'online' ? '#e8f5e9' : '#feebee'}
                      color={row.status === 'online' ? '#388e3c' : '#e54545'}
                    >
                      <span>{row.status}</span>
                    </StyledStatus>
                  </TableCell>
                  <TableCell align="center" padding="checkbox">
                    <StyledDotOption>
                      <Dotdotdot actions={['VIEW', 'DELETE']} />
                    </StyledDotOption>
                  </TableCell>
                </TableRow>
              );
            })}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <StyledTablePagination>
        <TablePagination
          component="div"
          rowsPerPageOptions={[5, 10, 25]}
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </StyledTablePagination>
    </TableWrapper>
  );
}
