import styled from 'styled-components';

export default styled.div`
  width: calc(100% - 60px);
  height: calc(100% - 64px);
  padding: 30px;
  background-color: #f0f0f0;
  position: relative;
  display: flex;
  justify-content: center;
  .container {
    width: 852px;
    .header {
      height: 80px;
      position: relative;
      .options {
        position: absolute;
        right: 0px;
        top: 0px;
      }
    }

    .body {
      width: 100%;
      padding: 60px 28px 60px 0px;
      background-color: #fff;
      border-radius: 10px;
      .settings {
        width: calc(100% - 270px);
        .setting {
          width: 480px;
          margin-bottom: 35px;
        }
        .setting-text-filed {
          width: 100%;
        }
        .setting-text-description {
          margin-top: 10px;
        }
      }
      .placeholder {
        width: 270px;
        height: 438px;
        padding: 14px 16px 27px 20px;
        border-radius: 10px;
        background-color: #fafafa;
        .p-row {
          padding-top: 10px;
          padding-bottom: 10px;
        }
        .p-txt-left {
          width: 60%;
        }
        .p-txt-right {
          width: 40%;
        }
      }
    }
  }

  .slash {
    margin-left: 5px;
    margin-right: 5px;
  }

  .btn-text {
    margin-left: 5px;
  }
`;
