export type QuickReplyCreateProps = {
  placeholders: string;
  value: string;
  description: string;
};

export type Placeholder = {
  key: string;
  value: string;
};
