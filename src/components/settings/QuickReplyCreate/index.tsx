import React from 'react';
import { findIndex } from 'lodash';
import Wrapper from './styles';
import { Flex } from '../../common/Flex';
import Text from '../../common/Text';
import Button from '../../common/Button';
import { IbTextField, StyledMenuItem } from '../../common/InputField';
import { placeholders } from '../../chat/AddQuickReply';

export const QuickReplyCreate = ({
  onCancel,
  dropdownCateFaq,
  cateSelectIndex,
  setCateSelectIndex,
  shortcut,
  setShortcut,
  message,
  setMessage,
  onSubmit,
}: {
  onCancel: () => void;
  dropdownCateFaq: { name: string; id: string }[];
  cateSelectIndex: number;
  setCateSelectIndex: (index: number) => void;
  shortcut: string;
  setShortcut: (value: string) => void;
  message: string;
  setMessage: (value: string) => void;
  onSubmit: () => void;
}) => {
  const currentCategoryDescription =
    'Write a keyboard shortcut for the message above. Your reply appears when this shortcut is the first word typed in a message.';

  const handleChangeCreated = (e: React.ChangeEvent<HTMLInputElement>) => {
    const index = findIndex(dropdownCateFaq, (o) => o.name === e.target.value);
    setCateSelectIndex(index);
  };
  return (
    <Wrapper>
      <Flex className="container" flexDirection="column">
        <Flex className="header" flexDirection="column">
          <Text size="xl" weight="bold" value="Quick Replies" />
          <Flex className="breadcrumb">
            <Text color="#097af4" size="xsm" value="SETTINGS" />
            <Text className="slash" size="xsm" value="/" />
            <Text size="xsm" value="QUICK REPLIES" />
          </Flex>
          <Flex className="options">
            <Button kind="default" onClick={onCancel}>
              Cancle
            </Button>
            <Button kind="dark" onClick={onSubmit}>
              Save
            </Button>
          </Flex>
        </Flex>
        <Flex className="body">
          <Flex className="settings" flexDirection="column" alignItems="center">
            <Flex className="setting" flexDirection="column">
              <IbTextField
                className="setting-text-filed"
                value={shortcut}
                label="Shortcut"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => setShortcut(e.target.value)}
              />
              <Text className="setting-text-description" size="xsm" value={currentCategoryDescription} />
            </Flex>
            <Flex className="setting" flexDirection="column">
              <IbTextField
                error={false}
                disabled={false}
                readOnly={false}
                required={false}
                select
                label="Selected a Categories"
                value={dropdownCateFaq[cateSelectIndex].name}
                onChange={handleChangeCreated}
                variant="outlined"
              >
                {dropdownCateFaq.map((item) => (
                  <StyledMenuItem value={item.name} key={item.id}>
                    {item.name}
                  </StyledMenuItem>
                ))}
              </IbTextField>
              <Text className="setting-text-description" size="xsm" color="blue">
                <Text
                  size="xsm"
                  value="Categories provide a helpful way to group related messages together. To add/delete a category go to Quick Replies > "
                />
                <Text size="xsm" color="blue" value="Categories" />
              </Text>
            </Flex>
            <Flex className="setting" flexDirection="column">
              <Text color="#0f0f10" weight="bold" value="Message" />
              <IbTextField
                className="setting-text-description"
                multiline
                rows={5}
                placeholder="Text ..."
                value={message}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => setMessage(e.target.value)}
              />
            </Flex>
          </Flex>
          <Flex className="placeholder" flexDirection="column">
            <Text size="md" weight="bold" value="Placeholder" />
            {placeholders.map((p, index) => {
              return (
                // eslint-disable-next-line react/no-array-index-key
                <Flex className="p-row" key={`${p.key}_${index}`}>
                  <Text className="p-txt-left" size="sm" value={p.key} />
                  <Text className="p-txt-right" size="sm" value={p.value} color="#98999f" />
                </Flex>
              );
            })}
          </Flex>
        </Flex>
      </Flex>
    </Wrapper>
  );
};

export default QuickReplyCreate;
