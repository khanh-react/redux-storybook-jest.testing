import React from 'react';

import { QuickReplyAddCategory } from './QuickReplyAddCategory';

export default {
  title: 'Settings/QuickReplyAddCategory',
  component: QuickReplyAddCategory,
};

export const Default = () => {
  return (
    <QuickReplyAddCategory
      closedModal={() => null}
      nameCate="string"
      setNameCate={() => null}
      // descriptionCate="string"
      // setDescriptionCate={() => null}
      submitCreate={() => null}
      deleteCate={() => null}
    />
  );
};
