import React from 'react';
import { QuickReplyCategories } from './QuickReplyCategories';

export default {
  title: 'Settings/QuickReplyCategories',
  component: QuickReplyCategories,
};

export const Default = () => {
  return (
    <QuickReplyCategories
      data={[{ name: 'string', id: 'string' }]}
      handleSearch={() => null}
      submitCaseOption={() => null}
      clickCreateCate={() => null}
      selected={['string']}
      setSelected={() => null}
      backToMain={() => null}
    />
  );
};
