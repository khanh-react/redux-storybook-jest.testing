import React from 'react';
import styled from 'styled-components';
import { AddCircleOutline } from '@material-ui/icons';
import { Tab } from '@material-ui/core';
import Wrapper from '../components/styles';
import { Flex, CenterFlex } from '../../common/Flex';
import { IbTabs } from '../../common/Tabs';
import EnhancedTable from '../components/table';
import { mockdata, Data } from '../components/types';
import Text from '../../common/Text';
import Button from '../../common/Button';

const StyleTab = styled(IbTabs)`
  width: 1289px;
  height: 50px;
  margin: -10px 0px 15px;
  border-radius: 4px;
  background-color: #ffffff;
`;

const StyledTextTab = styled(Tab)`
  text-transform: uppercase;
  width: 159px;
  height: 50px;
  padding: 13px 56px 12px 59px;
`;

export const AllUsers = ({ initialTab }: { initialTab: number }) => {
  const [selectedTab, setSelectedTab] = React.useState(initialTab);
  const [listData, setListData] = React.useState(mockdata);
  const handleChange = (event: React.ChangeEvent<unknown>, newValue: number) => {
    setSelectedTab(newValue);
    let newList: Data[] = mockdata;
    if (newValue === 1) {
      newList = mockdata.filter((item) => {
        return item.status !== 'disable';
      });
    } else if (newValue === 2) {
      newList = mockdata.filter((item) => {
        return item.status !== 'enabled';
      });
    }
    setListData(newList);
  };
  return (
    <Wrapper>
      <div className="header">
        <Text size="xl" weight="bold" value="All Users" />
        <Flex className="breadcrumb">
          <Text color="#097af4" size="xsm" value="SETTINGS" />
          <Text className="slash" size="xsm" value="/" />
          <Text size="xsm" value="USERS" />
        </Flex>
        <Flex className="options">
          <Button kind="dark">
            <CenterFlex>
              <AddCircleOutline fontSize="small" />
              <Text className="btn-text" color="#FFF" value="Create User" />
            </CenterFlex>
          </Button>
        </Flex>
      </div>
      <Flex justifyContent="center" flex={1} flexDirection="column">
        <StyleTab value={selectedTab} onChange={handleChange}>
          <StyledTextTab label="all" />
          <StyledTextTab label="active" />
          <StyledTextTab label="inactive" />
        </StyleTab>
        <EnhancedTable data={listData} />
      </Flex>
    </Wrapper>
  );
};

export default AllUsers;
