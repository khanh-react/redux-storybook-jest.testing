import React from 'react';

import { AllUsers } from './AllUsers';

export default {
  title: 'Settings/AllUsers',
  component: AllUsers,
};

export const Default = () => {
  return <AllUsers initialTab={0} />;
};
