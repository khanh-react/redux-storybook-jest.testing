import React from 'react';
import styled from 'styled-components';
import { Box, TablePagination, TableContainer, TableCell, TableBody, TableHead, TableRow } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import { TableProps as Props } from './types';
import Text from '../../common/Text';
import { Dotdotdot } from '../../common/Dotdotdot';
import { Flex } from '../../common/Flex';

const TableWrapper = styled.div`
  background-color: #fff;
  border-radius: 10px;
  width: 1289px;
  margin: 0px 0px 10px;
`;

const StyledStatus = styled(Box)`
  width: 80px;
  height: 19px;
  border-radius: 4px;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 19px;
  letter-spacing: 0.83px;
  text-transform: uppercase;
  text-align: center;
`;

const StyledTablePagination = styled(Flex)`
  float: right;
  p {
    font-size: 13px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 0.3px;
    text-align: right;
    color: #6c6c72;
  }
`;

export default function EnhancedTable({ data }: Props) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

  return (
    <TableWrapper>
      <TableContainer>
        <Table aria-labelledby="tableTitle" aria-label="enhanced table">
          <TableHead>
            <TableRow>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="USER NAME" />
              </TableCell>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="EMAIL" />
              </TableCell>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="ROLE" />
              </TableCell>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="LAST LOGIN" />
              </TableCell>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="STATUS" />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                  <TableCell align="left">
                    <Text className="cell-text" value={row.userName} />
                  </TableCell>
                  <TableCell align="left">
                    <Text className="cell-text" value={row.email} />
                  </TableCell>
                  <TableCell align="left">
                    <Text className="cell-text" value={row.role} />
                  </TableCell>
                  <TableCell align="left">
                    <Text className="cell-text" value={row.lastLogin} />
                  </TableCell>
                  <TableCell align="left">
                    <StyledStatus
                      bgcolor={row.status === 'enabled' ? '#e8f5e9' : '#feebee'}
                      color={row.status === 'enabled' ? '#388e3c' : '#e54545'}
                    >
                      <span>{row.status}</span>
                    </StyledStatus>
                  </TableCell>
                  <TableCell padding="checkbox" align="center">
                    <Dotdotdot actions={['VIEW', 'DISABLE', 'RE-INVITE']} />
                  </TableCell>
                </TableRow>
              );
            })}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <StyledTablePagination>
        <TablePagination
          component="div"
          rowsPerPageOptions={[5, 10, 25]}
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </StyledTablePagination>
    </TableWrapper>
  );
}
