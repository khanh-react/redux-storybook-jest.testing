export type Data = {
  id: string;
  userName: string;
  email: string;
  role: string;
  lastLogin: string;
  status: string;
};

export type DataChannel = {
  id: string;
  nameChannel: string;
  channel: string;
  owner: string;
  api: string;
  status: string;
};

export type TableProps = {
  data: Data[];
};

export function createData(
  id: string,
  userName: string,
  email: string,
  role: string,
  lastLogin: string,
  status: string,
): Data {
  return { id, userName, email, role, lastLogin, status };
}

export type TableChannelProps = {
  data: DataChannel[];
};

export function createDataChannel(
  id: string,
  nameChannel: string,
  channel: string,
  owner: string,
  api: string,
  status: string,
): DataChannel {
  return { id, nameChannel, channel, owner, api, status };
}

export const mockdata = [
  createData('/welcome', 'James Corden', 'A short welcome message for customers 1', '', '2 days ago', 'enabled'),
  createData('/payment', 'James Corden', 'A short welcome message for customers 2', '', '1 week ago', 'enabled'),
  createData('/thankyou', 'James Corden', 'A short welcome message for customers 3', '', '13/02/2020', 'enabled'),
  createData('/shiping', 'James Corden', 'A short welcome message for customers 4', '', '04/02/2020', 'enabled'),
  createData('/teams', 'James Corden', 'A short welcome message for customers 5', '', '21/12/2019', 'enabled'),
  createData('/welcome1', 'James Corden', 'A short welcome message for customers 6', '', '2 days ago', 'enabled'),
  createData('/payment1', 'James Corden', 'A short welcome message for customers 7', '', '1 week ago', 'enabled'),
  createData('/thankyou1', 'James Corden', 'A short welcome message for customers 8', '', '13/02/2020', 'disable'),
  createData('/shiping1', 'James Corden', 'A short welcome message for customers 9', '', '04/02/2020', 'disable'),
  createData('/teams1', 'James Corden', 'A short welcome message for customers 10', '', '21/12/2019', 'disable'),
  createData('/welcome2', 'James Corden', 'A short welcome message for customers 11', '', '2 days ago', 'disable'),
  createData('/payment2', 'James Corden', 'A short welcome message for customers 12', '', '1 week ago', 'disable'),
  createData('/thankyou2', 'James Corden', 'A short welcome message for customers 13', '', '13/02/2020', 'disable'),
  createData('/shiping2', 'James Corden', 'A short welcome message for customers 14', '', '04/02/2020', 'disable'),
  createData('/teams2', 'James Corden', 'A short welcome message for customers 15', '', '21/12/2019', 'disable'),
];

export const mockdataChannel = [
  createDataChannel('JG HK CS', 'JG HK CS', 'whatsapp', '+852 8888 9999', 'api-key-KhPfxwvoD…', 'online'),
  createDataChannel('/payment', 'James Corden', 'whatsapp', '+852 8888 9999', 'api-key-KhPfxwvoD…', 'online'),
  createDataChannel(
    '/shiping',
    'James Corden',
    'A short welcome message for customers 4',
    '+852 8888 9999',
    'api-key-KhPfxwvoD…',
    'online',
  ),
  createDataChannel(
    '/teams',
    'James Corden',
    'A short welcome message for customers 5',
    '+852 8888 9999',
    'api-key-KhPfxwvoD…',
    'online',
  ),
  createDataChannel(
    '/welcome1',
    'James Corden',
    'A short welcome message for customers 6',
    '+852 8888 9999',
    'api-key-KhPfxwvoD…',
    'online',
  ),
  createDataChannel(
    '/payment1',
    'James Corden',
    'A short welcome message for customers 7',
    '+852 8888 9999',
    'api-key-KhPfxwvoD…',
    'online',
  ),
  createDataChannel(
    '/thankyou1',
    'James Corden',
    'A short welcome message for customers 8',
    '+852 8888 9999',
    '13/02/2020',
    'disabled',
  ),
  createDataChannel(
    '/shiping1',
    'James Corden',
    'A short welcome message for customers 9',
    '+852 8888 9999',
    '04/02/2020',
    'disabled',
  ),
  createDataChannel(
    '/teams1',
    'James Corden',
    'A short welcome message for customers 10',
    '+852 8888 9999',
    '21/12/2019',
    'disabled',
  ),
  createDataChannel(
    '/welcome2',
    'James Corden',
    'A short welcome message for customers 11',
    '+852 8888 9999',
    '2 days ago',
    'disabled',
  ),
  createDataChannel(
    '/payment2',
    'James Corden',
    'A short welcome message for customers 12',
    '+852 8888 9999',
    '1 week ago',
    'disabled',
  ),
  createDataChannel(
    '/thankyou2',
    'James Corden',
    'A short welcome message for customers 13',
    '+852 8888 9999',
    '13/02/2020',
    'disabled',
  ),
  createDataChannel(
    '/shiping2',
    'James Corden',
    'A short welcome message for customers 14',
    '+852 8888 9999',
    '04/02/2020',
    'disabled',
  ),
  createDataChannel(
    '/teams2',
    'James Corden',
    'A short welcome message for customers 15',
    '+852 8888 9999',
    '21/12/2019',
    'disabled',
  ),
];
