import styled from 'styled-components';

export default styled.div`
  width: 1295px;
  padding: 30px;
  background-color: #f0f0f0;
  position: relative;

  .header {
    display: flex;
    flex-direction: column;
    height: 80px;
  }

  .options {
    position: absolute;
    right: 30px;
    top: 20px;
    .MuiButtonBase-root {
      margin-right: 20px;
    }
  }

  .slash {
    margin-left: 5px;
    margin-right: 5px;
  }

  .btn-text {
    margin-left: 5px;
  }

  .cell-text {
    font-family: Helvetica !important;
  }
`;

export const StyledTitle = styled.span`
  font-size: 24px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.17;
  letter-spacing: normal;
  color: #3b3b3b;
  margin-bottom: 10px;
`;

export const StyledTextButton = styled.span`
  ${({ color }) => `
  font-size: 15px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.73;
  letter-spacing: 0.3px;
  text-align: center;
  color: ${color};
`}
`;
