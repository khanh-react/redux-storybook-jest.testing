export type Data = {
  id: string;
  name: string;
};

export type TableProps = {
  data: Data[];
  handleSearch: (text: string) => void;
  submitCaseOption: (data: Data, onCase: string) => void;
  clickCreateCate?: () => void;
  setSelected: (value: string[]) => void;
  selected: string[];
  backToMain?: () => void;
};
