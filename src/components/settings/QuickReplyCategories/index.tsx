import React from 'react';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Wrapper from './styles';
import Table from './table';
import { TableProps as Props } from './types';
import { Flex, CenterFlex, PointerFlex } from '../../common/Flex';
import Text from '../../common/Text';
import Button from '../../common/Button';

export const QuickReplyCategories = ({
  data,
  handleSearch,
  submitCaseOption,
  clickCreateCate,
  selected,
  setSelected,
  backToMain,
}: Props) => {
  return (
    <Wrapper>
      <Flex className="header" flexDirection="column">
        <Text size="xl" weight="bold" value="Categories" />
        <Flex className="breadcrumb">
          <Text color="#097af4" size="xsm" value="SETTINGS" />
          <Text className="slash" size="xsm" value="/" />
          <PointerFlex onClick={backToMain}>
            <Text color="#097af4" size="xsm" value="QUICK REPLIES" />
          </PointerFlex>

          <Text className="slash" size="xsm" value="/" />
          <Text size="xsm" value="CATEGORIES" />
        </Flex>
        <Flex className="options">
          <Button kind="dark" onClick={clickCreateCate}>
            <CenterFlex>
              <AddCircleOutlineIcon fontSize="small" />
              <Text className="btn-text" color="#FFF" value="Create categories" />
            </CenterFlex>
          </Button>
        </Flex>
      </Flex>
      <Table
        data={data}
        handleSearch={handleSearch}
        submitCaseOption={submitCaseOption}
        setSelected={(value: string[]) => setSelected(value)}
        selected={selected}
      />
    </Wrapper>
  );
};

export default QuickReplyCategories;
