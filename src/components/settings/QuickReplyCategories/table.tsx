import React, { useRef } from 'react';
import styled from 'styled-components';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import SearchIcon from '@material-ui/icons/Search';
import { TableProps as Props } from './types';
import { Flex, PointerFlex } from '../../common/Flex';
import Text from '../../common/Text';
import { Dotdotdot } from '../../common/Dotdotdot';
import { TextFieldCus } from '../QuickReplySettings/table';

const TableWrapper = styled.div`
  background-color: #fff;
  border-radius: 10px;
  width: 100%;
`;

const TableOptionsWrapper = styled.div`
  padding: 20px 0px 20px 34px;
  position: relative;
  .txt-search {
    float: left;
    height: 60px;
  }
  .pagination {
    position: absolute;
    right: 0;
  }
`;

export default function EnhancedTable({ data, handleSearch, submitCaseOption, selected, setSelected }: Props) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const isSelected = (name: string) => selected.indexOf(name) !== -1;
  const refInput = useRef<HTMLInputElement>(null);
  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
    }

    setSelected(newSelected);
  };

  const handleClickAll = () => {
    setSelected([]);
    if (!isSelected('ALL')) {
      const newSelected: string[] = [];
      data.forEach((e) => {
        newSelected.push(e.id);
      });
      newSelected.push('ALL');
      setSelected(newSelected);
    }
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
  const handlePress = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key === 'Enter' && refInput.current !== null) {
      handleSearch(refInput.current.value);
    }
  };

  const onSearch = () => {
    if (refInput.current !== null) {
      handleSearch(refInput.current.value);
    }
  };
  const searchTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value === '') {
      handleSearch('');
    }
  };

  return (
    <TableWrapper>
      <TableOptionsWrapper>
        <Flex position="relative" onKeyPress={handlePress}>
          <PointerFlex position="absolute" left="25px" top="17px" onClick={onSearch} zIndex={2}>
            <SearchIcon />
          </PointerFlex>

          <TextFieldCus
            className="txt-search"
            placeholder="Search"
            variant="outlined"
            size="small"
            inputRef={refInput}
            onChange={searchTextChange}
          />
        </Flex>
        <TablePagination
          className="pagination"
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </TableOptionsWrapper>
      <TableContainer>
        <Table aria-labelledby="tableTitle" aria-label="enhanced table">
          <TableHead>
            <TableRow>
              <TableCell padding="checkbox">
                <Checkbox inputProps={{ 'aria-labelledby': 'all-ids' }} onClick={handleClickAll} />
              </TableCell>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="NAME" />
              </TableCell>
              {/* <TableCell align="left">
                <Text color="#98999f" weight="bold" value="DESCRIPTION" />
              </TableCell> */}
              {/* <TableCell align="left">
                <Text color="#98999f" weight="bold" value="CREATED BY" />
              </TableCell> */}
              <TableCell align="left" />
            </TableRow>
          </TableHead>
          <TableBody>
            {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
              const isItemSelected = isSelected(row.id);
              const labelId = `enhanced-table-checkbox-${index}`;
              return (
                <TableRow
                  hover
                  role="checkbox"
                  aria-checked={isItemSelected}
                  tabIndex={-1}
                  key={row.id}
                  selected={isItemSelected}
                >
                  <TableCell padding="checkbox">
                    <Checkbox
                      onClick={(event) => handleClick(event, row.id)}
                      checked={isItemSelected}
                      inputProps={{ 'aria-labelledby': labelId }}
                    />
                  </TableCell>
                  <TableCell align="left">
                    <Text className="cell-text" value={row.name} />
                  </TableCell>
                  {/* <TableCell align="left">
                    <Text className="cell-text" value={row.description} />
                  </TableCell> */}
                  {/* <TableCell align="left">
                    <Flex flexDirection="column">
                      <Text className="cell-text" value={row.createdBy} />
                      <Text className="cell-text" size="xsm" value={row.createdAt} />
                    </Flex>
                  </TableCell> */}
                  <TableCell padding="checkbox" align="center">
                    <Dotdotdot actions={['VIEW', 'DELETE']} submit={(value: string) => submitCaseOption(row, value)} />
                  </TableCell>
                </TableRow>
              );
            })}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </TableWrapper>
  );
}
