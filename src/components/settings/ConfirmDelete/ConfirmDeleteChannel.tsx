import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import React from 'react';
import styled from 'styled-components';

import Button from '../../common/Button';
import { Flex } from '../../common/Flex';
import { IbTextField } from '../../common/InputField';
import Wrapper from '../components/styles';

const StyledTitle = styled(DialogTitle)`
  h2 {
    font-size: 22px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1;
    letter-spacing: normal;
    color: #e54545;
  }
`;

const StyledInput = styled(IbTextField)`
  width: 280px;
  border-radius: 8px;
  .MuiOutlinedInput-root {
    height: 42px;
  }
`;

const StyledTitleBg = styled(Flex)`
  background: #e54545;
  height: 6px;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
`;

const ContainerDialog = styled(Dialog)`
  border-radius: 12px;
  .MuiDialog-paperScrollPaper {
    overflow: unset;
  }
  .MuiDialog-paperWidthSm {
    width: 600px;
  }
`;
const StyledDialogActions = styled(DialogActions)`
  padding: 0 15px 15px;
`;
const StyledDialogContentText = styled(DialogContentText)`
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: 0.1px;
  color: #2e2e32;
  margin-bottom: 25px;
`;

export const ConfirmDeleteChannel = ({ isShowModal, onClose }: { isShowModal: boolean; onClose?(): void }) => {
  return (
    <Wrapper>
      <ContainerDialog
        open={isShowModal}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <StyledTitleBg flexDirection="column"> </StyledTitleBg>
        <StyledTitle>Confirm Delete</StyledTitle>
        <DialogContent>
          <StyledDialogContentText id="alert-dialog-description">
            Are you sure you want to delete this channel ?
          </StyledDialogContentText>
          <StyledDialogContentText id="alert-dialog-description">
            Please type <strong>&quot;confirm&quot;</strong> to make sure you understand the deletion cannot be
            reverted.
          </StyledDialogContentText>
          <StyledInput />
        </DialogContent>
        <StyledDialogActions>
          <Button kind="cancel" onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button kind="warning" onClick={onClose} color="primary">
            Confirm
          </Button>
        </StyledDialogActions>
      </ContainerDialog>
    </Wrapper>
  );
};

export default ConfirmDeleteChannel;
