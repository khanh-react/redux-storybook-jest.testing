import React from 'react';
import styled from 'styled-components';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Wrapper from '../components/styles';
import { Flex } from '../../common/Flex';
import Button from '../../common/Button';

const StyledTitle = styled(DialogTitle)`
  h2 {
    font-size: 22px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1;
    letter-spacing: normal;
    color: #e54545;
  }
`;

const StyledTitleBg = styled(Flex)`
  background: #e54545;
  height: 6px;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
`;

const ContainerDialog = styled(Dialog)`
  border-radius: 12px;
  .MuiDialog-paperScrollPaper {
    overflow: unset;
  }
  .MuiDialog-paperWidthSm {
    width: 600px;
  }
`;

const StyledDialogContentText = styled(DialogContentText)`
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: 0.1px;
  color: #2e2e32;
  margin-bottom: 50px;
`;

export const ConfirmDelete = ({ isShowModal, onClose }: { isShowModal: boolean; onClose?(): void }) => {
  return (
    <Wrapper>
      <ContainerDialog
        open={isShowModal}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <StyledTitleBg flexDirection="column"> </StyledTitleBg>
        <StyledTitle>Confirm Delete</StyledTitle>
        <DialogContent>
          <StyledDialogContentText id="alert-dialog-description">
            Are you sure you want to delete this file? You can’t undo this action.
          </StyledDialogContentText>
        </DialogContent>
        <DialogActions>
          <Button kind="default" onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button kind="dark" onClick={onClose} color="primary">
            Confirm
          </Button>
        </DialogActions>
      </ContainerDialog>
    </Wrapper>
  );
};

export default ConfirmDelete;
