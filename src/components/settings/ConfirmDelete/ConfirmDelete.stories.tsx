import React from 'react';

import { ConfirmDelete } from './ConfirmDelete';
import { ConfirmDeleteChannel } from './ConfirmDeleteChannel';

export default {
  title: 'Settings/ConfirmDelete',
  component: ConfirmDelete,
};

export const Default = () => {
  return <ConfirmDelete isShowModal />;
};

export const Channel = () => {
  return <ConfirmDeleteChannel isShowModal />;
};
