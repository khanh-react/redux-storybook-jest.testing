import styled from 'styled-components';

export default styled.div`
  width: calc(100% - 60px);
  height: 100%;
  padding: 30px;
  background-color: #f0f0f0;
  position: relative;

  .header {
    height: 80px;
  }

  .options {
    position: absolute;
    right: 30px;
    top: 20px;
  }

  .slash {
    margin-left: 5px;
    margin-right: 5px;
  }

  .btn-text {
    margin-left: 5px;
  }

  .cell-text {
    font-family: Helvetica !important;
  }
`;
