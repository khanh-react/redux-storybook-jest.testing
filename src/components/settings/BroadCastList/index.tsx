import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import React from 'react';

import Button from '../../common/Button';
import { CenterFlex, Flex } from '../../common/Flex';
import Text from '../../common/Text';
import Wrapper from './styles';
import Table from './table';
import { TableProps as Props } from './types';

export const BroadcastList = ({ data }: Props) => {
  return (
    <Wrapper>
      <Flex className="header" flexDirection="column">
        <Text size="xl" weight="bold" value="Broadcast" />
        <Flex className="breadcrumb">
          <Text color="#097af4" size="xsm" value="SETTINGS" />
          <Text className="slash" size="xsm" value="/" />
          <Text size="xsm" value="TRIGGERS" />
        </Flex>
        <Flex className="options">
          <Button kind="dark">
            <CenterFlex>
              <AddCircleOutlineIcon fontSize="small" />
              <Text className="btn-text" color="#FFF" value="Add broadcast" />
            </CenterFlex>
          </Button>
        </Flex>
      </Flex>
      <Table data={data} />
    </Wrapper>
  );
};

export default BroadcastList;
