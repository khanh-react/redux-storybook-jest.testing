import React from 'react';
import styled from 'styled-components';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import SearchIcon from '@material-ui/icons/Search';
import { TableProps as Props } from './types';
import { Flex } from '../../common/Flex';
import Text from '../../common/Text';
import TextField from '../../common/TextField';
import { Dotdotdot } from '../../common/Dotdotdot';

const TableWrapper = styled.div`
  background-color: #fff;
  border-radius: 10px;
  width: 100%;
`;

const TableOptionsWrapper = styled.div`
  padding: 20px 0px 20px 34px;
  position: relative;
  .txt-search {
    float: left;
    height: 60px;
  }
  .pagination {
    position: absolute;
    right: 0;
  }
`;

export default function EnhancedTable({ data }: Props) {
  const [selected, setSelected] = React.useState<string[]>([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
    }

    setSelected(newSelected);
  };

  const handleClickAll = () => {
    setSelected([]);
    if (!isSelected('ALL')) {
      const newSelected: string[] = [];
      data.forEach((e) => {
        newSelected.push(e.id);
      });
      newSelected.push('ALL');
      setSelected(newSelected);
    }
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

  const TableOptions = () => {
    return (
      <TableOptionsWrapper>
        <TextField className="txt-search" value="" placeholder="Search" icon={<SearchIcon />} />
        <TablePagination
          className="pagination"
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </TableOptionsWrapper>
    );
  };

  return (
    <TableWrapper>
      <TableOptions />
      <TableContainer>
        <Table aria-labelledby="tableTitle" aria-label="enhanced table">
          <TableHead>
            <TableRow>
              <TableCell padding="checkbox">
                <Checkbox inputProps={{ 'aria-labelledby': 'all-ids' }} onClick={handleClickAll} />
              </TableCell>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="NAME" />
              </TableCell>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="SENT TIME ( SENT ON/ SCHEDULED ON)" />
              </TableCell>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="TOTAL SENT" />
              </TableCell>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="CREATED BY" />
              </TableCell>
              <TableCell align="left" />
            </TableRow>
          </TableHead>
          <TableBody>
            {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
              const isItemSelected = isSelected(row.id);
              const labelId = `enhanced-table-checkbox-${index}`;
              return (
                <TableRow
                  hover
                  role="checkbox"
                  aria-checked={isItemSelected}
                  tabIndex={-1}
                  key={row.id}
                  selected={isItemSelected}
                >
                  <TableCell padding="checkbox">
                    <Checkbox
                      onClick={(event) => handleClick(event, row.id)}
                      checked={isItemSelected}
                      inputProps={{ 'aria-labelledby': labelId }}
                    />
                  </TableCell>
                  <TableCell align="left">
                    <Text className="cell-text" value={row.name} />
                  </TableCell>
                  <TableCell align="left">
                    <Text className="cell-text" value={row.sentTime} />
                  </TableCell>
                  <TableCell align="left">
                    <Text className="cell-text" value={row.sentTotal.toString()} />
                  </TableCell>
                  <TableCell align="left">
                    <Flex flexDirection="column">
                      <Text className="cell-text" value={row.createdBy} />
                      <Text className="cell-text" size="xsm" value={row.createdAt} />
                    </Flex>
                  </TableCell>
                  <TableCell padding="checkbox" align="center">
                    <Dotdotdot actions={['VIEW', 'DELETE']} />
                  </TableCell>
                </TableRow>
              );
            })}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </TableWrapper>
  );
}
