export type Data = {
  id: string;
  name: string;
  sentTime: string;
  sentTotal: number;
  createdBy: string;
  createdAt: string;
};

export type TableProps = {
  data: Data[];
};

export function createData(
  id: string,
  name: string,
  sentTime: string,
  sentTotal: number,
  createdBy: string,
  createdAt: string,
): Data {
  return { id, name, sentTime, sentTotal, createdBy, createdAt };
}
