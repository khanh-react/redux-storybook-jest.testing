import styled from 'styled-components';
import { Flex } from '../../../common/Flex';

export default styled(Flex)`
  width: 400px;
  padding-top: 46px;

  .container {
    width: 338px;
    height: 423px;
    padding: 46px 21px 21px 17px;
    border-radius: 10px;
    box-shadow: 0 2px 7px 0 rgba(0, 0, 0, 0.2);
    background-color: #f6f6f6;
    position: relative;
  }

  .head {
    width: 260px;
    height: 54px;
    background-color: #fff;
    padding-left: 10px;
    border-radius: 12px;
    .big-line {
      width: 226px;
      height: 12px;
      margin: 0 0 7px;
      border-radius: 6.5px;
      background-color: #e9e9ed;
    }
    .small-line {
      width: 120px;
      height: 12px;
      border-radius: 6.5px;
      background-color: #e9e9ed;
    }
  }

  .body {
    height: 124px;
    padding: 7px 9px !important;
    margin-top: 49px;
    border-radius: 12px;
    background-color: #ffe880 !important;
    .thumb {
      width: 80px;
      height: 80px;
      background-color: #fff;
    }
    .brand {
      width: calc(100% - 80px);
      height: 80px;
      padding: 10px;
      background-color: rgba(0, 0, 0, 0.1);
    }
  }

  .foot {
    width: 338px;
    height: 64px;
    background: #fff;
    margin-left: -17px;
    border-bottom-right-radius: 10px;
    border-bottom-left-radius: 10px;
    position: absolute;
    bottom: 0;
    .comment {
      width: 268px;
      height: 33px;
      margin: 0 12px 0 0;
      border-radius: 18px;
      background-color: #f6f6f6;
    }
  }

  .txt {
    margin: 5px 10px;
  }

  .mrt5 {
    margin-top: 5px;
  }

  .mrt10 {
    margin-top: 10px;
  }
`;
