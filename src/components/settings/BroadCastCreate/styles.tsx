import styled from 'styled-components';

export default styled.div`
  width: calc(100% - 60px);
  height: 100%;
  padding: 30px;
  background-color: #f0f0f0;
  position: relative;

  .container {
    width: 1070px;
    .header {
      height: 80px;
      position: relative;
      .options {
        position: absolute;
        right: 0px;
        top: 0px;
      }
    }

    .body {
      width: 100%;
      padding: 45px 70px 45px 41px;
      background-color: #fff;
      border-radius: 10px;
      .settings {
        width: calc(100% - 400px);
        .setting {
          width: 480px;
          margin-bottom: 20px;
          .time {
            margin-left: auto;
          }
        }
        .setting-text-filed {
          width: 100%;
        }
        .setting-text-description {
          margin-top: 10px;
        }
      }
      .placeholder {
        width: 400px;
        /* height: 350px; */
        padding: 14px 16px 15px 20px;
        border-radius: 10px;
        background-color: #fafafa;
        .p-row {
          padding-top: 10px;
          padding-bottom: 10px;
        }
        .p-txt-left {
          width: 60%;
        }
        .p-txt-right {
          width: 40%;
        }
      }
    }
  }

  .slash {
    margin-left: 5px;
    margin-right: 5px;
  }

  .btn-text {
    margin-left: 5px;
  }

  .mrr8 {
    margin-right: 8px;
  }
`;
