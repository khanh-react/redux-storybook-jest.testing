import styled from 'styled-components';
import { Flex } from '../../../common/Flex';

export default styled(Flex)`
  width: calc(100% - 400px);
  padding-right: 83px;

  .container {
    width: 100%;
  }

  button {
    &.MuiTab-root {
      width: 33%;
    }
  }

  .tab {
    span {
      color: #000;
    }
  }

  .row-item {
    margin-bottom: 10px;
    input {
      font-weight: 600;
      font-size: 14px;
    }
  }

  .file {
  }

  .mgt-20 {
    margin-top: 20px;
  }

  .mgt-15 {
    margin-top: 15px;
  }
`;
