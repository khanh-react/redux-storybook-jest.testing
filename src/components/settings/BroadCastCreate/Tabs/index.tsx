import React, { useState } from 'react';
import Tab from '@material-ui/core/Tab';
import Wrapper from './styles';
import MessageTab from './Message';
import DocumentTab from './Document';
import LinkTab from './Link';
import { Flex } from '../../../common/Flex';
import { IbTabs } from '../../../common/Tabs';

type Props = {
  onChange: (currentTab: number) => void;
};

export const Tabs = ({ onChange }: Props) => {
  const [currentTab, setTab] = useState(0);

  const handleTabsChange = (e: React.ChangeEvent<unknown>, value: number) => {
    setTab(value);
    onChange(value);
  };

  return (
    <Wrapper>
      <Flex className="container" flexDirection="column">
        <IbTabs value={currentTab} onChange={handleTabsChange}>
          <Tab className="tab" label="Message" />
          <Tab className="tab" label="Document" />
          <Tab className="tab" label="Link" />
        </IbTabs>
        <MessageTab isVisible={currentTab === 0} />
        <DocumentTab isVisible={currentTab === 1} />
        <LinkTab isVisible={currentTab === 2} />
      </Flex>
    </Wrapper>
  );
};

export default Tabs;
