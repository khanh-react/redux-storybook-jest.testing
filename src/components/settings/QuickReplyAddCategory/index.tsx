import React from 'react';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/DeleteOutline';
import ModalWrapper from './styles';
import { Flex, PointerFlex } from '../../common/Flex';
import Text from '../../common/Text';
import Button from '../../common/Button';
import { IbTextField } from '../../common/InputField';

export const QuickReplyAddCategory = ({
  closedModal,
  nameCate,
  setNameCate,
  // descriptionCate,
  // setDescriptionCate,
  submitCreate,
  deleteCate,
}: {
  closedModal: () => void;
  nameCate: string;
  setNameCate: (value: string) => void;
  // descriptionCate: string;
  // setDescriptionCate: (value: string) => void;
  submitCreate: () => void;
  deleteCate: () => void;
}) => {
  return (
    <ModalWrapper size="medium">
      <Flex className="header">
        <Text weight="bold" value="Add Category" />
        <PointerFlex onClick={closedModal}>
          <CloseIcon className="close" onClick={closedModal} />
        </PointerFlex>
      </Flex>
      <Flex className="body" flexDirection="column">
        <Flex className="column">
          <IbTextField
            className="category-name"
            label="Name*"
            value={nameCate}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setNameCate(e.target.value)}
          />
        </Flex>
        {/* <Flex className="column" flexDirection="column">
          <Text className="description-label" size="xsm" value="Description" />
          <IbTextField
            className="description-content"
            value={descriptionCate}
            placeholder="Text ..."
            multiline
            rows={5}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setDescriptionCate(e.target.value)}
          />
        </Flex> */}
      </Flex>
      <Flex className="footer">
        <Flex className="footer-wrap" alignItems="center">
          <Flex className="left">
            <Button kind="danger" icon={<DeleteIcon />} onClick={deleteCate}>
              Delete
            </Button>
          </Flex>
          <Flex className="right">
            <Button kind="default" onClick={closedModal}>
              Cancel
            </Button>
            <Button kind="dark" onClick={() => nameCate !== '' && submitCreate()}>
              Create
            </Button>
          </Flex>
        </Flex>
      </Flex>
    </ModalWrapper>
  );
};

export default QuickReplyAddCategory;
