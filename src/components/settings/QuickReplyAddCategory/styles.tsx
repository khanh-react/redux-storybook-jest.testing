import styled from 'styled-components';
import FilterModal from '../../common/FilterModal';

export default styled(FilterModal)`
  width: 100%;
  position: fixed;
  top: 0;
  right: 0;
  overflow: hidden;
  .header {
    width: 100%;
    height: 72px;
    position: relative;
    background-color: #fff;
    padding: 26px 32px 26px 32px;
    border-bottom: 1px solid #eeeeee;
    .close {
      position: absolute;
      right: 10px;
      top: 20px;
    }
  }
  .body {
    width: 100%;
    padding: 41px;
    overflow-y: scroll;
    position: relative;
    .shortcut-name {
      font-weight: 600;
    }
    .column {
      width: 100%;
      margin-bottom: 28px;
    }
    .category-name {
      input {
        font-size: 14px;
        font-weight: 600;
      }
    }
    .description-label {
      margin-left: 12px;
      margin-bottom: 3px;
    }
    .description-content {
      .MuiInputBase-root {
        border-radius: 10px;
      }
      textarea {
        font-size: 14px;
        font-weight: 600;
      }
    }
    &::-webkit-scrollbar {
      display: none;
    }
  }
  .footer {
    width: 100%;
    height: 72px;
    padding-left: 41px;
    padding-right: 41px;
    position: absolute;
    bottom: 0;
    border-top: 1px solid #eeeeee;
    .footer-wrap {
      width: 100%;
      height: 100%;
      position: relative;
      .left {
        position: absolute;
        left: 0;
      }
      .right {
        position: absolute;
        right: 0;
      }
    }
  }
`;
