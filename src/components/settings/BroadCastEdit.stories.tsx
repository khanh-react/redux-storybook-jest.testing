import React from 'react';

import { BroadCastEdit } from './BroadCastEdit';

export default {
  title: 'Settings/Broadcast/BroadCastEdit',
  component: BroadCastEdit,
};

export const Default = () => {
  return <BroadCastEdit />;
};
