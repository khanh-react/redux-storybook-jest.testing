import React from 'react';

import { QuickReplySettings } from './QuickReplySettings';

export default {
  title: 'Settings/QuickReply',
  component: QuickReplySettings,
};

export const Default = () => {
  return <QuickReplySettings />;
};
