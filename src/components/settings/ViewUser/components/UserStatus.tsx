import React, { useState } from 'react';
import styled from 'styled-components';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Avatar, FormControlLabel, Switch } from '@material-ui/core';
import { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import { Add, Remove } from '@material-ui/icons';
import { Flex } from '../../../common/Flex';
import { IbTextField } from '../../../common/InputField';
import Text from '../../../common/Text';
import { TagSkill } from '../../InviteUser/components/types';
import InputTag from '../../../common/InputTag';

const StyleContainer = styled(Flex)`
  width: 1070px;
  padding: 40px 45px 56px 38px;
  border-radius: 10px;
  box-shadow: 0 2px 8px 0 rgba(210, 210, 210, 0.5);
  border: solid 1px #dcdcdc;
  background-color: #ffffff;
`;
const StyledInfoUser = styled(Flex)`
  width: 50%;
`;

const StyledTextStatus = styled(Text)`
  font-size: 15px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.71;
  letter-spacing: normal;
  color: #2e2e32;
`;

const StyledAvatar = styled(Avatar)`
  margin-top: 30px;
  background-color: #f16f63;
  width: 60px;
  height: 60px;
  span {
    font-size: 20px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 0.43px;
    text-align: center;
  }
`;

const StyleNameAvarta = styled(Text)`
  margin: 30px 0px 0px 30px;
  align-self: center;
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.34px;
  color: #2e2e32;
}
`;

const StyleTextRouting = styled.span`
  width: 490px;
  font-size: 12px;
  margin: 25px 0px 0px 30px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.3px;
  color: #98999f;
`;

const StyleInput = styled(IbTextField)`
  margin: 30px 0px 0px 0px;
`;

const StyleInputTeams = styled(IbTextField)`
  margin: 0px 0px 0px 30px;
`;

const StyleInputSkill = styled(IbTextField)`
  margin: 45px 0px 0px 30px;
`;

const StyleRouting = styled(Text)`
  margin: 0px 0px 0px 5px;
  font-size: 12px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: 0.3px;
`;

const StyledTextSkill = styled(Text)`
  margin: 60px 0px 0px 63px;
  font-size: 12px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1;
  letter-spacing: 0.4px;
  color: #0f0f10;
`;

const StyleInputTag = styled(Flex)`
  margin: 18px 0px 0px 30px;
  .MuiBox-root {
    margin: 5px 10px 0px 0px;
  }
`;

const StyleChatlimted = styled(Flex)`
  margin: 65px 0px 0px 30px;
`;

const StyleIconRemove = styled(Remove)`
  height: 27px;
  margin-right: 5px;
`;

const StyleIconAdd = styled(Add)`
  height: 27px;
  margin-left: 5px;
  color: #98999f;
`;

const StyleNumber = styled(Flex)``;

const StyleCountNumber = styled(IbTextField)`
  width: 60px;
  height: 30px;
  border-radius: 4px;
  .MuiOutlinedInput-input {
    padding: 4px 22px 6px;
  }
`;

const StyledSwitch = styled(FormControlLabel)`
  margin: -9px 0 0 0;
  .MuiSwitch-track {
    height: unset;
  }
`;

const StyledEnable = styled.span`
  font-size: 12px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.13;
  letter-spacing: normal;
  color: #98999f;
`;

interface Styles extends Partial<Record<SwitchClassKey, string>> {
  focusVisible?: string;
}

interface Props extends SwitchProps {
  classes: Styles;
}

const IOSSwitch = withStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 40,
      height: 23,
      padding: 0,
      margin: theme.spacing(1),
    },
    switchBase: {
      padding: 1,
      '&$checked': {
        transform: 'translateX(16px)',
        color: theme.palette.common.white,
        '& + $track': {
          backgroundColor: '#2e2e32',
          opacity: 1,
          border: 'none',
        },
      },
      '&$focusVisible $thumb': {
        color: '#2e2e32',
        border: '6px solid #fff',
      },
    },
    thumb: {
      width: 20,
      height: 20,
    },
    track: {
      borderRadius: 26 / 2,
      border: `1px solid ${theme.palette.grey[400]}`,
      backgroundColor: theme.palette.grey[50],
      opacity: 1,
      transition: theme.transitions.create(['background-color', 'border']),
    },
    checked: {},
    focusVisible: {},
  }),
)(({ classes, ...props }: Props) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});

export const UserStatus = ({ disabled }: { disabled?: boolean }) => {
  const [listTagSkill, setListTagSkill] = useState(TagSkill);
  const [number, setNumber] = useState(10);
  const [state, setState] = useState({ checkedB: true });

  const calculator = (type: string) => {
    let count: number = number;
    if (!disabled) {
      if (type === 'sub') {
        count--;
      } else {
        count++;
      }
    }
    setNumber(count);
  };

  const removeTag = (id: string) => {
    let NewListTagSkill = listTagSkill.splice(0, listTagSkill.length);
    NewListTagSkill = NewListTagSkill.filter((item) => {
      return item.id !== id;
    });
    setListTagSkill(NewListTagSkill);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  return (
    <StyleContainer flexDirection="row">
      <StyledInfoUser flexDirection="column">
        <Flex justifyContent="space-between">
          <StyledTextStatus value="User Status" />
          <Flex flexDirection="row">
            <StyledEnable>Enable</StyledEnable>
            {!disabled ? (
              <StyledSwitch
                control={<IOSSwitch checked={state.checkedB} onChange={handleChange} name="checkedB" />}
                label=""
              />
            ) : (
              ''
            )}
          </Flex>
        </Flex>
        <Flex flexDirection="row">
          <StyledAvatar>
            <span>RC</span>
          </StyledAvatar>
          <StyleNameAvarta>Rachel Chan</StyleNameAvarta>
        </Flex>
        <StyleInput disabled={disabled} defaultValue="Agent" label="Role" />
        <StyleInput disabled={disabled} label="Name" defaultValue="William Wong" />
        <StyleInput disabled label="Email" defaultValue="william.wong@imbee.io" />
        <StyleInput disabled={disabled} label="Phone Number" defaultValue="+852 9888 8888" />
      </StyledInfoUser>
      <StyledInfoUser flexDirection="column">
        <StyleInputTeams disabled={disabled} label="Teams" defaultValue="Hong Kong Team" />
        {disabled ? (
          <StyledTextSkill value="Skills" />
        ) : (
          <StyleInputSkill label="Skills" placeholder="Add your skills…" focused />
        )}
        <StyleInputTag flexDirection="row" flexWrap="wrap">
          {listTagSkill &&
            listTagSkill
              .filter(function remove(item) {
                return item.name !== '';
              })
              .map((item) => {
                return (
                  <Flex key={item.id} mr="10px">
                    <InputTag id={item.id} onClose={(id) => removeTag(id)} hasCloseIcon label={item.name} />
                  </Flex>
                );
              })}
        </StyleInputTag>
        <StyleTextRouting>
          Identify the agent’s capabilities. Assign up to 5 skills per agent. Add Skills under
          <StyleRouting value="Routing > Setting." />
        </StyleTextRouting>
        <StyleChatlimted justifyContent="space-between">
          <Text value="Chat Limit" />
          <StyleNumber>
            <StyleIconRemove onClick={() => calculator('sub')} fontSize="small" />
            <StyleCountNumber disabled={disabled} value={number} />
            <StyleIconAdd onClick={() => calculator('plus')} fontSize="small" />
          </StyleNumber>
        </StyleChatlimted>
      </StyledInfoUser>
    </StyleContainer>
  );
};

export default UserStatus;
