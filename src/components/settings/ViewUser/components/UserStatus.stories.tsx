import React from 'react';

import { UserStatus } from './UserStatus';

export default {
  title: 'Settings/Component/UserStatus',
  component: UserStatus,
};

export const Default = () => {
  return <UserStatus disabled />;
};
