import React from 'react';

import { ViewUsers } from './ViewUsers';

export default {
  title: 'Settings/ViewUsers',
  component: ViewUsers,
};

export const Default = () => {
  return <ViewUsers />;
};
