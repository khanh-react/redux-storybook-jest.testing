import React, { useState } from 'react';
import { EditOutlined, Delete } from '@material-ui/icons';
import Button from '../../common/Button';
import { CenterFlex, Flex } from '../../common/Flex';
import Text from '../../common/Text';
import Wrapper, { StyledTitle, StyledTextButton } from '../components/styles';
import ContactPermission from '../InviteUser/components/ContactPermission';
import FuturePermission from '../InviteUser/components/FuturePermission';
import Notification from '../InviteUser/components/Notification';
import UserStatus from './components/UserStatus';
import ConfirmDelete from '../ConfirmDelete/ConfirmDelete';

export const ViewUsers = () => {
  const [edit, setEdit] = useState(true);
  const [open, setOpen] = useState(false);
  const handleEdit = () => {
    setEdit(!edit);
  };

  const handeOpenPopup = () => {
    setOpen(!open);
  };

  /* eslint-disable consistent-return */
  const renderButton = () => {
    switch (edit) {
      case false:
        return (
          <Flex className="options" right="17.5% !important">
            <Button kind="danger" onClick={handeOpenPopup}>
              <CenterFlex>
                <Delete fontSize="small" />
                <StyledTextButton color="#fff">Delete</StyledTextButton>
              </CenterFlex>
            </Button>
            <Button kind="default">
              <StyledTextButton>Cancel</StyledTextButton>
            </Button>
            <Button kind="dark" onClick={handleEdit}>
              <StyledTextButton>Update</StyledTextButton>
            </Button>
          </Flex>
        );
      default:
        return (
          <Flex className="options" right="17.5% !important">
            <Button kind="default">
              <StyledTextButton color="#2e2e32">Cancel</StyledTextButton>
            </Button>
            <Button kind="default" onClick={handleEdit}>
              <CenterFlex>
                <EditOutlined fontSize="small" />
                <StyledTextButton color="#2e2e32">Edit Detail</StyledTextButton>
              </CenterFlex>
            </Button>
          </Flex>
        );
    }
  };

  return (
    <Wrapper>
      <div className="header">
        <StyledTitle>Edit User Detail</StyledTitle>
        <Flex className="breadcrumb">
          <Text color="#097af4" size="xsm" value="SETTINGS" />
          <Text className="slash" size="xsm" value="/" />
          <Text color="#097af4" size="xsm" value="USERS" />
          <Text className="slash" size="xsm" value="/" />
          <Text size="xsm" value="EDIT" />
        </Flex>
        {renderButton()}
      </div>
      <UserStatus disabled={edit} />
      <ContactPermission disabled={edit} />
      <FuturePermission disabled={edit} />
      <Notification disabled={edit} />
      <ConfirmDelete isShowModal={open} onClose={handeOpenPopup} />
    </Wrapper>
  );
};

export default ViewUsers;
