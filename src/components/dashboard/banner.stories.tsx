import React from 'react';

import { BannerDashBoard } from './banner';

export default {
  title: 'DashBoard/Component/Banner',
  component: BannerDashBoard,
};

export const Default = () => {
  return <BannerDashBoard />;
};
