import React from 'react';
import styled from 'styled-components';
import { Box } from '@material-ui/core';
import { Flex } from '../../common/Flex';
import Text from '../../common/Text';
import BootstrapButton from '../../common/Button';

const StyledImportText = styled(Flex)`
  background: #fff8da;
  box-shadow: 0px 2px 5px rgba(63, 63, 68, 0.15);
  border-radius: 10px;
  height: 180px;
  margin: 40px 40px;
  text-align: center;
`;

const StyledBox = styled(Box)`
  width: calc(100% - 210px);
`;

const StyleTextCover = styled(Text)`
  font-family: Source Sans Pro;
  font-style: normal;
  font-weight: bold;
  font-size: 32px;
  line-height: 66px;
  letter-spacing: 0.24px;
  color: #0f0f10;
  margin: 20px 10px;
`;
const TextContent = styled(Text)`
  display: block;
  font-family: Source Sans Pro;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  letter-spacing: 0.105px;
  color: #0f0f10;
`;
const StyledButtonImport = styled(BootstrapButton)`
  border-radius: 8px;
  margin-top: 20px;
  margin-bottom: 20px;
  background: #2e2e32;
`;

const StyledTextButton = styled(Text)`
  margin-left: 6px;
  font-family: Source Sans Pro;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.3px;
  color: #ffff;
`;

export const Banner = ({ textContent, textCover }: { textContent: string; textCover: string }) => {
  return (
    <StyledImportText>
      <img alt="cover" src="/static/coverDashBoard.svg" />
      <StyledBox>
        <StyleTextCover>{textCover}</StyleTextCover>
        <TextContent>{textContent}</TextContent>
        <StyledButtonImport kind="dark">
          <StyledTextButton>Invite your teammates</StyledTextButton>
        </StyledButtonImport>
      </StyledBox>
    </StyledImportText>
  );
};

export const BannerDashBoard = () => {
  const textContent = `Welcome to imBee! We’re excited to have you onboard. 
  Invite your teammates to join Imbee and their first 14 days are free.`;
  const textCover = `Welcome, Rachel 🙌🏻`;
  return <Banner textContent={textContent} textCover={textCover} />;
};

export default BannerDashBoard;
