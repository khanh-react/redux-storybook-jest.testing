import React, { useState } from 'react';
import { Theme, useTheme, makeStyles } from '@material-ui/core';
import {
  Container,
  StyleContent,
  StyleTitle,
  BtnDone,
  StyledInputIcon,
  GridEmailInvited,
  BtnInvite,
  StyledAvatar,
  TextInvite,
  colorEmail,
} from './styles';
import { Flex } from '../../common/Flex';
import Text from '../../common/Text';

const useStyles = makeStyles((theme) => ({
  subTitile: {
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: '14px',
    lineHeight: '18px',
    textAlign: 'center',
    color: theme.palette.grey[500],
    padding: '10px 100px',
  },
  to: {
    textAlign: 'left',
    marginTop: '37px',
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: '14px',
    lineHeight: '18px',
    letterSpacing: '0.105px',
    color: theme.palette.grey[500],
  },
  button: {
    marginRight: 'unset !important',
    position: 'absolute',
    lineHeight: 'unset',
    right: 0,
    top: 0,
    height: '56px',
    color: theme.palette.background.default,
    background: theme.palette.grey[800],
    borderBottomLeftRadius: 0,
    borderTopLeftRadius: 0,
  },
  nameEmail: {
    marginLeft: '12px',
    color: '#6d7278',
    fontWeight: 600,
    fontSize: '14px',
    lineHeight: 2,
  },
  textField: {
    marginTop: '8px',
    position: 'relative',
    width: '285px !important',
  },
  btnDone: {
    lineHeight: 'unset',
    width: '360px',
    color: theme.palette.background.default,
    background: theme.palette.grey[800],
  },
  mt15: {
    margin: '15px 25px',
  },
}));

export const Modal = () => {
  const theme = useTheme<Theme>();
  const classes = useStyles();
  let inputEmail: { name: string; color: string } = { name: '', color: '' };

  const [listEmail, setListEmail] = useState([{ name: '', color: '' }]);
  const [validator, setValidator] = useState({ helperText: '', error: false });
  const setEmailInvited = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const nameEmail: string = event.target.value;
    inputEmail = {
      name: nameEmail,
      color: colorEmail[Math.floor(Math.random() * 3)],
    };
  };

  const addEmailContact = (): void => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (inputEmail && re.test(inputEmail.name.toLocaleLowerCase())) {
      const newListEmail = listEmail.splice(0, listEmail.length);
      newListEmail.push(inputEmail);
      setListEmail(newListEmail);
      setValidator({ helperText: '', error: false });
    } else {
      setValidator({ helperText: 'Invalid format', error: true });
    }
  };

  const InvitedButton = () => (
    <BtnInvite kind="default" className={classes.button} onClick={addEmailContact}>
      Invites
    </BtnInvite>
  );
  return (
    <Container flex={1} bgcolor={theme.palette.background.default} flexDirection="row" justifyContent="space-between">
      <Flex>
        <img alt="Banner" src="/static/coverDashBoardBig.svg" />
      </Flex>
      <StyleContent flexDirection="column">
        <StyleTitle>Invite your teammates</StyleTitle>
        <span className={classes.subTitile}>Invite your teammates to join Imbee and their first 14 days are free.</span>
        <span className={classes.to}>To</span>
        <StyledInputIcon>
          <TextInvite
            error={validator.error}
            helperText={validator.helperText}
            variant="outlined"
            onChange={setEmailInvited}
            InputProps={{ endAdornment: <InvitedButton /> }}
          />
        </StyledInputIcon>
        <span className={classes.to}>Invited</span>
        <GridEmailInvited>
          {listEmail
            .filter(function remove(item) {
              return item.name !== '';
            })
            .map((value) => {
              return (
                <Flex className={classes.mt15} key={Math.floor(Math.random() * 20)}>
                  <StyledAvatar color={value.color}>{value.name.charAt(0)}</StyledAvatar>
                  <Text className={classes.nameEmail}>{value.name}</Text>
                </Flex>
              );
            })}
        </GridEmailInvited>
        <BtnDone kind="dark" className={classes.btnDone}>
          Done
        </BtnDone>
      </StyleContent>
    </Container>
  );
};

export default Modal;
