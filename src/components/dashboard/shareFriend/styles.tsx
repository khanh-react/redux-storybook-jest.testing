import { Avatar, TextField } from '@material-ui/core';
import styled from 'styled-components';
import { Flex } from '../../common/Flex';
import Text from '../../common/Text';
import Button from '../../common/Button';

export const Container = styled(Flex)`
  flex-wrap: wrap;
  height: 623px;
  width: 923px;
`;
export const StyleContent = styled(Flex)`
  text-align: center;
  height: auto;
  width: 470px;
`;
export const StyleTitle = styled(Text)`
  margin-top: 72px;
  font-style: normal;
  font-weight: bold;
  font-size: 38px;
  line-height: 48px;
  text-align: center;
`;

export const BtnDone = styled(Button)`
  margin-top: 70px;
  .MuiButton-label {
    height: 40px;
  }
`;

export const BtnInvite = styled(Button)`
  .MuiButton-label {
    height: 40px;
  }
`;

export const StyledInputIcon = styled(Flex)`
  width: 360px;
`;

export const GridEmailInvited = styled(Flex)`
  flex-wrap: wrap;
`;

export const StyledAvatar = styled(Avatar)`
  ${({ theme, color }) => `
   background: ${color};
   color: ${theme.palette.background.default};
   width: 25px;
   height: 25px;
   font-style: normal;
   font-weight: 600;
   font-size: 17px;
   line-height: 21px;
   text-transform: uppercase;
 `}
`;

export const TextInvite = styled(TextField)`
  ${({ theme }) => `
    .MuiOutlinedInput-notchedOutline {
      border-color: ${theme.palette.grey[800]} !important;
    }
  `};
  margin-top: 8px;
  width: 360px;
  .MuiOutlinedInput-root {
    border-radius: 12px;
  }
`;

export const colorEmail: ColorEmail = {
  0: '#fa6400',
  1: '#ffdb36',
  2: '#2b71f3',
};

type ColorEmail = {
  [key: number]: string;
};
