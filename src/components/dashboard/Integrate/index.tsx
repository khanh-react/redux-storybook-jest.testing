import React from 'react';
import styled from 'styled-components';
import { Box, Theme, useTheme, Button } from '@material-ui/core';
import { Flex } from '../../common/Flex';
import Icon from '../../common/Icon';
import BootstrapButton from '../../common/Button';
import Text from '../../common/Text';
import { DumdataType } from './types';

const StyledImportText = styled(Text)`
  ${({ theme }) => `
  margin: 32px 38px 5px 38px;
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: bold;
  font-size: 20px;
  line-height: 28px;
  text-align: center;
  letter-spacing: -0.05px;
  color: ${theme.palette.secondary.dark};
`};
`;

const StyledText = styled(Text)`
  margin-bottom: 28px;
  font-family: Source Sans Pro;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  letter-spacing: 0.105px;
  color: #0f0f10;
`;

const StyledTextHover = styled(Text)`
  font-family: Source Sans Pro;
  font-style: italic;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;
  color: #6c6c72;
`;

const StyledTextButton = styled(Text)`
  margin-left: 6px;
  font-family: Source Sans Pro;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.3px;
  color: #ffff;
`;

const StyleImport = styled(Flex)`
  background: #ffff;
  box-shadow: 0px 2px 5px rgba(63, 63, 68, 0.15);
  border-radius: 10px;
  margin-left: 20px;
`;

const StyledButtonTrial = styled(BootstrapButton)`
  border: 1px solid #ff7878;
  box-sizing: border-box;
  border-radius: 19px;
  margin-left: 20px;
  &.MuiButton-containedPrimary {
    background-color: #fff;
  }
  .MuiButton-label {
    font-family: Source Sans Pro;
    font-style: normal;
    font-weight: 600;
    font-size: 12px;
    line-height: 15px;
    text-align: center;
    color: #ff7878;
  }
`;

const StyledButtonImport = styled(BootstrapButton)`
  border-radius: 8px;
  margin-top: 40px;
  margin-bottom: 20px;
  background: #2e2e32;
`;

const StyledTextTrial = styled(Text)`
  margin-left: 7px;
  font-family: Source Sans Pro;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 18px;
  display: flex;
  align-items: center;
  letter-spacing: 0.105px;
  color: #ff7878;
`;

const StyledNumber = styled(Flex)`
  ${({ theme, borderRight }) => `
font-family: ${theme.typography.fontFamily};
flex: 1; 
flex-direction:column;
align-items:center;
border-right:${borderRight === 'unset' ? '' : '1px solid  #d5d5d5'};
padding: 45px;
position: relative;
margin-top: 100px;
margin-bottom: -55px;
& .content {
  padding: 10px 0;
  font-family: Source Sans Pro;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  letter-spacing: 0.105px;
  color: #0f0f10;
},
& .title {
  font-family: Source Sans Pro;
  font-style: normal;
  font-weight: bold;
  font-size: 20px;
  line-height: 28px;
  text-align: center;
  letter-spacing: -0.05px;
  color: #2e2e32;
}
`}
`;
const StyledBox = styled(Flex)`
  ${({ theme }) => `
  align-items: center;
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  color: #0f0f10;
  position: absolute;
  margin-top: 67px;

`}
`;
const Container = styled(Flex)`
  background: #ffff;
  box-shadow: 0px 2px 5px rgba(63, 63, 68, 0.15);
  border-radius: 10px;
`;
const StyledButtonApp = styled(Button)`
  ${({ color }) => `
margin-top: 37px;
margin-bottom: 80px;
background: ${color === 'primary' ? '#0098fa' : '#00b241'};
border-radius: 10px;
width: 130px;
height: 38px;
}
& .MuiButton-label {
  font-family: Source Sans Pro;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  letter-spacing: 0.105px;
  color: #ffff;
  text-transform: lowercase;
}
`}
`;

const Column = styled(Flex)`
  flex-direction: column;
  align-items: center;
  width: 426px;
`;

const Integrate = () => {
  const dumData: ReadonlyArray<DumdataType> = [
    {
      title: 'WhatsApp',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ornare pulvinar sem rutrum elementum.',
      icon: 'whatsapp',
      nameButton: 'integrate',
    },
    {
      title: 'Facebook Messenger',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ornare pulvinar sem rutrum elementum.',
      icon: 'fb',
      nameButton: 'integrate',
    },
  ];
  return (
    <Container
      boxShadow="0px 0px 0px 1px rgba(63, 63, 68, 0.05), 0px 1px 2px rgba(63, 63, 68, 0.15)"
      bgcolor="white"
      flexDirection="row"
      mt="30px"
      justifyContent="space-between"
      width="852px"
    >
      <Flex mt="25px" mb="25px">
        {dumData.map((item, index) => {
          return (
            <Column key={item.icon}>
              <StyledBox>
                <Icon name={item.icon} size={50} />
              </StyledBox>
              <StyledNumber borderRight={index === 1 ? 'unset' : ''}>
                <span className="title">{item.title}</span>
                <span className="content">{item.content}</span>
              </StyledNumber>
              <StyledButtonApp color={index === 1 ? 'primary' : 'default'}>{item.nameButton}</StyledButtonApp>
            </Column>
          );
        })}
      </Flex>
      <StyleImport>
        <Column>
          <StyledImportText>Import your project</StyledImportText>
          <StyledText>Bring all your existing projects to imbee.</StyledText>
          <img alt="shape" src="/static/importdashboard.svg" />
          <StyledButtonImport kind="dark">
            <img alt="shape" src="/static/outlinePlus.svg" />
            <StyledTextButton>Select Files to upload</StyledTextButton>
          </StyledButtonImport>
          <StyledTextHover>(.csz, .xlsx formats supported)</StyledTextHover>
        </Column>
      </StyleImport>
    </Container>
  );
};

export const HomeDashBoard = () => {
  const theme = useTheme<Theme>();
  return (
    <Flex justifyContent="center" flex={1} p="35px 35px 20px 40px" flexDirection="column">
      <Flex justifyContent="space-between" alignItems="center">
        <Box fontFamily={theme.typography.fontFamily} fontWeight="600" fontSize={24} lineHeight="28px" color="#263238">
          Integrate with your Favorite Apps
        </Box>
        <Flex>
          <img alt="shape" src="/static/Shape.svg" />
          <StyledTextTrial>
            You currently have &nbsp;<b> 7 days</b>&nbsp; left in your free trial.
          </StyledTextTrial>
          <StyledButtonTrial kind="default">Upgrade</StyledButtonTrial>
        </Flex>
      </Flex>
      <Integrate />
    </Flex>
  );
};
export default HomeDashBoard;
