import { IconName } from '../../common/Icon';

export interface DumdataType {
  title: string;
  content: string;
  nameButton: string;
  icon: IconName;
}
