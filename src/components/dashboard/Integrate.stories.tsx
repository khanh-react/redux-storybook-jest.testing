import React from 'react';

import { HomeDashBoard } from './Integrate';

export default {
  title: 'DashBoard/Integrate',
  component: HomeDashBoard,
};

export const Default = () => {
  return <HomeDashBoard />;
};
