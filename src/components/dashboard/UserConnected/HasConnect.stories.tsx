import React from 'react';

import { UserConnect } from './HasConnect';

export default {
  title: 'DashBoard/UserConnected/HasConnect',
  component: UserConnect,
};

export const Default = () => {
  return <UserConnect />;
};
