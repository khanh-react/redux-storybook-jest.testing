import React from 'react';

import { RenderTable } from './TableTicket';

export default {
  title: 'DashBoard/Component/TableTicket',
  component: RenderTable,
};

export const Default = () => {
  return <RenderTable />;
};
