import React from 'react';

import { GridCountChat } from './GridCountChat';

export default {
  title: 'DashBoard/Component/CountChat',
  component: GridCountChat,
};

export const Default = () => {
  return <GridCountChat title="Total Chats" value={100} percent="+45%" />;
};
