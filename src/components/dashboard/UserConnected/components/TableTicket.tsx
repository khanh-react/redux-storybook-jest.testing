import React from 'react';
import styled from 'styled-components';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  useTheme,
  Theme,
  Box,
} from '@material-ui/core';
import { Alarm, ArrowForward } from '@material-ui/icons';
import { Flex } from '../../../common/Flex';
import Icon, { IconName } from '../../../common/Icon';
import { dumData } from '../HasConnect/types';
import Text from '../../../common/Text';

const StyleTicket = styled(Text)`
  ${({ theme }) => `
  margin: 14px 0px 14px 20px;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  letter-spacing: -0.05px;
  color: ${theme.palette.grey[800]};
`}
`;

const StyleTextSeeMore = styled(Text)`
  cursor: pointer;
  flex-direction: row-reverse !important;
  margin: 10px 26px 10px 0px;
  font-size: 14px;
  text-align: right;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.71;
  letter-spacing: 0.3px;
  color: #2e2e32;
  span {
    margin-right: 5px;
  }
`;

const StyleTableBody = styled(TableBody)`
  .table-row: hover {
    background-color: #fff8da;
  }
`;

const StyleTableContainer = styled(TableContainer)`
  border-top: 1px solid rgba(224, 224, 224, 1);
`;

const StyleIcon = styled(Icon)`
  margin-right: 16px;
`;

const StyledCenter = styled(TableCell)`
  text-align: center;
`;

const StyleImg = styled(Alarm)`
  margin-right: 5px;
  width: 15px;
  height: 22px;
`;

const StyledTag = styled(Flex)`
  ${({ theme }) => `
  width: 88px;
  height: 23px;
  color: ${theme.palette.background.default};
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 22px;
  letter-spacing: 0.1px;
  border-radius: 11.5px;
`}
`;

const Container = styled(Flex)`
  ${({ theme }) => `
  box-shadow: 0px 0px 0px 1px rgba(63, 63, 68, 0.05), 0px 1px 2px rgba(63, 63, 68, 0.15);
  border-radius: 10px;
  background: ${theme.palette.background.default};
  margin: 20px 16px 20px 30px;
`}
`;

const StyledStatus = styled(Box)`
  ${({ bgcolor }) => `
  background: ${bgcolor === 'Open' ? 'rgba(0, 198, 146, 0.119154)' : '#dcdce4;'};
  border-radius: 11.5px;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  width: 56px;
  line-height: 22px;
  height: 22px;
  letter-spacing: 0.83px;
  text-transform: uppercase;
  color: ${bgcolor === 'Open' ? '#388e3c' : '#2e2e32'};
  text-align: center;
`}
`;

export type ITableTicket = {
  id: number;
  channel: IconName;
  chatName: string;
  ticketID: string;
  dateAssigned: string;
  assginBy: string;
  status: string;
};

export const TableTicket = ({ listData }: { listData?: ITableTicket[] }) => {
  const theme = useTheme<Theme>();
  return (
    <Container flex={1} flexDirection="column">
      <StyleTicket>Tickets assigned to me</StyleTicket>
      <StyleTableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Channel</TableCell>
              <TableCell>Chat Name</TableCell>
              <TableCell>Ticket ID</TableCell>
              <TableCell>Date Assigned</TableCell>
              <TableCell>Assigned by</TableCell>
              <TableCell>Status</TableCell>
            </TableRow>
          </TableHead>
          <StyleTableBody>
            {listData && listData.length > 0 ? (
              listData?.map((item) => {
                const compareDate = item.dateAssigned.split(' ');
                return (
                  <TableRow key={item.id} className="table-row">
                    <TableCell>
                      <Flex>
                        <StyleIcon name={item.channel} size={18} />
                        {item.channel === 'fb' ? 'Messenger' : item.channel}
                      </Flex>
                    </TableCell>
                    <TableCell>{item.chatName}</TableCell>
                    <TableCell>{item.ticketID}</TableCell>
                    <TableCell>
                      {compareDate.length > 1 ? (
                        item.dateAssigned
                      ) : (
                        <StyledTag bgcolor={theme.palette.error.main} justifyContent="center">
                          <StyleImg />
                          {item.dateAssigned}
                        </StyledTag>
                      )}
                    </TableCell>
                    <TableCell>{item.assginBy}</TableCell>
                    <TableCell>
                      <StyledStatus bgcolor={item.status}>
                        <span>{item.status}</span>
                      </StyledStatus>
                    </TableCell>
                  </TableRow>
                );
              })
            ) : (
              <TableRow>
                <StyledCenter colSpan={6}>No data found</StyledCenter>
              </TableRow>
            )}
          </StyleTableBody>
        </Table>
      </StyleTableContainer>
      {listData && listData.length > 0 ? (
        <Flex flexDirection="row" justifyContent="flex-end">
          <StyleTextSeeMore value="See More" icon={<ArrowForward fontSize="small" />} />
        </Flex>
      ) : (
        ''
      )}
    </Container>
  );
};

export const RenderTable = () => {
  return <TableTicket listData={dumData} />;
};

export default RenderTable;
