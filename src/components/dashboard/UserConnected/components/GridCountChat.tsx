import React from 'react';
import styled from 'styled-components';

import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';

const StyleTitle = styled(Text)`
  margin: 23px 0px 10px 0px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 31px;
  text-align: center;
  text-transform: uppercase;
  letter-spacing: 0.83px;
`;

const StyleValue = styled(Text)`
  ${({ theme }) => `
  font-style: normal;
  font-weight: 600;
  font-size: 36px;
  line-height: 24px;
  letter-spacing: -0.1px;
  color: ${theme.palette.grey[800]};
  margin-right: 8px;
`}
`;

const StylePercentLess = styled(Text)`
  ${({ theme, color }) => `
  background: ${color === '-' ? '#fdeef0' : 'rgba(0, 198, 146, 0.119154)'};
  border-radius: 5px;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  width: 42px;
  line-height: 18px;
  height: 19px;
  letter-spacing: 0.83px;
  text-transform: uppercase;
  color: ${color === '-' ? theme.palette.error.main : '#00c692'};
`}
`;

const Container = styled(Flex)`
  ${({ theme }) => `
  box-shadow: 0px 2px 5px rgba(63, 63, 68, 0.15);
  border-radius: 10px;
  background: ${theme.palette.background.default};
  width: 320px;
  height: 120px;
  text-align: center;
  margin: 30px 16px 0px 30px;
`}
`;

export const GridCountChat = ({ title, value, percent }: { title: string; value?: number; percent?: string }) => {
  return (
    <Container flex={1} flexDirection="column">
      <StyleTitle>{title}</StyleTitle>
      {value ? (
        <Flex flexDirection="row" justifyContent="center">
          <StyleValue>{value}</StyleValue>
          <StylePercentLess color={percent?.charAt(0)}>{percent}</StylePercentLess>
        </Flex>
      ) : (
        <Flex flexDirection="row" justifyContent="center">
          <StyleValue>- -</StyleValue>
        </Flex>
      )}
    </Container>
  );
};

export default GridCountChat;
