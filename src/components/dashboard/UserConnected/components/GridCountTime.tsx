import React from 'react';
import styled from 'styled-components';

import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';

const StyledTitle = styled(Text)`
  margin: 10px 0px 10px 0px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 31px;
  text-align: center;
  text-transform: uppercase;
  letter-spacing: 0.83px;
`;

const StyledValueEmpty = styled(Text)`
  ${({ theme }) => `
  font-style: normal;
  font-weight: 600;
  font-size: 36px;
  line-height: 24px;
  letter-spacing: -0.1px;
  color: ${theme.palette.grey[800]};
  margin-right: 15px;
`}
`;

const StyledTimeAvag = styled(Text)`
  ${({ theme }) => `
  font-style: normal;
  font-weight: 600;
  font-size: 36px;
  line-height: 24px;
  letter-spacing: -0.1px;
  color: ${theme.palette.grey[800]};
`}
`;

const StyledAvagrateEmpty = styled(Text)`
  ${({ theme }) => `
  font-style: normal;
  font-weight: 600;
  font-size: 36px;
  line-height: 24px;
  letter-spacing: -0.1px;
  color: ${theme.palette.grey[800]};
  margin-left: 15px;
`}
`;

const StyledText = styled(Text)`
  ${({ theme }) => `
  font-style: normal;
  font-size: 12px;
  line-height: 31px;
  letter-spacing: 0.83px;
  color: ${theme.palette.error.main};
`}
`;

const StyledTextAvg = styled(Text)`
  ${({ theme }) => `
  font-style: normal;
  font-size: 12px;
  line-height: 31px;
  letter-spacing: 0.83px;
  color: ${theme.palette.success.main};
`}
`;

const Container = styled(Flex)`
  ${({ theme }) => `
  box-shadow: 0px 2px 5px rgba(63, 63, 68, 0.15);
  border-radius: 10px;
  background: ${theme.palette.background.default};
  width: 420px;
  height: 120px;
  text-align: center;
  margin: 20px 16px 20px 30px;
`};
`;

export interface IGridCountTime {
  title: string;
  longTime?: string;
  averageTime?: string;
}

export const GridCountTime = ({ title, longTime, averageTime }: IGridCountTime) => {
  return (
    <Container flex={1} flexDirection="column">
      <StyledTitle>{title}</StyledTitle>
      {longTime ? (
        <Flex flexDirection="row" justifyContent="center">
          <Flex flexDirection="column" width="420px">
            <StyledTimeAvag>{longTime}</StyledTimeAvag>
            <StyledText>Longest</StyledText>
          </Flex>
          <Flex flexDirection="column" width="420px">
            <StyledTimeAvag>{averageTime}</StyledTimeAvag>
            <StyledTextAvg>Average</StyledTextAvg>
          </Flex>
        </Flex>
      ) : (
        <Flex flexDirection="row" justifyContent="center">
          <StyledValueEmpty>- -</StyledValueEmpty>
          <StyledAvagrateEmpty>- -</StyledAvagrateEmpty>
        </Flex>
      )}
    </Container>
  );
};

export default GridCountTime;
