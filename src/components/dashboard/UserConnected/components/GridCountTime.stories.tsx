import React from 'react';

import { GridCountTime } from './GridCountTime';

export default {
  title: 'DashBoard/Component/CountTime',
  component: GridCountTime,
};

export const Default = () => {
  return <GridCountTime title="Wait Time" longTime="2m 12s" averageTime="35s" />;
};
