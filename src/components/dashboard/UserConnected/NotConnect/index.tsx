import React from 'react';

import { Flex } from '../../../common/Flex';
import BannerDashBoard from '../../banner';
import GridCountChat from '../components/GridCountChat';
import GridCountTime from '../components/GridCountTime';
import { TableTicket } from '../components/TableTicket';
import { listGridChat, listGridCountTime } from './types';

export const NotUserConnect = () => {
  return (
    <Flex justifyContent="center" flex={1} flexDirection="column">
      <BannerDashBoard />
      <Flex flexDirection="row">
        {listGridChat?.map((item) => {
          return <GridCountChat key={item.title} title={item.title} />;
        })}
      </Flex>
      <Flex flexDirection="row">
        {listGridCountTime?.map((item) => {
          return <GridCountTime title={item.title} key={item.title} />;
        })}
      </Flex>
      <TableTicket />
    </Flex>
  );
};

export default NotUserConnect;
