import { ITableTicket } from '../components/TableTicket';

export const dumData: ITableTicket[] = [
  {
    id: Math.random() * 160,
    channel: 'whatsapp',
    chatName: 'Johnny Yeung',
    ticketID: '#8430',
    dateAssigned: '23:18:05',
    assginBy: 'System',
    status: 'Open',
  },
  {
    id: Math.random() * 160,
    channel: 'wechat',
    chatName: 'Kate Tsui',
    ticketID: '#8428',
    dateAssigned: '35:08:32',
    assginBy: 'System',
    status: 'Open',
  },
  {
    id: Math.random() * 160,
    channel: 'fb',
    chatName: 'William Leung',
    ticketID: '#8424',
    dateAssigned: '13/02/2020 12:23',
    assginBy: 'William Wong',
    status: 'Open',
  },
  {
    id: Math.random() * 160,
    channel: 'line',
    chatName: 'CT Group Chat',
    ticketID: '#8417',
    dateAssigned: '13/02/2020  15:09',
    assginBy: 'James Corden',
    status: 'Open',
  },
  {
    id: Math.random() * 160,
    channel: 'whatsapp',
    chatName: 'David Wong',
    ticketID: '#8416',
    dateAssigned: '10/02/2020  11:15',
    assginBy: 'System',
    status: 'Open',
  },
  {
    id: Math.random() * 160,
    channel: 'wechat',
    chatName: 'Julian Gruber',
    ticketID: '#8413',
    dateAssigned: '09/02/2020 16:02',
    assginBy: 'James Corden',
    status: 'Closed',
  },
];

export const listGridChat = [
  {
    title: 'Total Chats',
    value: 130,
    percent: '+45%',
  },
  {
    title: 'Severed Chats',
    value: 25,
    percent: '+9.3%',
  },
  {
    title: 'Missed Chats',
    value: 12,
    percent: '-2.5%',
  },
  {
    title: 'Assigned Chats',
    value: 18,
    percent: '+33.5%',
  },
];

export const listGridCountTime = [
  {
    title: 'Wait Time',
    longTime: '2m 12s',
    averageTime: '35s',
  },
  {
    title: 'Response Time',
    longTime: '1m 12s',
    averageTime: '20s',
  },
  {
    title: 'Resolution Time',
    longTime: '51m 20s',
    averageTime: '20m 12s',
  },
];
