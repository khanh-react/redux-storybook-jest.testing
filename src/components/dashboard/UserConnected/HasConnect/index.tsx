import React from 'react';

import { Flex } from '../../../common/Flex';
import BannerDashBoard from '../../banner';
import GridCountChat from '../components/GridCountChat';
import GridCountTime from '../components/GridCountTime';
import TableTicket from '../components/TableTicket';
import { listGridChat, listGridCountTime } from './types';

export const UserConnect = () => {
  return (
    <Flex justifyContent="center" flex={1} flexDirection="column">
      <BannerDashBoard />
      <Flex flexDirection="row">
        {listGridChat?.map((item) => {
          return <GridCountChat key={item.title} title={item.title} value={item.value} percent={item.percent} />;
        })}
      </Flex>
      <Flex flexDirection="row">
        {listGridCountTime?.map((item) => {
          return (
            <GridCountTime
              title={item.title}
              key={item.title}
              averageTime={item.averageTime}
              longTime={item.longTime}
            />
          );
        })}
      </Flex>
      <TableTicket />
    </Flex>
  );
};

export default UserConnect;
