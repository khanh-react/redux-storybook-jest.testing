import React from 'react';

import { NotUserConnect } from './NotConnect';

export default {
  title: 'DashBoard/UserConnected/NotConnect',
  component: NotUserConnect,
};

export const Default = () => {
  return <NotUserConnect />;
};
