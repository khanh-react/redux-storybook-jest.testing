import React from 'react';
import CheckIcon from '@material-ui/icons/Check';
import styled from 'styled-components';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';

const StyledItemTag = styled.div`
  padding: 2px 10px;
  &:hover {
    background: #fff8da;
  }
  cursor: pointer;
`;

const TagSelect = ({
  color,
  label,
  id,
  deleteTag,
  hidden,
  handleChecked,
  onclosedDropdown,
}: {
  color: string;
  label: string;
  id: string;
  deleteTag: (idTag: string) => void;
  hidden: boolean;
  handleChecked: (idTag: string, status: boolean) => void;
  onclosedDropdown: () => void;
}) => {
  const [flagDelete, setFlagDelete] = React.useState(false);
  return (
    <Flex>
      <StyledItemTag onMouseEnter={() => setFlagDelete(true)} onMouseLeave={() => setFlagDelete(false)}>
        <Flex justifyContent="space-between" width="230px">
          <Flex
            alignItems="center"
            justifyContent="center"
            onClick={() => {
              handleChecked(id, hidden);
              onclosedDropdown();
            }}
          >
            <Icon mr="10px" name="tags" color={color} />
            <Flex color={color} fontWeight={600} fontSize={14} lineHeight="18px">
              {label}
            </Flex>
          </Flex>
          <Flex>
            <Flex alignItems="center" justifyContent="center">
              {hidden && <CheckIcon fontSize="small" />}
            </Flex>
            {flagDelete && (
              <PointerFlex onClick={() => deleteTag(id)}>
                <Icon name="delete" />
              </PointerFlex>
            )}
          </Flex>
        </Flex>
      </StyledItemTag>
    </Flex>
  );
};

export default TagSelect;
