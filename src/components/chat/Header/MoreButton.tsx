import React from 'react';
import CachedIcon from '@material-ui/icons/Cached';
import { useSelector } from 'react-redux';
import { AppState } from '../../../redux/store';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';

const MoreButton = ({ handleActionMore }: { handleActionMore: (action: string) => void }) => {
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const currentChatTicket = useSelector(
    (s: AppState) => currentChatId && s.chat.chatTicket && s.chat.chatTicket[Number(currentChatId)],
  );
  const currentChatType = useSelector(
    (s: AppState) => currentChatId && s.chat.chats[currentChatId] && s.chat.chats[currentChatId].type,
  );
  const currentChatRead = useSelector(
    (s: AppState) => currentChatId && s.chat.chats[currentChatId] && s.chat.chats[currentChatId].read,
  );
  const { anchorEl, handleClick, handleClose } = useIbMenu();

  const handleAction = (action: string) => () => {
    handleActionMore(action);
    handleClose();
  };

  return (
    <>
      <PointerFlex onClick={handleClick}>
        <Icon name="ellipsis" />
      </PointerFlex>
      <IbMenu id="filter-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
        {currentChatRead === 1 ? (
          <IbMenuItem onClick={handleAction('mark-as-unread')}>
            <Icon ml="-10px" mr="5px" name="read" />
            Mark as unread
          </IbMenuItem>
        ) : (
          <IbMenuItem onClick={handleAction('mark-as-read')}>
            <Icon ml="-10px" mr="5px" name="read" />
            Mark as read
          </IbMenuItem>
        )}
        {/* <IbMenuItem onClick={handleAction('export')}>
            <Icon ml="-10px" mr="5px" name="printer" />
            Export
          </IbMenuItem>
          <IbMenuItem onClick={handleAction('mark-as-spam')}>
            <Icon ml="-10px" mr="5px" name="info" />
            Mark as spam
          </IbMenuItem> */}
        {(!currentChatTicket || currentChatTicket.status === 'Closed') && (
          <IbMenuItem onClick={handleAction('open-new-ticket')}>
            <Icon ml="-10px" mr="5px" name="ticket" />
            Open New ticket
          </IbMenuItem>
        )}
        {currentChatTicket && currentChatTicket.status === 'Open' && (
          <IbMenuItem onClick={handleAction('close-current-ticket')}>
            <Icon ml="-10px" mr="5px" name="ticket" />
            Close current ticket
          </IbMenuItem>
        )}
        {/* <IbMenuItem onClick={handleAction('clear-messages')}>
            <Icon ml="-10px" mr="5px" name="clear" />
            Clear Messages
          </IbMenuItem> */}

        <IbMenuItem onClick={handleAction('delete-chat')}>
          <Icon ml="-10px" mr="5px" name="delete" />
          Delete Chat
        </IbMenuItem>

        {currentChatType === 'Group' && (
          <>
            <IbMenuItem onClick={handleAction('export-member')}>
              <Flex width="32px" height="32px" ml="-10px" mr="5px" justifyContent="center">
                <Icon name="export" />
              </Flex>
              Export Member List
            </IbMenuItem>
            <IbMenuItem onClick={handleAction('refresh-member')}>
              <Flex width="32px" height="32px" ml="-10px" mr="5px" justifyContent="center" alignItems="center">
                <CachedIcon />
              </Flex>
              Refresh Member List
            </IbMenuItem>
          </>
        )}
      </IbMenu>
    </>
  );
};

export default MoreButton;
