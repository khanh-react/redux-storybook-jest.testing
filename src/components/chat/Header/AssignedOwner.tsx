import { Avatar, Box } from '@material-ui/core';
import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { assignUser, unAssignUser } from '../../../redux/action/chat';
import { BasicUser, ChatTicket } from '../../../redux/services/chat';
import { SubdomainUser } from '../../../redux/services/login';
import { AppState } from '../../../redux/store';
import firstChars from '../../../utils/firstChars';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';

const StaffHeaderAvatar = styled(Avatar)`
  width: 20px;
  height: 20px;
`;

type AssignedOwnerData = {
  id: string;
  name: string;
  status: string;
  assignedUser: BasicUser;
  chatTicket?: ChatTicket;
};

type AssignedOwnerProps = {
  users: SubdomainUser[];
};

const AssignedOwner = ({ users }: AssignedOwnerProps) => {
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const chats = useSelector((s: AppState) => s.chat.chats);
  const chatIds = useSelector((s: AppState) => s.chat.chatList);
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const assignedOwnerData = useMemo(() => {
    return (chatIds ?? []).map(
      (chatId): AssignedOwnerData => {
        return {
          id: String(chatId),
          name: chats[chatId].name || '',
          status: chats[chatId].status || '',
          assignedUser: chats[chatId].assignedUser,
          chatTicket: chats[chatId].chatTicket,
        };
      },
    );
  }, [chatIds, chats]);
  const data = useMemo(
    () =>
      (assignedOwnerData ?? []).filter((mess) => {
        return mess.id === currentChatId;
      }),
    [currentChatId, assignedOwnerData],
  );

  const assignedUser: { id: string; avatar: string; name: string } = useMemo(() => {
    if (data && data[0]?.assignedUser) {
      return {
        id: data[0].assignedUser.id.toString(),
        avatar: firstChars(data[0].assignedUser.name),
        name: data[0].assignedUser.name,
      };
    }
    return {
      id: '',
      avatar: '',
      name: 'Unassigned',
    };
  }, [data]);
  const dispatch = useDispatch();
  return (
    <>
      <PointerFlex
        height={32}
        p="5px"
        borderRadius={15}
        border="1px solid #DCDCE4"
        mr="10px"
        width="160px"
        onClick={handleClick}
      >
        <StaffHeaderAvatar src={assignedUser.avatar} />
        <Flex fontWeight={600} fontSize={14} mx="5px" minWidth={0}>
          <Box textOverflow="ellipsis" overflow="hidden" whiteSpace="nowrap">
            {assignedUser.name}
          </Box>
        </Flex>
        <Flex flex={1} justifyContent="flex-end">
          <Icon name="triangle-down" />
        </Flex>
      </PointerFlex>
      <IbMenu
        id="assign-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        PaperProps={{
          style: {
            maxHeight: `240px`,
          },
        }}
      >
        <IbMenuItem
          onClick={() => {
            data[0].chatTicket && dispatch(unAssignUser(data[0].chatTicket.id));
            handleClose();
          }}
        >
          <Flex fontStyle="normal" fontWeight={600} fontSize={14} lineHeight="24px">
            Unassigned
          </Flex>
        </IbMenuItem>
        {users.map((user) => (
          <IbMenuItem
            key={user.id}
            onClick={() => {
              data[0].chatTicket && dispatch(assignUser(user.user_id, data[0].chatTicket.id));
              handleClose();
            }}
          >
            <Flex ml="-5px" mr="10px">
              <StaffHeaderAvatar src={firstChars(user.name)} />
            </Flex>
            <Flex fontStyle="normal" fontWeight={600} fontSize={14} lineHeight="24px">
              {user.name}
            </Flex>
          </IbMenuItem>
        ))}
      </IbMenu>
    </>
  );
};
export default AssignedOwner;
