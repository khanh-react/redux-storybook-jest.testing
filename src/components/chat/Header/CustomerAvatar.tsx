import { Avatar, BoxProps } from '@material-ui/core';
import React from 'react';
import { Flex } from '../../common/Flex';
import Icon, { IconName } from '../../common/Icon';

export interface CustomerAvatarProps {
  name: string;
  image: string;
  source: IconName | null;
}

const CustomerAvatar = ({ name, image, source, ...props }: CustomerAvatarProps & BoxProps) => (
  <Flex alignItems="center" justifyContent="center" {...props}>
    <Flex position="relative">
      <Avatar alt={name} src={image} />
      <Flex
        bottom={-6}
        right={0}
        position="absolute"
        display="flex"
        borderRadius={10}
        boxShadow="0px 0px 4px rgba(0, 0, 0, 0.233419)"
      >
        {source && <Icon name={source} />}
      </Flex>
    </Flex>
  </Flex>
);

export default CustomerAvatar;
