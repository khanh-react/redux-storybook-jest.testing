import React from 'react';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import ListSubheader from '@material-ui/core/ListSubheader';
import styled from 'styled-components';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';
import { autoColors } from '../../common/AutoSelectColors';
import TagSelect from './TagSelect';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';

const StyledSubHeader = styled(ListSubheader)`
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 18px;
  color: #bbbbc5;
  margin-left: 5px;
  margin-bottom: 5px;
`;

type PropsTagButton = {
  handleAction: (action: string) => void;
  tagSelectData: { id: string; subdomain_id: number; name: string }[];
  deleteTag: (id: string) => void;
  tags: { id: string; name: string }[];
  handleChecked: (id: string, hidden: boolean) => void;
};

const TagButton = ({ handleAction, tagSelectData, deleteTag, tags, handleChecked }: PropsTagButton) => {
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  return (
    <>
      <PointerFlex onClick={handleClick}>
        <Icon name="tags" />
      </PointerFlex>
      <IbMenu id="filter-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
        <IbMenuItem
          onClick={() => {
            handleAction('addNewTag');
            handleClose();
          }}
        >
          <Flex ml="-5px" mr="10px">
            <AddCircleOutlineIcon />
          </Flex>
          <Flex fontStyle="normal" fontWeight={600} fontSize={14} lineHeight="24px">
            New Tag
          </Flex>
        </IbMenuItem>
        <StyledSubHeader>All tags</StyledSubHeader>
        <Flex flexDirection="column" height="250px" overflow="auto">
          {tagSelectData.map((tag, index) => {
            return (
              <TagSelect
                key={tag.id}
                color={autoColors(index).textColor}
                label={tag.name}
                id={tag.id}
                deleteTag={deleteTag}
                onclosedDropdown={handleClose}
                hidden={!tags.every((item) => item.id !== tag.id)}
                handleChecked={handleChecked}
              />
            );
          })}
        </Flex>
      </IbMenu>
    </>
  );
};

export default TagButton;
