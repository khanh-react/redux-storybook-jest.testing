import React from 'react';
import { MenuItem, TextField } from '@material-ui/core';
import { useStyles } from './styles';

const Owner = [
  {
    value: 'Rachel Chan',
  },
  { value: 'Alex Ho' },
];

const OwnerField = () => {
  const classes = useStyles();
  const [own, setOwn] = React.useState('Rachel Chan');
  const handleChangeOwner = (event: React.ChangeEvent<HTMLInputElement>) => {
    setOwn(event.target.value);
  };
  return (
    <TextField
      id="outlined-select-created"
      select
      label="Owner"
      value={own}
      onChange={handleChangeOwner}
      variant="outlined"
      InputProps={{
        classes: {
          root: classes.outlinedInputFocused,
          focused: classes.focused,
          notchedOutline: classes.notchedOutline,
        },
      }}
      InputLabelProps={{
        classes: {
          root: classes.inputLabel,
          focused: 'focused',
        },
      }}
    >
      {Owner.map((option) => (
        <MenuItem key={option.value} value={option.value}>
          {option.value}
        </MenuItem>
      ))}
    </TextField>
  );
};

export default OwnerField;
