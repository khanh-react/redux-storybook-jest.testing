import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: '25px 0px 0px',
    },
    // width: '295px',
    boxSizing: 'border-box',
    MozBoxSizing: 'border-box',
    WebkitBoxSizing: 'border-box',
  },
  fontstyle: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 14,
  },
  cursorPointer: {
    cursor: 'pointer',
  },
  Typography1: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 14,
    marginTop: 25,
    cursor: 'pointer',
  },
  Typography2: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 16,
  },
  Typography3: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 12,
    color: '#B6B7BB',
  },
  Typography4: {
    fontFamily: 'theme.typography.fontFamily',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#DCDCE4',
    marginLeft: 18,
    marginBottom: 15,
  },
  avatar: {
    margin: '0px 5px',
  },
  textField: {
    width: '290px',
    borderColor: '#98999F',
    '&::placeholder': {
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: 14,
      color: '#BBBBC5',
    },
  },
  sidebarStyle: {
    marginRight: 15,
    fontFamily: 'theme.typography.fontFamily',
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 12,
    color: '#B6B7BB',
    listStyleType: 'none',
    cursor: 'pointer',
    '&:hover': {
      color: '#2E2E32',
      backgroundColor: '',
    },
  },
  CheckedIcon: {
    '& svg': {
      fontSize: 18.8,
    },
    color: '#FFD000',
    backgroundColor: 'transparent',
    padding: 0,
  },
  container: {
    marginTop: 55,
    cursor: 'pointer',
  },
  ulHidescrollbar: {
    '&::-webkit-scrollbar': { width: '0 !important' },
  },
  inputLabel: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 14.67,
    borderColor: 'black',
    '&.focused': {
      color: 'black',
    },
  },
  focused: {},
  error: {},
  outlinedInputFocused: {
    borderRadius: 8,
    '&$focused $notchedOutline': {
      border: '1.3px solid black',
      borderRadius: 8,
      color: 'black',
    },
  },
  notchedOutline: {},
}));
