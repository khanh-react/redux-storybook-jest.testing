import React from 'react';

import { Flex } from '../../common/Flex';
import CreateGroup from '../Container/CreateGroup';

export default {
  title: 'Chat/CreateGroup',
  component: CreateGroup,
};

export const Primary = () => {
  return (
    <Flex height="80vh">
      <CreateGroup />
    </Flex>
  );
};
