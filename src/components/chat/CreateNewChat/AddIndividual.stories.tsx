import React from 'react';

import AddIndividual from '../Container/AddIndividual';

export default {
  title: 'Chat/AddIndividual',
  component: AddIndividual,
};

export const Primary = () => {
  return <AddIndividual />;
};
