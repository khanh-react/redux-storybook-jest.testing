import React from 'react';
import { useSelector } from 'react-redux';
import { findIndex } from 'lodash';
import { MenuItem, TextField } from '@material-ui/core';
import { AppState } from '../../../redux/store';
import { useStyles } from './styles';

const CreatedByField = ({
  setApiKey,
  created,
  setCreated,
}: {
  setApiKey: (value: string) => void;
  created: string;
  setCreated: (value: string) => void;
}) => {
  const classes = useStyles();
  const userDomainChannel = useSelector((s: AppState) => s.subdomain.subdomainChannel);
  const handleChangeCreated = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCreated(event.target.value);
    const regexp = new RegExp(/(?<=\().*?(?=\))/);
    const matches_array = event.target.value.match(regexp);
    const index = findIndex(userDomainChannel, (o) => o.owner === matches_array?.[0]);
    userDomainChannel?.[index].id && setApiKey(userDomainChannel?.[index].id);
  };

  return (
    <TextField
      id="outlined-select-created"
      select
      // label="Created by"
      value={created}
      onChange={handleChangeCreated}
      variant="outlined"
      InputProps={{
        classes: {
          root: classes.outlinedInputFocused,
          focused: classes.focused,
          notchedOutline: classes.notchedOutline,
        },
      }}
      InputLabelProps={{
        classes: {
          root: classes.inputLabel,
          focused: 'focused',
        },
      }}
    >
      {(userDomainChannel ?? []).map((option) => (
        <MenuItem key={option.id} value={`${option.name} (${option.owner})`}>
          {`${option.name} (${option.owner})`}
        </MenuItem>
      ))}
    </TextField>
  );
};

export default CreatedByField;
