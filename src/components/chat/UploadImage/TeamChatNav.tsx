import React from 'react';
import CloseIcon from '@material-ui/icons/Close';
import { Flex, PointerFlex } from '../../common/Flex';

type Props = {
  handleClosed: () => void;
};

const TeamChatNav = ({ handleClosed }: Props) => {
  return (
    <Flex borderBottom="1px solid #EEEEEE" px="10px" height={68} alignItems="center" bgcolor="#fff">
      <PointerFlex alignItems="center" onClick={handleClosed}>
        <CloseIcon fontSize="small" />
      </PointerFlex>
      <Flex fontWeight="bold" fontSize={16} lineHeight="20px" ml="25px" color="0F0F10" flex={1}>
        Upload Image
      </Flex>
    </Flex>
  );
};

export default TeamChatNav;
