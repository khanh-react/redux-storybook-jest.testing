import React from 'react';

import { Flex } from '../../common/Flex';
import TeamChatUploadImg from '../Container/TeamChatUpload';

export default {
  title: 'Chat/TeamChatUploadImg',
  component: TeamChatUploadImg,
};

export const Primary = () => {
  return (
    <Flex justifyContent="flex-end">
      <TeamChatUploadImg
        handleClosed={() => {
          return null;
        }}
        imageCurrent={[]}
        setImageCurrent={() => null}
      />
    </Flex>
  );
};
