import React from 'react';
import Picker, { IEmojiData, SKIN_TONE_NEUTRAL } from 'emoji-picker-react';
import styled from 'styled-components';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';
import useOnClickOutside from '../../common/useOnClickOutside';

const ComposerInput = styled.input`
  width: 100%;
  height: 100%;
  background: unset;
  border: none;
  outline: none;
`;
const StyledEmoji = styled.div`
  .emoji-picker-react {
    position: absolute;
    bottom: 100%;
    left: 0px;
    z-index: 90;
  }
`;

type PropsComposer = {
  setCaptionImage: (value: string) => void;
  captionImage: string;
  onSend: () => void;
};
const Composer = ({ setCaptionImage, captionImage, onSend }: PropsComposer) => {
  const emojiRef = React.useRef(null);
  const [flagEmojiTab, setFlagEmojiTab] = React.useState(false);
  useOnClickOutside(emojiRef, () => {
    setFlagEmojiTab(false);
  });
  const onEmojiClick = (e: MouseEvent, emojiObject: IEmojiData) => {
    const temp = `${captionImage}${emojiObject.emoji}`;
    setCaptionImage(temp);
  };
  return (
    <Flex minHeight={68} alignItems="center" flexShrink={0}>
      <Flex flex={1} minHeight="36px" bgcolor="#F6F6F6" borderRadius={18} alignItems="center" paddingLeft="10px">
        <Flex flex={1}>
          <ComposerInput onChange={(e) => setCaptionImage(e.target.value)} value={captionImage} />
        </Flex>
        <PointerFlex onClick={() => setFlagEmojiTab(true)} position="relative">
          <Icon name="emoji" />
          {flagEmojiTab && (
            <StyledEmoji ref={emojiRef}>
              <Picker
                disableAutoFocus
                disableSearchBar
                disableSkinTonePicker
                onEmojiClick={onEmojiClick}
                skinTone={SKIN_TONE_NEUTRAL}
              />
            </StyledEmoji>
          )}
        </PointerFlex>
      </Flex>
      <PointerFlex ml="5px" mr={1} onClick={onSend}>
        <Icon name="send" />
      </PointerFlex>
    </Flex>
  );
};
export default Composer;
