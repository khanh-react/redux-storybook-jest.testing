import React from 'react';
import { IconButton, Theme, useTheme } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { validateFileImage } from '../../../utils/validateFileName';
import { Flex, PointerFlex } from '../../common/Flex';

type PropsUpload = {
  onClickClosed: () => void;
  imageCurrent: File[];
  setImageCurrent: (image: File[]) => void;
};
export const ChatRoomUploadImg = ({ onClickClosed, imageCurrent, setImageCurrent }: PropsUpload) => {
  const theme = useTheme<Theme>();
  const inputFile = React.useRef<HTMLInputElement>(null);
  const [selectImageIndex, setSelectImageIndex] = React.useState(0);
  const onButtonClick = () => {
    inputFile.current && inputFile.current.click();
  };

  const dragOver = (e: React.DragEvent) => {
    e.preventDefault();
  };

  const dragEnter = (e: React.DragEvent) => {
    e.preventDefault();
  };

  const dragLeave = (e: React.DragEvent) => {
    e.preventDefault();
  };
  const onAddFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      if (imageCurrent.length > 5) {
        return;
      }
      const imageSelect = e.target.files;
      const tempFiles: File[] = [...imageCurrent] ?? [];
      Array.from(imageSelect).forEach((item) => {
        const checked = validateFileImage(item);
        if (checked) {
          tempFiles.push(item);
        }
      });
      setSelectImageIndex(tempFiles.length - 1);
      setImageCurrent(tempFiles);
    }
  };

  const fileDrop = (e: React.DragEvent) => {
    e.preventDefault();
    if (imageCurrent.length > 5) {
      return;
    }
    const { files } = e.dataTransfer;
    const tempFiles: File[] = [...imageCurrent] ?? [];
    Array.from(files).forEach((item) => {
      const checked = validateFileImage(item);
      if (checked) {
        tempFiles.push(item);
      }
    });
    setSelectImageIndex(tempFiles.length - 1);
    setImageCurrent(tempFiles);
  };
  return (
    <Flex flexDirection="column" width="100%" bgcolor="#F6F6F6" height="calc(100% - 68px)">
      <Flex
        justifyContent="space-between"
        alignItems="center"
        fontFamily={theme.typography.fontFamily}
        fontWeight="600"
        fontSize={16}
        lineHeight="20px"
        borderBottom="1px solid #DCDCE4"
        pl="27px"
        height="68px"
      >
        Upload image
        <IconButton onClick={onClickClosed}>
          <CloseIcon />
        </IconButton>
      </Flex>
      <Flex
        height="calc(100% - 68px)"
        onDragOver={dragOver}
        onDragEnter={dragEnter}
        onDragLeave={dragLeave}
        onDrop={fileDrop}
        flexDirection="column"
        padding="30px"
      >
        {imageCurrent && imageCurrent[selectImageIndex] && (
          <Flex justifyContent="center" alignItems="center" height="calc(100% - 125px)">
            <img
              alt="alt"
              src={URL.createObjectURL(imageCurrent[selectImageIndex])}
              style={{ maxHeight: 'calc(100% - 125px)', maxWidth: '100%' }}
            />
          </Flex>
        )}

        <Flex mt="45px" flex={1} justifyContent="flex-end" alignItems="flex-end">
          <Flex flexWrap="wrap">
            {imageCurrent.length > 0 &&
              imageCurrent.map((img, index) => {
                return (
                  <Flex
                    key={img.name}
                    marginRight="10px"
                    borderRadius="8px"
                    height="80px"
                    width="80px"
                    justifyContent="center"
                    alignItems="center"
                    bgcolor="#fff"
                    onClick={() => setSelectImageIndex(index)}
                  >
                    <img
                      alt="alt"
                      src={URL.createObjectURL(img)}
                      style={{ borderRadius: '8px', maxHeight: '80px', maxWidth: '100%' }}
                    />
                  </Flex>
                );
              })}
          </Flex>
          <PointerFlex
            width="80px"
            height="80px"
            border="1px dashed #DCDCE4"
            borderRadius="8px"
            justifyContent="center"
            alignItems="center"
            flexDirection="column"
            onClick={onButtonClick}
          >
            <AddIcon />
            <Flex>Add file</Flex>
            <input
              type="file"
              accept="image/*"
              id="file"
              ref={inputFile}
              style={{ display: 'none' }}
              onChange={onAddFile}
              multiple
            />
          </PointerFlex>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default ChatRoomUploadImg;
