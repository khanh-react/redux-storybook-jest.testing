import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import { Flex, PointerFlex } from '../../common/Flex';
import { validateFileImage } from '../../../utils/validateFileName';

type PropsUpload = {
  imageCurrent: File[];
  setImageCurrent: (image: File[]) => void;
};

export const TeamChatUploadImg = ({ imageCurrent, setImageCurrent }: PropsUpload) => {
  const [selectImageIndex, setSelectImageIndex] = React.useState(0);
  const inputFile = React.useRef<HTMLInputElement>(null);
  const onButtonClick = () => {
    inputFile.current && inputFile.current.click();
  };

  const dragOver = (e: React.DragEvent) => {
    e.preventDefault();
  };

  const dragEnter = (e: React.DragEvent) => {
    e.preventDefault();
  };

  const dragLeave = (e: React.DragEvent) => {
    e.preventDefault();
  };
  const onAddFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      if (imageCurrent.length > 2) {
        return;
      }
      const imageSelect = e.target.files;
      const tempFiles: File[] = [...imageCurrent] ?? [];
      Array.from(imageSelect).forEach((item) => {
        const checked = validateFileImage(item);
        if (checked) {
          tempFiles.push(item);
        }
      });
      setImageCurrent(tempFiles);
    }
  };

  const fileDrop = (e: React.DragEvent) => {
    e.preventDefault();
    if (imageCurrent.length > 2) {
      return;
    }
    const { files } = e.dataTransfer;
    const tempFiles: File[] = [...imageCurrent] ?? [];
    Array.from(files).forEach((item) => {
      const checked = validateFileImage(item);
      if (checked) {
        tempFiles.push(item);
      }
    });
    setSelectImageIndex(tempFiles.length - 1);
    setImageCurrent(tempFiles);
  };

  return (
    <Flex flexDirection="column" width="100%" bgcolor="#F6F6F6" height="100%">
      <Flex
        height="100%"
        flexDirection="column"
        onDragOver={dragOver}
        onDragEnter={dragEnter}
        onDragLeave={dragLeave}
        onDrop={fileDrop}
        padding="20px"
      >
        {imageCurrent && imageCurrent[selectImageIndex] && (
          <Flex justifyContent="center" alignItems="center" height="calc(100% - 170px)">
            <img
              alt="alt"
              src={URL.createObjectURL(imageCurrent[selectImageIndex])}
              style={{ maxHeight: 'calc(100% - 170px)', maxWidth: '100%' }}
            />
          </Flex>
        )}

        <Flex mt="20px" justifyContent="flex-end" alignItems="flex-end" flexDirection="column" flex={1}>
          <Flex flexWrap="wrap">
            {imageCurrent.length > 0 &&
              imageCurrent.map((img, index) => {
                return (
                  <Flex
                    key={img.name}
                    marginRight="7px"
                    borderRadius="8px"
                    height="70px"
                    width="70px"
                    justifyContent="center"
                    alignItems="center"
                    bgcolor="#fff"
                    onClick={() => setSelectImageIndex(index)}
                  >
                    <img
                      alt="alt"
                      src={URL.createObjectURL(img)}
                      style={{ borderRadius: '8px', maxHeight: '70px', maxWidth: '100%' }}
                    />
                  </Flex>
                );
              })}
          </Flex>
          <PointerFlex
            width="70px"
            height="70px"
            border="1px dashed #DCDCE4"
            borderRadius="8px"
            justifyContent="center"
            alignItems="center"
            flexDirection="column"
            onClick={onButtonClick}
            marginTop="10px"
          >
            <AddIcon />
            <Flex>Add file</Flex>
            <input
              type="file"
              accept="image/*"
              id="file"
              ref={inputFile}
              style={{ display: 'none' }}
              onChange={onAddFile}
              multiple
            />
          </PointerFlex>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default TeamChatUploadImg;
