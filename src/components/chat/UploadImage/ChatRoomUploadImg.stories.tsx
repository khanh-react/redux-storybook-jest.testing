import React from 'react';

import { Flex } from '../../common/Flex';
import ChatRoomUpload from '../Container/ChatRoomUpload';

export default {
  title: 'Chat/ChatRoomUpload',
  component: ChatRoomUpload,
};

export const Primary = () => {
  return (
    <Flex>
      <ChatRoomUpload
        onClickClosed={() => {
          return null;
        }}
      />
    </Flex>
  );
};
