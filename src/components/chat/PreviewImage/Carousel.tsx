import React from 'react';
import Box from '@material-ui/core/Box/Box';
import styled from 'styled-components';
import { Flex } from '../../common/Flex';

type Props = {
  images: string;
  onClose: () => void;
};

const ClosedIcon = styled.div`
  position: absolute;
  z-index: 20;
  cursor: pointer;
  &:hover {
    opacity: 75;
  }
  top: -50px;
  right: -60px;
`;

// const IconRight = styled.div`
//   position: absolute;
//   z-index: 20;
//   cursor: pointer;
//   &:hover {
//     opacity: 75;
//   }
//   top: 50%;
//   right: -60px;
//   transform: translateY(-50%);
// `;

// const IconLeft = styled.div`
//   position: absolute;
//   z-index: 20;
//   cursor: pointer;
//   &:hover {
//     opacity: 75;
//   }
//   top: 50%;
//   left: -60px;
//   transform: translateY(-50%);
// `;

// const ThumbnailImageActive = styled.div`
//   margin-left: 10px;
//   margin-right: 10px;
//   transform: scale(1.2);
//   transition: ease-in-out 0.3s;
// `;

// const ThumbnailImage = styled.div`
//   margin-left: 5px;
//   margin-right: 5px;
//   transition: ease-in-out 0.3s;
// `;

const Carousel = ({ images, onClose }: Props) => {
  return (
    <Flex
      position="fixed"
      width="100vw"
      height="100vh"
      top={0}
      left={0}
      zIndex={20}
      justifyContent="center"
      alignItems="center"
    >
      <Box position="relative" zIndex={20}>
        <Flex alignItems="center" justifyContent="center">
          <Flex
            position="relative"
            minHeight="calc(100vh - 200px)"
            width="80vw"
            justifyContent="center"
            alignItems="center"
            bgcolor="rgba(0,0,0,0.8)"
          >
            <img src={images} alt="img-item" style={{ maxHeight: 'calc(100vh - 200px)', maxWidth: '100%' }} />
            <ClosedIcon onClick={onClose}>
              <img src="/static/closed.svg" alt="closed icon" />
            </ClosedIcon>
            {/* <IconRight
                onClick={() => (activeIdx === images.length - 1 ? setActiveIdx(0) : setActiveIdx(activeIdx + 1))}
              >
                <img src="/static/arrow-r.svg" alt="icon" />
              </IconRight>
              <IconLeft
                onClick={() => (activeIdx === 0 ? setActiveIdx(images.length - 1) : setActiveIdx(activeIdx - 1))}
              >
                <img src="/static/arrow-l.svg" alt="icon" />
              </IconLeft> */}
          </Flex>
        </Flex>
      </Box>
      {/* <Flex position="relative" zIndex={20} marginTop="20px">
          {thumbnails.map((item, index) => {
            const keys = index;
            return activeIdx === index ? (
              <ThumbnailImageActive className="thumbnails-item ml-5 mr-5 scale" key={keys}>
                <img src={item} alt="img-item" style={{ borderRadius: '10px', maxHeight: '125px' }} />
              </ThumbnailImageActive>
            ) : (
              <ThumbnailImage className="thumbnails-item ml-2 mr-2" onClick={() => setActiveIdx(index)} key={keys}>
                <img src={item} alt="img-item" style={{ borderRadius: '10px', maxHeight: '125px' }} />
              </ThumbnailImage>
            );
          })}
        </Flex> */}
      <Box
        position="absolute"
        height="100vh"
        width="100vw"
        top={0}
        left={0}
        bgcolor="#282833"
        zIndex={10}
        onClick={onClose}
        style={{ opacity: 0.8 }}
      />
    </Flex>
  );
};

export default Carousel;
