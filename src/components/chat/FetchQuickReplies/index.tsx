import React, { useState, useCallback, useEffect } from 'react';
import update from 'immutability-helper';
import Wrapper from './styles';
import { Props, DataProps } from './types';
import { Flex, PointerFlex } from '../../common/Flex';
import { Text } from '../../common/Text';
import { Dropdown } from '../../common/Dropdown';
import Container from './Container';
import { updateFaq } from '../../../redux/services/chat';

export const FetchQuickReplies = ({ dropdown, categories, handleClickAdd }: Props) => {
  const [currentShortcutID, setShortcutID] = useState(dropdown[0].id);
  const [renderList, setRenderList] = useState<DataProps[]>([]);

  useEffect(() => {
    setRenderList(categories.filter((item) => item.faqCategory === currentShortcutID));
  }, [currentShortcutID, categories]);
  useEffect(() => {
    setShortcutID(dropdown[0].id);
  }, [dropdown]);

  const handleIndexChanged = (index: number) => {
    setShortcutID(dropdown[index].id);
  };

  const onMoveFaq = useCallback(
    (dragIndex: number, hoverIndex: number, id: string) => {
      const dragCard = renderList[dragIndex];
      setRenderList(
        update(renderList, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        }),
      );
      updateFaq({ ordering: hoverIndex + 1, id });
    },
    [renderList],
  );

  return (
    <Wrapper flexDirection="column">
      <Flex justifyContent="center" alignItems="center" position="relative" paddingY="20px">
        <PointerFlex
          position="absolute"
          top="50%"
          left="20px"
          style={{ transform: 'translateY(-50%)' }}
          onClick={handleClickAdd}
        >
          <img src="/static/add-icon.svg" alt="add" />
        </PointerFlex>
        <Text weight="bold" value="Quick Replies" />
      </Flex>
      <Flex className="dropdown-wrap" justifyContent="center">
        <Dropdown items={dropdown} key="id" keyValue="name" onIndexChanged={handleIndexChanged} />
      </Flex>
      <Container renderList={renderList} onMoveFaq={onMoveFaq} />
    </Wrapper>
  );
};

export default FetchQuickReplies;
