import React, { useCallback } from 'react';
import { DataProps } from './types';
import Card from './Card';
import { Flex } from '../../common/Flex';

function Container({
  renderList,
  onMoveFaq,
}: {
  renderList: DataProps[];
  onMoveFaq: (dragIndex: number, hoverIndex: number, id: string) => void;
}) {
  const moveCard = useCallback(
    (dragIndex, hoverIndex, id) => {
      onMoveFaq(dragIndex, hoverIndex, id);
    },
    [onMoveFaq],
  );

  return (
    <Flex height="calc(100% - 107px)" overflow="auto" flexDirection="column" padding="10px 15px">
      {renderList.map((item, index) => {
        return (
          <Card
            shortcut={item.shortcut}
            question={item.question}
            key={item.id}
            id={item.id}
            index={index}
            moveCard={moveCard}
          />
        );
      })}
    </Flex>
  );
}

export default Container;
