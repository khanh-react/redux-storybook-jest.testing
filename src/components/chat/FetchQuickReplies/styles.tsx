import styled from 'styled-components';
import { Flex } from '../../common/Flex';

export default styled(Flex)`
  width: 300px;
  height: 100%;

  .dropdown-wrap {
    height: 49px;
    border: 1px solid #ebeff1;
  }

  .qreply-item {
    margin: 10px 0;
    padding: 8px 10px 15px 14px;
    border-radius: 11px;
    border: solid 1.2px #dcdce4;
    &:hover {
      background-color: #f6f7f8;
    }
  }

  .qreply-name {
    margin-bottom: 5px;
  }

  .qreply-content {
  }

  .mrb20 {
    margin-bottom: 20px;
  }
`;
