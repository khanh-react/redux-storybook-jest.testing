import { get, set } from 'lodash';
import React, { useRef } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import Highlighter from 'react-highlight-words';

import { Text } from '../../common/Text';
import { placeholders } from '../AddQuickReply';

function Card({
  shortcut,
  question,
  id,
  index,
  moveCard,
}: {
  shortcut: string;
  question: string;
  id: string;
  index: number;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  moveCard: any;
}) {
  const ref = useRef<HTMLDivElement>(null);
  const [, drop] = useDrop({
    accept: 'CARD',
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = get(item, 'index', 0);
      const hoverIndex = index;
      const dragId = get(item, 'id', '0');
      if (dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current?.getBoundingClientRect();
      // Get vertical middle
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      // Determine mouse position
      const clientOffset = monitor.getClientOffset();
      // Get pixels to the top
      const hoverClientY = get(clientOffset, 'y', 0) - hoverBoundingRect.top;
      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }
      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      moveCard(dragIndex, hoverIndex, dragId);
      set(item, 'index', hoverIndex);
    },
  });
  const [{ isDragging }, drag] = useDrag({
    item: { type: 'CARD', id, index },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  const opacity = isDragging ? 0 : 1;
  drag(drop(ref));

  return (
    <div
      className="qreply-item"
      ref={ref}
      style={{
        flexDirection: 'column',
        flexShrink: 0,
        cursor: 'move',
        display: 'flex',
        opacity,
      }}
    >
      <Text className="qreply-name" size="xsm" value={`/${shortcut}`} color="#77777A" />
      <Highlighter
        highlightClassName="YourHighlightClass"
        searchWords={placeholders.map((h) => h.key)}
        autoEscape
        textToHighlight={question}
        highlightStyle={{ fontSize: '14px', color: '#77777A', background: 'transparent' }}
        unHighlightStyle={{ fontSize: '14px', color: '#2E2E32' }}
      />
    </div>
  );
}

export default Card;
