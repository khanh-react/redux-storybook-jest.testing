import React from 'react';

import { FetchQuickReplies } from '.';

export default {
  title: 'Chat/ChooseQuickReplies',
  component: FetchQuickReplies,
};

const dropdown = [
  {
    id: 'all',
    name: 'All categories',
  },
  {
    id: 'thankyou',
    name: 'Thank You',
  },
  {
    id: 'welcome',
    name: 'Wellcome',
  },
  {
    id: 'team',
    name: 'Teams',
  },
];

const categories = [
  {
    id: '39',
    shortcut: 'fqa-1',
    question: 'Text faq',
    answer: 'answer',
    faqCategory: '20',
    ordering: 1,
  },
];

export const Default = () => {
  return <FetchQuickReplies dropdown={dropdown} categories={categories} handleClickAdd={() => null} />;
};
