export type DropdownProps = {
  id: string;
  name: string;
};

export type DataProps = {
  id: string;
  shortcut: string;
  question: string;
  answer: string;
  faqCategory: string;
  ordering: number;
};

export type Props = {
  dropdown: DropdownProps[];
  categories: DataProps[];
  handleClickAdd: () => void;
  // onMoveFaq: (dragIndex: number, hoverIndex: number, id: string) => void;
};
