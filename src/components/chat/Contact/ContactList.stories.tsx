import React from 'react';

import { Flex } from '../../common/Flex';
import ContactList from './ContactList';

export default {
  title: 'Chat/ContactList',
  component: ContactList,
};

export const Primary = () => {
  return (
    <Flex height="80vh">
      <ContactList
        onBack={() => {
          return null;
        }}
        onRemove={() => {
          return null;
        }}
        onSelect={[{ id: '1', name: '2', phoneNumber: '124324343' }]}
        handleSelect={() => {
          return null;
        }}
      />
    </Flex>
  );
};
