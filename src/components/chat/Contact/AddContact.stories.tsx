import React from 'react';

import AddContact from './AddContact';

export default {
  title: 'Chat/AddContact',
  component: AddContact,
};

export const Primary = () => {
  return <AddContact />;
};
