import { Button } from '@material-ui/core';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import React from 'react';
import styled from 'styled-components';
import { Flex } from '../../common/Flex';
import { IbTextField } from '../../common/InputField';
import { Nav } from '../../common/Nav';
import PhoneField from './PhoneField';

const StyledButton = styled(Button)`
  border-radius: 8px;
  .MuiButton-label {
    font-weight: 600;
    font-size: 14px;
  }
`;

const AddGroup = () => {
  return (
    <Flex flexDirection="column" width={345}>
      <Nav backButton title="Add Contact" />
      <form noValidate autoComplete="off">
        <Flex flexDirection="column" justifyContent="space-between" height="90vh" p="0px 25px 25px">
          <Flex flexDirection="column">
            <Flex flexDirection="column" borderBottom="1px solid #EEEEEE" pb="25px" mt="30px">
              <Flex>
                <IbTextField label="Name" required={false} error={false} disabled={false} readOnly={false} />
              </Flex>
              <Flex mt="20px">
                <PhoneField />
              </Flex>
            </Flex>
            <Flex flexDirection="column">
              <Flex mt="25px">
                <IbTextField label="Name" required={false} error={false} disabled={false} readOnly={false} />
              </Flex>
              <Flex mt="20px">
                <PhoneField />
              </Flex>
            </Flex>
            <StyledButton startIcon={<ControlPointIcon />} style={{ marginLeft: 'auto', marginTop: 15 }}>
              Add Contact
            </StyledButton>
          </Flex>
          <StyledButton color="secondary" variant="contained">
            Next
          </StyledButton>
        </Flex>
      </form>
    </Flex>
  );
};

export default AddGroup;
