import React from 'react';
import { InputAdornment, Select } from '@material-ui/core';
import { StyledMenuItem, IbTextField } from '../../common/InputField';

const PhoneField = () => {
  const [Flag, setFlag] = React.useState('HK');
  const handleChangeFlag = (event: React.ChangeEvent<{ value: unknown }>) => {
    setFlag(event.target.value as string);
  };

  return (
    <IbTextField
      required={false}
      error={false}
      disabled={false}
      readOnly={false}
      defaultValue="68888 8888"
      label="Phone"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Select
              value={Flag}
              onChange={handleChangeFlag}
              MenuProps={{
                anchorOrigin: {
                  vertical: 'bottom',
                  horizontal: 'left',
                },
                getContentAnchorEl: null,
              }}
              displayEmpty
              disableUnderline
            >
              <StyledMenuItem value="HK">
                <img alt="HKFlag icon" src="/static/HKFlag.svg" />
              </StyledMenuItem>
              <StyledMenuItem value="None">
                <em>None</em>
              </StyledMenuItem>
            </Select>
          </InputAdornment>
        ),
      }}
    />
  );
};

export default PhoneField;
