import { Box, TextField, Typography } from '@material-ui/core';
import React, { useEffect, useRef, useState } from 'react';
import { useScrolling } from '../../../utils/useScrolling';
import ContactItem, { ContactListProps, useStyles } from './ContactItem';
import { Flex } from '../../common/Flex';
import InputTag from '../../common/InputTag';
import { Nav } from '../../common/Nav';

// import ControlPointIcon from '@material-ui/icons/ControlPoint';
// import styled from 'styled-components';

// const StyledBottomButton = styled(Button)`
//   .MuiButton-label {
//     font-style: normal;
//     font-weight: 600;
//     font-size: 15px;
//     line-height: 26px;
//     text-transform: none;
//   }
//   margin-left: 21.5px;
//   border-radius: 8px;
//   min-height: 38px;
//   width: 145px;
//   box-shadow: none;
// `;
const CONTACTS: ContactListProps[] = [
  {
    id: '1',
    name: 'Alex Ho',
    phoneNumber: '85363408999',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'line',
    status: 'accept',
  },
  {
    id: '2',
    name: 'Anthony Wu',
    phoneNumber: '85363408998',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'whatsapp',
    status: 'accepted',
  },
  {
    id: '3',
    name: 'Austin Yu',
    phoneNumber: '85363408997',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'whatsapp',
    status: 'accepted',
  },
  {
    id: '4',
    name: 'Austin Tseng',
    phoneNumber: '85363408996',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'whatsapp',
    status: 'accepted',
  },
  {
    id: '5',
    name: 'Bosco Chu',
    phoneNumber: '85363408995',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'line',
    status: 'accepted',
  },
  {
    id: '7',
    name: 'Michael N',
    phoneNumber: '85363408993',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'line',
    status: 'accept',
  },
  {
    id: '8',
    name: 'Bianka Matthews',
    phoneNumber: '85363408994',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'line',
    status: 'accept',
  },
  {
    id: '9',
    name: 'Catrina Robbins',
    phoneNumber: '85363408993',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'line',
    status: 'accepted',
  },
  {
    id: '10',
    name: 'Willard Obrien',
    phoneNumber: '85363408992',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'line',
    status: 'accepted',
  },
  {
    id: '11',
    name: 'Hope Craft',
    phoneNumber: '85363408991',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'whatsapp',
    status: 'accepted',
  },
  {
    id: '12',
    name: 'Ella-Louise Wharton',
    phoneNumber: '85363408919',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'whatsapp',
    status: 'accepted',
  },
  {
    id: '13',
    name: 'Destiny Leblanc',
    phoneNumber: '85363408949',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'line',
    status: 'accept',
  },
  {
    id: '14',
    name: 'Poppy-Rose Russo',
    phoneNumber: '85363408959',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'whatsapp',
    status: 'accepted',
  },
  {
    id: '15',
    name: 'alan Tang',
    phoneNumber: '85292281747',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'whatsapp',
    status: 'accepted',
  },
  {
    id: '16',
    name: 'Quy Le',
    phoneNumber: '84888222293',
    image: 'https://via.placeholder.com/150?text=MN',
    source: 'whatsapp',
    status: 'accepted',
  },
];

type Props = {
  onBack?: () => void;
  handleSelect: (user: { id: string; name: string; phoneNumber: string }) => void;
  onRemove: (user: string) => void;
  onSelect: { id: string; name: string; phoneNumber: string }[];
};

const ContactList = ({ onBack, handleSelect, onRemove, onSelect }: Props) => {
  const classes = useStyles();
  const [hidden, setHidden] = useState(true);
  const scrollRef = useRef(null);
  const scrolling = useScrolling(scrollRef);
  const [listContact, setListContact] = useState<{ [key: string]: ContactListProps[] }>({});
  const handleMouseEnter = () => {
    setHidden(false);
  };
  const handleMouseLeave = () => {
    setHidden(true);
  };

  useEffect(() => {
    scrolling ? setHidden(false) : setHidden(true);
  }, [scrolling]);

  useEffect(() => {
    const group = CONTACTS.sort((a, b) => a.name.localeCompare(b.name)).reduce<{ [key: string]: ContactListProps[] }>(
      (acc, contact) => {
        const key = contact.name[0];
        const contacts = acc[key] ?? [];
        contacts.push(contact);
        return { ...acc, [key]: contacts };
      },
      {},
    );
    setListContact(group);
  }, []);

  const filterContact = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const group = CONTACTS.filter((fill) =>
      fill.name.toLocaleLowerCase().trim().includes(value.toLocaleLowerCase().trim()),
    )
      .sort((a, b) => a.name.localeCompare(b.name))
      .reduce<{ [key: string]: ContactListProps[] }>((acc, contact) => {
        const key = contact.name[0];
        const contacts = acc[key] ?? [];
        contacts.push(contact);
        return { ...acc, [key]: contacts };
      }, {});
    setListContact(group);
  };

  const alphabets = [];
  for (let i = 0; i < 26; i++) {
    alphabets.push((i + 10).toString(36).toUpperCase());
  }
  function scrollList(props: string) {
    const list = document.getElementById('scrollList');
    const targetLi = document.getElementById(props);
    if (list != null && targetLi != null) {
      list.scrollTop = targetLi.offsetTop - 300;
    }
  }
  return (
    <>
      <Nav backButton title="Add Group" onBack={onBack} />
      <Flex flexDirection="column" alignItems="center" justifyContent="space-between" pl="15px">
        <Flex flexDirection="column" ml={-1} alignItems="center">
          <Flex flexWrap="wrap" ml="10px" alignSelf="flex-start">
            <Typography className={classes.Typography1}>Add Group Participants</Typography>
          </Flex>
          <Flex flexWrap="wrap" ml="25px">
            {onSelect.map((tag) => {
              return (
                <Flex m="5px" key={tag.id}>
                  <InputTag hasCloseIcon label={tag.phoneNumber} id={tag.id} onClose={onRemove} />
                </Flex>
              );
            })}
          </Flex>
          <form noValidate autoComplete="off">
            <TextField
              className={classes.textField}
              id="standard-basic"
              placeholder="Type contact name"
              InputProps={{
                classes: {
                  input: classes.textField,
                  underline: classes.textField,
                },
              }}
              onChange={filterContact}
            />
          </form>
          <Flex position="relative">
            <ul
              ref={scrollRef}
              id="scrollList"
              className={classes.ulHideScrollbar}
              style={{
                height: '600px',
                listStyleType: 'none',
                paddingInlineStart: 0,
                marginTop: 30,
                overflow: 'hidden',
                overflowY: 'scroll',
              }}
            >
              {Object.entries(listContact).map(([key, value]) => {
                return (
                  <Box key={key} id={key} ml="20px">
                    <Typography className={classes.Typography4}>{key}</Typography>
                    {value.map((item) => (
                      <Flex key={item.id}>
                        <ContactItem
                          id={item.id}
                          name={item.name}
                          image={item.image}
                          phoneNumber={item.phoneNumber}
                          source={item.source}
                          status={!onSelect.every((itemSelect) => itemSelect.id !== item.id) ? 'accept' : 'accepted'}
                          handleSelect={handleSelect}
                        />
                      </Flex>
                    ))}
                  </Box>
                );
              })}
            </ul>
            <Flex
              className="side-bar"
              onMouseEnter={handleMouseEnter}
              onMouseLeave={handleMouseLeave}
              width="25px"
              height="80vh"
              overflow="hidden"
              position="absolute"
              right={0}
              top={0}
            >
              {!hidden && (
                <Flex flexDirection="column" alignItems="center" justifyContent="center" mt="-50px">
                  {alphabets.map((alphabet) => {
                    return (
                      <li
                        key={alphabet}
                        className={classes.sidebarStyle}
                        onClick={() => scrollList(alphabet)}
                        role="menuitem"
                      >
                        {alphabet}
                      </li>
                    );
                  })}
                </Flex>
              )}
            </Flex>
          </Flex>
        </Flex>
      </Flex>
      {/* <Flex alignItems="center" justifyContent="center">
        <Flex mr="12px">
          <StyledBottomButton variant="outlined" startIcon={<ControlPointIcon />}>
            Add Contact
          </StyledBottomButton>
          <StyledBottomButton color="secondary" variant="contained">
            Next
          </StyledBottomButton>
        </Flex>
      </Flex> */}
    </>
  );
};
export default ContactList;
