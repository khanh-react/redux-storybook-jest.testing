import { Avatar, Box, IconButton, makeStyles, Typography } from '@material-ui/core';
import React from 'react';
import CheckIcon from '@material-ui/icons/Check';
import Icon from '../../common/Icon';

export const useStyles = makeStyles((theme) => ({
  Typography1: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 14,
    marginTop: 25,
    marginLeft: 23,
    cursor: 'pointer',
  },
  Typography2: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 16,
  },
  Typography3: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 12,
    color: '#B6B7BB',
  },
  Typography4: {
    fontFamily: 'theme.typography.fontFamily',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#DCDCE4',
    marginLeft: 18,
    marginBottom: 15,
  },
  avatar: {
    margin: '0px 5px',
  },
  textField: {
    width: '290px',
    borderColor: '#98999F',
    '&::placeholder': {
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: 14,
      color: '#BBBBC5',
    },
  },
  sidebarStyle: {
    marginRight: 15,
    fontFamily: 'theme.typography.fontFamily',
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 12,
    color: '#B6B7BB',
    listStyleType: 'none',
    cursor: 'pointer',
    '&:hover': {
      color: '#2E2E32',
      backgroundColor: '',
    },
  },
  CheckedIcon: {
    '& svg': {
      fontSize: 18.8,
    },
    color: '#FFD000',
    backgroundColor: 'transparent',
    padding: 0,
  },
  container: {
    marginTop: 55,
    cursor: 'pointer',
  },
  ulHideScrollbar: {
    '&::-webkit-scrollbar': { width: '0 !important' },
  },
}));

export type ContactListProps = {
  id: string;
  name: string;
  phoneNumber: string;
  image: string;
  source: 'line' | 'whatsapp';
  status: 'accept' | 'accepted';
  handleSelect?: (user: { id: string; name: string; phoneNumber: string }) => void;
};

const ContactItem = (props: ContactListProps) => {
  const classes = useStyles();

  return (
    <li
      style={{
        overflow: 'hidden',
        borderBottom: '1px solid #ECEFF1',
        marginBottom: 10,
        paddingBottom: 15,
        width: 345,
        cursor: 'pointer',
      }}
    >
      <Box
        className={classes.avatar}
        display="flex"
        alignItems="flex-start"
        onClick={
          () =>
            props.handleSelect && props.handleSelect({ id: props.id, name: props.name, phoneNumber: props.phoneNumber })
          // eslint-disable-next-line react/jsx-curly-newline
        }
      >
        <Box position="relative">
          <Avatar alt={props.name} src={props.image} />
          <Box
            bottom={-6}
            right={0}
            position="absolute"
            display="flex"
            borderRadius={10}
            boxShadow="0px 0px 4px rgba(0, 0, 0, 0.233419)"
          >
            <Icon name={props.source} />
          </Box>
        </Box>
        <div style={{ marginLeft: 15 }}>
          <Typography className={classes.Typography2}>{props.name}</Typography>
          <Typography className={classes.Typography3}>{props.phoneNumber}</Typography>
        </div>
        <div style={{ marginLeft: 'auto', marginTop: 10, marginRight: 25 }}>
          {props.status === 'accept' ? (
            <Box display="flex" alignItems="center" justifyContent="center">
              <IconButton className={classes.CheckedIcon}>
                <CheckIcon />
              </IconButton>
            </Box>
          ) : (
            <Box />
          )}
        </div>
      </Box>
    </li>
  );
};

export default ContactItem;
