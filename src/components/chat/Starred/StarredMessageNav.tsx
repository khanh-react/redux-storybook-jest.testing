/* eslint-disable no-nested-ternary */
import { get } from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../../redux/store';
import { readRecord } from '../../../utils/localStorageService';
import { Flex } from '../../common/Flex';
import { Nav } from '../../common/Nav';
import { updateCaseLeftNav, updateStarredMessages } from '../../../redux/action/chat';
import { starredMessage } from '../../../redux/services/chat';
import { StarredMessage } from './StarredMessage';

const StarredMessageNav = () => {
  const dispatch = useDispatch();
  const staredMessages = useSelector((state: AppState) => state.chat.starredMessages);
  // const userId = useSelector((state: AppState) => state.auth.userId);
  const subDomainId = useSelector((s: AppState) => (s.auth.subdomainId === undefined ? undefined : s.auth.subdomainId));
  const onClose = () => {
    dispatch(updateCaseLeftNav('message'));
  };
  const handleDeleteAll = (action: string) => {
    if (action === 'unstar-all') {
      if (staredMessages && staredMessages.length > 0) {
        staredMessages.forEach((item) => {
          starredMessage(item.id, 0);
        });
      }
      dispatch(updateStarredMessages());
    }
  };

  return (
    <>
      <Nav
        backButton
        title="Starred messages"
        actions={[{ key: 'unstar-all', title: 'Unstar All' }]}
        onBack={onClose}
        onAction={handleDeleteAll}
      />
      <Flex flexDirection="column" height="calc(100% - 68px)" overflow="auto" p="20px">
        {(staredMessages ?? []).map((message) => (
          <Flex key={message.id}>
            <StarredMessage
              id={message.id}
              name={get(message, 'chat.name', '')}
              customer={
                message.from_type === 'System'
                  ? 'System'
                  : message.from_type === 'Web'
                  ? 'Web'
                  : message.from_type === 'Phone'
                  ? 'Phone'
                  : message.from_type === 'Customer Service'
                  ? message.user.name
                  : get(message, 'chat.name', '') ?? get(message, 'chat.whatsapp_name', '')
              }
              label={message.text || ''}
              date={new Date(message.timestamp)}
              received={message.from_type === 'Other'}
              type={message.message_type}
              filePath={`${process.env.REACT_APP_API_URL}/media/${message.id}?access-token=${readRecord(
                'accessToken',
              )}&subdomain-id=${subDomainId}`}
              fileName={message.file_name}
              file_mime_type={message.file_mime_type}
            />
          </Flex>
        ))}
      </Flex>
    </>
  );
};

export default StarredMessageNav;
