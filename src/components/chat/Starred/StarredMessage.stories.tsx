import React from 'react';
import { MessageType } from '../../../redux/services/chat';
import { Flex } from '../../common/Flex';
import { Nav } from '../../common/Nav';
import { StarredMessage, StarredMessProps } from './StarredMessage';

export default {
  title: 'Chat/StarredMessage',
  component: StarredMessage,
};

const STARRED_MESSAGES: StarredMessProps[] = [
  {
    id: '1',
    name: 'Julian Grubber',
    customer: 'Rachel Chan',
    label: 'My phone number is 58888888. Thanks!Lorem ipsum dolor sit amet, consectetur adipiscing',
    date: new Date('2020-09-04T15:40:57.640Z'),
    received: true,
    type: MessageType.Text,
  },
  {
    id: '2',
    name: 'Kelvin Kwan',
    customer: 'Rachel Chan',
    label: 'Your personal discount code is: JUY825302',
    date: new Date('2020-09-04T15:40:57.640Z'),
    received: false,
    type: MessageType.Text,
  },
  {
    id: '3',
    name: 'Julian Grubber',
    customer: 'Rachel Chan',
    label: 'My phone number is 58888888. Thanks!',
    date: new Date('2020-09-04T15:40:57.640Z'),
    received: true,
    type: MessageType.Text,
  },
  {
    id: '4',
    name: 'Kelvin Kwan',
    customer: 'Rachel Chan',
    date: new Date('2020-09-04T15:40:57.640Z'),
    received: false,
    type: MessageType.Media,
    filePath: 'https://swagger.com.vn/wp-content/uploads/2020/08/nike-air-jordan-1-dior-low-1.jpg',
  },
  {
    id: '5',
    name: 'Julian Grubber',
    customer: 'Rachel Chan',
    label: 'My phone number is 58888888. Thanks!',
    date: new Date('2020-09-04T15:40:57.640Z'),
    received: false,
    type: MessageType.Document,
    filePath: 'http://www.example.com/dir/Product List_2938910.pdf',
    fileSize: '11.67kb',
  },
];

export const Primary = () => {
  return (
    <Flex flexDirection="column" width={345} height="90vh">
      <Nav backButton title="Starred messages" actions={[{ key: 'unstar-all', title: 'Unstar All' }]} />
      <Flex flexDirection="column" justifyContent="space-between" height="100%" overflow="scroll">
        <Flex flexDirection="column">
          {STARRED_MESSAGES.map((message) => (
            <Flex key={message.id}>
              <StarredMessage {...message} />
            </Flex>
          ))}
        </Flex>
      </Flex>
    </Flex>
  );
};

export const Test = (props: StarredMessProps) => {
  return <StarredMessage {...props} />;
};
Test.args = {
  id: '1',
  name: 'Test 1',
  customer: 'Rachel Chan',
  label: 'Test string',
  received: true,
  date: new Date('2020-09-04T15:40:57.640Z'),
  formatOfMess: 'text',
};
