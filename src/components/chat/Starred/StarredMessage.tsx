/* eslint-disable no-nested-ternary */
import { Avatar, Box, makeStyles, Typography, withStyles, CardMedia } from '@material-ui/core';
import AudioPlayer from 'material-ui-audio-player';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { format } from 'date-fns';
import React from 'react';
import { useDispatch } from 'react-redux';
import { MessageType } from '../../../redux/services/chat';
import { Flex, PointerFlex } from '../../common/Flex';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';
import { updateStarredMess } from '../../../redux/action/chat';
import { isExcelFile, isWordFile, isZipFile, isPdfFile, isPowerPointFile } from '../../../utils/validateFileName';
import firstChars from '../../../utils/firstChars';

export type StarredMessProps = {
  id: string;
  name: string;
  customer: string;
  label?: string;
  date: Date;
  received: boolean;
  type: MessageType;
  filePath?: string;
  fileSize?: string;
  fileName?: string;
  file_mime_type?: string;
};

const MessageBox = withStyles(() => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    alignSelf: 'flex-start',
    position: 'relative',
    borderRadius: 12,
    boxSizing: 'border-box',
    MozBoxSizing: 'border-box',
    WebkitBoxSizing: 'border-box',
    margin: '0px 5px 5px 35px',
  },
}))(Box);

const useStyles = makeStyles((theme) => ({
  fontstyle: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 11,
    color: '#A2A3A9',
    lineHeight: '20px',
    marginRight: 15,
  },
  cursorPointer: {
    cursor: 'pointer',
  },
  avatar: {
    margin: '0px 5px',
  },
  file: {
    height: 72,
    width: 250,
    paddingLeft: '5px',
    '& img[src$=".pdf"]': {
      content: 'url(/static/Pdf.svg)',
      maxWidth: '40px',
      width: '100%',
      margin: '15px 15px 15px 20px',
    },
    '& a': {
      fontFamily: theme.typography.fontFamily,
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: 15,
      color: '#080809',
      marginRight: 15,
      lineHeight: '22px',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
    },
    '& span': {
      fontFamily: 'theme.typography.fontFamily',
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: 12,
      lineHeight: '17px',
      letterSpacing: 0.075,
      color: '#626262',
    },
  },
  dateStyle: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 12,
    color: '#98999C',
    marginRight: 15,
  },
  labelStyle: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 14,
    letterSpacing: '0.1px',
    lineHeight: '20px',
    padding: '5px 10px',
    wordBreak: 'break-all',
  },
  titleStyle: {
    flex: '1 1 auto',
    marginLeft: 8,
    alignSelf: 'flex-start',
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 16,
    color: '#080809',
  },
  iconStyle: {
    '&:hover': {
      borderRadius: '50%',
      backgroundColor: 'rgba(255, 255, 255, 0.5)',
    },
  },
  imgStyle: {
    height: 'auto',
    width: '185px',
    backgroundColor: 'white',
    borderRadius: 12,
    '& img': {
      height: 'auto',
      width: '185px',
      borderRadius: 8,
    },
    '& a': {
      fontFamily: theme.typography.fontFamily,
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: 14,
      lineHeight: '18px',
      letterSpacing: 0.0875,
      color: '#040405',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textDecoration: 'none',
    },
  },
}));

// function getFileSize(url: string) {
//   let fileSize = '';
//   const http = new XMLHttpRequest();
//   http.open('HEAD', url, false);
//   http.send(null);
//   if (http.status === 200) {
//     fileSize = http.getResponseHeader('content-length');
//   }
//   return fileSize;
// }

export const StarredMessage = (props: StarredMessProps) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [hidden, setHidden] = React.useState(false);
  const handleMouseEnter = () => {
    setHidden(true);
  };
  const handleMouseLeave = () => {
    setHidden(false);
  };
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const handleAction = async (action: string) => {
    if (action === 'unstar-message') {
      dispatch(updateStarredMess(props.id, 0));
      handleClose();
    }
  };

  const useStylesMedia = makeStyles(() => {
    return {
      root: {
        backgroundColor: props.received ? 'rgb(246, 247, 248)' : '#FFEB98',
        borderRadius: '6px',
        padding: '0 10px',
        marginTop: '5px',
      },
      loopIcon: {
        color: '#3f51b5',
        '&.selected': {
          color: '#0921a9',
        },
        '&:hover': {
          color: '#7986cb',
        },
      },
      playIcon: {
        color: 'black',
        '&:hover': {
          color: 'black',
        },
        fontSize: '25px',
      },
      replayIcon: {
        color: 'black',
        fontSize: '25px',
      },
      pauseIcon: {
        color: '#000',
        fontSize: '25px',
      },
      volumeIcon: {
        display: 'none',
      },
      progressTime: {
        color: 'rgba(0, 0, 0, 0.54)',
        position: 'relative',
        bottom: '-1px',
      },
      mainSlider: {
        color: '#3f51b5',
        '& .MuiSlider-rail': {
          color: '#7986cb',
        },
        '& .MuiSlider-track': {
          color: '#3f51b5',
        },
        '& .MuiSlider-thumb': {
          color: '#2C87D8',
        },
        marginLeft: '-5px',
      },
    };
  });

  return (
    <Flex
      flexDirection="column"
      position="relative"
      width="100%"
      pb="10px"
      style={{ borderBottom: '1px solid #E0E0E0' }}
    >
      <Flex alignItems="center" justifyContent="center" mt="10px">
        <Avatar
          alt={props.name}
          src={props.received ? firstChars(props.name) : firstChars(props.customer)}
          style={{ width: 28, height: 28 }}
        />
        {props.received ? (
          <div className={classes.titleStyle}>
            <span>{props.name}</span>
            <span style={{ color: '#A2A3A9' }}> &nbsp;&gt; </span>
            <span>you</span>
          </div>
        ) : (
          <div className={classes.titleStyle}>
            <span>You </span>
            <span style={{ color: '#A2A3A9' }}> &gt;&nbsp;</span>
            <span>{props.name}</span>
          </div>
        )}
        <Typography className={classes.dateStyle}>{format(props.date, 'd/M/y')}</Typography>
      </Flex>
      <MessageBox
        style={{
          backgroundColor: props.received || props.type === MessageType.Voice ? 'white' : '#FFEB98',
          boxShadow: props.type === MessageType.Media ? '0px' : '0px 1px 3px rgba(0, 0, 0, 0.095389)',
        }}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        data-testid="message-box"
      >
        {props.type === MessageType.Media && props.file_mime_type !== 'video/mp4' && (
          <Flex flexDirection="column" className={classes.imgStyle}>
            <img alt="mess-img" src={props.filePath} style={{}} />
            {/* <Flex flexDirection="column" style={{ minWidth: 0 }}>
              <a href={props.img}>{props.img.replace(/^.*[\\/]/, '').replace(/^(.+?)(?:\.[^.]*)?$/, '$1')}</a>
            </Flex> */}
          </Flex>
        )}
        {props.type === MessageType.Media && props.file_mime_type === 'video/mp4' && (
          <CardMedia src={props.filePath} component="video" image={props.filePath} controls />
        )}

        {props.type === MessageType.Text && <Typography className={classes.labelStyle}>{props.label}</Typography>}
        {props.type === MessageType.Document && props.fileName && (
          <Flex flexDirection="row" alignItems="center" className={classes.file}>
            {isExcelFile(props.fileName) ? (
              <img src="/static/Xlsx.svg" alt="Xlsx" style={{ marginRight: '15px' }} />
            ) : isWordFile(props.fileName) ? (
              <img src="/static/doc.svg" alt="doc" style={{ width: '45px', marginRight: '5px' }} />
            ) : isZipFile(props.fileName) ? (
              <img src="/static/zip.svg" alt="zip" style={{ width: '40px', marginRight: '15px' }} />
            ) : isPdfFile(props.fileName) ? (
              <img src="/static/Pdf.svg" alt="pdf" style={{ marginRight: '15px' }} />
            ) : isPowerPointFile(props.fileName) ? (
              <img src="/static/nolabel.svg" alt="Xlsx" style={{ width: '40px', marginRight: '10px' }} />
            ) : (
              <img src="/static/nolabel.svg" alt="pdf" style={{ width: '40px', marginRight: '10px' }} />
            )}
            <Flex flexDirection="column" style={{ minWidth: 0 }}>
              <a href={props.filePath} target="_blank" rel="noopener noreferrer">
                {props.fileName}
              </a>
              <span>{props.fileSize}</span>
            </Flex>
          </Flex>
        )}

        {props.type === MessageType.Voice && (
          <AudioPlayer
            src={props.filePath || ''}
            elevation={0}
            width="280px"
            height="50px"
            useStyles={useStylesMedia}
            time="double"
            spacing={1}
          />
        )}

        {hidden && (
          <PointerFlex style={{ position: 'absolute', top: 0, right: 0 }} data-testid="hidden-box">
            <Box
              aria-controls="customized-menu"
              aria-haspopup="true"
              onClick={handleClick}
              color="#2E2E32"
              borderRadius="50%"
              className={classes.iconStyle}
              display="flex"
              alignItems="center"
            >
              <ExpandMoreIcon />
            </Box>
            <IbMenu id="filter-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
              <IbMenuItem onClick={() => handleAction('download')}>Download</IbMenuItem>
              <IbMenuItem onClick={() => handleAction('reply')}>Reply</IbMenuItem>
              <IbMenuItem onClick={() => handleAction('forward-message')}>Forward message</IbMenuItem>
              <IbMenuItem onClick={() => handleAction('unstar-message')}>Unstar message</IbMenuItem>
              {/* <IbMenuItem onClick={() => handleAction('delete-message')}>Delete message</IbMenuItem> */}
            </IbMenu>
          </PointerFlex>
        )}
      </MessageBox>
      <Box className={classes.fontstyle} style={{ marginLeft: 'auto' }}>
        {props.received ? <span>Read by &nbsp;&nbsp;</span> : <span>Sent from &nbsp;&nbsp;</span>}
        <span style={{ color: '#080809' }}>{props.customer}</span>
      </Box>
    </Flex>
  );
};

export default StarredMessage;
