import { Box } from '@material-ui/core';
import React from 'react';

import { Nav } from '../../common/Nav';
import { ChatPlatform } from '../../../redux/services/chat';
import { ChatList } from '../Container/ChatList';
import { ChatListItemProps } from '../ChatList/ChatListItem';

export default {
  title: 'Chat/ArchivedChat',
  // component: ArchivedChats,
};
const data: ChatListItemProps[] = [
  {
    id: '1',
    name: 'Michael N.',
    unreadCount: 0,
    lastMessage: 'Hi, there.',
    description: '5 participants',
    status: 'assigned',
    image: 'https://via.placeholder.com/150?text=MN',
    platform: ChatPlatform.WhatsApp,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
  },
  {
    id: '2',
    name: 'Kate Halligan',
    lastMessage: 'Ok. Thanks!',
    description: '+85251115111',
    unreadCount: 3,
    status: 'assigned',
    image: 'https://via.placeholder.com/150?text=KH',
    platform: ChatPlatform.WhatsApp,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
  },
  {
    id: '3',
    name: 'Julian Gruber',
    description: 'GT Group fb',
    lastMessage: 'That’s great! Thank you so much!',
    status: 'assigned',
    image: 'https://via.placeholder.com/150?text=JG',
    platform: ChatPlatform.Facebook,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
    unreadCount: 0,
  },
  {
    id: '4',
    name: 'Johnny Yeung ',
    description: 'KB Shop',
    lastMessage: 'Product List_2938910.pdf',
    status: 'unassigned',
    image: 'https://via.placeholder.com/150?text=JG',
    platform: ChatPlatform.Line,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
    unreadCount: 0,
  },
  {
    id: '5',
    name: 'Kate Tsui',
    description: '+852  5377 8888',
    lastMessage: 'Ok. Thanks!',
    unreadCount: 1,
    status: 'warning',
    image: 'https://via.placeholder.com/150?text=JG',
    platform: ChatPlatform.Line,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
  },
  {
    id: '6',
    name: 'William Leung',
    description: 'GT Group fb',
    lastMessage: 'newproduct_8292681315.jpg',
    status: 'assigned',
    image: 'https://via.placeholder.com/150?text=WL',
    platform: ChatPlatform.Facebook,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
    unreadCount: 0,
  },
  {
    id: '11',
    name: 'Michael N.',
    lastMessage: 'Hi, there.',
    description: 'Ok. Thanks!',
    status: 'unassigned',
    image: 'https://via.placeholder.com/150?text=MN',
    platform: ChatPlatform.WhatsApp,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
    unreadCount: 0,
  },
  {
    id: '12',
    name: 'Kate Halligan',
    lastMessage: 'newproduct_8292681315 .jpg',
    description: '+85251115111',
    unreadCount: 3,
    status: 'assigned',
    image: 'https://via.placeholder.com/150?text=KH',
    platform: ChatPlatform.WhatsApp,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
  },
  {
    id: '13',
    name: 'Julian Gruber',
    description: 'GT Group fb',
    lastMessage: 'That’s great! Thank you so much!',
    status: 'assigned',
    image: 'https://via.placeholder.com/150?text=JG',
    platform: ChatPlatform.Facebook,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
    unreadCount: 0,
  },
  {
    id: '14',
    name: 'Johnny Yeung ',
    description: 'KB Shop',
    lastMessage: 'Product List_2938910.pdf',
    status: 'unassigned',
    image: 'https://via.placeholder.com/150?text=JG',
    platform: ChatPlatform.Line,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
    unreadCount: 0,
  },
  {
    id: '15',
    name: 'Kate Tsui',
    description: '+852  5377 8888',
    lastMessage: 'Ok. Thanks!',
    unreadCount: 1,
    status: 'warning',
    image: 'https://via.placeholder.com/150?text=JG',
    platform: ChatPlatform.Line,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
  },
  {
    id: '16',
    name: 'William Leung',
    description: 'GT Group fb',
    lastMessage: 'newproduct_8292681315.jpg',
    status: 'assigned',
    image: 'https://via.placeholder.com/150?text=WL',
    platform: ChatPlatform.Facebook,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    archived: true,
    unreadCount: 0,
  },
];

export const Default = () => {
  return (
    <Box width={345}>
      <Nav backButton title="Archived Chats" />
      <ChatList data={data} height="calc(100vh - 70px)" chatIds={[]} />
    </Box>
  );
};
