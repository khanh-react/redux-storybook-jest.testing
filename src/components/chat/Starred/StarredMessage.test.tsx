import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';
import { configureStore } from '../../../redux/store';
import * as Stories from './StarredMessage.stories';

const store = configureStore();
test('fires native mouseEnter/mouseLeave events', () => {
  const utils = render(
    <Provider store={store}>
      <Stories.Primary />
    </Provider>,
  );
  const messageBox = utils.getAllByTestId('message-box');
  fireEvent.mouseEnter(messageBox[0]);
  const hiddenBox = utils.getAllByTestId('hidden-box');
  expect(hiddenBox[0]).toBeInTheDocument();
  fireEvent.mouseLeave(messageBox[0]);
  expect(hiddenBox[0]).not.toBeInTheDocument();
});
