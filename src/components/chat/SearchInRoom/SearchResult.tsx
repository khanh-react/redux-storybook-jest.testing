import React from 'react';
import { format } from 'date-fns';
import { Flex } from '../../common/Flex';
import HighlightedMessage from './HighlightedMessage';

const SearchResult = (props: { name: string; date: Date; message: string; searchTerm: string }) => {
  return (
    <Flex flexDirection="column" p="10px 12px 20px 17px" borderBottom="1px solid #ECEFF1">
      <Flex justifyContent="space-between">
        <Flex fontWeight={600} fontSize={16}>
          {props.name}
        </Flex>
        <Flex fontSize={12} color="#838388">
          {format(props.date, 'iiii')}
        </Flex>
      </Flex>
      <Flex>
        <HighlightedMessage text={props.message} highlight={props.searchTerm} />
      </Flex>
    </Flex>
  );
};

export default SearchResult;
