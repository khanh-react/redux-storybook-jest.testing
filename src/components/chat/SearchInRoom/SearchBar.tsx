import React from 'react';
import { BareInput } from '../../common/BareInput';
import Icon from '../../common/Icon';
import { Flex } from '../../common/Flex';

type PropsInput = {
  textSearch: string;
  setTextSearch: (text: string) => void;
};
const SearchBar = ({ textSearch, setTextSearch }: PropsInput) => {
  return (
    <Flex flex={1} minHeight="36px" bgcolor="#F6F6F6" borderRadius={18} alignItems="center">
      <Flex flex={1}>
        <Flex mx="15px" style={{ cursor: 'pointer' }}>
          <Icon name="search" color="black" />
        </Flex>
        <Flex flex={1} mr="15px">
          <BareInput fullWidth onChange={(e) => setTextSearch(e.target.value)} value={textSearch} />
        </Flex>
      </Flex>
    </Flex>
  );
};

export default SearchBar;
