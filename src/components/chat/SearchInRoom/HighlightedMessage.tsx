import React from 'react';
import { useTheme } from '@material-ui/core';
import { useStyles } from './Styles';

const HighlightedMessage = ({ text = '', highlight = '' }) => {
  const parts = (text ?? '').split(new RegExp(`(${highlight})`, 'gi'));
  const classes = useStyles();
  const theme = useTheme();
  return (
    <span className={classes.highlightText}>
      {parts.map((part, i) => (
        <span
          /* eslint-disable */
          key={`${i}:${part}`}
          /* eslint-enable */
          style={
            part.toLowerCase() === highlight.toLowerCase()
              ? {
                  fontFamily: theme.typography.fontFamily,
                  fontStyle: 'normal',
                  fontWeight: 600,
                  fontSize: 14,
                  lineHeight: '20px',
                  backgroundColor: '#FFEB98',
                  color: '#0F0F10',
                }
              : {}
          }
        >
          {part}
        </span>
      ))}
    </span>
  );
};

export default HighlightedMessage;
