import React from 'react';
import { useTheme } from '@material-ui/core';
import { useStyles } from './Styles';

const HighlightedTitle = ({ text = '', highlight = '' }) => {
  const parts = (text ?? '').split(new RegExp(`(${highlight})`, 'gi'));
  const classes = useStyles();
  const theme = useTheme();
  return (
    <span className={classes.nameStyle}>
      {parts.map((part, i) => (
        <span
          /* eslint-disable */
          key={`${i}:${part}`}
          /* eslint-enable */
          style={
            part.toLowerCase() === highlight.toLowerCase()
              ? {
                  fontFamily: theme.typography.fontFamily,
                  fontStyle: 'normal',
                  fontWeight: 600,
                  fontSize: 16,
                  lineHeight: '22px',
                  backgroundColor: '#FFEB98',
                  color: '#0F0F10',
                }
              : {}
          }
        >
          {part}
        </span>
      ))}
    </span>
  );
};

export default HighlightedTitle;
