import { Box, makeStyles, Typography } from '@material-ui/core';
import React from 'react';

import { Flex } from '../../common/Flex';
import MainNav from '../LeftNav/MainNav';
import SearchBar from '../FilterChat/SearchBar';
import { ChatSearchResult } from './SearchMessage';

export default {
  title: 'Chat/SearchMessage',
  component: ChatSearchResult,
};

const useStyles = makeStyles((theme) => ({
  navBar: {
    color: '#98999F',
    '&:hover': {
      borderBottom: '2px solid #FFDB36',
      color: '#FFDB36',
    },
  },
  cursorPointer: {
    cursor: 'pointer',
  },
  titleStyle: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 15,
    lineHeight: '19px',
    letterSpacing: 0.09375,
    textTransform: 'uppercase',
    color: '#BBBBC5',
    margin: '21px 17px 10px',
  },
}));

const data = [
  {
    id: '1',
    title: 'MESSAGE',
    items: [
      {
        id: '1',
        name: 'Michael N.',
        lastMessage: 'Hi, there.',
        highlightText: 'there',
        description: '5 participants',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '2',
        name: 'Kate Halligan',
        lastMessage: 'Ok. Thanks!',
        highlightText: 'Ok',
        description: '+85251115111',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '3',
        name: 'Julian Gruber',
        description: 'GT Group fb',
        lastMessage: 'That’s great! Thank you so much!',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '4',
        name: 'Johnny Yeung ',
        description: 'KB Shop',
        lastMessage: 'Product List_2938910.pdf',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '5',
        name: 'Kate Tsui',
        description: '+852  5377 8888',
        lastMessage: 'Ok. Thanks!',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '6',
        name: 'William Leung',
        description: 'GT Group fb',
        lastMessage: 'newproduct_8292681315.jpg',
        highlightText: 'newproduct',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
    ],
  },
  {
    id: '2',
    title: 'CHAT',
    items: [
      {
        id: '11',
        name: 'Michael N.',
        lastMessage: 'Hi, there.',
        description: 'Ok. Thanks!',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '12',
        name: 'Kate Halligan',
        lastMessage: 'newproduct_8292681315 .jpg',
        description: '+85251115111',
        highlightText: 'Kate',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '13',
        name: 'Julian Gruber',
        description: 'GT Group fb',
        lastMessage: 'That’s great! Thank you so much!',
        highlightText: 'great',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '14',
        name: 'Johnny Yeung ',
        description: 'KB Shop',
        lastMessage: 'Product List_2938910.pdf',
        highlightText: 'Yeung',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '15',
        name: 'Kate Tsui',
        description: '+852  5377 8888',
        lastMessage: 'Ok. Thanks!',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
      {
        id: '16',
        name: 'William Leung',
        description: 'GT Group fb',
        lastMessage: 'newproduct_8292681315.jpg',
        updatedAt: new Date('2020-09-04T15:40:57.640Z'),
      },
    ],
  },
];

export const Primary = () => {
  const classes = useStyles();
  return (
    <Box width={345} height="80vh">
      <MainNav
        caseFilter="string"
        setCaseFilter={() => {
          return null;
        }}
        listUnread={10}
        listUnassign={10}
        closedSearch={() => null}
      />
      <SearchBar handleText={() => null} textSearch="hihi" />
      <Flex flexDirection="column" height="100%" overflow="scroll">
        {data.map((props) => {
          return (
            <div key={props.id}>
              <Typography className={classes.titleStyle}>{props.title}</Typography>
              {props.items.map((item) => (
                <ChatSearchResult
                  key={item.id}
                  active={item.id === '2'}
                  id={item.id}
                  name={item.name}
                  description={item.description}
                  lastMessage={item.lastMessage}
                  highlightText={item.highlightText}
                  updatedAt={item.updatedAt}
                />
              ))}
            </div>
          );
        })}
      </Flex>
    </Box>
  );
};
