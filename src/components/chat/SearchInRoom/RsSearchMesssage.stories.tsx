import React from 'react';

import { Flex } from '../../common/Flex';
import RsSearchMessage from '../Container/RsSearchMessage';

export default {
  title: 'Chat/RsSearchMessage',
  component: RsSearchMessage,
};

export const Primary = () => {
  return (
    <Flex>
      <RsSearchMessage onSelectSearchMessage={() => null} />
    </Flex>
  );
};
