import { Box, Typography } from '@material-ui/core';
import { format } from 'date-fns';
import React from 'react';
import HighlightedMessage from './HighlightedMessage';
import HighlightedTitle from './HighlightedTitle';
import { useStyles } from './Styles';

export type ChatSearchResultProps = {
  id: string;
  name: string;
  description?: string;
  lastMessage: string;
  updatedAt?: Date;
  active?: boolean;
  highlightText?: string;
};

export const ChatSearchResult = (props: ChatSearchResultProps) => {
  const styles = useStyles();
  return (
    <Box
      display="flex"
      flexDirection="row"
      alignItems="stretch"
      className={`${props.active ? styles.active : ''}`}
      px="15px"
      boxShadow="0px 1px 0px #ECEFF1"
      minHeight="62px"
    >
      <Box flex={1} display="flex" flexDirection="column" alignItems="stretch" justifyContent="flex-start">
        <Box display="flex" alignItems="flex-start" justifyContent="space-between" mt="10px">
          <HighlightedTitle text={props.name} highlight={props.highlightText} />
          {props.updatedAt && <Typography variant="subtitle1">{format(props.updatedAt, 'iiii')}</Typography>}
        </Box>
        <Box display="flex" alignItems="center">
          <Box flex={1} display="flex">
            <HighlightedMessage text={props.lastMessage} highlight={props.highlightText} />
          </Box>
        </Box>
        <Box display="flex" justifyContent="space-between" alignItems="flex-start" mb="10px">
          <Typography variant="subtitle1">{props.description}</Typography>
        </Box>
      </Box>
    </Box>
  );
};
