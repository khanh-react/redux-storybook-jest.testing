import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  active: {
    backgroundColor: '#FDF3E4',
  },
  highlightText: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 14,
    lineHeight: '20px',
    letterSpacing: 0.1,
    color: '#77777A',
  },
  nameStyle: {
    fontFamily: theme.typography.fontFamily,
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 16,
    lineHeight: '22px',
    color: '#0F0F10',
  },
}));
