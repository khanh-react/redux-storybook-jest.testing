import React, { useCallback, useEffect } from 'react';
import Picker, { IEmojiData, SKIN_TONE_NEUTRAL } from 'emoji-picker-react';
import { ReactMic } from '@cleandersonlobo/react-mic';
import { IconButton } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import CloseIcon from '@material-ui/icons/Close';
import styled from 'styled-components';
import { validateFileAudio, validateFileImage, validateFileVideo } from '../../../utils/validateFileName';
import { BareInput } from '../../common/BareInput';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';
import { Touchable } from '../../common/Touchable';
import useOnClickOutside from '../../common/useOnClickOutside';
import { MessageType } from '../../../redux/services/chat';
import {
  markReadChat,
  ResetMessageAction,
  sendDocumentTempMessage,
  sendMessage,
  updateChatInput,
  updateEmojiInput,
} from '../../../redux/action/chat';
import { AppState } from '../../../redux/store';

const StyledEmoji = styled.div`
  .emoji-picker-react {
    position: absolute;
    bottom: 100%;
    left: 0px;
    z-index: 90;
  }
`;

const RecoderStyle = styled.div`
  display: flex;
  align-items: center;
  canvas {
    display: none;
  }
  .cricle {
    width: 10px;
    height: 10px;
    background: red;
    border-radius: 50%;
    margin-right: 10px;
  }
  .time {
    margin-right: 5px;
    font-size: 18px;
  }
  .time .positionTop {
    position: relative;
    top: -1px;
  }
`;

type Props = {
  onClickUploadIcon: () => void;
};

const Composer = ({ onClickUploadIcon }: Props) => {
  const emojoRef = React.useRef(null);
  const inputFile = React.useRef<HTMLInputElement>(null);
  const dispatch = useDispatch();
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const currentChat = useSelector((s: AppState) => (currentChatId ? s.chat.chats[currentChatId] : undefined));
  const [flagEmojiTab, setFlagEmojiTab] = React.useState(false);
  const text = useSelector((s: AppState) => (s.chat.currentChatId && s.chat.inputs?.[s.chat.currentChatId]) ?? '');
  const userId = useSelector((s: AppState) => s.auth.userId);
  const [record, setRecord] = React.useState(false);
  const nameUser = useSelector((s: AppState) => s.subdomain.subdomainConfig?.name);
  const [minutes, setMinutes] = React.useState('00');
  const [second, setsecond] = React.useState('00');
  const [tempSecond, setTempSecond] = React.useState(0);
  const [tempMinutes, setTempMinutes] = React.useState(0);
  const [flagRecordTab, setFlagRecordTab] = React.useState(false);

  useEffect(() => {
    setFlagRecordTab(false);
  }, [currentChatId]);
  const setMinutesfunc = useCallback(() => {
    setTempMinutes((state) => state + 1);

    if (tempMinutes < 10) {
      setMinutes(`0${tempMinutes}`);
    } else {
      setMinutes(`${tempMinutes}`);
    }
  }, [tempMinutes]);

  const setSecondfunc = useCallback(() => {
    setTempSecond((state) => state + 1);
    if (tempSecond < 10) {
      setsecond(`0${tempSecond}`);
    } else if (tempSecond === 60) {
      setsecond(`00`);
      setTempSecond(0);
    } else {
      setsecond(`${tempSecond}`);
    }
  }, [tempSecond]);

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let functionMinutes: any = null;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let functionSecond: any = null;
    if (record) {
      functionMinutes = setInterval(setMinutesfunc, 60000);
      functionSecond = setInterval(setSecondfunc, 1000);
    } else if (!record && tempSecond !== 0 && tempMinutes !== 0) {
      clearInterval(functionMinutes);
      clearInterval(functionSecond);
    }

    return () => {
      clearInterval(functionMinutes);
      clearInterval(functionSecond);
    };
  }, [record, tempSecond, setMinutesfunc, setSecondfunc, tempMinutes]);

  const startRecording = () => {
    setRecord(!record);
  };

  const stopRecording = () => {
    setRecord(false);
    setTempSecond(0);
    setTempMinutes(0);
    setsecond('00');
    setMinutes('00');
  };
  // eslint-disable-next-line
  const onStop = async (recordedBlob: any) => {
    try {
      setRecord(false);
      const blob = await fetch(recordedBlob.blobURL).then((r) => r.blob());
      const fileName = `${recordedBlob.startTime}`;
      const newFile = new File([blob], `${fileName}.mp3`, { type: 'audio/mpeg' });
      if (currentChatId && userId && newFile && newFile.size > 5000) {
        dispatch(sendDocumentTempMessage(currentChatId, newFile, nameUser ?? '', 'Voice', ''));
        dispatch(ResetMessageAction());
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  useOnClickOutside(emojoRef, () => {
    setFlagEmojiTab(false);
  });
  const setText = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      if (currentChatId) dispatch(updateChatInput(currentChatId, e.currentTarget.value));
    },
    [currentChatId, dispatch],
  );
  const send = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      if (currentChat) dispatch(sendMessage({ type: MessageType.Text }));
    },
    [currentChat, dispatch],
  );
  const handleClickEmoji = () => {
    setFlagEmojiTab(true);
  };

  // handle click emotion icon
  const onEmojiClick = useCallback(
    (e: MouseEvent, emojiObject: IEmojiData) => {
      if (currentChatId) dispatch(updateEmojiInput(currentChatId, emojiObject.emoji));
    },
    [currentChatId, dispatch],
  );

  const onClickAttachIcon = () => {
    inputFile.current && inputFile.current.click();
  };

  const onAddFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const inputCurrent = document.getElementById('file-input-attach') as HTMLInputElement;
    const inputEmpty = document.createElement('input');
    inputEmpty.type = 'file';
    if (e.target.files && currentChatId && userId) {
      const fileSelect = e.target.files[0];
      if (!fileSelect) {
        return;
      }
      if (validateFileImage(fileSelect)) {
        dispatch(sendDocumentTempMessage(currentChatId, fileSelect, nameUser ?? '', 'Media', ''));
      } else if (validateFileAudio(fileSelect)) {
        dispatch(sendDocumentTempMessage(currentChatId, fileSelect, nameUser ?? '', 'Voice', ''));
      } else if (validateFileVideo(fileSelect)) {
        dispatch(sendDocumentTempMessage(currentChatId, fileSelect, nameUser ?? '', 'Media', ''));
      } else {
        dispatch(sendDocumentTempMessage(currentChatId, fileSelect, nameUser ?? '', 'Document', ''));
      }
      if (inputCurrent) {
        inputCurrent.files = inputEmpty.files;
      }
    }
    dispatch(ResetMessageAction());
  };
  const clickInput = () => {
    currentChatId && dispatch(markReadChat(currentChatId));
  };

  return (
    <form onSubmit={send}>
      <Flex minHeight={68} alignItems="center" flexShrink={0}>
        <PointerFlex mr="5px" ml={1}>
          <Icon
            name="mic"
            onMouseDown={() => {
              if (flagRecordTab) {
                startRecording();
              } else {
                setFlagRecordTab(true);
              }
            }}
            onMouseUp={stopRecording}
            onClick={() => setFlagRecordTab(true)}
            style={{
              border: record ? '1px solid red' : 'none',
              borderRadius: record ? '50%' : 0,
              marginRight: record ? '10px' : 0,
              position: 'relative',
              top: '-1px',
            }}
          />
          {flagRecordTab && (
            <div>
              <RecoderStyle>
                <ReactMic
                  record={record}
                  className="sound-wave"
                  onStop={onStop}
                  strokeColor="transparent"
                  backgroundColor="transparent"
                  mimeType="audio/mp3"
                />
                <div className="cricle" />
                <div className="time">
                  {minutes} <span className="positionTop">:</span> {second}
                </div>
                <IconButton onClick={() => setFlagRecordTab(false)} size="small">
                  <CloseIcon color="error" fontSize="small" />
                </IconButton>
              </RecoderStyle>
            </div>
          )}
        </PointerFlex>
        <Flex flex={1} minHeight="36px" bgcolor="#F6F6F6" borderRadius={18} alignItems="center">
          <Flex flex={1} ml="18px">
            <BareInput fullWidth value={text} onChange={setText} disabled={!currentChatId} onClick={clickInput} />
          </Flex>
          <PointerFlex onClick={handleClickEmoji} position="relative">
            <Icon name="emoji" />
            {flagEmojiTab && (
              <StyledEmoji ref={emojoRef}>
                <Picker
                  disableAutoFocus
                  disableSearchBar
                  disableSkinTonePicker
                  onEmojiClick={onEmojiClick}
                  skinTone={SKIN_TONE_NEUTRAL}
                />
              </StyledEmoji>
            )}
          </PointerFlex>
          <PointerFlex onClick={onClickUploadIcon}>
            <Icon name="image" />
          </PointerFlex>
          <PointerFlex onClick={onClickAttachIcon}>
            <Icon name="attach" />
            <input
              type="file"
              id="file-input-attach"
              ref={inputFile}
              style={{ display: 'none' }}
              onChange={onAddFile}
            />
          </PointerFlex>
        </Flex>
        <PointerFlex ml="5px" mr={1}>
          <Touchable type="submit" disabled={!currentChat}>
            <Icon name="send" />
          </Touchable>
        </PointerFlex>
      </Flex>
    </form>
  );
};

export default Composer;
