/* eslint-disable react/jsx-one-expression-per-line */
import { addHours } from 'date-fns';
import React from 'react';
import { Flex } from '../../common/Flex';
import { MessageStatus, MessageType } from '../../../redux/services/chat';
import ChatMessage, { ChatMessageProps } from './ChatMessage';

export default {
  title: 'Chat/ChatRoom',
  //   component: ChatRoomFilters,
};

const MESSAGES: ChatMessageProps[] = [
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Text,
    text: 'Hello, there.',
    createdAt: addHours(new Date(), -4),
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Text,
    text: 'I’d like to buy 2 more shoes for my family, Can i get a discount?',
    createdAt: addHours(new Date(), -4),
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Text,
    outgoing: true,
    status: MessageStatus.Sent,
    text: 'Hi,',
    createdAt: addHours(new Date(), -4),
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Text,
    outgoing: true,
    status: MessageStatus.Sent,
    text:
      'Hi, Kate, thank you for your interest in our products. May I know which model you are interested in? Then, I will check for you if there are any promotions.',
    createdAt: addHours(new Date(), -4),
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Media,
    text: 'Nike air-270',
    thumbnailUrl: 'https://via.placeholder.com/140x185.png',
    createdAt: addHours(new Date(), -4),
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Text,
    outgoing: true,
    text: 'Thanks, i will check it for you now.',
    createdAt: new Date(),
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Media,
    outgoing: true,
    text: 'Product List_29389.pdf',
    thumbnailUrl: '/static/Pdf.svg',
    createdAt: new Date(),
    size: '11.67kb',
    download: 'done',
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Media,
    outgoing: true,
    text: 'Product List_293892.pdf',
    thumbnailUrl: '/static/Pdf.svg',
    createdAt: new Date(),
    size: '11.67kb',
    download: 'ongoing',
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Media,
    outgoing: true,
    text: 'Product List_29389.pdf',
    thumbnailUrl: '/static/Pdf.svg',
    createdAt: new Date(),
    size: '11.67kb',
    download: 'done',
    forwarded: true,
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Media,
    outgoing: true,
    text: 'Product List_29389.pdf',
    thumbnailUrl: '/static/Pdf.svg',
    createdAt: new Date(),
    size: '11.67kb',
    download: 'done',
    failed: true,
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Media,
    outgoing: true,
    text: 'Product List_29389.pdf',
    thumbnailUrl: '/static/Pdf.svg',
    createdAt: new Date(),
    size: '11.67kb',
    download: 'done',
  },
  {
    revoke: 1,
    id: '123',
    sender: 'username',
    type: MessageType.Media,
    outgoing: true,
    text: 'Product List_293892.pdf',
    thumbnailUrl: '/static/Pdf.svg',
    createdAt: new Date(),
    size: '11.67kb',
    download: 'ongoing',
  },
];

export const Primary = () => {
  return (
    <Flex width={676} bgcolor="#222">
      {[...MESSAGES, ...MESSAGES, ...MESSAGES, ...MESSAGES].map((props, idx) => (
        // eslint-disable-next-line react/no-array-index-key
        <ChatMessage {...props} key={idx} />
      ))}
    </Flex>
  );
};
