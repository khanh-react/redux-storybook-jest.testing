/* eslint-disable no-nested-ternary */
import React from 'react';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import styled from 'styled-components';
import { isExcelFile, isPdfFile, isPowerPointFile, isWordFile, isZipFile } from '../../../utils/validateFileName';
import { MessageType } from '../../../redux/services/chat';
import { Flex } from '../../common/Flex';

const MessageReply = styled.div`
  position: absolute;
  bottom: 69px;
  left: 50%;
  background-color: #fff;
  padding: 10px 9px 10px 13px;
  border-radius: 6px;
  transform: translateX(-50%);
  border-left: 3px solid #ffeb98;
  overflow: hidden;
  text-overflow: ellipsis;
  width: calc(100% - 40px);
  box-sizing: border-box;
`;

type onReplyMessage = {
  user: string;
  text?: string;
  messageType: MessageType;
  imageUrl?: string;
  fileName?: string;
  onClickClosed: () => void;
};

const ReplyMessage = ({ user, text, messageType, imageUrl, fileName, onClickClosed }: onReplyMessage) => {
  return (
    <MessageReply>
      <Flex position="absolute" top={5} right={5}>
        <IconButton onClick={onClickClosed} size="small">
          <CloseIcon />
        </IconButton>
      </Flex>

      <Flex fontSize={13} fontWeight="500">
        {user}
      </Flex>
      {messageType === MessageType.Text && <Flex fontSize={13}>{text}</Flex>}
      {messageType === MessageType.Media && (
        <Flex fontSize={13} width={50} height={50} justifyContent="center" alignItems="center" marginTop="5px">
          <img
            src={imageUrl}
            alt="reply"
            style={{ objectFit: 'cover', width: '100%', borderRadius: 3, maxHeight: '50px' }}
          />
        </Flex>
      )}
      {messageType === MessageType.Voice && (
        <Flex fontSize={13} width={50} height={50} justifyContent="center" alignItems="center" marginTop="5px">
          <img
            src="/static/audio-file.svg"
            alt="reply"
            style={{ objectFit: 'cover', width: '100%', borderRadius: 3 }}
          />
        </Flex>
      )}
      {messageType === MessageType.Document && fileName && (
        <Flex fontSize={13} width={50} height={50} justifyContent="center" alignItems="center" marginTop="5px">
          {isExcelFile(fileName) ? (
            <img src="/static/Xlsx.svg" alt="Xlsx" />
          ) : isWordFile(fileName) ? (
            <img src="/static/doc.svg" alt="doc" style={{ width: '45px' }} />
          ) : isZipFile(fileName) ? (
            <img src="/static/zip.svg" alt="zip" style={{ width: '40px' }} />
          ) : isPdfFile(fileName) ? (
            <img src="/static/Pdf.svg" alt="pdf" />
          ) : isPowerPointFile(fileName) ? (
            <img src="/static/nolabel.svg" alt="Xlsx" style={{ width: '40px', marginRight: '10px' }} />
          ) : (
            <img src="/static/nolabel.svg" alt="pdf" style={{ width: '40px', marginRight: '10px' }} />
          )}
        </Flex>
      )}
    </MessageReply>
  );
};

export default ReplyMessage;
