/* eslint-disable no-nested-ternary */
import React, { useCallback, useEffect } from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { format, formatDistanceToNow } from 'date-fns';
import AudioPlayer from 'material-ui-audio-player';
import { BoxProps, CardMedia, makeStyles } from '@material-ui/core';
import styled, { css, keyframes } from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import Box from '@material-ui/core/Box/Box';
import LinearProgress from '@material-ui/core/LinearProgress';
import { omit } from 'lodash';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';
import Carousel from '../PreviewImage/Carousel';
import { deleteMessage, Message, MessageStatus, MessageType } from '../../../redux/services/chat';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';
import { readRecord } from '../../../utils/localStorageService';
import { isExcelFile, isPdfFile, isPowerPointFile, isWordFile, isZipFile } from '../../../utils/validateFileName';

import {
  deleteMessageAction,
  replyMessage,
  resendMessageAct,
  resendMessageDisconnectAct,
  updateStarredMess,
} from '../../../redux/action/chat';
import { AppState } from '../../../redux/store';
import { MESSAGE_STATUS_TO_ICON_NAME } from '../../../redux/type/chat/iconChat';

const WrapDefaultBubble = styled((props: BoxProps & { outgoing?: boolean }) => <Flex {...omit(props, ['outgoing'])} />)`
  margin-top: 13px;
  margin-bottom: 13px;
  & + .bubble-system {
    margin-top: 27px;
  }
`;

const WrapSystemBubble = styled((props: BoxProps & { outgoing?: boolean }) => <Flex {...omit(props, ['outgoing'])} />)`
  margin-top: 13px;
  margin-bottom: 13px;
  & + .bubble-default {
    margin-top: 27px;
  }
`;

const MessageBubble = styled((props: BoxProps & { type: MessageType; outgoing?: boolean; revoke?: number }) => (
  <Flex {...omit(props, ['type', 'outgoing', 'evoke'])} />
))`
  max-width: 500px;
  position: relative;
  ${(props) =>
    props.type === MessageType.Media &&
    props.outgoing &&
    `
          padding: 7px;
          border-radius: 12px;
          border-top-right-radius: 0px;
        `}
  ${(props) =>
    props.type === MessageType.Media &&
    !props.outgoing &&
    `
          padding: 7px;
          border-radius: 12px;
          border-top-left-radius: 0px;
        `}

        ${(props) =>
    props.type === MessageType.Voice &&
    props.outgoing &&
    `
                padding: 7px;
                border-radius: 12px;
                border-top-right-radius: 0px;
              `}
        ${(props) =>
    props.type === MessageType.Voice &&
    !props.outgoing &&
    `
                padding: 7px;
                border-radius: 12px;
                border-top-left-radius: 0px;
              `}

  ${(props) =>
    props.type === MessageType.Text &&
    props.outgoing &&
    `
      padding: 8px 18px;
      border-radius: 12px;
      border-top-right-radius: 0px;
        `}
  ${(props) =>
    props.type === MessageType.Text &&
    !props.outgoing &&
    `
      padding: 8px 18px;
      border-radius: 20px;
      border-top-left-radius: 0px;
        `}
        ${(props) =>
    props.type === MessageType.Event &&
    `
            padding: 8px 18px;
            border-radius: 20px;
              `}

  ${(props) =>
    props.type === MessageType.Document && props.outgoing
      ? `
              padding: 0px;
              border-radius: 12px;
              background-color: #FFEB98;
              border-top-right-radius: 0px;
            `
      : ``}
      ${(props) =>
    !(props.type in MessageType)
      ? `
      padding: 8px 18px;
                  border-radius: 12px;
                  border-top-right-radius: 0px;
                `
      : ``}
  ${(props) =>
    props.type === MessageType.Document && !props.outgoing
      ? `
              padding: 0px;
              border-radius: 12px;
              border-top-left-radius: 0px;
            `
      : ``}
  ${(props) =>
    props.outgoing
      ? `
          background-color: #FFEB98;
        `
      : `
          background-color: white;
        `};
  ${(props) =>
    props.revoke &&
    props.revoke > 0 &&
    `
    padding:8px 18px
              `};
`;

const StyledProgressBar = styled(LinearProgress)`
  width: 100%;
  height: 3px;
  margin: -3px 4px 0px 4px;
  border-bottom-left-radius: 50px 15px;
  border-bottom-right-radius: 50px 15px;
  .MuiLinearProgress-barColorPrimary {
    background: #0091ff;
  }
  background: #e3d28a;
`;
const StyledFileName = styled((props: BoxProps & { ongoing?: boolean }) => <Box {...omit(props, ['ongoing'])} />)`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  margin-top: 5px;
  font-weight: 600;
  font-size: 15px;
  color: black;
  line-height: 22px;
  & > a {
    ${(props) =>
      props.ongoing
        ? `
         text-decoration: none;
        `
        : `
         text-decoration: underline;
        `}
  }
`;
export interface ChatMessageProps {
  id: Message['id'];
  type: MessageType;
  outgoing?: boolean;
  file_name?: string;
  text?: string;
  thumbnailUrl?: string;
  createdAt: Date;
  status?: MessageStatus;
  size?: string;
  failed?: boolean;
  download?: 'done' | 'ongoing';
  forwarded?: boolean;
  sender: string;
  revoke: number;
  captionImage?: string;
  fileBig?: boolean;
  quotedMessage?: Message;
  contact?: {
    name: string;
    id: string;
  };
  file_mime_type?: string;
}

const ChatMessage = (props: ChatMessageProps) => {
  const alignItems = props.outgoing ? 'flex-end' : 'flex-start';
  const staredMessages = useSelector((state: AppState) => state.chat.starredMessages);
  const dispatch = useDispatch();
  const [hidden, setHidden] = React.useState(false);
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const [flagCarousel, setFlagCarousel] = React.useState<boolean>(false);
  const [listSlideImage, setListSlideImage] = React.useState('');
  const [valueUpload, setValueUpload] = React.useState(0);
  const [statusFailed, setStatusFailed] = React.useState(false);
  const timeLoading = useSelector((state: AppState) => state.chat.fileProgress);
  const [flagResend, setFlagResend] = React.useState(false);
  const subDomainId = useSelector((s: AppState) => (s.auth.subdomainId === undefined ? undefined : s.auth.subdomainId));
  const handleAction = async (action: string) => {
    if (action === 'start-message') {
      dispatch(updateStarredMess(props.id, 1));
    }
    if (action === 'un-start-message') {
      dispatch(updateStarredMess(props.id, 0));
    }

    if (action === 'delete-message') {
      dispatch(deleteMessageAction(props.id));
      deleteMessage(props.id);
    }
    if (action === 'reply') {
      dispatch(
        replyMessage({
          user: props.sender,
          text: props.text,
          message_id: props.id,
          messageType: props.type,
          imageUrl: props.thumbnailUrl,
          fileName: props.file_name,
        }),
      );
    }
    if (action === 'resend') {
      if (props.id.length < 20) {
        dispatch(resendMessageAct(props.id));
      } else {
        dispatch(resendMessageDisconnectAct(props.id));
      }
    }

    handleClose();
  };
  const useStyles = makeStyles(() => {
    return {
      root: {
        backgroundColor: !props.outgoing ? 'white' : '#FFEB98',
        borderRadius: '6px',
        padding: '0 10px',
      },
      loopIcon: {
        color: '#3f51b5',
        '&.selected': {
          color: '#0921a9',
        },
        '&:hover': {
          color: '#7986cb',
        },
      },
      playIcon: {
        color: 'black',
        '&:hover': {
          color: 'black',
        },
        fontSize: '25px',
      },
      replayIcon: {
        color: 'black',
        fontSize: '25px',
      },
      pauseIcon: {
        color: '#000',
        fontSize: '25px',
      },
      volumeIcon: {
        display: 'none',
      },
      progressTime: {
        color: 'rgba(0, 0, 0, 0.54)',
        position: 'relative',
        bottom: '-1px',
      },
      mainSlider: {
        color: '#3f51b5',
        '& .MuiSlider-rail': {
          color: '#7986cb',
        },
        '& .MuiSlider-track': {
          color: '#3f51b5',
        },
        '& .MuiSlider-thumb': {
          color: '#2C87D8',
        },
        marginLeft: '-5px',
      },
    };
  });

  const handleClickImage = (url: string) => {
    setListSlideImage(url);
    setFlagCarousel(true);
  };

  const runProgressDown = useCallback(() => {
    if (valueUpload < 90) {
      setValueUpload((state) => state + 15);
    }
  }, [valueUpload]);

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let functionInterval: any = null;
    if (timeLoading?.[props.id]) {
      const tempTime = timeLoading?.[props.id].time ?? 300;
      if (props.download === 'ongoing') {
        functionInterval = setInterval(runProgressDown, tempTime);
      } else {
        clearInterval(functionInterval);
      }
    }

    return () => {
      clearInterval(functionInterval);
    };
  }, [valueUpload, setValueUpload, runProgressDown, props.download, props.id, timeLoading]);

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let functionInterval: any = null;
    functionInterval = setTimeout(() => {
      setStatusFailed(true);
    }, 20000);
    return () => clearInterval(functionInterval);
  }, [setStatusFailed, props.captionImage, props.type]);

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let functionInterval: any = null;
    functionInterval = setTimeout(() => {
      setFlagResend(false);
    }, 15000);
    return () => clearInterval(functionInterval);
  }, [flagResend]);

  const pulse = keyframes`
    from {
      transform: rotate(0deg)
    }
  
    to {
      transform: rotate(360deg)
    }
  `;
  const rotate = css`
    ${pulse} 1.2s ease-in-out
  `;
  const WrapResendICon = styled.div`
    align-items: center;
    position: absolute;
    left: -35px;
    top: calc(50% - 13px);
    cursor: pointer;
    animation: ${() => (flagResend ? rotate : 'none')};
    animation-iteration-count: infinite;
    z-index: 2;
  `;

  return (
    <>
      {props.type !== MessageType.Event ? (
        <WrapDefaultBubble
          mx="32px"
          outgoing={props.outgoing}
          flexShrink={0}
          className="bubble-default"
          flexDirection={props.outgoing ? 'row-reverse' : 'row'}
          id={props.id}
        >
          {flagCarousel && <Carousel images={listSlideImage} onClose={() => setFlagCarousel(false)} />}
          <Flex
            alignItems={props.type === MessageType.Document ? 'stretch' : alignItems}
            flexDirection="column"
            position="relative"
            onMouseEnter={() => setHidden(true)}
            onMouseLeave={() => setHidden(false)}
          >
            <Flex justifyContent="flex-end">
              {/* {props.outgoing && props.failed && (
                <PointerFlex mr="18px">
                  <Icon name="reload" />
                </PointerFlex>
              )} */}
              <MessageBubble
                outgoing={props.outgoing}
                type={props.type}
                revoke={props.revoke}
                flexDirection="column"
                alignItems="flex-start"
                className="inner-message"
              >
                {props.quotedMessage && (
                  <Flex flexDirection="column" marginBottom="5px">
                    <Flex fontSize={14} fontStyle="italic" marginBottom="5px" color="#7F7E7C">
                      {props.outgoing && !props.quotedMessage.contact && `You replied to ${props.sender}`}
                      {props.outgoing &&
                        props.quotedMessage.contact &&
                        `You replied to ${props.quotedMessage.contact?.name}`}
                      {!props.outgoing &&
                        props.quotedMessage.contact &&
                        `${props.contact?.name} replied to ${props.contact?.name}`}
                      {!props.outgoing && !props.quotedMessage.contact && `${props.contact?.name} replied to You`}
                    </Flex>
                    <Flex
                      borderLeft={props.outgoing ? '4px solid #AE9629' : '4px solid #A2A0A8'}
                      padding="5px"
                      maxWidth="260px"
                    >
                      {props.quotedMessage.message_type === MessageType.Media && (
                        <Flex fontSize={13} width={50} justifyContent="center" alignItems="center">
                          <img
                            src={`${process.env.REACT_APP_API_URL}/media/${
                              props.quotedMessage.id
                            }?access-token=${readRecord('accessToken')}&subdomain-id=${subDomainId}`}
                            alt="reply"
                            style={{ objectFit: 'cover', width: '100%', borderRadius: 3, maxHeight: '50px' }}
                          />
                        </Flex>
                      )}

                      {props.quotedMessage.message_type === MessageType.Voice && (
                        <AudioPlayer
                          src={`${process.env.REACT_APP_API_URL}/media/${
                            props.quotedMessage.id
                          }?access-token=${readRecord('accessToken')}&subdomain-id=${subDomainId}`}
                          elevation={0}
                          width="260px"
                          height="50px"
                          useStyles={useStyles}
                          time="double"
                          spacing={1}
                        />
                      )}

                      {props.quotedMessage.message_type === MessageType.Text ||
                      props.quotedMessage.message_type === MessageType.Media ? (
                        <Flex color={props.outgoing ? '#AE9629' : '#A2A0A8'} fontSize={14}>
                          {props.quotedMessage.text}
                        </Flex>
                      ) : null}
                      {props.quotedMessage.message_type === MessageType.Document && props.quotedMessage.file_name && (
                        <Flex alignItems="center" width="100%">
                          <Flex
                            fontSize={13}
                            width={50}
                            height={50}
                            justifyContent="center"
                            alignItems="center"
                            marginTop="5px"
                          >
                            {isExcelFile(props.quotedMessage.file_name) ? (
                              <img src="/static/Xlsx.svg" alt="Xlsx" />
                            ) : isWordFile(props.quotedMessage.file_name) ? (
                              <img src="/static/doc.svg" alt="doc" style={{ width: '45px' }} />
                            ) : isZipFile(props.quotedMessage.file_name) ? (
                              <img src="/static/zip.svg" alt="zip" style={{ width: '40px' }} />
                            ) : isPdfFile(props.quotedMessage.file_name) ? (
                              <img src="/static/Pdf.svg" alt="pdf" />
                            ) : isPowerPointFile(props.quotedMessage.file_name) ? (
                              <img
                                src="/static/nolabel.svg"
                                alt="Xlsx"
                                style={{ width: '40px', marginRight: '10px' }}
                              />
                            ) : (
                              <img src="/static/nolabel.svg" alt="pdf" style={{ width: '40px', marginRight: '10px' }} />
                            )}
                          </Flex>
                          <Flex
                            fontSize="14px"
                            marginLeft="5px"
                            style={{
                              width: 'calc(100% - 55px)',
                              whiteSpace: 'nowrap',
                              overflow: 'hidden',
                              textOverflow: 'ellipsis',
                            }}
                          >
                            {props.quotedMessage.file_name}
                          </Flex>
                        </Flex>
                      )}
                    </Flex>
                  </Flex>
                )}
                {props.revoke ?? 0 >= 1 ? (
                  <Flex fontStyle="italic" fontWeight="normal" fontSize={14} color="gray">
                    This message has been delete
                  </Flex>
                ) : !(props.type in MessageType) ? (
                  <Flex fontStyle="italic" fontWeight="normal" fontSize={14} color="gray">
                    [ This message is unsupported type. ]
                  </Flex>
                ) : (
                  <>
                    {props.type === MessageType.Text && (
                      <Flex
                        fontStyle="normal"
                        fontWeight="normal"
                        fontSize={14}
                        style={{ wordBreak: 'break-word', lineHeight: '22px', letterSpacing: '0.1px' }}
                      >
                        {props.text}
                      </Flex>
                    )}
                    {props.type === MessageType.Media && props.file_mime_type !== 'video/mp4' && (
                      <>
                        <Flex
                          max-width={336}
                          max-height={336}
                          borderRadius={4}
                          overflow="hidden"
                          onClick={() => handleClickImage(props.thumbnailUrl || '')}
                        >
                          <img
                            src={props.thumbnailUrl}
                            alt="Media message"
                            style={{
                              maxHeight: '336px',
                              maxWidth: '100%',
                              transition: '3s ease-out',
                              display: 'block',
                              filter: props.id.length > 20 ? 'grayscale(80%)' : 'none',
                            }}
                          />
                        </Flex>
                        <Flex mt="5px" style={{ wordBreak: 'break-word', lineHeight: '22px', letterSpacing: '0.1px' }}>
                          {props.captionImage}
                        </Flex>
                      </>
                    )}
                    {props.type === MessageType.Voice && props.thumbnailUrl && (
                      <>
                        <AudioPlayer
                          src={props.thumbnailUrl}
                          elevation={0}
                          width="300px"
                          height="50px"
                          useStyles={useStyles}
                          time="double"
                          spacing={1}
                        />

                        {props.download === 'ongoing' &&
                          timeLoading?.[props.id] &&
                          timeLoading?.[props.id].run &&
                          !statusFailed &&
                          !props.fileBig && (
                            <Flex position="absolute" width="100%" bottom={0} left={0}>
                              <StyledProgressBar color="primary" variant="determinate" value={valueUpload} />
                            </Flex>
                          )}
                      </>
                    )}
                    {props.type === MessageType.Media && props.thumbnailUrl && props.file_mime_type === 'video/mp4' && (
                      <CardMedia src={props.thumbnailUrl} component="video" image={props.thumbnailUrl} controls />
                    )}
                    {props.type === MessageType.Document && props.file_name && (
                      <Flex flexDirection="column">
                        {props.forwarded && (
                          <Flex fontStyle="italic" fontWeight={600} fontSize={12} color="#77777A" mt="7px" ml="20px">
                            <Icon name="forward" />
                            <Flex ml="5px">Forwarded</Flex>
                          </Flex>
                        )}
                        <Flex
                          height={75}
                          width={260}
                          borderRadius={4}
                          alignItems="center"
                          justifyContent="flex-start"
                          paddingLeft="10px"
                          paddingRight="10px"
                        >
                          <Flex>
                            {isExcelFile(props.file_name) ? (
                              <img src="/static/Xlsx.svg" alt="Xlsx" style={{ marginRight: '15px' }} />
                            ) : isWordFile(props.file_name) ? (
                              <img src="/static/doc.svg" alt="doc" style={{ width: '45px', marginRight: '5px' }} />
                            ) : isZipFile(props.file_name) ? (
                              <img src="/static/zip.svg" alt="zip" style={{ width: '40px', marginRight: '15px' }} />
                            ) : isPdfFile(props.file_name) ? (
                              <img src="/static/Pdf.svg" alt="pdf" style={{ marginRight: '15px' }} />
                            ) : isPowerPointFile(props.file_name) ? (
                              <img
                                src="/static/nolabel.svg"
                                alt="Xlsx"
                                style={{ width: '40px', marginRight: '10px' }}
                              />
                            ) : (
                              <img src="/static/nolabel.svg" alt="pdf" style={{ width: '40px', marginRight: '10px' }} />
                            )}
                          </Flex>
                          <Flex flexDirection="column" minWidth="0px" style={{ width: 'calc(100% - 55px)' }}>
                            <StyledFileName ongoing={props.download === 'ongoing'} style={{ width: '100%' }}>
                              <a
                                href={props.thumbnailUrl}
                                target="_blank"
                                rel="noopener noreferrer"
                                style={{
                                  color: '#000',
                                  width: '100%',
                                  whiteSpace: 'nowrap',
                                  overflow: 'hidden',
                                  textOverflow: 'ellipsis',
                                }}
                              >
                                {props.file_name}
                              </a>
                            </StyledFileName>
                            <Box fontWeight={600} fontSize={12} color="#626262" lineHeight="17px">
                              {props.size}
                            </Box>
                          </Flex>
                        </Flex>
                        {props.download === 'ongoing' &&
                          timeLoading?.[props.id].run &&
                          !statusFailed &&
                          !props.fileBig && (
                            <Flex>
                              <StyledProgressBar color="primary" variant="determinate" value={valueUpload} />
                            </Flex>
                          )}
                      </Flex>
                    )}
                    {(hidden && props.download === 'done') || (hidden && statusFailed && !props.fileBig) ? (
                      <PointerFlex position="absolute" top={0} right={0}>
                        <Box
                          aria-controls="customized-menu"
                          aria-haspopup="true"
                          onClick={handleClick}
                          bgcolor="rgba(255, 255, 255, 0.5)"
                          borderRadius="50%"
                          display="flex"
                          alignItems="center"
                        >
                          <ExpandMoreIcon />
                        </Box>
                        <IbMenu
                          id="filter-menu"
                          anchorEl={anchorEl}
                          keepMounted
                          open={Boolean(anchorEl)}
                          onClose={handleClose}
                        >
                          {props.id.length < 20 && (
                            <>
                              <IbMenuItem onClick={() => handleAction('reply')}>Reply</IbMenuItem>
                              {/* <IbMenuItem onClick={() => handleAction('forward-message')}>Forward message</IbMenuItem> */}
                            </>
                          )}

                          {(staredMessages ?? []).every((item) => item.id !== props.id) && props.id.length < 20 && (
                            <IbMenuItem onClick={() => handleAction('start-message')}>Star message</IbMenuItem>
                          )}
                          {!(staredMessages ?? []).every((item) => item.id !== props.id) && props.id.length < 20 && (
                            <IbMenuItem onClick={() => handleAction('un-start-message')}>Unstar message</IbMenuItem>
                          )}
                          {(props.status === -1 && props.outgoing) ||
                          (statusFailed && !props.fileBig && props.id.length > 20) ? (
                            <IbMenuItem onClick={() => handleAction('resend')}>Resend</IbMenuItem>
                          ) : null}
                          {props.id.length < 20 && (
                            <IbMenuItem onClick={() => handleAction('delete-message')}>Delete message</IbMenuItem>
                          )}
                        </IbMenu>
                      </PointerFlex>
                    ) : null}
                  </>
                )}
                {props.outgoing && statusFailed && !props.fileBig && props.id.length > 20 && (
                  <WrapResendICon
                    onClick={() => {
                      if (!flagResend) {
                        handleAction('resend');
                        setFlagResend(true);
                      }
                    }}
                  >
                    <Icon name="resend" />
                  </WrapResendICon>
                )}
              </MessageBubble>
            </Flex>
            <Flex
              justifyContent="space-between"
              flexDirection={props.outgoing ? 'row-reverse' : 'row'}
              fontSize={11}
              fontWeight={600}
              color="#98999F"
              mt="9px"
              width="100%"
            >
              <Flex>
                {props.outgoing ? 'Sent from' : 'Read by'}&nbsp;
                <Box color="#080809" display="inline">
                  {props.sender}
                </Box>
              </Flex>
              <Flex width={10} />
              {!props.outgoing && formatDistanceToNow(props.createdAt, { addSuffix: true })}
              {props.outgoing && !props.failed && props.status !== undefined && (
                <Flex alignItems="center">
                  {formatDistanceToNow(props.createdAt, { addSuffix: true })}
                  {props.outgoing && <Icon name={MESSAGE_STATUS_TO_ICON_NAME[props.status] || 'pending'} ml="4px" />}
                </Flex>
              )}
              {props.fileBig ? (
                <Flex alignItems="center" mr="5px">
                  <Icon name="error" />
                  <Flex fontWeight={600} fontSize={11} color="#FA3434" ml="5px">
                    Failed to send File upload too big
                  </Flex>
                </Flex>
              ) : null}
              {props.outgoing && statusFailed && !props.fileBig && props.id.length > 20 && (
                <Flex alignItems="center" mr="5px">
                  <Icon name="error" />
                  <Flex fontWeight={600} fontSize={11} color="#FA3434" ml="5px">
                    Failed to send
                  </Flex>
                </Flex>
              )}
            </Flex>
          </Flex>
        </WrapDefaultBubble>
      ) : (
        <WrapSystemBubble
          style={{ wordBreak: 'break-word', lineHeight: '22px', letterSpacing: '0.1px' }}
          justifyContent="center"
          paddingX="32px"
          className="bubble-system"
        >
          <Flex textAlign="center" fontSize={12} fontWeight={500} color="#A2A3A9">{`${props.text}   -   ${format(
            props.createdAt,
            'd/M/yyyy k:mm',
          )}`}</Flex>
        </WrapSystemBubble>
      )}
    </>
  );
};

export default ChatMessage;
