import React, { useMemo } from 'react';
import styled from 'styled-components';
import CardMedia from '@material-ui/core/CardMedia';
import Carousel from '../PreviewImage/Carousel';
import { ChatMessageProps } from '../ChatRoom/ChatMessage';
import { Flex } from '../../common/Flex';

const StyledPicture = styled(CardMedia)`
  border-radius: 4px;
  height: 75px;
  width: 75px;
`;

const Media = ({ data }: { data: ChatMessageProps[] }) => {
  const [flagCarousel, setFlagCarousel] = React.useState<boolean>(false);
  const [listSlideImage, setListSlideImage] = React.useState('');

  const handleClickImage = (url: string) => {
    setListSlideImage(url);
    setFlagCarousel(true);
  };
  const mediaData = useMemo(
    () =>
      (data ?? []).filter((mess) => {
        return mess.revoke !== 1;
      }),
    [data],
  );
  return (
    <Flex flexWrap="wrap" alignItems="center" py="10px" px="25px" width={300} overflow="auto">
      {flagCarousel && <Carousel images={listSlideImage} onClose={() => setFlagCarousel(false)} />}
      {mediaData.map((media) => {
        if (media.file_mime_type !== 'video/mp4') {
          return (
            <Flex
              key={media.id}
              flex="0 1 30%"
              height="75px"
              width="75px"
              mt="20px"
              mr="5px"
              onClick={() => handleClickImage(media.thumbnailUrl || '')}
            >
              <StyledPicture image={media.thumbnailUrl} />
            </Flex>
          );
        }
        return <CardMedia src={media.thumbnailUrl} component="video" image={media.thumbnailUrl} controls />;
      })}
    </Flex>
  );
};

export default Media;
