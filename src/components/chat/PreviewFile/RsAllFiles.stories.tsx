import React from 'react';

import { Flex } from '../../common/Flex';
import RsAllFiles from '../Container/RsAllFiles';

export default {
  title: 'Chat/RsAllFiles',
  component: RsAllFiles,
};

export const Media = () => {
  return (
    <Flex width={300}>
      <RsAllFiles initialTab={0} />
    </Flex>
  );
};

export const Docs = () => {
  return (
    <Flex width={300}>
      <RsAllFiles initialTab={1} />
    </Flex>
  );
};
