/* eslint-disable no-nested-ternary */
import React, { useMemo } from 'react';
import { Box } from '@material-ui/core';
import { format } from 'date-fns';
import styled from 'styled-components';
import { isExcelFile, isWordFile, isZipFile, isPdfFile, isPowerPointFile } from '../../../utils/validateFileName';
import { ChatMessageProps } from '../ChatRoom/ChatMessage';
import { Flex } from '../../common/Flex';

const StyledLink = styled.a`
  color: #080809;
`;

const Docs = ({ data }: { data: ChatMessageProps[] }) => {
  const docsData = useMemo(
    () =>
      (data ?? []).filter((mess) => {
        return mess.revoke !== 1;
      }),
    [data],
  );

  return (
    <Flex flexDirection="column" p="20px" mb="20px" width={300} overflow="auto">
      {docsData.map((doc) => {
        return (
          <Flex flexDirection="column" key={doc.id} mb="15px" fontSize={12} color="#A2A3A6">
            {doc.file_name && (
              <>
                <Flex justifyContent="flex-end" mb="8px">
                  {format(doc.createdAt, 'd/M/y')}
                </Flex>
                <Flex
                  boxShadow="0px 1px 3px rgba(0, 0, 0, 0.095389)"
                  borderRadius="12px"
                  bgcolor={doc.outgoing ? '#FFEB98' : 'white'}
                  p="15px"
                  alignItems="center"
                >
                  {isExcelFile(doc.file_name) ? (
                    <img src="/static/Xlsx.svg" alt="Xlsx" style={{ marginRight: '15px' }} />
                  ) : isWordFile(doc.file_name) ? (
                    <img src="/static/doc.svg" alt="doc" style={{ width: '45px', marginRight: '5px' }} />
                  ) : isZipFile(doc.file_name) ? (
                    <img src="/static/zip.svg" alt="zip" style={{ width: '40px', marginRight: '15px' }} />
                  ) : isPdfFile(doc.file_name) ? (
                    <img src="/static/Pdf.svg" alt="pdf" style={{ marginRight: '15px' }} />
                  ) : isPowerPointFile(doc.file_name) ? (
                    <img src="/static/nolabel.svg" alt="Xlsx" style={{ width: '40px', marginRight: '10px' }} />
                  ) : (
                    <img src="/static/nolabel.svg" alt="pdf" style={{ width: '40px', marginRight: '10px' }} />
                  )}
                  <Flex flexDirection="column" minWidth={0}>
                    <Flex>
                      <Box
                        textOverflow="ellipsis"
                        overflow="hidden"
                        whiteSpace="nowrap"
                        color="#080809"
                        fontWeight={600}
                        fontSize={15}
                      >
                        <StyledLink href={doc.thumbnailUrl} target="_blank" rel="noopener noreferrer">
                          {doc.file_name}
                        </StyledLink>
                      </Box>
                    </Flex>
                    <Flex fontWeight={600} fontSize={12} color="#626262" lineHeight="17px">
                      {doc.size}
                    </Flex>
                  </Flex>
                </Flex>
              </>
            )}
          </Flex>
        );
      })}
    </Flex>
  );
};

export default Docs;
