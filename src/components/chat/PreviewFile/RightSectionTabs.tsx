/* eslint-disable no-nested-ternary */
import React, { useMemo } from 'react';
import styled from 'styled-components';
import { Tab, Tabs } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { MessageType } from '../../../redux/services/chat';
import { ChatMessageProps } from '../ChatRoom/ChatMessage';
import Docs from './Docs';
import Media from './Media';
import { AppState } from '../../../redux/store';
import { readRecord } from '../../../utils/localStorageService';

const StyledTabs = styled(Tabs)`
  border-bottom: 1px solid #dcdce4;
  .MuiTabs-indicator {
    background-color: #ffdb36;
  }
  .MuiTab-root {
    font-family: ${({ theme }) => theme.typography.fontFamily};
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    color: #98999f;
    min-width: 100px;
  }
  .Mui-selected {
    color: #ffdb36;
    font-weight: bold;
  }
`;

const RightSectionTabs = ({ initialTab }: { initialTab: number }) => {
  const [selectedTab, setSelectedTab] = React.useState(initialTab);
  const handleChange = (event: React.ChangeEvent<unknown>, newValue: number) => {
    setSelectedTab(newValue);
  };
  const messages = useSelector((s: AppState) => (s.chat.currentChatId === undefined ? undefined : s.chat.messages));
  const messageList = useSelector((s: AppState) =>
    s.chat.currentChatId === undefined ? undefined : s.chat.messageList?.[s.chat.currentChatId],
  );
  const subDomainId = useSelector((s: AppState) => (s.auth.subdomainId === undefined ? undefined : s.auth.subdomainId));
  const chatRoomMessages = useMemo(
    () =>
      messages &&
      (messageList ?? []).map(
        (mId): ChatMessageProps => ({
          id: messages[mId].id,
          type: messages[mId].message_type,
          createdAt: new Date(messages[mId].timestamp),
          file_name: messages[mId].file_name || '',
          text: messages[mId].text,
          outgoing: messages[mId].from_type !== 'Other',
          status: messages[mId].status,
          sender:
            messages[mId].from_type === 'System'
              ? 'System'
              : messages[mId].from_type === 'Web'
              ? 'Web'
              : messages[mId].from_type === 'Phone'
              ? 'Phone'
              : messages[mId].from_type === 'Customer Service'
              ? messages[mId].user.name || ''
              : messages[mId].contact?.name || messages[mId].contact?.tel || '',

          thumbnailUrl:
            messages[mId].message_type !== MessageType.Text
              ? `${process.env.REACT_APP_API_URL}/media/${messages[mId].id}?access-token=${readRecord(
                  'accessToken',
                )}&subdomain-id=${subDomainId}`
              : undefined,
          revoke: messages[mId].revoke,
          captionImage: messages[mId].file_caption,
          download: messages[mId].id.length > 20 ? 'ongoing' : 'done',
          file_mime_type: messages[mId].file_mime_type,
        }),
      ),
    [messageList, messages, subDomainId],
  );
  const mediaData = useMemo(
    () =>
      (chatRoomMessages ?? []).filter((mess) => {
        return mess.type === 'Media';
      }),
    [chatRoomMessages],
  );
  const docsData = useMemo(
    () =>
      (chatRoomMessages ?? []).filter((mess) => {
        return mess.type === 'Document';
      }),
    [chatRoomMessages],
  );
  // console.log(mediaData);
  return (
    <>
      <StyledTabs value={selectedTab} onChange={handleChange} variant="fullWidth">
        <Tab label="MEDIA" />
        <Tab label="DOCS" />
      </StyledTabs>
      {selectedTab === 0 && <Media data={mediaData} />}
      {selectedTab === 1 && <Docs data={docsData} />}
    </>
  );
};

export default RightSectionTabs;
