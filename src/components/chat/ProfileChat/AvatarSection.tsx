import React, { useRef } from 'react';
import styled from 'styled-components';
import EditIcon from '@material-ui/icons/Edit';
import { Avatar, Theme, useTheme } from '@material-ui/core';
import { useSelector } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import InlineEditable from '../../common/InlineEditable';
import { updateChat } from '../../../redux/services/chat';
import useOnClickOutside from '../../common/useOnClickOutside';
import { Flex, PointerFlex } from '../../common/Flex';
import { AppState } from '../../../redux/store';

const StyledAvatar = styled(Avatar)`
  width: 52px;
  height: 52px;
  margin-bottom: 8px;
`;

const AvatarSection = ({ name, avatar }: { name: string; avatar: string }) => {
  const theme = useTheme<Theme>();
  const [storedHeading, setStoredHeading] = React.useState('');
  const [nameEdit, setNameEdit] = React.useState('');
  const [flagEdit, setFlagEdit] = React.useState(false);
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const ref = useRef(null);
  useOnClickOutside(ref, () => {
    setFlagEdit(false);
  });
  const handleClickEdit = () => {
    if (!flagEdit) {
      setFlagEdit(true);
      setNameEdit(name);
    } else {
      setFlagEdit(false);
      if (nameEdit !== name && currentChatId && nameEdit !== '') {
        updateChat(currentChatId, nameEdit);
      }
    }
  };

  const changePressKey = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key === 'Enter') {
      if (currentChatId && nameEdit !== name) {
        updateChat(currentChatId, nameEdit);
      }
      setFlagEdit(false);
    }
  };

  return (
    <Flex flexDirection="column" justifyContent="center" alignItems="center" mt="42px">
      <StyledAvatar alt={name} src={avatar} />
      <div
        ref={ref}
        style={{ display: 'flex', alignItems: 'center', width: '100%', justifyContent: 'space-between' }}
        onKeyPress={changePressKey}
      >
        {flagEdit ? (
          <TextField
            value={nameEdit}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setNameEdit(e.target.value)}
            style={{ marginRight: '10px' }}
          />
        ) : (
          <Flex
            fontFamily={theme.typography.fontFamily}
            fontStyle="normal"
            fontWeight="600"
            fontSize="18px"
            lineHeight="23px"
            color="#2E2E32"
            marginRight="10px"
            width="calc(100% - 30px)"
            textOverflow="ellipsis"
            overflow="hidden"
            whiteSpace="nowrap"
            display="block"
          >
            {name}
          </Flex>
        )}

        <PointerFlex onClick={handleClickEdit}>
          <EditIcon fontSize="small" />
        </PointerFlex>
      </div>
      <Flex width="100%" justifyContent="center" alignItems="center" mt="12px">
        <InlineEditable
          text={storedHeading}
          placeholder="Click here to edit!"
          onSetText={(text: string) => setStoredHeading(text)}
        />
      </Flex>
    </Flex>
  );
};
export default AvatarSection;
