import { Tab, Tabs } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';

import { ChatContactList } from '../../../redux/services/chat';
import GroupInfo from './GroupInfo';
import { RSActivity } from './RSActivity';

const StyledTabs = styled(Tabs)`
  border-bottom: 1px solid #dcdce4;
  margin-top: 15px;
  .MuiTabs-indicator {
    background-color: #ffdb36;
  }
  .MuiTab-root {
    font-family: ${({ theme }) => theme.typography.fontFamily};
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    color: #98999f;
    min-width: 100px;
  }
  .Mui-selected {
    color: #ffdb36;
    font-weight: bold;
  }
`;

const RightSectionTabs = ({
  initialTab,
  onAdd,
  contactList,
}: {
  initialTab: number;
  onAdd: () => void;
  contactList: ChatContactList[];
}) => {
  const [selectedTab, setSelectedTab] = React.useState(initialTab);
  const handleChange = (event: React.ChangeEvent<unknown>, newValue: number) => {
    setSelectedTab(newValue);
  };

  return (
    <>
      <StyledTabs value={selectedTab} onChange={handleChange} variant="fullWidth">
        <Tab label="INFO" />
        <Tab label="ACTIVITY" />
      </StyledTabs>
      {selectedTab === 0 && <GroupInfo onAdd={onAdd} contactList={contactList} />}
      {selectedTab === 1 && <RSActivity />}
    </>
  );
};

export default RightSectionTabs;
