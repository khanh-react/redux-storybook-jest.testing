import { Select } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';

import { AppState } from '../../../redux/store';
import { FieldHeader } from '../../../redux/type/contact/state';
import { Flex } from '../../common/Flex';
import { IbTextField, StyledMenuItem } from '../../common/InputField';

export interface IFields extends FieldHeader {
  value: string | string[] | { [field: string]: number };
}

const General = (props: { contactInfo: IFields[] }) => {
  // eslint-disable-next-line no-console
  // console.log(props.contactInfo);
  const contactFieldInfo = useSelector((s: AppState) => s.subdomain.subdomainSetting?.contactField);

  return (
    <Flex flexDirection="column" mt="15px">
      <Flex fontWeight="bold" fontSize={12} lineHeight="15px" color="#bbbbc5">
        GENERAL
      </Flex>

      {props.contactInfo.map((field, idx) => {
        if (field.type === 'select' || field.type === 'radio')
          return (
            // eslint-disable-next-line react/no-array-index-key
            <Flex pt="25px" key={idx}>
              <IbTextField
                name={field.label}
                label={field.label}
                placeholder={field.label}
                select
                value={field.value || ''}
                // onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                //   handleTextChange(e, idx, field.name);
                // }}
                disabled={field.name === 'field_36'}
                variant="standard"
                InputLabelProps={{
                  shrink: true,
                }}
              >
                {contactFieldInfo &&
                  contactFieldInfo.map((c) => {
                    if (c.name === field.name)
                      return c.option_list.map((o, i) => (
                        // eslint-disable-next-line react/no-array-index-key
                        <StyledMenuItem key={i} value={o}>
                          {o}
                        </StyledMenuItem>
                      ));
                    return null;
                  })}
              </IbTextField>
            </Flex>
          );
        if (field.type === 'checkbox') {
          const result =
            contactFieldInfo &&
            contactFieldInfo.filter((c) => {
              return c.name === field.name;
            });

          return (
            // eslint-disable-next-line react/no-array-index-key
            <Flex pt="25px" key={idx}>
              <Select
                multiple
                defaultValue={field.value ? Object.keys(field.value) : []}
                // onChange={(e) => {
                //   handleChange(e, field.name);
                // }}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
                input={
                  <IbTextField
                    name={field.label}
                    label={field.label}
                    placeholder={field.label}
                    select
                    variant="standard"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                }
              >
                {result &&
                  result[0].option_list.map((o, i) => (
                    // eslint-disable-next-line react/no-array-index-key
                    <StyledMenuItem key={i} value={o}>
                      {o}
                    </StyledMenuItem>
                  ))}
              </Select>
            </Flex>
          );
        }
        if (field.type === 'textarea') {
          const result =
            contactFieldInfo &&
            contactFieldInfo.filter((c) => {
              return c.name === field.name;
            });

          return (
            // eslint-disable-next-line react/no-array-index-key
            <Flex pt="25px" key={idx}>
              <IbTextField
                type="textarea"
                name={field.label}
                label={field.label}
                placeholder={field.label}
                value={result && result[0].option_list}
                // onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                //   handleTextChange(e, idx, field.name);
                // }}
                disabled={field.name === 'field_36'}
                variant="standard"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Flex>
          );
        }
        if (field.type === 'date' || field.type === 'time') {
          return (
            // eslint-disable-next-line react/no-array-index-key
            <Flex pt="25px" key={idx}>
              <IbTextField
                type={field.type}
                name={field.label}
                label={field.label}
                placeholder={field.label}
                value={field.value || ''}
                InputLabelProps={{
                  shrink: true,
                }}
                // onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                //   handleTextChange(e, idx, field.name);
                // }}
                disabled={field.name === 'field_36'}
                variant="standard"
              />
            </Flex>
          );
        }
        return (
          // eslint-disable-next-line react/no-array-index-key
          <Flex pt="25px" key={idx}>
            <IbTextField
              name={field.label}
              label={field.label}
              placeholder={field.label}
              type="text"
              value={field.value || ''}
              // onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              //   handleTextChange(e, idx, field.name);
              // }}
              disabled={field.name === 'field_36'}
              variant="standard"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Flex>
        );
      })}
    </Flex>
  );
};

export default General;
