import React from 'react';
import styled from 'styled-components';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import { Avatar, Box, Button, IconButton } from '@material-ui/core';
import Icon from '../../common/Icon';
import { BareInput } from '../../common/BareInput';
import { Flex } from '../../common/Flex';

const StyledIcon = styled(Icon)`
  border-radius: 5px;
  padding: 5px;
  &:hover {
    background-color: #fff8da;
  }
`;
const StyledButton = styled(Button)`
  ${({ theme }) => `
margin-left: 10px;
.MuiButton-label {
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: 600;
  font-size: 15px;
  line-height: 26px;
  text-transform: capitalize;
}
border-radius: 8px;
height: 28px;
box-shadow: none;
`}
`;
const StyledIconButton = styled(IconButton)`
  padding: 0px;
  height: 16px;
  .MuiSvgIcon-root {
    font-size: 16px;
    border-radius: 5px;
    padding: 5px;
    &:hover {
      background-color: #fff8da;
    }
  }
`;
const StyledNote = styled(Flex)`
  ${({ theme }) => `
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 16px;
  color: #6c6c72;
  padding-top: 8px;
  padding-bottom: 5px;
  width: 232px;
  word-break: break-word;
  `}
`;
const StyledEditNote = styled(BareInput)`
  ${({ theme }) => `
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: normal;
font-size: 13px;
line-height: 16px;
color: #6C6C72;
align-items: flex-start;
min-height:50px;
max-width: 232px;
padding-top: 8px;
`}
`;

const StyledNoteTitle = styled(Flex)`
  ${({ theme }) => `
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 22px;
  color: #050506;
  .MuiAvatar-root {
    width: 22px;
    height: 22px;
    margin-right: 10px;
  }
  `}
`;

export type NoteProps = {
  id: string;
  name: string;
  avatar: string;
  note: string;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  editNote?: (id: string, newNote: string) => void;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  deleteNote?: (id: string) => void;
};

const Note = (props: NoteProps) => {
  const [isEditing, setEditing] = React.useState(false);
  const [newNote, setNewNote] = React.useState('');
  const editFieldRef = React.useRef<HTMLLinkElement>(null);
  const handleCancel = () => {
    setEditing(false);
  };
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNewNote(event.target.value);
  };

  const handleSubmit = () => {
    if (props.editNote) {
      props.editNote(props.id, newNote);
    }
    setNewNote('');
    setEditing(false);
  };
  const handleEdit = () => {
    setEditing(true);
  };
  const handleDelete = () => {
    if (props.deleteNote) props.deleteNote(props.id);
  };
  const editingTemplate = (
    <Flex flexDirection="column">
      <StyledNoteTitle marginTop="5px">
        <Avatar alt={props.name} src={props.avatar} />
        <Box>{props.name}</Box>
      </StyledNoteTitle>
      <StyledEditNote
        id={props.id}
        value={newNote}
        onChange={handleChange}
        inputRef={editFieldRef}
        multiline
        autoFocus
      />
      <Flex justifyContent="flex-end">
        <StyledButton variant="outlined" onClick={handleCancel}>
          Cancel
        </StyledButton>
        <StyledButton type="submit" variant="contained" color="secondary" onClick={handleSubmit}>
          Next
        </StyledButton>
      </Flex>
    </Flex>
  );

  const viewTemplate = (
    <Flex flexDirection="column">
      <Flex justifyContent="space-between">
        <StyledNoteTitle marginTop="5px">
          <Avatar alt={props.name} src={props.avatar} />
          <Box>{props.name}</Box>
        </StyledNoteTitle>
        <Flex>
          <StyledIconButton onClick={handleEdit}>
            <StyledIcon name="edit-note" />
          </StyledIconButton>
          <StyledIconButton onClick={handleDelete}>
            <DeleteOutlineOutlinedIcon />
          </StyledIconButton>
        </Flex>
      </Flex>
      <Flex flexWrap="wrap">
        <StyledNote>{props.note}</StyledNote>
      </Flex>
    </Flex>
  );
  return <>{isEditing ? editingTemplate : viewTemplate}</>;
};

export default Note;
