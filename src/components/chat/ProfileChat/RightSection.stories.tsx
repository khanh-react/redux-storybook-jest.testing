import React from 'react';

import { Flex } from '../../common/Flex';
import RightSection from '../Container/RightSection';

export default {
  title: 'Chat/RightSection',
  component: RightSection,
};

export const InfoTab = () => {
  return (
    <Flex flexDirection="row" width="300px">
      <RightSection initialTab={0} />
    </Flex>
  );
};

export const ActivityTab = () => {
  return (
    <Flex flexDirection="row" width="300px">
      <RightSection initialTab={1} />
    </Flex>
  );
};

export const NoteTab = () => {
  return (
    <Flex flexDirection="row" width="300px">
      <RightSection initialTab={2} />
    </Flex>
  );
};
