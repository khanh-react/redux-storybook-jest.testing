import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { get } from 'lodash';
import General, { IFields } from './General';

import { getContactDetail } from '../../../redux/action/contact';
import { AppState } from '../../../redux/store';
import PersonalData from './PersonalData';
import { Flex } from '../../common/Flex';

export const RSInfo = (props: { contactId: string }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getContactDetail(props.contactId));
  }, [props.contactId, dispatch]);
  const contactDetail = useSelector((state: AppState) => state.contacts.contactDetail);
  const contactInfo = useMemo(() => {
    return contactDetail?.contact;
  }, [contactDetail]);

  const contactField: IFields[] | undefined = useMemo(() => {
    if (contactInfo && contactDetail) {
      return [
        ...contactDetail.fieldHeaders.map((field) => ({
          ...field,
          value: field.name === 'name' ? contactInfo.name : get(contactInfo.fields, [field.name]),
        })),
      ];
    }
    return undefined;
  }, [contactInfo, contactDetail]);
  return (
    <Flex flexDirection="column" justifyContent="center" p="25px">
      {contactDetail && (
        <PersonalData phone={contactDetail.contact.tel} email="katehalligan@email.com" address="Causeway Bay" />
      )}
      {contactField && <General contactInfo={contactField} />}
    </Flex>
  );
};

export default RSInfo;
