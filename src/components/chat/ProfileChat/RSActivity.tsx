import React from 'react';
import { Flex } from '../../common/Flex';
import Ticket from './Ticket';

export const RSActivity = () => {
  return (
    <Flex flexDirection="column" justifyContent="center" p="25px 15px">
      <Ticket type="ticket" code="#3205" by="Rachel Chan" date={new Date('2020-09-04T15:40:57.640Z')} />
      <Ticket type="invoice" code="IN#82918821002" by="Rachel Chan" date={new Date('2020-09-04T15:40:57.640Z')} />
      <Ticket type="contact" by="Rachel Chan" date={new Date('2020-09-04T15:40:57.640Z')} />
    </Flex>
  );
};

export default RSActivity;
