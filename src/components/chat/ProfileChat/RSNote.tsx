import { Button } from '@material-ui/core';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import React from 'react';

import styled from 'styled-components';

import { Flex } from '../../common/Flex';
import NoteList from './NoteList';

export const StyledInfoTitle = styled(Flex)`
  ${({ theme }) => `
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 15px;
  color: #bbbbc5;
  margin-bottom: 15px;
  `}
`;

const StyledAddNoteButton = styled(Button)`
  ${({ theme }) => `
.MuiButton-label {
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
font-weight: 600;
font-size: 14px;
line-height: 24px;
  text-transform: capitalize;
}
border-radius: 8px;
`}
`;

const NoteData = [
  {
    id: '1',
    name: 'Wattana Carlson',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
    note: 'Kate ha',
  },
  {
    id: '2',
    name: 'Wattana Carlson',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
    note: 'I spoke with Kate yesterday. She’s very excited about our new products.',
  },
  {
    id: '3',
    name: 'Rachel Chan',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
    note: 'Kate is our VIP. Please add her to out fav.',
  },
];

export const RSNote = () => {
  return (
    <Flex flexDirection="column" justifyContent="space-between" p="25px 15px" height="80vh">
      <NoteList noteList={NoteData} />
      <StyledAddNoteButton variant="outlined" startIcon={<AddCircleOutlineIcon />}>
        Create Note
      </StyledAddNoteButton>
    </Flex>
  );
};
