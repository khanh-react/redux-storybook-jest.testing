import React, { useRef } from 'react';
import { Avatar, Box, Theme, useTheme } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import TextField from '@material-ui/core/TextField';
import EditIcon from '@material-ui/icons/Edit';
import useOnClickOutside from '../../common/useOnClickOutside';
import { Flex, PointerFlex } from '../../common/Flex';
import { AppState } from '../../../redux/store';
import { changeNameChatRoom } from '../../../redux/action/chat';

const StyledAvatar = styled(Avatar)`
  width: 52px;
  height: 52px;
  margin-bottom: 8px;
`;
const StyledTag = styled(Box)`
  ${({ theme }) => `
  display: inline-flex;
  justify-content: center;
  align-items: center;
  border-radius: 11.5px;
  background: #fff8da;
  padding: 4px 24px;
  margin-top: 10px;
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 15px;
  color: #0F0F10;
  `}
`;

const AvatarSection = ({
  name,
  description,
  status,
  avatar,
}: {
  name: string;
  description: string;
  status: string;
  avatar: string;
}) => {
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const theme = useTheme<Theme>();
  const [flagEdit, setFlagEdit] = React.useState(false);
  const [nameEdit, setNameEdit] = React.useState('');
  const dispatch = useDispatch();
  const ref = useRef(null);
  useOnClickOutside(ref, () => {
    setFlagEdit(false);
  });
  const handleClickEdit = () => {
    if (!flagEdit) {
      setFlagEdit(true);
      setNameEdit(name);
    } else {
      setFlagEdit(false);
      if (nameEdit !== name && currentChatId && nameEdit !== '') {
        dispatch(changeNameChatRoom(currentChatId, nameEdit));
      }
    }
  };

  const changePressKey = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key === 'Enter') {
      if (currentChatId && nameEdit !== name) {
        dispatch(changeNameChatRoom(currentChatId, nameEdit));
      }
      setFlagEdit(false);
    }
  };

  return (
    <Flex flexDirection="column" justifyContent="center" alignItems="center" mt="42px">
      <StyledAvatar alt={name} src={avatar} />
      <div ref={ref} style={{ display: 'flex', alignItems: 'center' }} onKeyPress={changePressKey}>
        {flagEdit ? (
          <TextField
            value={nameEdit}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setNameEdit(e.target.value)}
          />
        ) : (
          <Flex
            fontFamily={theme.typography.fontFamily}
            fontStyle="normal"
            fontWeight="600"
            fontSize="18px"
            lineHeight="23px"
            color="#2E2E32"
            marginRight="10px"
          >
            {name}
          </Flex>
        )}

        <PointerFlex onClick={handleClickEdit}>
          <EditIcon fontSize="small" />
        </PointerFlex>
      </div>

      <Flex
        fontFamily={theme.typography.fontFamily}
        fontStyle="italic"
        fontWeight="normal"
        fontSize="12px"
        lineHeight="15px"
        color="#77777A"
      >
        {description}
      </Flex>
      <StyledTag>{status}</StyledTag>
    </Flex>
  );
};

export default AvatarSection;
