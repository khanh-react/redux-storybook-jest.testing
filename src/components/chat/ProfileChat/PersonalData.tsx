import { Box } from '@material-ui/core';
import PhoneOutlinedIcon from '@material-ui/icons/PhoneOutlined';
import React from 'react';
import styled from 'styled-components';

import { Flex } from '../../common/Flex';
import { StyledInfoTitle } from './RSNote';

const StyledBox = styled(Flex)`
  ${({ theme }) => `
align-items: center;
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 18px;
color: #0F0F10;
margin-bottom: 15px;
.MuiSvgIcon-root {
font-size: 15px;
margin-right: 10px;
}
`}
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const PersonalData = ({ phone, email, address }: { phone: string; email?: string; address?: string }) => {
  return (
    <>
      <StyledInfoTitle>CONTACT</StyledInfoTitle>
      <StyledBox>
        <PhoneOutlinedIcon />
        <Box>{phone}</Box>
      </StyledBox>
      {/* <StyledBox>
        <AlternateEmailOutlinedIcon />
        <Box>{email}</Box>
      </StyledBox>
      <StyledBox>
        <HomeOutlinedIcon />
        <Box>{address}</Box>
      </StyledBox> */}
    </>
  );
};

export default PersonalData;
