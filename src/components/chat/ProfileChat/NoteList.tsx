import React from 'react';
import Note, { NoteProps } from './Note';
import { Flex } from '../../common/Flex';

const NoteList = (props: { noteList: NoteProps[] }) => {
  const [noteList, setNoteList] = React.useState(props.noteList);
  const deleteNote = (id: string) => {
    const remainingNote = noteList.filter((note) => id !== note.id);
    setNoteList(remainingNote);
  };

  const editNote = (id: string, newNote: string) => {
    const editedNoteList = noteList.map((note) => {
      if (id === note.id) {
        return { ...note, note: newNote };
      }
      return note;
    });
    setNoteList(editedNoteList);
  };
  // const addNote=(note: string) => {
  //   const newNote = { id: 'numb', note: note };
  //   setNoteList([...noteList, newNote]);
  // }
  const newNoteList = noteList.map((note) => (
    <Flex key={note.id} flexDirection="column" border="1.2px solid #DCDCE4" borderRadius="8px" mb="15px">
      <Flex flexDirection="column" p="10px 15px 15px">
        <Note
          id={note.id}
          name={note.name}
          deleteNote={deleteNote}
          editNote={editNote}
          note={note.note}
          avatar={note.avatar}
        />
      </Flex>
    </Flex>
  ));
  return <div>{newNoteList}</div>;
};

export default NoteList;
