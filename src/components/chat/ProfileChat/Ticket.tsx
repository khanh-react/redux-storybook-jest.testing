import React from 'react';
import { Box } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import { format } from 'date-fns';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import BookIcon from '@material-ui/icons/Book';
import styled from 'styled-components';
import { Flex } from '../../common/Flex';

const StyledDate = styled(Flex)`
  ${({ theme }) => `
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: normal;
  font-size: 11px;
  line-height: 14px;
  color: #98999f;
  `}
`;

const StyledActivityDescription = styled(Flex)`
  ${({ theme }) => `
  font-family: ${theme.typography.fontFamily};
  align-items: center;
  font-style: normal;
  font-size: 12px;
  line-height: 15px;
  letter-spacing: 0.3px;
  `}
`;
const StyledActivityTitle = styled(Flex)`
  ${({ theme }) => `
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 18px;
  color: #2e2e32;
  align-items: center;
  .MuiSvgIcon-root {
    font-size: 16px;
    margin-right: 8px;
  }
  `}
`;

export type ActivityProps = {
  type: 'ticket' | 'invoice' | 'contact';
  by: string;
  code?: string;
  date: Date;
};

const Ticket = ({ type, code, by, date }: ActivityProps) => {
  return (
    <Flex flexDirection="column" border="1.2px solid #DCDCE4" borderRadius="8px" mb="15px">
      {type === 'ticket' && (
        <Flex flexDirection="column" p="15px">
          <Flex justifyContent="space-between">
            <StyledActivityTitle>
              <BookIcon />
              <Box>Ticket Created </Box>
            </StyledActivityTitle>
            <StyledDate>{format(date, 'd/M/y')}</StyledDate>
          </Flex>
          <Flex>
            <StyledActivityDescription color="#828389">Ticket</StyledActivityDescription>
            &nbsp;
            <StyledActivityDescription color="#0B6CFF">{code}</StyledActivityDescription>
            &nbsp;
            <StyledActivityDescription color="#828389">by</StyledActivityDescription>
            &nbsp;
            <StyledActivityDescription fontWeight="600">{by}</StyledActivityDescription>
          </Flex>
        </Flex>
      )}
      {type === 'invoice' && (
        <Flex flexDirection="column" p="15px">
          <Flex justifyContent="space-between">
            <StyledActivityTitle>
              <AttachMoneyIcon />
              <Box>Invoice Created </Box>
            </StyledActivityTitle>
            <StyledDate>{format(date, 'd/M/y')}</StyledDate>
          </Flex>
          <Flex mt="5px">
            <StyledActivityDescription color="#828389">Invoice</StyledActivityDescription>
            &nbsp;
            <StyledActivityDescription color="#0B6CFF">{code}</StyledActivityDescription>
            &nbsp;
            <StyledActivityDescription color="#828389">by</StyledActivityDescription>
            &nbsp;
            <StyledActivityDescription fontWeight="600">{by}</StyledActivityDescription>
          </Flex>
        </Flex>
      )}
      {type === 'contact' && (
        <Flex flexDirection="column" p="15px">
          <Flex justifyContent="space-between">
            <StyledActivityTitle>
              <PersonIcon />
              <Box>Contact Created </Box>
            </StyledActivityTitle>
            <StyledDate>{format(date, 'd/M/y')}</StyledDate>
          </Flex>
          <Flex mt="5px">
            <StyledActivityDescription color="#828389">This contact was created by</StyledActivityDescription>
            &nbsp;
            <StyledActivityDescription fontWeight="600">{by}</StyledActivityDescription>
          </Flex>
        </Flex>
      )}
    </Flex>
  );
};

export default Ticket;
