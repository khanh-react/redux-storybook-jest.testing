import React from 'react';

import { Flex } from '../../common/Flex';
import RsGroupChat from '../Container/RsGroupChat';

export default {
  title: 'Chat/RsGroupChat',
  component: RsGroupChat,
};

export const InfoTab = () => {
  return (
    <Flex flexDirection="row" width="300px">
      <RsGroupChat initialTab={0} />
    </Flex>
  );
};

export const ActivityTab = () => {
  return (
    <Flex flexDirection="row" width="300px">
      <RsGroupChat initialTab={1} />
    </Flex>
  );
};

// export const NoteTab = () => {
//   return (
//     <Flex flexDirection="row" width="300px">
//       <RsGroupChat initialTab={2} />
//     </Flex>
//   );
// };
