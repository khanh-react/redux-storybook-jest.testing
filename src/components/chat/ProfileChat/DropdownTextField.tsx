import React from 'react';
import styled from 'styled-components';
import { IbTextField, StyledMenuItem } from '../../common/InputField';

const StyledTextField = styled(IbTextField)`
  margin-bottom: 25px;
  .MuiInput-underline:after {
    border-bottom: 2px solid black;
  }
`;
const StyledTextFieldLongLabel = styled(IbTextField)`
  margin-bottom: 25px;
  .MuiInput-underline:after {
    border-bottom: 2px solid black;
  }
  .MuiInputBase-input {
    margin-top: 10px;
  }
`;

const DropdownTextField = ({ label, data }: { label: string; data: { id: string; label: string }[] }) => {
  const [value, setValue] = React.useState(data[0].label);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };
  return (
    <>
      {label.length < 50 ? (
        <StyledTextField
          select
          disabled={false}
          error={false}
          readOnly={false}
          required={false}
          label={label}
          onChange={handleChange}
          value={value}
          variant="standard"
        >
          {data.map((prop) => {
            return (
              <StyledMenuItem key={prop.id} value={prop.label} disabled={prop.label === '— Select type --'}>
                {prop.label}
              </StyledMenuItem>
            );
          })}
        </StyledTextField>
      ) : (
        <StyledTextFieldLongLabel
          select
          disabled={false}
          error={false}
          readOnly={false}
          required={false}
          label={label}
          onChange={handleChange}
          value={value}
          variant="standard"
        >
          {data.map((prop) => {
            return (
              <StyledMenuItem key={prop.id} value={prop.label} disabled={prop.label === '— Select type --'}>
                {prop.label}
              </StyledMenuItem>
            );
          })}
        </StyledTextFieldLongLabel>
      )}
    </>
  );
};

export default DropdownTextField;
