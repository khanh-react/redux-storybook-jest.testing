import { Avatar, Box } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { removeParticipants } from '../../../redux/action/chat';
import { AppState } from '../../../redux/store';
import { Flex, PointerFlex } from '../../common/Flex';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';

export interface ParticipantProps {
  id: string;
  name: string;
  tel: string;
  avatar: string;
  isAdmin: boolean;
  isSuperAdmin: boolean;
}

const GroupParticipant = (props: ParticipantProps) => {
  const dispatch = useDispatch();
  const [hidden, setHidden] = React.useState(false);
  const handleMouseEvent = () => {
    setHidden(!hidden);
  };
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId) || '';
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const handleAction = (action: string, phoneNumber: string) => () => {
    /* eslint-disable */

    console.log(action);
    if (action === 'remove') dispatch(removeParticipants(Number(currentChatId), [`${phoneNumber}`]));
    handleClose();
  };
  return (
    <Flex
      position="relative"
      onMouseEnter={handleMouseEvent}
      onMouseLeave={handleMouseEvent}
      p="20px 18px"
      borderBottom="1px solid #ECEFF1"
    >
      <Flex>
        <Avatar alt="" src={props.avatar} />
      </Flex>
      <Flex flexDirection="column" flex={1} ml="15px">
        <Flex fontWeight={600} fontSize={16} lineHeight="22px">
          {props.name}
        </Flex>
        <Flex flex={1} justifyContent="space-between" alignItems="center">
          <Flex fontSize={12} lineHeight="15px" color="#A4A4A9">
            {props.tel}
          </Flex>
          <Flex>
            {props.isAdmin && (
              <Box
                fontWeight={600}
                fontSize={10}
                color="#11C24F"
                p="5px"
                borderRadius="12px"
                border="0.8px solid #11C24F"
              >
                Group admin
              </Box>
            )}
          </Flex>
        </Flex>
      </Flex>
      {hidden && (
        <PointerFlex position="absolute" top={0} right={0}>
          <Box
            aria-controls="customized-menu"
            aria-haspopup="true"
            onClick={handleClick}
            bgcolor="rgba(255, 255, 255, 0.5)"
            borderRadius="50%"
            display="flex"
            alignItems="center"
            p="5px"
          >
            <ExpandMoreIcon />
          </Box>
          <IbMenu
            id="filter-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            disableAutoFocusItem
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: -120,
            }}
          >
            <IbMenuItem onClick={handleAction('make-group-admin', props.tel)}>Make group admin</IbMenuItem>
            <IbMenuItem onClick={handleAction('remove', props.tel)}>Remove</IbMenuItem>
          </IbMenu>
        </PointerFlex>
      )}
    </Flex>
  );
};

export default GroupParticipant;
