import { Button } from '@material-ui/core';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import React, { useMemo } from 'react';
import styled from 'styled-components';

import { ChatContactList } from '../../../redux/services/chat';
import firstChars from '../../../utils/firstChars';
import { Flex } from '../../common/Flex';
import GroupParticipant from './GroupParticipant';

// const Participants = [
//   {
//     id: '1',
//     name: 'You',
//     phone: '+852 8888 8888',
//     avatar: 'https://via.placeholder.com/50/858585/FFFFFF/?text=GT',
//     admin: true,
//   },
//   {
//     id: '2',
//     name: 'Jessie Wong',
//     phone: '+852 8888 8888',
//     avatar: 'https://via.placeholder.com/50/858585/FFFFFF/?text=MN',
//     admin: true,
//   },
//   {
//     id: '3',
//     name: 'David Spade',
//     phone: '+852 8888 8888',
//     avatar: 'https://via.placeholder.com/50/858585/FFFFFF/?text=MN',
//     admin: false,
//   },
//   {
//     id: '4',
//     name: 'Jerry Wong',
//     phone: '+852 8888 8888',
//     avatar: 'https://via.placeholder.com/50/858585/FFFFFF/?text=MN',
//     admin: false,
//   },
//   {
//     id: '5',
//     name: 'Samuel Tsang',
//     phone: '+852 8888 8888',
//     avatar: 'https://via.placeholder.com/50/858585/FFFFFF/?text=MN',
//     admin: false,
//   },
// ];
const StyledButton = styled(Button)`
  .MuiButton-label {
    font-family: ${({ theme }) => theme.typography.fontFamily};
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    text-transform: none;
  }
  margin: 0 15px 15px;
  border-radius: 8px;
  min-height: 36px;
  box-shadow: none;
`;

const GroupInfo = ({ onAdd, contactList }: { onAdd: () => void; contactList: ChatContactList[] }) => {
  const newArr = useMemo(
    () =>
      contactList.map((contact) => ({
        id: contact.contact.id.toString(),
        name: contact.contact.name,
        tel: contact.contact.tel,
        avatar: firstChars(contact.contact.name),
        isAdmin: Boolean(contact.is_admin),
        isSuperAdmin: Boolean(contact.is_super_admin),
      })),
    [contactList],
  );
  return (
    <Flex width="100%" height="720px">
      <Flex flexDirection="column" width="100%">
        <Flex fontWeight={600} fontSize={16} lineHeight="20px" m="20px 0px 15px 25px">
          PARTICIPANTS
        </Flex>
        <Flex flex={1} overflow="auto" flexDirection="column">
          {newArr.map((participant) => (
            <GroupParticipant key={participant.id} {...participant} />
          ))}
        </Flex>
        <Flex flexDirection="column" justifyContent="flex-end" pt="30px" pb="50px">
          <StyledButton variant="outlined" color="inherit" startIcon={<ControlPointIcon />} onClick={onAdd}>
            Add participant
          </StyledButton>
          <Flex color="#F16F63">
            <StyledButton variant="outlined" color="inherit" fullWidth>
              Exit group
            </StyledButton>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default GroupInfo;
