import { Box } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { updateCaseLeftNav } from '../../../redux/action/chat';
import { AppState } from '../../../redux/store';
import { Flex, PointerFlex } from '../../common/Flex';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';

const StyledFlex = styled(Flex)`
  color: #98999f;
  &:hover {
    border-bottom: 2px solid #ffdb36;
    color: #ffdb36;
    cursor: pointer;
  }
`;
type PropsFilter = {
  caseFilter: string;
  setCaseFilter: (caseFilter: string) => void;
  listUnread: number;
  listUnassign: number;
  closedSearch: () => void;
};

const Filter = ({ caseFilter, setCaseFilter, listUnread, listUnassign, closedSearch }: PropsFilter) => {
  const listChat = useSelector((s: AppState) => s.chat.chatList);

  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const handleAction = (action: string) => {
    handleClose();
    return action;
  };

  return (
    <StyledFlex
      px={1}
      py="21px"
      mx={1}
      flex={1}
      textAlign="center"
      position="relative"
      justifyContent="center"
      alignItems="center"
    >
      <Box p="0px 5px" mr="10px" onClick={closedSearch}>
        {caseFilter !== 'ME' ? caseFilter.toUpperCase() : 'ALL'}
      </Box>
      <Box position="absolute" right="0px" top="21px">
        <Box aria-controls="customized-menu" aria-haspopup="true" mb="-10px" color="#2E2E32" onClick={handleClick}>
          <ExpandMoreIcon />
        </Box>
        <IbMenu id="filter-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
          <IbMenuItem
            onClick={() => {
              setCaseFilter('all');
              handleAction('all');
            }}
          >
            <Flex justifyContent="space-between" width="100%">
              <span>All Chats</span>
              <span>{listChat?.length}</span>
            </Flex>
          </IbMenuItem>
          <IbMenuItem
            onClick={() => {
              setCaseFilter('news');
              handleAction('new');
            }}
          >
            <Flex justifyContent="space-between" width="100%">
              <span>New</span>
              <span>{listUnread}</span>
            </Flex>
          </IbMenuItem>
          <IbMenuItem
            onClick={() => {
              setCaseFilter('unassigned');
              handleAction('unassigned');
            }}
          >
            <Flex justifyContent="space-between" width="100%">
              <span>Unassigned</span>
              <span>{listUnassign}</span>
            </Flex>
          </IbMenuItem>
          {/* <IbMenuItem onClick={handleAction('resolved')}>Resolved</IbMenuItem> */}
        </IbMenu>
      </Box>
    </StyledFlex>
  );
};

const Me = ({ handleClickMe }: { handleClickMe: (value: string) => void }) => {
  const dispatch = useDispatch();
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const handleAction = (action: string) => () => {
    /* eslint-disable */
    dispatch(updateCaseLeftNav(action));
    handleClose();
  };

  return (
    <>
      <StyledFlex
        px={1}
        py="21px"
        mx={1}
        flex={1}
        textAlign="center"
        position="relative"
        justifyContent="center"
        alignItems="center"
        onClick={() => handleClickMe('ME')}
      >
        ME
      </StyledFlex>
      <Box position="relative" top={-4}>
        <PointerFlex
          aria-controls="customized-menu"
          aria-haspopup="true"
          onClick={handleClick}
          mb="-10px"
          color="#2E2E32"
        >
          <MoreVertIcon />
        </PointerFlex>
        <IbMenu id="filter-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
          <IbMenuItem onClick={handleAction('new-group')}>New group</IbMenuItem>
          <IbMenuItem onClick={handleAction('new-individual')}>New individual</IbMenuItem>
          <IbMenuItem onClick={handleAction('starred')}>Starred</IbMenuItem>
          {/* <IbMenuItem onClick={handleAction('archived')}>Archived</IbMenuItem> */}
        </IbMenu>
      </Box>
    </>
  );
};

export default function MainNav({ caseFilter, setCaseFilter, listUnread, listUnassign, closedSearch }: PropsFilter) {
  return (
    <Flex
      height={68}
      borderRight="1px solid #DCDCE4"
      borderBottom="1px solid #DCDCE4"
      fontWeight="bold"
      lineHeight="24.5px"
      fontSize={14}
      alignItems="center"
      flexShrink={0}
    >
      <Filter
        caseFilter={caseFilter}
        setCaseFilter={setCaseFilter}
        listUnread={listUnread}
        listUnassign={listUnassign}
        closedSearch={closedSearch}
      />
      <Me handleClickMe={setCaseFilter} />
    </Flex>
  );
}
