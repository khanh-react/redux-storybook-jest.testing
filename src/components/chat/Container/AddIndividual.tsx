import { Button, InputAdornment, MenuItem, Select } from '@material-ui/core';
import getUnixTime from 'date-fns/getUnixTime';
import { findIndex } from 'lodash';
import React, { useEffect, useState } from 'react';
import ReactCountryFlag from 'react-country-flag';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { AppState } from '../../../redux/store';
import { Flex } from '../../common/Flex';
import { IbTextField, StyledMenuItem } from '../../common/InputField';
import { Nav } from '../../common/Nav';
import { createNewChat, updateCaseLeftNav } from '../../../redux/action/chat';

const Owner = [
  {
    value: 'Rachel Chan',
  },
  { value: 'Alex Ho' },
];
const StyledButton = styled(Button)`
  border-radius: 8px;
  .MuiButton-label {
    font-weight: 600;
    font-size: 14px;
  }
  margin-top: 20px;
`;

const COUNTRY = [
  { name: 'HK', phone: 852 },
  { name: 'US', phone: 1 },
  { name: 'CN', phone: 86 },
  { name: 'VN', phone: 84 },
];

const AddIndividual = () => {
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [number, setNumber] = useState('');
  const [own, setOwn] = useState('Rachel Chan');
  const [Flag, setFlag] = React.useState('852');
  const subdomainId = useSelector((s: AppState) => s.auth.subdomainId);
  const [apikey, setApiKey] = useState(0);
  const userDomainChannel = useSelector((s: AppState) => s.subdomain.subdomainChannel)?.filter((item) => item.owner);
  const [created, setCreated] = useState('');
  const handleChangeCreated = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCreated(event.target.value);
    const regexp = new RegExp(/(?<=\().*?(?=\))/);
    const matches_array = event.target.value.match(regexp);
    const index = findIndex(userDomainChannel, (o) => o.owner === matches_array?.[0]);
    userDomainChannel?.[index].id && setApiKey(parseInt(userDomainChannel?.[index].id, 10));
  };
  const [incorrectPhone, setIncorrectPhone] = useState(false);
  const handleChangeOwner = (event: React.ChangeEvent<HTMLInputElement>) => {
    setOwn(event.target.value);
  };
  const handleChangeFlag = (event: React.ChangeEvent<{ value: unknown }>) => {
    setFlag(event.target.value as string);
  };
  const handleChangName = (event: React.ChangeEvent<{ value: unknown }>) => {
    setName(event.target.value as string);
  };

  const handleChangNumber = (event: React.ChangeEvent<{ value: unknown }>) => {
    setNumber(event.target.value as string);
    if (incorrectPhone) {
      setIncorrectPhone(false);
    }
  };

  const handleSubmit = () => {
    if (`${Flag}${number}`.length >= 10 && name !== '' && subdomainId) {
      dispatch(
        createNewChat({
          to: `${Flag}${number}`,
          name,
          type: 'Individual',
          api_key_id: apikey,
          timestamp: getUnixTime(new Date()),
        }),
      );
    }
    setTimeout(() => {
      setIncorrectPhone(true);
    }, 3000);
  };
  const onClose = () => {
    dispatch(updateCaseLeftNav('message'));
  };
  useEffect(() => {
    if (userDomainChannel && userDomainChannel.length > 0) {
      setCreated(`${userDomainChannel?.[0].name} (${userDomainChannel?.[0].owner})`);
      userDomainChannel?.[0].id && setApiKey(parseInt(userDomainChannel?.[0].id, 10));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userDomainChannel]);
  return (
    <Flex flexDirection="column" width={345}>
      <Nav
        backButton
        title="Add Individual"
        onBack={onClose}
        // actions={[
        //   { key: 'new-group', title: 'New group' },
        //   { key: 'new-individual', title: 'New individual' },
        //   { key: 'starred', title: 'Starred' },
        //   { key: 'archived', title: 'Archived' },
        // ]}
      />

      <Flex flexDirection="column" height="80vh" p="30px 25px 20px 25px">
        <Flex flexDirection="column" ml={-1}>
          <form noValidate autoComplete="off">
            <IbTextField
              error={false}
              disabled={false}
              readOnly={false}
              required={false}
              select
              label="Created by"
              value={created}
              onChange={handleChangeCreated}
              variant="outlined"
            >
              {(userDomainChannel ?? []).map((option) => (
                <MenuItem key={option.id} value={`${option.name} (${option.owner})`}>
                  {`${option.name} (${option.owner})`}
                </MenuItem>
              ))}
            </IbTextField>
            <Flex mt="20px">
              <IbTextField
                label="Name"
                required={false}
                placeholder="Name of Conversation"
                error={false}
                disabled={false}
                readOnly={false}
                helper=""
                onChange={handleChangName}
              />
            </Flex>
            <Flex mt="20px" flexDirection="column">
              <IbTextField
                required={false}
                error={false}
                disabled={false}
                readOnly={false}
                placeholder="68888 8888"
                label="Phone"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Select value={Flag} onChange={handleChangeFlag} displayEmpty disableUnderline>
                        {COUNTRY.map((item) => (
                          <StyledMenuItem value={item.phone} key={item.phone}>
                            <ReactCountryFlag
                              countryCode={item.name}
                              svg
                              style={{
                                width: '25px',
                                height: '17px',
                                borderRadius: '2px',
                              }}
                            />
                          </StyledMenuItem>
                        ))}

                        <StyledMenuItem value="">
                          <em>None</em>
                        </StyledMenuItem>
                      </Select>
                    </InputAdornment>
                  ),
                }}
                onChange={handleChangNumber}
              />
              {incorrectPhone && number !== '' && (
                <Flex color="red" fontSize={13} mt="5px">
                  the phone number is incorrect
                </Flex>
              )}
            </Flex>
            <Flex mt="20px">
              <IbTextField
                required={false}
                error={false}
                disabled={false}
                readOnly={false}
                label="Owner"
                value={own}
                onChange={handleChangeOwner}
              >
                {Owner.map((option) => (
                  <StyledMenuItem key={option.value} value={option.value}>
                    {option.value}
                  </StyledMenuItem>
                ))}
              </IbTextField>
            </Flex>
          </form>
        </Flex>
        <StyledButton color="secondary" variant="contained" onClick={handleSubmit}>
          Create
        </StyledButton>
      </Flex>
    </Flex>
  );
};

export default AddIndividual;
