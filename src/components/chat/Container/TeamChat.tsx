import React from 'react';

import { useDispatch, useSelector } from 'react-redux';

// import { scrollToLastMessage } from '../sagas';

import { AppState } from '../../../redux/store';

import { Flex } from '../../common/Flex';

import { loadMoreRoomScrollTeamChat, uploadFileInternalAct, sendTextInternalChat } from '../../../redux/action/chat';

import LoadingMessage from '../../common/LoadingMessage';
import TeamChatUploadImg from './TeamChatUpload';
import TeamChatNav from '../Teamchat/TeamChatNav';
import Composer from '../Teamchat/Composer';
import { MessageType } from '../../../redux/services/chat';
import MessageBubble from '../Teamchat/MessageBubble';

export interface MessageProps {
  type: MessageType;
  outgoing?: boolean;
  text?: string;
  thumbnailUrl?: string;
  createdAt: Date;
  sender: string;
  avatar: string;
  zoomOutImage?: (url: string) => void;
  file_name?: string;
  id: string;
  senderId: number;
}

const TeamChat = (props: { messages: MessageProps[] }) => {
  const roomInternal = useSelector((s: AppState) => s.chat.currentInternalChat);
  const [input, setValue] = React.useState('');
  const [imageCurrent, setImageCurrent] = React.useState<File[]>([]);
  const [flagTabUploadImage, setFlagTabUploadImage] = React.useState(false);
  const loadingInternal = useSelector((s: AppState) => s.chat.loadingInternal);

  const dispatch = useDispatch();

  const submitSend = () => {
    if (roomInternal) {
      if (flagTabUploadImage) {
        if (imageCurrent.length > 0) {
          (imageCurrent ?? []).forEach((item) => {
            dispatch(
              uploadFileInternalAct({ chatId: roomInternal, document: item, type: 'Image', fileCaption: input }),
            );
          });
          setImageCurrent([]);
        } else {
          dispatch(sendTextInternalChat(roomInternal, input));
        }

        setFlagTabUploadImage(false);
      } else {
        dispatch(sendTextInternalChat(roomInternal, input));
      }
      setValue('');
    }
  };

  React.useEffect(() => {
    const divScroll = document.getElementById('team-chat') as HTMLDivElement;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const eventScroll = (e: any) => {
      const position = (e?.target as HTMLDivElement).scrollTop;
      const clientHeight = divScroll?.clientHeight;
      const scrollHeight = divScroll?.scrollHeight;
      if (position === 0 && scrollHeight > clientHeight) {
        dispatch(loadMoreRoomScrollTeamChat(scrollHeight));
      }
    };
    if (divScroll) {
      divScroll.addEventListener('scroll', eventScroll);
    }
    return () => {
      window.removeEventListener('event', eventScroll);
    };
  }, [dispatch]);

  // useEffect(() => {
  //   const divScroll = document.getElementById('team-chat') as HTMLDivElement;
  //   if (divScroll) {
  //     divScroll.style.overflow = 'hidden auto';
  //     scrollToLastMessage(false, 0, 'team-chat');
  //   }
  // }, [props.messages]);
  return (
    <Flex flexDirection="column" borderTop="4px solid #01E1E8" width="300px" position="relative">
      {loadingInternal && <LoadingMessage top={70} />}
      {flagTabUploadImage && (
        <TeamChatUploadImg
          handleClosed={() => {
            setFlagTabUploadImage(false);
            setImageCurrent([]);
          }}
          imageCurrent={imageCurrent}
          setImageCurrent={(image: File[]) => setImageCurrent(image)}
        />
      )}
      <TeamChatNav />
      <Flex
        flexDirection="column"
        pb="30px"
        borderBottom="1px solid #EEEEEE"
        height="calc(100% - 136px)"
        id="team-chat"
        overflow="hidden"
      >
        {props.messages.map((message, i) => (
          // eslint-disable-next-line react/no-array-index-key
          <Flex mt="20px" flexDirection="column" key={i}>
            <MessageBubble {...message} />
          </Flex>
        ))}
      </Flex>
      <Flex mt="5px">
        <Composer
          submitSend={submitSend}
          valueInput={input}
          setValueInput={(text: string) => setValue(text)}
          clickAttachImage={() => setFlagTabUploadImage(true)}
        />
      </Flex>
    </Flex>
  );
};

export default TeamChat;
