import { Box } from '@material-ui/core';
import React, { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../../redux/store';
import firstChars from '../../../utils/firstChars';
import ChatListItem, { ChatListItemProps } from '../ChatList/ChatListItem';
import { Touchable } from '../../common/Touchable';
import {
  deleteChat,
  markReadChat,
  markUnReadChat,
  selectChat,
  updateCaseRightNav,
  loadMoreRoomScroll,
} from '../../../redux/action/chat';

export const ChatList = ({
  data,
  height,
  chatIds,
}: {
  data?: ChatListItemProps[];
  height?: string;
  chatIds: string[];
}) => {
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const dispatch = useDispatch();
  const chats = useSelector((s: AppState) => s.chat.chats);
  const messages = useSelector((s: AppState) => s.chat.messages);
  const caseRightNav = useSelector((s: AppState) => s.chat.caseRightNav);

  const onAction = useCallback(
    (action: string, chat_id: string) => {
      if (action === 'delete') {
        dispatch(deleteChat(chat_id));
      }
      if (action === 'markRead') {
        dispatch(markReadChat(chat_id));
      }
      if (action === 'markUnRead') {
        dispatch(markUnReadChat());
      }
    },
    [dispatch],
  );

  const chatListData = useMemo(
    () =>
      (chatIds ?? []).map(
        (chatId): ChatListItemProps => {
          const lastMess = chats[chatId].lastMessage;
          return {
            id: String(chatId),
            name: chats[chatId].name ?? chats[chatId].whatsapp_name,
            description: chats[chatId].to,
            image: firstChars(chats[chatId].name ?? chats[chatId].whatsapp_name),
            unreadCount: chats[chatId].unread_count,
            platform: chats[chatId].platform,
            lastMessage:
              // eslint-disable-next-line no-nested-ternary
              chats[chatId].lastMessage?.revoke ?? 0 >= 1
                ? 'Messages has been delete'
                : chats[chatId].lastMessage?.text !== null
                ? chats[chatId].lastMessage?.text
                : chats[chatId].lastMessage?.message_type,
            updatedAt: new Date(chats[chatId].timestamp),
            lastMessageStatus: ((lastMess && messages?.[lastMess.id]) ?? lastMess)?.status,
            read: chats[chatId].read,
            status: chats[chatId].status || '',
            assignedUser: chats[chatId].assignedUser,
            // assignedUser:
            // active?: boolean;
            // archived?: boolean;
            // chatTicket: chats[chatId].chatTicket,
            onAction,
          };
        },
      ),
    [chatIds, chats, messages, onAction],
  );

  React.useEffect(() => {
    const divScroll = document.getElementById('list-room-chat') as HTMLDivElement;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const eventScroll = (e: any) => {
      const position = (e?.target as HTMLDivElement).scrollTop;
      const clientHeight = divScroll?.clientHeight;
      const scrollHeight = divScroll?.scrollHeight;
      if (scrollHeight - clientHeight === position && scrollHeight > clientHeight) {
        dispatch(loadMoreRoomScroll(position));
      }
    };
    if (divScroll) {
      divScroll.addEventListener('scroll', eventScroll);
    }
    return () => {
      window.removeEventListener('event', eventScroll);
    };
  }, [dispatch]);

  return (
    <Box height={height ?? '100vh'} overflow="hidden auto" width="100%" id="list-room-chat">
      {(data || chatListData).map((props) => (
        <Touchable
          key={props.id}
          onClick={() => {
            dispatch(selectChat(props.id));
            if (caseRightNav !== 'chat-room-filter') {
              dispatch(updateCaseRightNav('user'));
            }
          }}
        >
          <ChatListItem {...props} active={props.id === currentChatId} />
        </Touchable>
      ))}
    </Box>
  );
};
