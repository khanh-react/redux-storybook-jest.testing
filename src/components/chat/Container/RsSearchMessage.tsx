import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import { Flex, PointerFlex } from '../../common/Flex';
import SearchResult from '../SearchInRoom/SearchResult';
import {
  updateCaseRightNav,
  searchMessInRoom,
  searchMessInRoomSuccess,
  flagSearchInRoom,
} from '../../../redux/action/chat';
import { HoverSearch } from '../../../route/chat';
import { AppState } from '../../../redux/store';

import SearchBar from '../SearchInRoom/SearchBar';
import LoadingMessage from '../../common/LoadingMessage';

// const HighlightedMessage = ({ text = '', highlight = '' }) => {
//   const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
//   return (
//     <Flex>
//       {parts.map((part, i) => (
//         // eslint-disable-next-line react/no-array-index-key
//         <Flex key={i}>
//           <Flex
//             bgcolor={part.toLowerCase() === highlight.toLowerCase() ? '#FFEB98' : 'white'}
//             color={part.toLowerCase() === highlight.toLowerCase() ? 'black' : '#8D8E91'}
//             fontWeight={600}
//             fontSize={14}
//           >
//             {part}
//           </Flex>
//           &nbsp;
//         </Flex>
//       ))}
//     </Flex>
//   );
// };

type Props = {
  onSelectSearchMessage: (room: string, id: string, time: number) => void;
};
const RsSearchMessage = ({ onSelectSearchMessage }: Props) => {
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const dataSearch = useSelector((s: AppState) => s.chat.searchMessageResult);
  const flagLoadingSearch = useSelector((s: AppState) => s.chat.flagSearchInRoom);
  const [textSearch, setTextSearch] = React.useState('');
  const dispatch = useDispatch();
  const onClose = () => {
    dispatch(updateCaseRightNav('user'));
  };

  const handleSearch = (text: string) => {
    if (text === '') {
      dispatch(flagSearchInRoom(false));
      dispatch(searchMessInRoomSuccess([]));
    } else {
      currentChatId && dispatch(searchMessInRoom(currentChatId, text));
    }
  };

  return (
    <Flex flexDirection="column" width={300} height="100%">
      <Flex alignItems="center" height={68} borderBottom="1px solid #EEEEEE" px="10px">
        <PointerFlex onClick={onClose} fontSize={22}>
          <ArrowBackIcon fontSize="inherit" />
        </PointerFlex>
        <Flex alignItems="center" justifyContent="space-between" flex={1}>
          <Flex fontWeight="bold" fontSize={16} lineHeight="20px" ml="19px" color="0F0F10" flex={1}>
            Search Message
          </Flex>
        </Flex>
      </Flex>
      <Flex p="9px 7px 8px 11px" borderBottom="1px solid #ECEFF1">
        <SearchBar
          textSearch={textSearch}
          setTextSearch={(text) => {
            setTextSearch(text);
            handleSearch(text);
          }}
        />
      </Flex>
      <Flex flexDirection="column" height="calc(100% - 77px)" overflow="hidden auto" position="relative">
        {flagLoadingSearch && <LoadingMessage top={20} />}
        {(dataSearch ?? []).map((result) => (
          <HoverSearch
            onClick={() => onSelectSearchMessage(result.chat.id, result.id, result.timestamp)}
            key={result.id}
          >
            <SearchResult
              name={result.contact?.name ?? result.user.name}
              message={result.text ?? result.file_name}
              searchTerm={textSearch}
              date={new Date(result.timestamp)}
            />
          </HoverSearch>
        ))}
      </Flex>
    </Flex>
  );
};

export default RsSearchMessage;
