import { Button, Typography } from '@material-ui/core';
import getUnixTime from 'date-fns/getUnixTime';
import { findIndex } from 'lodash';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CreatedByField from '../CreateNewChat/CreatedByField';
import { AppState } from '../../../redux/store';
import { Flex } from '../../common/Flex';
import { IbTextField } from '../../common/InputField';
import InputTag from '../../common/InputTag';
import { Nav } from '../../common/Nav';
import { createNewChat, updateCaseLeftNav } from '../../../redux/action/chat';
import ContactList from '../Contact/ContactList';
import { useStyles } from '../CreateNewChat/styles';
import OwnerField from '../CreateNewChat/OwnerField';

const CreateGroup = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [flagAddMember, setFlagAddMember] = useState(true);
  const [onSelect, setOnSelect] = useState<{ id: string; name: string; phoneNumber: string }[]>([]);
  const subdomainId = useSelector((s: AppState) => s.auth.subdomainId);
  const [apikey, setApiKey] = useState(0);
  const [name, setName] = useState('');
  const userDomainChannel = useSelector((s: AppState) => s.subdomain.subdomainChannel);
  const [created, setCreated] = useState('');
  const onClose = () => {
    dispatch(updateCaseLeftNav('message'));
  };
  const handleSelect = (user: { id: string; name: string; phoneNumber: string }) => {
    const index = findIndex(onSelect, (o) => {
      return o.id === user.id;
    });
    const temp = [...onSelect];
    if (index === -1) {
      temp.push(user);
    } else {
      temp.splice(index, 1);
    }
    setOnSelect(temp);
  };

  const onRemove = (id: string) => {
    const index = findIndex(onSelect, (o) => {
      return o.id === id;
    });
    const temp = [...onSelect];
    temp.splice(index, 1);
    setOnSelect(temp);
  };

  const handleChangName = (event: React.ChangeEvent<{ value: unknown }>) => {
    setName(event.target.value as string);
  };

  const handleSubmit = () => {
    if (onSelect.length > 0 && name !== '' && subdomainId) {
      dispatch(
        createNewChat({
          to: onSelect.map((item) => item.phoneNumber),
          name,
          type: 'Group',
          api_key_id: apikey,
          timestamp: getUnixTime(new Date()),
        }),
      );
    }
  };

  useEffect(() => {
    if (userDomainChannel) {
      setCreated(`${userDomainChannel?.[0].name} (${userDomainChannel?.[0].owner})`);
      userDomainChannel?.[0].id && setApiKey(parseInt(userDomainChannel?.[0].id, 10));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userDomainChannel]);

  return (
    <Flex flexDirection="column" width={345} position="relative">
      {flagAddMember ? (
        <>
          <Nav backButton title="Create Group" onBack={onClose} />
          <Flex flexDirection="column" justifyContent="space-between" height="100%" p="0px 25px 25px">
            <form className={classes.root} noValidate autoComplete="off">
              <Flex flexDirection="column">
                <CreatedByField
                  setApiKey={(value: string) => setApiKey(parseInt(value, 10))}
                  created={created}
                  setCreated={(value: string) => setCreated(value)}
                />
                <IbTextField
                  label="Group Name"
                  required={false}
                  error={false}
                  disabled={false}
                  readOnly={false}
                  helper=""
                  onChange={handleChangName}
                  placeholder="Name of Conversation"
                  value={name}
                />
              </Flex>
              <Flex flexDirection="column" alignItems="center">
                <Flex flexWrap="wrap" alignSelf="flex-start">
                  <Typography className={classes.Typography1} onClick={() => setFlagAddMember(false)}>
                    Add Group Participants
                  </Typography>
                </Flex>
                <Flex flexWrap="wrap" ml={-1}>
                  {onSelect.map((tag) => {
                    return (
                      <Flex m="5px" key={tag.id}>
                        <InputTag hasCloseIcon label={tag.phoneNumber} id={tag.id} onClose={onRemove} />
                      </Flex>
                    );
                  })}
                </Flex>
              </Flex>
              <Flex flexDirection="column">
                <OwnerField />
              </Flex>
            </form>
            <Button color="secondary" variant="contained" onClick={handleSubmit}>
              Create
            </Button>
          </Flex>
        </>
      ) : (
        <ContactList
          onBack={() => setFlagAddMember(true)}
          onSelect={onSelect}
          onRemove={onRemove}
          handleSelect={handleSelect}
        />
      )}
    </Flex>
  );
};

export default CreateGroup;
