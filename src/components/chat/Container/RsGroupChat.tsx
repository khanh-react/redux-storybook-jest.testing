import { findIndex } from 'lodash';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { ChatContactList } from '../../../redux/services/chat';
import { AppState } from '../../../redux/store';
import firstChars from '../../../utils/firstChars';
import { Flex } from '../../common/Flex';
import ParticipantsList from '../ParticipantsList/ParticipantsList';
import AvatarSection from '../ProfileChat/AvatarSection';
import RightSectionTabs from '../ProfileChat/RightSectionTabs';

const RsGroupChat = ({ initialTab = 0 }: { initialTab?: number }) => {
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const currentChat = useSelector((s: AppState) => (currentChatId ? s.chat.chats[currentChatId] : undefined));
  const [name, setName] = React.useState('');
  const [flagAddMember, setFlagAddMember] = useState(true);
  const [onSelect, setOnSelect] = useState<{ id: string; name: string; phoneNumber: string }[]>([]);
  const [contactList, setContactList] = React.useState<ChatContactList[]>([]);
  const handleSelect = (user: { id: string; name: string; phoneNumber: string }) => {
    const index = findIndex(onSelect, (o) => {
      return o.id === user.id;
    });
    const temp = [...onSelect];
    if (index === -1) {
      temp.push(user);
    } else {
      temp.splice(index, 1);
    }
    setOnSelect(temp);
  };

  const onRemove = (id: string) => {
    const index = findIndex(onSelect, (o) => {
      return o.id === id;
    });
    const temp = [...onSelect];
    temp.splice(index, 1);
    setOnSelect(temp);
  };
  useEffect(() => {
    currentChat && setName(currentChat.name ?? currentChat.whatsapp_name);
  }, [currentChat]);
  useEffect(() => {
    currentChat && setContactList(currentChat.chatContactList);
  }, [currentChat]);

  return (
    <Flex flexDirection="column" overflow="hidden" width="300px" padding="0 10px">
      {!flagAddMember ? (
        <ParticipantsList
          onBack={() => setFlagAddMember(true)}
          onSelect={onSelect}
          onRemove={onRemove}
          handleSelect={handleSelect}
          currentChatId={currentChatId || ''}
        />
      ) : (
        <>
          <AvatarSection name={name} avatar={firstChars(name)} />
          <RightSectionTabs initialTab={initialTab} onAdd={() => setFlagAddMember(false)} contactList={contactList} />
        </>
      )}
    </Flex>
  );
};
export default RsGroupChat;
