import React from 'react';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon, { IconName } from '../../common/Icon';

const ListIcon = [
  { name: 'user', color: '#FF3AA3' },
  { name: 'team-chat', color: '#01CDD4' },
  { name: 'calendar', color: '#FA6400' },
  { name: 'message', color: '#616bff' },
  { name: 'note', color: '#ffc822' },
];

type Props = {
  active: string;
  handleClick: (index: string) => void;
};
function RightNavChat({ active, handleClick }: Props) {
  return (
    <Flex
      width="45px"
      height="100%"
      flexDirection="column"
      alignItems="center"
      borderLeft="1px solid #EEEEEE"
      paddingY="14px"
    >
      {ListIcon.map((item, index) => {
        const indexs = index + 1;
        return (
          <PointerFlex
            bgcolor={active === item.name || (active === 'rs-group' && index === 0) ? item.color : '#BBBBC5'}
            width="28px"
            height="28px"
            key={indexs}
            borderRadius="50%"
            mb="14px"
            justifyContent="center"
            alignItems="center"
            position="relative"
            onClick={() => handleClick(item.name)}
          >
            <Icon name={item.name === '' ? 'user' : (item.name as IconName)} />
            {/* <Flex
              bgcolor="#e02020"
              width="10px"
              height="10px"
              borderRadius="50%"
              top="-1px"
              right="-1px"
              position="absolute"
            /> */}
          </PointerFlex>
        );
      })}
    </Flex>
  );
}

export default RightNavChat;
