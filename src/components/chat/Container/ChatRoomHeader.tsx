/* eslint-disable react/jsx-one-expression-per-line */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../../redux/store';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';
import { useIbMenu } from '../../common/Menu';

import {
  clearChat,
  closeChatTicket,
  deleteChat,
  exportParticipants,
  markReadChat,
  markUnReadChat,
  openChatTicket,
  refreshParticipants,
  updateCaseRightNav,
} from '../../../redux/action/chat';

import TagTicket from '../Tags/TagTicket';
import CustomerAvatar, { CustomerAvatarProps } from '../Header/CustomerAvatar';
import MoreButton from '../Header/MoreButton';
import TagButton from '../Header/TagButton';
import AssignedOwner from '../Header/AssignedOwner';

// import { stubFalse } from 'lodash';

export const ChatRoomHeader = (props: {
  customer: CustomerAvatarProps;
  onlineStatus: 'online' | 'offline';
  tags: { id: string; name: string }[];
  handleActionAddTag: (action: string, id?: string) => void;
  tagSelectData: { id: string; subdomain_id: number; name: string }[];
  deleteTag: (id: string) => void;
  handleChecked: (id: string, hidden: boolean) => void;
  type: string;
  setHeightHeader: (h: number) => void;
}) => {
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const caseRightNav = useSelector((s: AppState) => s.chat.caseRightNav);
  const currentChatTicket = useSelector(
    (s: AppState) => currentChatId && s.chat.chatTicket && s.chat.chatTicket[Number(currentChatId)],
  );

  const { handleClose } = useIbMenu();
  const dispatch = useDispatch();
  const handleAction = (action: string, id: string) => {
    /* eslint-disable */
    props.handleActionAddTag(action, id);
    handleClose();
  };

  const handleFolder = () => {
    dispatch(updateCaseRightNav('rs-all-file'));
  };

  const handleSearch = () => {
    dispatch(updateCaseRightNav('rs-search-message'));
  };

  const subDomainUsers = useSelector((s: AppState) => s.subdomain.subdomainUser);
  useEffect(() => {
    const headerEle = document.getElementById('header-chat');
    if (headerEle) {
      props.setHeightHeader(headerEle.offsetHeight);
    }
  }, [props.tags, currentChatId, caseRightNav]);

  const handleActionMore = (action: string) => {
    /* eslint-disable */
    if (action === 'delete-chat') {
      dispatch(deleteChat());
    }
    if (action === 'clear-messages') {
      dispatch(clearChat());
    }

    if (action === 'mark-as-unread') {
      dispatch(markUnReadChat());
    }
    if (action === 'mark-as-read') {
      currentChatId && dispatch(markReadChat(currentChatId));
    }
    if (action === 'open-new-ticket') {
      currentChatId && dispatch(openChatTicket(currentChatId));
    }
    if (action === 'close-current-ticket') {
      currentChatTicket && dispatch(closeChatTicket(currentChatTicket.id));
    }
    if (action === 'export-member') {
      currentChatId && dispatch(exportParticipants(currentChatId));
    }
    if (action === 'refresh-member') {
      currentChatId && dispatch(refreshParticipants(currentChatId));
    }
  };

  return (
    <Flex
      alignItems="stretch"
      justifyContent="space-between"
      pl="20px"
      pr="15px"
      borderBottom="1px solid #DCDCE4"
      borderRight="1px solid #DCDCE4"
      minHeight={props.tags.length > 3 ? 'auto' : '68px'}
      paddingY={props.tags.length > 3 ? '5px' : '0px'}
      flexDirection={props.tags.length <= 3 ? 'row' : 'column'}
      id="header-chat"
    >
      <Flex justifyContent="space-between" width="100%">
        <Flex alignItems="center">
          <Flex>
            <Flex flexShrink={0}>
              <CustomerAvatar {...props.customer} />
              <Flex flexDirection="column" alignItems="flex-start" justifyContent="space-around" ml="10px">
                <Flex fontWeight="bold" fontSize={14}>
                  {props.customer.name}
                </Flex>
                <Flex>
                  {props.type === 'Group' ? (
                    <Flex alignItems="center" color="#6C6C72" fontSize={14}>
                      n participants
                      {/* Missing participants field */}
                    </Flex>
                  ) : null}
                </Flex>
              </Flex>
            </Flex>
            {props.tags.length <= 3 && (
              <Flex
                alignItems="center"
                flexWrap="wrap"
                paddingTop="5px"
                marginLeft="15px"
                marginRight="5px"
                paddingBottom="5px"
              >
                {props.tags.map((item, index) => (
                  <TagTicket
                    name={item.name}
                    id={item.id}
                    outline
                    handleAction={handleAction}
                    key={item.id}
                    orderColor={index}
                  />
                ))}
              </Flex>
            )}
          </Flex>
        </Flex>
        <Flex alignItems="center">
          {subDomainUsers && <AssignedOwner users={subDomainUsers} />}

          <TagButton
            handleAction={props.handleActionAddTag}
            tagSelectData={props.tagSelectData}
            deleteTag={props.deleteTag}
            tags={props.tags}
            handleChecked={props.handleChecked}
          />
          <PointerFlex width={30} height={30} justifyContent="center" alignItems="center" onClick={handleSearch}>
            <Icon name="search" size={18} color="#26292C" />
          </PointerFlex>
          <PointerFlex onClick={handleFolder}>
            <Icon name="folder" />
          </PointerFlex>
          <MoreButton handleActionMore={handleActionMore} />
        </Flex>
      </Flex>
      {props.tags.length > 3 && (
        <Flex alignItems="center" flexWrap="wrap" paddingTop="5px" marginRight="5px">
          {props.tags.map((item, index) => (
            <TagTicket
              name={item.name}
              id={item.id}
              outline
              handleAction={handleAction}
              key={item.id}
              orderColor={index}
            />
          ))}
        </Flex>
      )}
    </Flex>
  );
};
