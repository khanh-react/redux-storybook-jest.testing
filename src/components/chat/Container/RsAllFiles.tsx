/* eslint-disable no-nested-ternary */
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import React from 'react';
import { useDispatch } from 'react-redux';
import { Flex, PointerFlex } from '../../common/Flex';
import { updateCaseRightNav } from '../../../redux/action/chat';
import RightSectionTabs from '../PreviewFile/RightSectionTabs';

const RsAllFiles = ({ initialTab = 0 }: { initialTab?: number }) => {
  const dispatch = useDispatch();

  const onClose = () => {
    dispatch(updateCaseRightNav('user'));
  };
  return (
    <Flex flexDirection="column">
      <Flex alignItems="center" height={68} borderBottom="1px solid #EEEEEE" px="10px">
        <PointerFlex onClick={onClose} fontSize={22}>
          <ArrowBackIcon fontSize="inherit" />
        </PointerFlex>
        <Flex alignItems="center" justifyContent="space-between" flex={1}>
          <Flex fontWeight="bold" fontSize={16} lineHeight="20px" ml="19px" color="0F0F10" flex={1}>
            All Files
          </Flex>
        </Flex>
      </Flex>
      <RightSectionTabs initialTab={initialTab} />
    </Flex>
  );
};
export default RsAllFiles;
