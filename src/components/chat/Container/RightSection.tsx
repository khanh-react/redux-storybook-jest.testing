import { Tab } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { AppState } from '../../../redux/store';
import firstChars from '../../../utils/firstChars';
import { Flex } from '../../common/Flex';
import { IbTabs } from '../../common/Tabs';
import AvatarSection from '../ProfileChat/const AvatarSection ';
import { RSNote } from '../ProfileChat/RSNote';
import RSInfo from '../ProfileChat/RSInfo';
import RSActivity from '../ProfileChat/RSActivity';

const RightSectionTabs = ({ initialTab, contactId }: { initialTab: number; contactId?: string }) => {
  const [selectedTab, setSelectedTab] = React.useState(initialTab);
  const handleChange = (event: React.ChangeEvent<unknown>, newValue: number) => {
    setSelectedTab(newValue);
  };
  return (
    <>
      <IbTabs value={selectedTab} onChange={handleChange}>
        <Tab label="INFO" />
        <Tab label="ACTIVITY" />
        <Tab label="NOTE" />
      </IbTabs>
      {selectedTab === 0 && contactId && <RSInfo contactId={contactId} />}
      {selectedTab === 1 && <RSActivity />}
      {selectedTab === 2 && <RSNote />}
    </>
  );
};

const RightSection = ({ initialTab = 0, contactId }: { initialTab?: number; contactId?: string }) => {
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const currentChat = useSelector((s: AppState) => (currentChatId ? s.chat.chats[currentChatId] : undefined));
  const [name, setName] = React.useState('');

  useEffect(() => {
    currentChat && setName(currentChat.name ?? currentChat.whatsapp_name);
  }, [currentChat]);

  return (
    <Flex flexDirection="column" style={{ overflow: 'auto' }} width="300px">
      <AvatarSection name={name} description="Sale Manager" status="VIP" avatar={firstChars(name)} />
      <RightSectionTabs initialTab={initialTab} contactId={contactId} />
    </Flex>
  );
};
export default RightSection;
