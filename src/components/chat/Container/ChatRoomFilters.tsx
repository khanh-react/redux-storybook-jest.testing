import { Button, Theme, useTheme } from '@material-ui/core';
import { findIndex, sortBy } from 'lodash';
import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { AppState } from '../../../redux/store';
import { Flex } from '../../common/Flex';
import { Nav } from '../../common/Nav';
import { chatFilterSuccess, selectChat, updateCaseRightNav } from '../../../redux/action/chat';
import { listChatFilter } from '../../../redux/services/chat';
import FilterTagsItem from '../FilterChat/FilterTagsItem';
import TagsDropdown from '../FilterChat/TagsDropdown';
import AssigneeDropdown from '../FilterChat/AssigneeDropdown';
import FilterCheckBoxItem from '../FilterChat/FilterCheckBoxItem';

const BottomButton = styled(Button)`
  ${({ theme }) => `
margin-left: 10px;
.MuiButton-label {
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: 600;
  font-size: 15px;
  line-height: 26px;
  text-transform: capitalize;
}
border-radius: 8px;
`}
`;

// const channelProps = [
//   { id: 1, label: 'Whatsapp' },
//   { id: 2, label: 'Facebook Messenger' },
//   { id: 3, label: 'Line' },
// ];

export const ChatRoomFilters = () => {
  const subDomainUsers = useSelector((s: AppState) => s.subdomain.subdomainUser);
  const tags = useSelector((s: AppState) => s.subdomain.subdomainSetting);
  const theme = useTheme<Theme>();
  const dispatch = useDispatch();
  const [name, setName] = React.useState('');
  const [allUserAssign, setAllUserAssign] = React.useState<{ label: string; id: number }[]>([]);
  const [selectUser, setSelectUser] = React.useState<{ label: string; id: number }[]>([]);
  // const [selectChannel, setSelectChannel] = React.useState<{ label: string; id: number }[]>([]);
  const [allSelectTag, setAllSelectTag] = React.useState<{ label: string; id: number }[]>([]);
  const [selectTag, setSelectTag] = React.useState<{ label: string; id: number }[]>([]);
  const [selectStatusTicket, setSelectStatusTicket] = React.useState<number[]>([]);
  const ticketStatus = useSelector((s: AppState) => s.subdomain.subdomainSetting?.ticketStatus);

  const checkBoxProps = useMemo(() => {
    return (ticketStatus ?? []).map((item) => {
      return { id: item.id, label: item.type };
    });
  }, [ticketStatus]);
  useEffect(() => {
    if (subDomainUsers) {
      const assignItem: { label: string; id: number } = { label: 'unAssignUser', id: 1 };
      const userAssign = subDomainUsers.map((item) => {
        return { label: item.name, id: item.user_id };
      });
      userAssign.push(assignItem);
      setAllUserAssign(sortBy(userAssign, (o) => o.id));
    }
  }, [subDomainUsers]);

  useEffect(() => {
    if (tags) {
      const tagsTemp = tags.tag.map((item) => {
        return { label: item.name, id: item.id };
      });
      setAllSelectTag(tagsTemp);
    }
  }, [tags]);

  const onSelectUser = (e: number) => {
    const indexSelect = findIndex(selectUser, (o) => o.id === e);
    const indexOfALll = findIndex(allUserAssign, (o) => o.id === e);
    if (indexSelect === -1) {
      setSelectUser((state) => {
        const temp = [...state];
        temp.push(allUserAssign[indexOfALll]);
        return temp;
      });
    } else {
      setSelectUser((state) => {
        const temp = [...state];
        temp.splice(indexSelect, 1);
        return temp;
      });
      // eslint-disable-next-line no-console
      console.log(name);
    }
  };
  const onCloseUser = (id: string) => {
    const indexSelect = findIndex(selectUser, (o) => `${o.id}` === id);
    setSelectUser((state) => {
      const temp = [...state];
      temp.splice(indexSelect, 1);
      return temp;
    });
  };
  // const onSelectChannel = (e: number) => {
  //   const indexSelect = findIndex(selectChannel, (o) => o.id === e);
  //   const indexOfALll = findIndex(channelProps, (o) => o.id === e);
  //   if (indexSelect === -1) {
  //     setSelectChannel((state) => {
  //       const temp = [...state];
  //       temp.push(channelProps[indexOfALll]);
  //       return temp;
  //     });
  //   } else {
  //     setSelectChannel((state) => {
  //       const temp = [...state];
  //       temp.splice(indexSelect, 1);
  //       return temp;
  //     });
  //   }
  // };
  // const onCloseChannel = (id: string) => {
  //   const indexSelect = findIndex(selectChannel, (o) => `${o.id}` === id);
  //   setSelectChannel((state) => {
  //     const temp = [...state];
  //     temp.splice(indexSelect, 1);
  //     return temp;
  //   });
  // };

  const onSelectTag = (e: number) => {
    const indexSelect = findIndex(selectTag, (o) => o.id === e);
    const indexOfALll = findIndex(allSelectTag, (o) => o.id === e);
    if (indexSelect === -1) {
      setSelectTag((state) => {
        const temp = [...state];
        temp.push(allSelectTag[indexOfALll]);
        return temp;
      });
    } else {
      setSelectTag((state) => {
        const temp = [...state];
        temp.splice(indexSelect, 1);
        return temp;
      });
    }
  };
  const onCloseTag = (id: string) => {
    const indexSelect = findIndex(selectTag, (o) => `${o.id}` === id);
    setSelectTag((state) => {
      const temp = [...state];
      temp.splice(indexSelect, 1);
      return temp;
    });
  };

  const checkedStatus = (id: number) => {
    const indexSelect = findIndex(selectStatusTicket, (o) => o === id);
    const indexOfALll = findIndex(checkBoxProps, (o) => o.id === id);
    if (indexSelect === -1) {
      setSelectStatusTicket((state) => {
        const temp = [...state];
        temp.push(checkBoxProps[indexOfALll].id);
        return temp;
      });
    } else {
      setSelectStatusTicket((state) => {
        const temp = [...state];
        temp.splice(indexSelect, 1);
        return temp;
      });
    }
  };

  const onSubmitFilter = async () => {
    const data = await listChatFilter(
      name,
      selectUser.map((item) => item.id),
      selectTag.map((item) => item.id),
      selectStatusTicket,
    );
    if (data) {
      dispatch(chatFilterSuccess(data));
      dispatch(selectChat(''));
    }
  };

  const cancelFilter = async () => {
    const data = await listChatFilter('', [], [], [], 12);
    if (data) {
      dispatch(chatFilterSuccess(data));
      dispatch(updateCaseRightNav(''));
    }
  };

  return (
    <Flex
      flexDirection="column"
      width={454}
      border="1px solid #E9E9F2"
      justifyContent="space-between"
      height="calc(100vh - 64px)"
      position="absolute"
      right={0}
      bottom={0}
      overflow="auto"
      zIndex={11}
      bgcolor="#fff"
    >
      <Flex flexDirection="column">
        <Nav title="Filters">
          {/* <StyledButton theme={theme} variant="contained" color="secondary" startIcon={<AddCircleOutlineIcon />}>
            Add field
          </StyledButton> */}
        </Nav>
        <FilterTagsItem changeNameFilter={(e) => setName(e.target.value)} />
        {/* <AssigneeDropdown
          data={channelProps}
          selectData={selectChannel}
          onSelect={onSelectChannel}
          onClose={onCloseChannel}
          name="Channel"
        /> */}
        <TagsDropdown data={allSelectTag} onSelect={onSelectTag} selectData={selectTag} onClose={onCloseTag} />
        <FilterCheckBoxItem title="Status" data={checkBoxProps} checkedStatus={checkedStatus} />
        <AssigneeDropdown
          data={allUserAssign}
          selectData={selectUser}
          onSelect={onSelectUser}
          onClose={onCloseUser}
          name="Assignee"
        />
      </Flex>
      <Flex justifyContent="flex-end" mr="32px" mt="15px" mb="20px">
        <BottomButton theme={theme} variant="outlined" onClick={cancelFilter}>
          Cancel
        </BottomButton>
        <BottomButton theme={theme} variant="contained" color="secondary" onClick={onSubmitFilter}>
          Apply
        </BottomButton>
      </Flex>
    </Flex>
  );
};

export default ChatRoomFilters;
