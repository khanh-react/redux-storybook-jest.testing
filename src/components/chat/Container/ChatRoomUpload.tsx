import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../../redux/store';
import { Flex } from '../../common/Flex';
import ChatRoomUploadImg from '../UploadImage/ChatRoomUploadImg';
import { ResetMessageAction, sendDocumentTempMessage } from '../../../redux/action/chat';
import Composer from '../UploadImage/Composer';

type Props = {
  onClickClosed: () => void;
  heighHeader?: number;
};
const ChatRoomUpload = ({ onClickClosed, heighHeader }: Props) => {
  const [imageCurrent, setImageCurrent] = React.useState<File[]>([]);
  const [captionImage, setCaptionImage] = React.useState<string>('');
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const nameUser = useSelector((s: AppState) => s.subdomain.subdomainConfig?.name);
  const dispatch = useDispatch();
  const onSend = async () => {
    if (imageCurrent && currentChatId) {
      imageCurrent.forEach((item) => {
        dispatch(sendDocumentTempMessage(currentChatId, item, nameUser ?? '', 'Media', captionImage));
      });
      dispatch(ResetMessageAction());
      onClickClosed();
    }
  };
  return (
    <Flex
      flexDirection="column"
      height={`calc(100% - ${heighHeader}px)`}
      position="absolute"
      bottom={0}
      left={0}
      width="100%"
      bgcolor="#fff"
    >
      <ChatRoomUploadImg
        imageCurrent={imageCurrent}
        setImageCurrent={(image: File[]) => setImageCurrent(image)}
        onClickClosed={onClickClosed}
      />
      <Composer setCaptionImage={(value) => setCaptionImage(value)} captionImage={captionImage} onSend={onSend} />
    </Flex>
  );
};
export default ChatRoomUpload;
