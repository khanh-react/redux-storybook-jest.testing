/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-one-expression-per-line */
import Collapse from '@material-ui/core/Collapse';
import { findIndex } from 'lodash';
import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  assignTagTicket,
  createTag,
  deleteTag,
  editTagSuccess,
  getTag,
  loadMore,
  ResetMessageAction,
  updateTag,
} from '../../../redux/action/chat';
import { scrollToLastMessage } from '../../../redux/saga/chat';
import { MessageType } from '../../../redux/services/chat';
import { AppState } from '../../../redux/store';
import { CHAT_PLATFORM_TO_ICON_NAME } from '../../../redux/type/chat/iconChat';
import { CustomerSource } from '../../../redux/type/chat/types';
import firstChars from '../../../utils/firstChars';
import { readRecord } from '../../../utils/localStorageService';
import Alert from '../../common/Alert';
import { Flex } from '../../common/Flex';
import LoadingMessage from '../../common/LoadingMessage';
import ChatMessage, { ChatMessageProps } from '../ChatRoom/ChatMessage';
import Composer from '../ChatRoom/Composer';
import ReplyMessage from '../ChatRoom/ReplyMessage';
import CreateNewTag from '../Tags/CreateNewTag';
import { ChatRoomHeader } from './ChatRoomHeader';
import ChatRoomUpload from './ChatRoomUpload';

export interface CustomerAvatarProps {
  name: string;
  image: string;
  source: CustomerSource;
}

export const ChatRoom = () => {
  const messageList = useSelector((s: AppState) =>
    s.chat.currentChatId === undefined ? undefined : s.chat.messageList?.[s.chat.currentChatId],
  );
  const [caseActionRoom, setCaseActionRoom] = React.useState(false);
  const messages = useSelector((s: AppState) => (s.chat.currentChatId === undefined ? undefined : s.chat.messages));
  const subDomainId = useSelector((s: AppState) => (s.auth.subdomainId === undefined ? undefined : s.auth.subdomainId));
  const currentChatId = useSelector((s: AppState) => s.chat.currentChatId);
  const currentChatRoom = useSelector((s: AppState) => (currentChatId ? s.chat.chats[currentChatId] : undefined));
  const currentChat = useSelector((s: AppState) =>
    s.chat.currentChatId ? s.chat.chats[s.chat.currentChatId] : undefined,
  );
  const currentAssignedUser = useSelector((s: AppState) =>
    currentChatId ? s.chat.chats[currentChatId]?.assignedUser : undefined,
  );
  const onReplyMessage = useSelector((s: AppState) => s.chat.onReplyMessage);
  const onLoadingMore = useSelector((s: AppState) => s.chat.statusLoadMessage);
  const chatTickets = useSelector((s: AppState) => s.chat.chatTicket);
  const [flagCreatedTags, setFlagCreatedTags] = React.useState(false);
  const [onEditTag, setOnEditTag] = React.useState(false);
  const [idTagOnEdit, setIdTagOnEdit] = React.useState('');
  const tagSelectData = useSelector((s: AppState) => s.chat.tag);
  const [tags, setTags] = React.useState<{ id: string; name: string }[]>([]);
  const [value, setValue] = React.useState('');
  const [color] = React.useState('#4D4D4F');
  const [heightHeader, setHeightHeader] = React.useState(68);
  const [open, setOpen] = React.useState(true);
  const dispatch = useDispatch();
  const chatRoomMessages = useMemo(
    () =>
      messages &&
      (messageList ?? []).map(
        (mId): ChatMessageProps => ({
          id: messages[mId].id,
          type: messages[mId].message_type,
          createdAt: new Date(messages[mId].timestamp),
          file_name: messages[mId].file_name || '',
          text: messages[mId].text,
          outgoing: messages[mId].from_type !== 'Other',
          status: messages[mId].status,
          sender:
            messages[mId].from_type === 'System'
              ? 'System'
              : messages[mId].from_type === 'Web'
              ? 'Web'
              : messages[mId].from_type === 'Phone'
              ? 'Phone'
              : messages[mId].from_type === 'Customer Service'
              ? messages[mId].user.name || ''
              : messages[mId].contact?.name || messages[mId].contact?.tel || '',
          thumbnailUrl:
            messages[mId].message_type !== MessageType.Text && messages[mId].id.length < 20
              ? `${process.env.REACT_APP_API_URL}/media/${messages[mId].id}?access-token=${readRecord(
                  'accessToken',
                )}&subdomain-id=${subDomainId}`
              : messages[mId].message_type !== MessageType.Text && messages[mId].id.length > 20
              ? URL.createObjectURL(messages[mId].file)
              : undefined,
          revoke: messages[mId].revoke,
          captionImage: messages[mId].file_caption,
          download: messages[mId].id.length > 20 ? 'ongoing' : 'done',
          fileBig:
            messages[mId].id.length > 20 &&
            messages[mId].user.id > 16 &&
            messages[mId].message_type !== MessageType.Text,
          quotedMessage: messages[mId].quotedMessage && messages[mId].quotedMessage,
          contact: messages[mId].contact,
          failed: messages[mId].id.length > 20,
          file_mime_type: messages[mId].file_mime_type,
        }),
      ),
    [messageList, messages, subDomainId],
  );
  // const currentChatTicket = useMemo(() => {
  //   if (chatTickets && currentChatId) return chatTickets[currentChatId];
  // }, [currentChatId, chatTickets]);
  useEffect(() => {
    if (!tagSelectData) {
      dispatch(getTag());
    }
  }, [dispatch, tagSelectData]);

  useEffect(() => {
    if (currentChatRoom) {
      setTags(currentChatRoom?.tagList);
    }
  }, [currentChatRoom]);

  const onClickClosed = () => {
    dispatch(ResetMessageAction());
  };
  const handleActionAddTag = (action: string, id?: string) => {
    if (action === 'addNewTag') {
      setFlagCreatedTags(true);
      setOnEditTag(false);
    }
    if (action === 'edit-tag') {
      setFlagCreatedTags(true);
      const index = findIndex(tagSelectData, (item) => item.id === id);
      const itemEdit = tagSelectData?.[index];
      setValue(itemEdit?.name ?? '');
      setIdTagOnEdit(itemEdit?.id ?? '');
      setOnEditTag(true);
    }
    if (action === 'remove-tag') {
      const indexRemove = findIndex(tags, (item) => item.id === id);
      const data = [...tags];
      data.splice(indexRemove, 1);
      currentChatId && dispatch(assignTagTicket(currentChatId, data));
    }
  };
  const onSubmit = async (name: string, colorType: string, onEdit: boolean) => {
    if (!onEdit) {
      dispatch(createTag(name));
      setFlagCreatedTags(false);
      setValue('');
    } else {
      dispatch(updateTag(name, idTagOnEdit));

      currentChatId && dispatch(editTagSuccess(currentChatId, { name, id: idTagOnEdit }));
      setFlagCreatedTags(false);
    }
  };

  const deleteTagHandle = async (id: string) => {
    dispatch(deleteTag(id));
    const indexDelete = findIndex(tags, (item) => item.id === id);
    if (indexDelete !== -1) {
      setTags((state) => {
        const temp = [...state];
        temp.splice(indexDelete, 1);
        return temp;
      });
    }
  };
  const handleChecked = (id: string, hidden: boolean) => {
    const itemSelect = (tagSelectData ?? []).find((item) => item.id === id);
    const itemPush = { id: itemSelect?.id || '', name: itemSelect?.name || '' };
    const currentTags = [...tags];
    if (hidden) {
      const index = findIndex(currentTags, (item) => item.id === id);
      currentTags.splice(index, 1);
    } else {
      currentTags.push(itemPush);
    }
    currentChatId && dispatch(assignTagTicket(currentChatId, currentTags));
  };
  useEffect(() => {
    const divScroll = document.getElementById('current-chat') as HTMLDivElement;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const eventScroll = (e: any) => {
      const position = (e?.target as HTMLDivElement).scrollTop;
      const clientHeight = divScroll?.clientHeight;
      const scrollHeight = divScroll?.scrollHeight;
      if (position === 0 && scrollHeight > clientHeight) {
        dispatch(loadMore('top', scrollHeight));
      }
    };
    if (divScroll) {
      divScroll.addEventListener('scroll', eventScroll);
    }
    return () => {
      window.removeEventListener('event', eventScroll);
    };
  }, [dispatch, caseActionRoom]);
  // chatTickets && currentChatId && console.log(chatTickets[Number(currentChatId)]);

  useEffect(() => {
    setOpen(true);
  }, [chatTickets, currentChatId]);

  const closedUploadCase = () => {
    setCaseActionRoom(false);
    scrollToLastMessage(false, 0, 'current-chat');
  };

  useEffect(() => {
    Promise.all(
      Array.from(document.images)
        .filter((img) => !img.complete)
        .map(
          (img: HTMLImageElement) =>
            new Promise((resolve) => {
              // eslint-disable-next-line no-param-reassign
              img.onload = resolve;
              // eslint-disable-next-line no-param-reassign
              img.onerror = resolve;
            }),
        ),
    ).then(() => {
      setTimeout(() => {
        scrollToLastMessage(false, 0, 'current-chat');
      }, 1000);
    });
  }, [currentChatId]);

  return (
    <Flex
      width="100%"
      flexDirection="column"
      alignItems="stretch"
      height="100%"
      bgcolor="white"
      position="relative"
      justifyContent="space-between"
    >
      <Collapse in={open}>
        <Flex position="absolute" top={heightHeader} left="0px" zIndex={10} width="100%">
          {chatTickets &&
            currentChatId &&
            chatTickets[Number(currentChatId)] &&
            currentAssignedUser &&
            !caseActionRoom && (
              <Alert
                type="info"
                roundedCorner={false}
                variant="small"
                // onClose={() => closeTicketApi(chatTickets[Number(currentChatId)].id)}
                onClose={() => {
                  setOpen(false);
                }}
              >
                <span>
                  Ticket <b>#{chatTickets[Number(currentChatId)].id}</b> is assigned to &nbsp;
                  {currentAssignedUser.name}
                </span>
              </Alert>
            )}
          {chatTickets && currentChatId && !chatTickets[Number(currentChatId)] && !caseActionRoom && (
            <Alert
              type="warn"
              roundedCorner={false}
              variant="big"
              onClose={() => {
                setOpen(false);
              }}
            >
              Open Chat Ticket First
            </Alert>
          )}
        </Flex>
      </Collapse>
      {flagCreatedTags && (
        <CreateNewTag
          onClosed={() => setFlagCreatedTags(false)}
          onSubmit={onSubmit}
          value={value}
          color={color}
          setValue={(text: string) => setValue(text)}
          // setColor={(text: string) => setColor(text)}
          onEditTag={onEditTag}
        />
      )}
      {currentChat && (
        <>
          <ChatRoomHeader
            customer={{
              name: currentChat?.name ?? currentChat?.whatsapp_name,
              image: firstChars(currentChat?.name ?? currentChat?.whatsapp_name),
              source: CHAT_PLATFORM_TO_ICON_NAME[currentChat.platform],
            }}
            onlineStatus="online"
            tags={tags}
            handleActionAddTag={handleActionAddTag}
            tagSelectData={tagSelectData ?? []}
            deleteTag={deleteTagHandle}
            handleChecked={handleChecked}
            type={currentChat.type}
            setHeightHeader={(h: number) => setHeightHeader(h)}
          />

          <Flex
            flexDirection="column"
            height={`calc(100% - ${heightHeader}px)`}
            position="relative"
            borderRight="1px solid rgb(224 224 231)"
            justifyContent="space-between"
          >
            {currentChatId && onLoadingMore?.[currentChatId] && <LoadingMessage top={20} />}
            <Flex
              width="100%"
              flexDirection="column"
              flex={1}
              bgcolor="#F6F6F6"
              flexShrink={0}
              overflow="hidden auto"
              id="current-chat"
              borderBottom="1px solid #EBEFF1"
              // boxShadow="rgb(108, 108, 114,0.5) 0px 2px 2px 0px"
              height="calc(100% - 68px)"
            >
              {(chatRoomMessages ?? []).map((m, idx) => (
                // eslint-disable-next-line react/no-array-index-key
                <ChatMessage key={m.id ?? idx} {...m} />
              ))}

              {onReplyMessage && <ReplyMessage {...onReplyMessage} onClickClosed={onClickClosed} />}
            </Flex>
            <Composer onClickUploadIcon={() => setCaseActionRoom(true)} />
          </Flex>
        </>
      )}
      {caseActionRoom && <ChatRoomUpload onClickClosed={() => closedUploadCase()} heighHeader={heightHeader} />}
    </Flex>
  );
};
