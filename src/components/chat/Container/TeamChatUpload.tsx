import React from 'react';
import { Flex } from '../../common/Flex';
import TeamChatUploadImg from '../UploadImage/TeamChatUploadImg';
import TeamChatNav from '../UploadImage/TeamChatNav';

type PropsUploadTeam = {
  handleClosed: () => void;
  imageCurrent: File[];
  setImageCurrent: (file: File[]) => void;
};

const TeamChatUpload = ({ handleClosed, imageCurrent, setImageCurrent }: PropsUploadTeam) => {
  return (
    <Flex
      flexDirection="column"
      height="calc(100% - 64px)"
      top={0}
      right={0}
      zIndex={30}
      position="absolute"
      width="100%"
    >
      <TeamChatNav handleClosed={handleClosed} />
      <Flex flex={1} height="calc(100% - 68px)" borderBottom="1px solid #EEEEEE" bgcolor="#F9F9F9">
        <TeamChatUploadImg imageCurrent={imageCurrent} setImageCurrent={setImageCurrent} />
      </Flex>
    </Flex>
  );
};

export default TeamChatUpload;
