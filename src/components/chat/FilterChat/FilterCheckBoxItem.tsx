import React from 'react';
import { Checkbox, FormControl, FormControlLabel, FormGroup, Theme, useTheme } from '@material-ui/core';
import { Flex } from '../../common/Flex';
import { StyledCloseButton } from './FilterTagsItem';

export interface CheckBoxProps {
  id: number;
  label: string;
}

const FilterCheckBoxItem = ({
  data,
  title,
  checkedStatus,
}: {
  data: CheckBoxProps[];
  title: string;
  checkedStatus: (id: number) => void;
}) => {
  const theme = useTheme<Theme>();

  return (
    <Flex flexDirection="column" p="20px 15px 23px" borderBottom="1px solid #EEEEEE">
      <Flex justifyContent="space-between" alignItems="center" mb="15px">
        <Flex
          fontFamily={theme.typography.fontFamily}
          fontWeight="bold"
          fontSize={14}
          lineHeight="28px"
          color="#19191A"
          ml="20px"
        >
          {title}
        </Flex>
        <Flex justifyContent="flex-end" mr="19px">
          <StyledCloseButton style={{ fontSize: 20 }} />
        </Flex>
      </Flex>
      <FormControl component="fieldset">
        <FormGroup>
          <Flex>
            <Flex flexDirection="column" flexBasis="50%">
              {data.map((prop) => {
                return (
                  <Flex borderBottom="1px solid #EEEEEE" pl="20px" key={prop.id}>
                    <FormControlLabel
                      control={<Checkbox />}
                      label={prop.label}
                      // eslint-disable-next-line @typescript-eslint/ban-types
                      onChange={() => checkedStatus(prop.id)}
                    />
                  </Flex>
                );
              })}
            </Flex>
          </Flex>
        </FormGroup>
      </FormControl>
    </Flex>
  );
};
export default FilterCheckBoxItem;
