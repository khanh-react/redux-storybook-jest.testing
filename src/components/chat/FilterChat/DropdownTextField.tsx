import React from 'react';
import { Theme, useTheme, MenuItem } from '@material-ui/core';
import { StyledTextField } from './FilterTagsItem';

export interface InputTagProps {
  id: number;
  label: string;
}

const DropdownTextField = ({
  label,
  data,
  onSelect,
}: {
  label: string;
  data: InputTagProps[];
  onSelect: (event: number) => void;
}) => {
  const theme = useTheme<Theme>();
  return (
    <StyledTextField theme={theme} select label={label} variant="outlined">
      {data.map((prop) => {
        return (
          <MenuItem key={prop.id} value={prop.label} onClick={() => onSelect(prop.id)}>
            {prop.label}
          </MenuItem>
        );
      })}
    </StyledTextField>
  );
};

export default DropdownTextField;
