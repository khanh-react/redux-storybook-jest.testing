import React from 'react';
import DropdownTextField, { InputTagProps } from './DropdownTextField';
import { Flex } from '../../common/Flex';
import { StyledCloseButton } from './FilterTagsItem';
import InputTag from '../../common/InputTag';

const AssigneeDropdown = ({
  data,
  selectData,
  onSelect,
  onClose,
  name,
}: {
  data: InputTagProps[];
  selectData: { label: string; id: number }[];
  onSelect: (event: number) => void;
  onClose: (id: string) => void;
  name: string;
}) => {
  return (
    <Flex flexDirection="column" p="20px 32px 45px" borderBottom="1px solid #EEEEEE">
      <Flex justifyContent="flex-end" mb="25px" mr="3px">
        <StyledCloseButton style={{ fontSize: 20 }} />
      </Flex>
      <Flex flexDirection="column" mr="10px" flex={1}>
        <DropdownTextField label={name} data={data} onSelect={onSelect} />
      </Flex>
      <Flex flexWrap="wrap" mt="20px">
        {selectData.map((prop) => {
          return (
            <Flex key={prop.id} mr="10px">
              <InputTag hasCloseIcon label={prop.label} onClose={onClose} id={`${prop.id}`} />
            </Flex>
          );
        })}
      </Flex>
    </Flex>
  );
};

export default AssigneeDropdown;
