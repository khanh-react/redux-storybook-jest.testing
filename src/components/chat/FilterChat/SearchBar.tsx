import { Box } from '@material-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { updateCaseRightNav } from '../../../redux/action/chat';
import { BareInput } from '../../common/BareInput';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';

type Props = {
  handleText: (e: React.ChangeEvent<HTMLInputElement>) => void;
  textSearch: string;
};

export default function SearchBar({ handleText, textSearch }: Props) {
  const dispatch = useDispatch();
  return (
    <Flex alignItems="center" height={52} borderRight="1px solid #DCDCE4" borderBottom="1px solid #DCDCE4" p="10px">
      <PointerFlex width={30} height={30}>
        <Icon name="search" />
      </PointerFlex>
      <Box flex={1} mx={0.5}>
        <BareInput fullWidth placeholder="Search..." id="search-input" onChange={handleText} value={textSearch} />
      </Box>
      <PointerFlex width={30} height={30} onClick={() => dispatch(updateCaseRightNav('chat-room-filter'))}>
        <Icon name="filter" />
      </PointerFlex>
    </Flex>
  );
}
