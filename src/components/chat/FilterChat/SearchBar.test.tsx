import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';

import { configureStore } from '../../../redux/store';
import SearchBar from './SearchBar';

describe('With React Testing Library', () => {
  const mockStore = configureStore();
  const setup = () => {
    const utils = render(
      <Provider store={mockStore}>
        <SearchBar />
      </Provider>,
    );
    const input = utils.getByPlaceholderText('Search...') as HTMLInputElement;
    return {
      input,
      ...utils,
    };
  };

  test('It should render value=test', () => {
    const { input } = setup();
    fireEvent.change(input, { target: { value: 'test' } });
    expect(input.value).toBe('test');
  });
});
