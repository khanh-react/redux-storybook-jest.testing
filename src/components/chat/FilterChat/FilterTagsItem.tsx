import React from 'react';
import { Theme, useTheme, TextField } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import styled from 'styled-components';
import { Flex } from '../../common/Flex';

export const StyledCloseButton = styled(CloseIcon)`
  &:hover {
    cursor: pointer;
  }
  font-size: 20px;
`;

export const StyledTextField = styled(TextField)`
  ${({ theme }) => `
.MuiOutlinedInput-root {
  border-radius: 8px;
  width: 100%;
  height: 50px;
  &.Mui-focused fieldset {
    border-color: black;
  }
}
.MuiInputLabel-outlined {
  font-family: ${theme.typography.fontFamily};
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.4px;
  color: #0F0F10;
}
`}
`;

const FilterTagsItem = ({
  changeNameFilter,
}: {
  changeNameFilter: (e: React.ChangeEvent<HTMLInputElement>) => void;
}) => {
  const theme = useTheme<Theme>();
  return (
    <Flex flexDirection="column" p="20px 32px 45px" borderBottom="1px solid #EEEEEE">
      <Flex justifyContent="flex-end" mb="25px" mr="3px">
        <StyledCloseButton style={{ fontSize: 20 }} />
      </Flex>
      <Flex flexDirection="column" mr="10px" flex={1}>
        <StyledTextField theme={theme} variant="outlined" label="Name" onChange={changeNameFilter} />
      </Flex>
    </Flex>
  );
};

export default FilterTagsItem;
