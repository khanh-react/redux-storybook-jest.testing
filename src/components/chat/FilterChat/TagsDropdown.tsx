import React from 'react';
import DropdownTextField, { InputTagProps } from './DropdownTextField';
import { Flex } from '../../common/Flex';
import { autoColors } from '../../common/AutoSelectColors';
import ColorTag from './ColorTag';
import { StyledCloseButton } from './FilterTagsItem';

const TagsDropdown = ({
  data,
  selectData,
  onClose,
  onSelect,
}: {
  data: InputTagProps[];
  onSelect: (event: number) => void;
  selectData: { label: string; id: number }[];
  onClose: (id: string) => void;
}) => {
  return (
    <Flex flexDirection="column" p="20px 32px 45px" borderBottom="1px solid #EEEEEE">
      <Flex justifyContent="flex-end" mb="25px" mr="3px">
        <StyledCloseButton style={{ fontSize: 20 }} />
      </Flex>
      <Flex flexDirection="column" mr="10px" flex={1}>
        <DropdownTextField label="Tags" data={data} onSelect={onSelect} />
      </Flex>
      <Flex flexWrap="wrap" mt="20px">
        {selectData.map((prop, index) => {
          return (
            <Flex key={prop.id} mr="10px">
              <ColorTag label={prop.label} color={autoColors(index).textColor} onClose={onClose} id={`${prop.id}`} />
            </Flex>
          );
        })}
      </Flex>
    </Flex>
  );
};

export default TagsDropdown;
