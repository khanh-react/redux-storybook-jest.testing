import React from 'react';
import { Box, Theme, useTheme } from '@material-ui/core';
import CancelRoundedIcon from '@material-ui/icons/CancelRounded';
import styled from 'styled-components';
import { Flex } from '../../common/Flex';

const StyledTag = styled(Box)`
  display: inline-flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  border-style: solid;
  border-width: 0.8px;
  border-radius: 16.5px;
  height: 32px;
`;
const StyledCancelButton = styled(CancelRoundedIcon)`
  &:hover {
    cursor: pointer;
  }
  font-size: 18.8px;
  color: #263238;
`;
const ColorTag = ({
  label,
  color,
  id,
  onClose,
}: {
  label: string;
  color: string;
  id: string;
  onClose: (key: string) => void;
}) => {
  const theme = useTheme<Theme>();
  return (
    <StyledTag color={color} fontFamily={theme.typography.fontFamily} fontWeight="600" fontSize={13} lineHeight="19px">
      <Flex pl="15px" pr="5px">
        <span style={{ marginRight: '10px' }}>{label}</span>
        <StyledCancelButton onClick={() => onClose(id)} />
      </Flex>
    </StyledTag>
  );
};

export default ColorTag;
