import React from 'react';

import { Flex } from '../../common/Flex';
import ChatRoomFilters from '../Container/ChatRoomFilters';

export default {
  title: 'Chat/ChatRoomFilters',
  component: ChatRoomFilters,
};

export const Primary = () => {
  return (
    <Flex flexDirection="row" justifyContent="flex-end">
      <ChatRoomFilters />
    </Flex>
  );
};
