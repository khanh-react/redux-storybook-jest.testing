import React, { useState } from 'react';
import CloseIcon from '@material-ui/icons/Close';
import { Theme, Button, useTheme } from '@material-ui/core';
import styled from 'styled-components';
import { findIndex } from 'lodash';
import Wrapper from './styles';
import { Placeholder } from './types';
import { Flex, PointerFlex } from '../../common/Flex';
import Text from '../../common/Text';
import { IbTextField, StyledMenuItem } from '../../common/InputField';

const BottomButton = styled(Button)`
  ${({ theme }) => `
margin-left: 10px;
.MuiButton-label {
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: 600;
  font-size: 15px;
  line-height: 26px;
  text-transform: capitalize;
}
border-radius: 8px;
`}
`;
export const placeholders: Placeholder[] = [
  {
    key: '{username}',
    value: '客戶名稱',
  },
  {
    key: '{chat_name}',
    value: 'Chat ID (e.g. Phone No, Group No)',
  },
  {
    key: '{appointment_time}',
    value: '預約時間',
  },
  {
    key: '{appointment_date}',
    value: '預約日期',
  },
  {
    key: '{field_437}',
    value: '客戶公司名',
  },
  {
    key: '{field_460}',
    value: '客戶公司網站',
  },
  {
    key: '{field_439}',
    value: '公司電郵',
  },
  {
    key: '{field_Owner}',
    value: 'Owner',
  },
  {
    key: '{field_621}',
    value: '客戶計劃',
  },
];
export const AddQuickReply = ({
  categories,
  handleClosed,
  onSubmitCreate,
}: {
  categories: { name: string; id: string }[];
  handleClosed: () => void;
  onSubmitCreate: (shortcut: string, category: string, message: string) => void;
}) => {
  const [category, setCategory] = useState(categories[0].name);
  const [cateSelectCreated, setCateSelectCreated] = React.useState('');
  const [shortcut, setShortcut] = React.useState('');
  const [message, setMessage] = React.useState('');

  const currentCategoryDescription =
    'Write a keyboard shortcut for the message above. Your reply appears when this shortcut is the first word typed in a message.';

  const handleCategoriesChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCategory(event.target.value);
    const index = findIndex(categories, (o) => o.name === event.target.value);
    setCateSelectCreated(categories[index].id);
  };
  const theme = useTheme<Theme>();

  return (
    <Wrapper size="medium">
      <Flex className="header">
        <Text weight="bold" value="New quick reply" />
        <PointerFlex onClick={handleClosed}>
          <CloseIcon className="close" />
        </PointerFlex>
      </Flex>
      <Flex className="body" flexDirection="column">
        <Flex className="column" flexDirection="column">
          <IbTextField
            className="shortcut-name"
            value={shortcut}
            label="Shortcut"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setShortcut(e.target.value)}
          />
          <Text color="#98999f" size="xsm" value={currentCategoryDescription} />
        </Flex>
        <Flex className="column" flexDirection="column">
          <IbTextField
            className="select-category"
            error={false}
            disabled={false}
            readOnly={false}
            required={false}
            select
            label="Caterogy"
            value={category}
            onChange={handleCategoriesChange}
            variant="outlined"
          >
            {categories.map((cate) => (
              <StyledMenuItem key={cate.name} value={cate.name}>
                {cate.name}
              </StyledMenuItem>
            ))}
          </IbTextField>
        </Flex>
        <Flex className="column" flexDirection="column">
          <Text color="#0f0f10" weight="bold" value="Message" />
          <IbTextField
            className="message-content"
            multiline
            rows={5}
            value={message}
            placeholder="Text ..."
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setMessage(e.target.value)}
          />
        </Flex>
        <Flex className="column placeholder" flexDirection="column">
          <Text className="title" size="md" weight="bold" value="Placeholder" />
          {placeholders.map((p, index) => {
            return (
              // eslint-disable-next-line react/no-array-index-key
              <Flex className="p-row" key={`${p.key}_${index}`}>
                <Text className="p-txt-left" size="sm" value={p.key} />
                <Text className="p-txt-right" size="sm" value={p.value} color="#98999f" />
              </Flex>
            );
          })}
        </Flex>
        <Flex justifyContent="flex-end" mr="32px">
          <BottomButton theme={theme} variant="outlined" onClick={handleClosed}>
            Cancel
          </BottomButton>
          <BottomButton
            theme={theme}
            variant="contained"
            color="secondary"
            onClick={() => {
              message !== '' &&
                onSubmitCreate(shortcut, cateSelectCreated !== '' ? cateSelectCreated : categories[0].id, message);
            }}
          >
            Apply
          </BottomButton>
        </Flex>
      </Flex>
    </Wrapper>
  );
};

export default AddQuickReply;
