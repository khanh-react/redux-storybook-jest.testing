import styled from 'styled-components';
import FilterModal from '../../common/FilterModal';

export default styled(FilterModal)`
  width: 100%;
  .header {
    width: 100%;
    height: 72px;
    position: relative;
    background-color: #fff;
    padding: 26px 32px 26px 32px;
    border-bottom: 1px solid #eeeeee;
    .close {
      position: absolute;
      right: 10px;
      top: 20px;
    }
  }
  .body {
    width: 100%;
    height: calc(100% - 72px);
    padding: 41px;
    overflow-y: scroll;
    position: relative;
    .shortcut-name {
      input {
        font-size: 14px;
        font-weight: 600;
      }
    }
    .select-category {
      .MuiSelect-root {
        font-size: 14px;
        font-weight: 600;
      }
    }
    .column {
      margin-bottom: 28px;
    }
    .placeholder {
      padding: 13px 30px 13px 30px;
      border-radius: 10px;
      background-color: #fafafa;
      margin-bottom: 30px;
      .title {
        margin-bottom: 10px;
      }
      .p-row {
        padding-top: 10px;
        padding-bottom: 10px;
      }
      .p-txt-left {
        width: 60%;
      }
      .p-txt-right {
        width: 40%;
      }
    }
    .message-content {
      margin-top: 10px;
      .MuiInputBase-root {
        border-radius: 10px;
      }
    }
    &::-webkit-scrollbar {
      display: none;
    }
  }
`;
