import React from 'react';

import { AddQuickReply } from '.';

export default {
  title: 'Chat/AddQuickReply',
  component: AddQuickReply,
};

export const Default = () => {
  return (
    <AddQuickReply
      categories={[
        { name: '123', id: '321' },
        { name: 'hihi', id: '3212' },
      ]}
      handleClosed={() => null}
      onSubmitCreate={() => null}
    />
  );
};
