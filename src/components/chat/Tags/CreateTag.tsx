import React from 'react';
import styled from 'styled-components';
import { IbTextField } from '../../common/InputField';
import { Flex } from '../../common/Flex';

const StyledTextField = styled(IbTextField)`
  .MuiInputBase-input {
    font-size: 14px;
    font-weight: 600;
    line-height: 16px;
    color: #2e2e32;
  }
`;
type PropsCreate = {
  value: string;
  setValue: (text: string) => void;
  // color: string;
  // setColor: (text: string) => void;
};

const CreateTag = ({ value, setValue }: PropsCreate) => {
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setValue(event.target.value as string);
  };
  // const handleCheck = (arg: string) => {
  //   setColor(arg);
  // };
  return (
    <Flex flexDirection="column" p="33px 46px 0px 28px">
      <Flex>
        <StyledTextField
          disabled={false}
          error={false}
          readOnly={false}
          required
          label="Name"
          onChange={handleChange}
          value={value}
        />
      </Flex>
      {/* <Flex mt="26px">
          <StyledTextField
            label="Access"
            error={false}
            disabled={false}
            readOnly={false}
            required
            value={value}
            select
            onChange={handleChange}
          >
            <StyledMenuItem value="sales-team">Sales Team</StyledMenuItem>
          </StyledTextField>
        </Flex>
        <Flex fontWeight="bold" fontSize={14} lineHeight="18px" mt="31px">
          Highlight
        </Flex>
        <Flex flex={1} mt="16px">
          {data.map((c, idx) => (
            // eslint-disable-next-line react/no-array-index-key
            <CheckCircle key={idx} bgcolor={c} onClick={() => handleCheck(c)}>
              {color === c && <CheckIcon fontSize="small" />}
            </CheckCircle>
          ))}
        </Flex>
        <Flex mt="45px">
          <FormControlLabel control={<Checkbox />} label="Update existing contacts (with overwrite)" />
        </Flex> */}
    </Flex>
  );
};

export default CreateTag;
