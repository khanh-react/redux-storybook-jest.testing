import { Box, withStyles } from '@material-ui/core';
import React from 'react';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';
import { Flex, PointerFlex } from '../../common/Flex';
import { autoColors } from '../../common/AutoSelectColors';

export interface TagProps {
  name?: string;
  outline?: boolean;
  onRemove?: () => void;
  subdomain_id?: number;
  label?: string;
  id: string;
  handleAction: (action: string, id: string) => void;
  orderColor: number;
}

export const StyledBox = withStyles((theme) => ({
  root: {
    display: 'inline-flex',
    alignItems: 'center',
    borderRadius: 12,
    borderWidth: 0.8,
    padding: '4px 18px',
    boxSizing: 'border-box',
    MozBoxSizing: 'border-box',
    WebkitBoxSizing: 'border-box',
    '& span': {
      fontFamily: theme.typography.fontFamily,
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: 12,
      letterSpacing: 0.07,
    },
  },
}))(Box);

function TagTicket(props: TagProps) {
  const [bgColor, setBgColor] = React.useState('white');
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  return (
    <Flex marginX="2px" marginY="2px">
      <PointerFlex onClick={handleClick} borderRadius="12px">
        <StyledBox
          style={{
            color: autoColors(props.orderColor).textColor,
            borderStyle: props.outline ? 'solid' : 'none',
            backgroundColor: props.outline ? bgColor : autoColors(props.orderColor).backgroundColor,
          }}
          onMouseEnter={() => {
            props.outline && setBgColor(autoColors(props.orderColor).backgroundColor);
          }}
          onMouseLeave={() => {
            props.outline && setBgColor('white');
          }}
        >
          <span>{props.name}</span>
        </StyledBox>
      </PointerFlex>
      <IbMenu
        id="filter-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 30,
          horizontal: 'left',
        }}
      >
        <IbMenuItem
          onClick={() => {
            props.handleAction('edit-tag', props.id);
            handleClose();
          }}
        >
          Edit Tag
        </IbMenuItem>
        <IbMenuItem
          onClick={() => {
            props.handleAction('remove-tag', props.id);
            handleClose();
          }}
        >
          Remove
        </IbMenuItem>
      </IbMenu>
    </Flex>
  );
}

export default TagTicket;
