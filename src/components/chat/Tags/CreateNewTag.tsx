import { Button } from '@material-ui/core';
// import { BoxProps } from '@material-ui/core';
// import { Checkbox, FormControlLabel } from '@material-ui/core';
// import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import styled from 'styled-components';

import { Flex, PointerFlex } from '../../common/Flex';

// import { StyledMenuItem } from '../../common/InputField';
import { Nav } from '../../common/Nav';
import CreateTag from './CreateTag';

const StyledBottomButton = styled(Button)`
  ${({ theme }) => `
.MuiButton-label {
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: 600;
font-size: 15px;
line-height: 26px;
text-transform: none;
}
margin-left: 10px;
border-radius: 8px;
min-height: 38px;
width: 90px;
box-shadow: none;
`}
`;
// const CheckCircle = styled((props: BoxProps) => (
//   <PointerFlex
//     {...props}
//     borderRadius="50%"
//     color="white"
//     width="34px"
//     height="34px"
//     alignItems="center"
//     justifyContent="center"
//     mr="15px"
//   />
// ))``;
// const data = ['#4D4D4F', '#BE52F2', '#6979F8', '#0084F4', '#FF647C', '#00C48C', '#FFCF5C', '#FFA26B'];

type Props = {
  onClosed: () => void;
  onSubmit: (name: string, color: string, onEdit: boolean) => void;
  value: string;
  color: string;
  setValue: (text: string) => void;
  // setColor: (text: string) => void;
  onEditTag: boolean;
};
export const CreateNewTag = ({ onClosed, onSubmit, value, color, setValue, onEditTag }: Props) => {
  return (
    <>
      <Flex
        position="absolute"
        bottom={0}
        right={0}
        bgcolor="rgba(0,0,0,0.5)"
        zIndex={9}
        width="100vw"
        height="100vh"
        onClick={onClosed}
      />

      <Flex
        flexDirection="column"
        justifyContent="space-between"
        border="1px solid #E9E9F2"
        position="fixed"
        bottom={0}
        right={0}
        bgcolor="#fff"
        height="100vh"
        zIndex={10}
      >
        <Flex flexDirection="column" width={453} height={880} borderBottom="1px solid #E9E9F2">
          <Nav drawer title={onEditTag ? 'Edit tag' : 'Creat new tag'}>
            <PointerFlex alignItems="center" p="20px" color="#969696" onClick={onClosed}>
              <CloseIcon />
            </PointerFlex>
          </Nav>
          <CreateTag value={value} setValue={setValue} />
        </Flex>
        <Flex justifyContent="flex-end" mr="32px" mt="15px" mb="20px">
          <StyledBottomButton variant="outlined" onClick={onClosed}>
            Cancel
          </StyledBottomButton>
          <StyledBottomButton variant="contained" color="secondary" onClick={() => onSubmit(value, color, onEditTag)}>
            Apply
          </StyledBottomButton>
        </Flex>
      </Flex>
    </>
  );
};

export default CreateNewTag;
