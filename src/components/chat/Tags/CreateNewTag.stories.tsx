import React from 'react';

import { Flex } from '../../common/Flex';
import CreateNewTag from './CreateNewTag';

export default {
  title: 'Chat/CreateNewTag',
  component: CreateNewTag,
};

export const Primary = () => {
  return (
    <Flex flexDirection="row" justifyContent="flex-end">
      <CreateNewTag
        onClosed={() => null}
        onSubmit={() => null}
        value="string"
        color="string"
        setValue={() => null}
        onEditTag={false}
      />
    </Flex>
  );
};
