import React, { useCallback, useEffect } from 'react';
import { Mention, MentionsInput, OnChangeHandlerFunc } from 'react-mentions';
import { Avatar } from '@material-ui/core';
import Picker, { IEmojiData, SKIN_TONE_NEUTRAL } from 'emoji-picker-react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import useOnClickOutside from '../../common/useOnClickOutside';
import Icon from '../../common/Icon';
import { StyledMenuItem } from '../../common/InputField';
import { Flex, PointerFlex } from '../../common/Flex';
import firstChars from '../../../utils/firstChars';
import { validateFileAudio, validateFileImage, validateFileVideo } from '../../../utils/validateFileName';
import { AppState } from '../../../redux/store';
import { IbMenu, useIbMenu } from '../../common/Menu';
import { uploadFileInternalAct } from '../../../redux/action/chat';

const StyledEmoji = styled.div`
  .emoji-picker-react {
    position: absolute;
    bottom: 100%;
    right: 0px;
    z-index: 90;
  }
`;

const SuggestAvatar = styled(Avatar)`
  width: 20px;
  height: 20px;
  margin-right: 10px;
`;
const MentionNoOutline = styled(MentionsInput)`
  div > textarea {
    border: none;
    outline: none;
  }
  div > textarea:focus {
    border: none;
    outline: none;
  }
  div > textarea::placeholder {
    color: #a2a2a2;
  }
`;
const StyledMention = {
  suggestions: {
    list: {
      backgroundColor: 'white',
      fontSize: 14,
      bottom: 20,
      left: -10,
      position: 'absolute',
      borderRadius: 8,
      boxShadow: '0px 2px 10px rgba(175, 175, 175, 0.5)',
      minWidth: 160,
      overflow: 'hidden',
    },
    item: {
      padding: '5px 15px',
      '&focused': {
        backgroundColor: '#FFF8DA',
        fontWeight: 600,
      },
    },
  },
  '&singleLine': {
    highlighter: {
      padding: 1,
      border: 'none',
      '&focused': {
        border: 'none',
        outline: 'none',
      },
      width: '100%',
    },
    input: {
      padding: 1,
      border: 'none',
      '&focused': {
        border: 'none',
        outline: 'none',
      },
    },
  },
  width: '100%',
  border: 'none',
  '&focused': {
    border: 'none',
    outline: 'none',
  },
};

const Composer = (props: {
  submitSend: () => void;
  valueInput: string;
  setValueInput: (text: string) => void;
  clickAttachImage: () => void;
}) => {
  const dispatch = useDispatch();
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const roomInternal = useSelector((s: AppState) => s.chat.currentInternalChat);
  const [flagEmojiTab, setFlagEmojiTab] = React.useState(false);
  const emojiRef = React.useRef(null);
  const inputFileDocument = React.useRef<HTMLInputElement>(null);
  const statusLoading = useSelector((s: AppState) => s.chat.statusLoadingFileInternal);
  const allUser = useSelector((s: AppState) => s.subdomain.subdomainUser);
  useOnClickOutside(emojiRef, () => {
    setFlagEmojiTab(false);
  });
  const handleAction = (action: string) => () => {
    /* eslint-disable */
    console.log(action);
    handleClose();
  };

  useEffect(() => {
    const input = document.getElementById('inputField');
    if (input) {
      input.addEventListener('focus', function () {
        this.style.outline = 'none';
      });

      input.setAttribute('autocomplete', 'off');
    }
  }, []);

  const handleInputChange: OnChangeHandlerFunc = (event) => {
    props.setValueInput(event.target.value);
  };

  const newData = (allUser ?? []).map((prop) => {
    return {
      id: prop.name,
      display: prop.name,
    };
  });
  const nameData = Array.from(newData.reduce((a, o) => a.set(o.id, o), new Map()).values());

  const handleEnterSearch = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === 'Enter') {
      props.submitSend();
    }
  };

  // handle click emotion icon
  const onEmojiClick = useCallback((e: MouseEvent, emojiObject: IEmojiData) => {
    const inputField = document.getElementById('inputField') as HTMLInputElement;
    if (inputField) {
      const stateCurrent = inputField.value;
      props.setValueInput(`${stateCurrent}${emojiObject.emoji}`);
    }
  }, []);

  const onButtonClick = () => {
    inputFileDocument.current && inputFileDocument.current.click();
  };

  const onAddFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const inputCurrent = document.getElementById('file-input-attach') as HTMLInputElement;
    const inputEmpty = document.createElement('input');
    inputEmpty.type = 'file';
    if (e.target.files && inputCurrent && roomInternal) {
      const fileSelect = e.target.files[0];
      if (validateFileImage(fileSelect)) {
        dispatch(uploadFileInternalAct({ chatId: roomInternal, document: fileSelect, type: 'Image', fileCaption: '' }));
      } else if (validateFileAudio(fileSelect)) {
        dispatch(uploadFileInternalAct({ chatId: roomInternal, document: fileSelect, type: 'Voice', fileCaption: '' }));
      } else if (validateFileVideo(fileSelect)) {
        dispatch(uploadFileInternalAct({ chatId: roomInternal, document: fileSelect, type: 'Video', fileCaption: '' }));
      } else {
        dispatch(
          uploadFileInternalAct({ chatId: roomInternal, document: fileSelect, type: 'Document', fileCaption: '' }),
        );
      }
      inputCurrent.files = inputEmpty.files;
    }
  };

  return (
    <Flex minHeight={68} alignItems="center" flex={1} width="100%">
      <input
        type="file"
        ref={inputFileDocument}
        style={{ display: 'none' }}
        onChange={onAddFile}
        id="file-input-attach"
      />
      <PointerFlex position="relative">
        <PointerFlex mr="10px" ml={1} onClick={handleClick}>
          <Icon name="add" />
        </PointerFlex>
        {statusLoading && (
          <Flex position="absolute" left="50%" top="50%" style={{ transform: 'translate(-50%, -50%)' }}>
            <CircularProgress disableShrink />
          </Flex>
        )}

        <IbMenu
          id="filter-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          anchorOrigin={{
            vertical: -105,
            horizontal: -20,
          }}
        >
          <StyledMenuItem onClick={handleAction('attach')}>
            <PointerFlex onClick={onButtonClick}>
              <Icon name="attach" />
            </PointerFlex>
          </StyledMenuItem>
          <StyledMenuItem onClick={handleAction('image')}>
            <PointerFlex onClick={props.clickAttachImage}>
              <Icon name="image" />
            </PointerFlex>
          </StyledMenuItem>
        </IbMenu>
      </PointerFlex>
      <Flex
        flex={1}
        minHeight="36px"
        borderRadius={18}
        alignItems="center"
        border="1px solid #DEDEE6"
        width="calc(100% - 79px)"
        paddingX="10px"
      >
        <Flex flex={1} width="calc(100% - 26px)">
          <MentionNoOutline
            placeholder="@mention"
            value={props.valueInput}
            onChange={handleInputChange}
            style={StyledMention}
            onKeyPress={handleEnterSearch}
            id="inputField"
            singleLine
          >
            <Mention
              trigger="@"
              data={nameData}
              renderSuggestion={(suggestion) => (
                <Flex alignItems="center">
                  <SuggestAvatar alt="name avatar" src={firstChars(suggestion.display)} />
                  {suggestion.id}
                </Flex>
              )}
              markup=" @__id__"
              appendSpaceOnAdd
              displayTransform={(id, display) => `@${display}`}
            />
          </MentionNoOutline>
        </Flex>
        <PointerFlex position="relative" onClick={() => setFlagEmojiTab(true)}>
          <Icon name="emoji" />
          {flagEmojiTab && (
            <StyledEmoji ref={emojiRef}>
              <Picker
                disableAutoFocus
                disableSearchBar
                disableSkinTonePicker
                onEmojiClick={onEmojiClick}
                skinTone={SKIN_TONE_NEUTRAL}
              />
            </StyledEmoji>
          )}
        </PointerFlex>
      </Flex>
      <PointerFlex
        ml="5px"
        mr={1}
        onClick={() => {
          props.submitSend();
        }}
      >
        <Icon name="send" />
      </PointerFlex>
    </Flex>
  );
};

export default Composer;
