import React from 'react';
import CloseIcon from '@material-ui/icons/Close';
import { Flex, PointerFlex } from '../../common/Flex';

const TeamChatNavStarred = () => {
  const handleAction = (action: string) => () => {
    /* eslint-disable */
    console.log(action);
  };
  return (
    <Flex borderBottom="1px solid #EEEEEE" px="10px" height={68} alignItems="center">
      <PointerFlex alignItems="center" onClick={handleAction('close')}>
        <CloseIcon fontSize="small" />
      </PointerFlex>
      <Flex fontWeight="bold" fontSize={16} lineHeight="20px" ml="25px" color="0F0F10" flex={1}>
        Starred Message
      </Flex>
    </Flex>
  );
};

export default TeamChatNavStarred;
