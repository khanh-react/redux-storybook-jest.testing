import React from 'react';
import styled from 'styled-components';
import { formatDistanceToNow } from 'date-fns';
import { Avatar, Box } from '@material-ui/core';
import { Flex } from '../../common/Flex';

export interface StarredMessageProps {
  type: 'image' | 'text' | 'file';
  outgoing?: boolean;
  text?: string;
  thumbnailUrl?: string;
  createdAt: Date;
  sender: string;
  avatar: string;
  size?: string;
}
const TeamChatAvatar = styled(Avatar)`
  width: 28px;
  height: 28px;
`;
const StyledFileName = styled(Box)`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  margin-top: 5px;
  font-weight: 600;
  font-size: 15px;
  color: black;
  line-height: 22px;
  & > a {
  }
`;
const StarredMessage = ({
  type,
  outgoing,
  text,
  thumbnailUrl,
  createdAt,
  sender,
  avatar,
  size,
}: StarredMessageProps) => {
  return (
    <Flex flexDirection="column" width={300} p="15px 20px 20px 15px">
      <Flex justifyContent="space-between" alignItems="center">
        <Flex alignItems="center">
          <Flex mr="10px">
            <TeamChatAvatar alt="" src={avatar} />
          </Flex>
          <Flex fontWeight={600} fontSize={16} lineHeight="22px" color="#050506">
            {sender}
          </Flex>
        </Flex>
        <Flex fontSize={12} lineHeight="15px" color="#98999C">
          {formatDistanceToNow(createdAt, { addSuffix: true })}
        </Flex>
      </Flex>
      <Flex
        borderRadius={12}
        p="5px 10px"
        bgcolor={outgoing ? '#CFF8F9' : '#F6F6F6'}
        mt="5px"
        ml="38px"
        alignSelf="flex-start"
        boxShadow="0px 1px 3px rgba(0, 0, 0, 0.095389)"
        maxWidth={235}
      >
        {type === 'text' && (
          <Flex fontSize={14} lineHeight="20px">
            {text}
          </Flex>
        )}
        {type === 'image' && (
          <Flex flexDirection="column">
            <Flex mt="5px" height={185} width={140} borderRadius={4} overflow="hidden">
              <img src={thumbnailUrl} alt="" />
            </Flex>
            <Flex mt="5px" fontSize={12} lineHeight="16px" color="#77777A">
              {text}
            </Flex>
          </Flex>
        )}
        {type === 'file' && (
          <Flex flexShrink={0} justifyContent="center" p="10px 15px 10px 5px" width="100%">
            <Flex>
              <img src={thumbnailUrl} alt="" />
            </Flex>
            <Flex flexDirection="column" minWidth="0px" ml="10px" mt="-5px">
              <StyledFileName>
                <a href="/">{text}</a>
              </StyledFileName>
              <Box fontWeight={600} fontSize={12} color="#626262" lineHeight="17px">
                {size}
              </Box>
            </Flex>
          </Flex>
        )}
      </Flex>
    </Flex>
  );
};

export default StarredMessage;
