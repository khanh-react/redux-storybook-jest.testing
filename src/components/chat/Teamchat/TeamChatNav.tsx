import React from 'react';
import { useSelector } from 'react-redux';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Flex, PointerFlex } from '../../common/Flex';
import { clearInternalMessage } from '../../../redux/services/chat';
import { AppState } from '../../../redux/store';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';

const TeamChatNav = () => {
  const roomInternal = useSelector((s: AppState) => s.chat.currentInternalChat);
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const handleAction = async (action: string) => {
    handleClose();
    if (action === 'clear-messages') {
      try {
        roomInternal && (await clearInternalMessage(roomInternal));
      } catch (error) {
        throw new Error(error);
      }
    }
  };
  return (
    <Flex borderBottom="1px solid #EEEEEE" px="10px" height={68} alignItems="center">
      <Flex fontWeight="bold" fontSize={16} lineHeight="20px" ml="19px" color="0F0F10" flex={1} justifyContent="center">
        Team Chat
      </Flex>
      <PointerFlex alignItems="center" onClick={handleClick}>
        <MoreVertIcon />
      </PointerFlex>
      <IbMenu
        id="filter-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 30,
          horizontal: -120,
        }}
      >
        <IbMenuItem onClick={() => handleAction('clear-messages')}>Clear messages</IbMenuItem>
        <IbMenuItem onClick={() => handleAction('starred')}>Starred</IbMenuItem>
      </IbMenu>
    </Flex>
  );
};

export default TeamChatNav;
