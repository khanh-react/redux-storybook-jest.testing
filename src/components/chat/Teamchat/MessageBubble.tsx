/* eslint-disable no-nested-ternary */
import React from 'react';
import { Avatar, CardMedia, makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box/Box';
import Highlighter from 'react-highlight-words';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AudioPlayer from 'material-ui-audio-player';
import { formatDistanceToNow } from 'date-fns';
import { isExcelFile, isPdfFile, isPowerPointFile, isWordFile, isZipFile } from '../../../utils/validateFileName';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';
import { Flex, PointerFlex } from '../../common/Flex';
import { deleteInternalMessage, MessageType } from '../../../redux/services/chat';
import { AppState } from '../../../redux/store';
import { MessageProps } from '../Container/TeamChat';

const TeamChatAvatar = styled(Avatar)`
  width: 35px;
  height: 35px;
`;
const StyledFileName = styled(Box)`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  margin-top: 5px;
  font-weight: 600;
  font-size: 15px;
  color: black;
  line-height: 22px;
`;

const MessageBubble = ({
  type,
  outgoing,
  text,
  thumbnailUrl,
  createdAt,
  sender,
  avatar,
  zoomOutImage,
  file_name,
  id,
  senderId,
}: MessageProps) => {
  const roomInternal = useSelector((s: AppState) => s.chat.currentInternalChat);
  const userId = useSelector((s: AppState) => s.auth.userId);
  const allUser = useSelector((s: AppState) => s.subdomain.subdomainUser);
  const [hidden, setHidden] = React.useState(false);
  const handleMouseEvent = () => {
    setHidden(!hidden);
  };
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const handleAction = (action: string, idMess: string) => {
    if (roomInternal && idMess) {
      if (action === 'delete-message') {
        try {
          deleteInternalMessage(roomInternal, idMess);
        } catch (error) {
          throw new Error(error);
        }
      }
    }

    handleClose();
  };

  const useStyles = makeStyles(() => {
    return {
      root: {
        backgroundColor: 'transparent',
        borderRadius: '6px',
        padding: '0 10px',
      },
      loopIcon: {
        color: '#3f51b5',
        '&.selected': {
          color: '#0921a9',
        },
        '&:hover': {
          color: '#7986cb',
        },
      },
      playIcon: {
        color: 'black',
        '&:hover': {
          color: 'black',
        },
        fontSize: '25px',
      },
      replayIcon: {
        color: 'black',
        fontSize: '25px',
      },
      pauseIcon: {
        color: '#000',
        fontSize: '25px',
      },
      volumeIcon: {
        display: 'none',
      },
      progressTime: {
        color: 'rgba(0, 0, 0, 0.54)',
        position: 'relative',
        bottom: '-1px',
      },
      mainSlider: {
        color: '#3f51b5',
        '& .MuiSlider-rail': {
          color: '#7986cb',
        },
        '& .MuiSlider-track': {
          color: '#3f51b5',
        },
        '& .MuiSlider-thumb': {
          color: '#2C87D8',
        },
        marginLeft: '-5px',
      },
    };
  });

  return (
    <Flex ml="15px">
      <Flex mr="10px">
        <TeamChatAvatar alt="avatar-chat" src={avatar} />
      </Flex>
      <Flex
        width={216}
        flexDirection="column"
        borderRadius={20}
        bgcolor={outgoing ? '#CFF8F9' : '#F6F6F6'}
        p="10px 15px 15px"
        position="relative"
        onMouseEnter={handleMouseEvent}
        onMouseLeave={handleMouseEvent}
      >
        <Flex flex={1} alignItems="center" justifyContent="space-between">
          <Flex fontWeight={600} fontSize={12} lineHeight="22px" color="#353539">
            {sender}
          </Flex>
          <Flex fontSize={12} lineHeight="15px" color="#828389">
            {formatDistanceToNow(createdAt, { addSuffix: true })}
          </Flex>
        </Flex>
        {type === MessageType.Text && text && (
          <Highlighter
            searchWords={(allUser ?? []).map((h) => `@${h.name}`)}
            autoEscape
            textToHighlight={text}
            highlightStyle={{
              fontSize: '12px',
              color: '#2B71F3',
              fontWeight: 600,
              background: 'transparent',
              wordBreak: 'break-word',
            }}
            unhighlightStyle={{
              fontSize: '12px',
              color: '#77777A',
              fontWeight: 'normal',
              wordBreak: 'break-word',
            }}
          />
        )}

        {type === MessageType.Image && (
          <Flex
            onClick={() => {
              zoomOutImage && zoomOutImage(thumbnailUrl ?? '');
            }}
            flexDirection="column"
          >
            <Flex mt="5px" width="100%" borderRadius={4} overflow="hidden" justifyContent="center">
              <img src={thumbnailUrl} alt="" style={{ maxHeight: '185px', maxWidth: '100%' }} />
            </Flex>
            <Flex mt="5px" fontSize={12} lineHeight="16px" color="#77777A" style={{ wordBreak: 'break-word' }}>
              {text}
            </Flex>
          </Flex>
        )}

        {type === MessageType.Video && <CardMedia src={thumbnailUrl} component="video" image={thumbnailUrl} controls />}

        {type === MessageType.Voice && thumbnailUrl && (
          <AudioPlayer
            src={thumbnailUrl}
            elevation={0}
            width="100%"
            height="50px"
            useStyles={useStyles}
            time="double"
            spacing={1}
          />
        )}
        {type === MessageType.Document && file_name && (
          <Flex flexDirection="column">
            <Flex
              height={75}
              width="100%"
              borderRadius={4}
              alignItems="center"
              justifyContent="flex-start"
              paddingLeft="10px"
              paddingRight="10px"
            >
              <Flex>
                {isExcelFile(file_name) ? (
                  <img src="/static/Xlsx.svg" alt="Xlsx" style={{ marginRight: '15px' }} />
                ) : isWordFile(file_name) ? (
                  <img src="/static/doc.svg" alt="doc" style={{ width: '45px', marginRight: '5px' }} />
                ) : isZipFile(file_name) ? (
                  <img src="/static/zip.svg" alt="zip" style={{ width: '40px', marginRight: '15px' }} />
                ) : isPdfFile(file_name) ? (
                  <img src="/static/Pdf.svg" alt="pdf" style={{ marginRight: '15px' }} />
                ) : isPowerPointFile(file_name) ? (
                  <img src="/static/nolabel.svg" alt="Xlsx" style={{ width: '40px', marginRight: '10px' }} />
                ) : (
                  <img src="/static/nolabel.svg" alt="pdf" style={{ width: '40px', marginRight: '10px' }} />
                )}
              </Flex>
              <Flex flexDirection="column" minWidth="0px" style={{ width: 'calc(100% - 55px)' }}>
                <StyledFileName style={{ width: '100%' }}>
                  <a
                    href={thumbnailUrl}
                    target="_blank"
                    rel="noopener noreferrer"
                    style={{
                      color: '#000',
                      width: '100%',
                      whiteSpace: 'nowrap',
                      overflow: 'hidden',
                      textOverflow: 'ellipsis',
                    }}
                  >
                    {file_name}
                  </a>
                </StyledFileName>
              </Flex>
            </Flex>
          </Flex>
        )}

        {hidden && (
          <PointerFlex position="absolute" top={0} right={0}>
            <Flex
              aria-controls="customized-menu"
              aria-haspopup="true"
              onClick={handleClick}
              bgcolor="rgba(255, 255, 255, 0.5)"
              borderRadius="50%"
              alignItems="center"
              p="5px"
            >
              <ExpandMoreIcon />
            </Flex>
            <IbMenu
              id="filter-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              disableAutoFocusItem
              anchorOrigin={{
                vertical: 40,
                horizontal: -110,
              }}
            >
              <IbMenuItem onClick={() => handleAction('reply', id)}>Reply</IbMenuItem>
              <IbMenuItem onClick={() => handleAction('star-message', id)}>Star message</IbMenuItem>
              {userId === senderId && (
                <IbMenuItem onClick={() => handleAction('delete-message', id)}>Delete message</IbMenuItem>
              )}
            </IbMenu>
          </PointerFlex>
        )}
      </Flex>
    </Flex>
  );
};

export default MessageBubble;
