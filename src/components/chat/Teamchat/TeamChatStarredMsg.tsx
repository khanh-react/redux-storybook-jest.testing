import React from 'react';
import { Flex } from '../../common/Flex';
import StarredMessage, { StarredMessageProps } from './StarredMessage';
import TeamChatNavStarred from './TeamChatNavStarred';

const TeamChatStarredMsg = (props: { messages: StarredMessageProps[] }) => {
  return (
    <Flex flexDirection="column">
      <TeamChatNavStarred />
      <Flex flex={1} height={820} flexDirection="column">
        {props.messages.map((message, i) => (
          // eslint-disable-next-line react/no-array-index-key
          <Flex flexDirection="column" key={i} borderBottom="1px solid #EEEEEE ">
            <StarredMessage {...message} />
          </Flex>
        ))}
      </Flex>
    </Flex>
  );
};

export default TeamChatStarredMsg;
