import { addHours } from 'date-fns';
import React from 'react';

import { Flex } from '../../common/Flex';
import TeamChat, { MessageProps } from '../Container/TeamChat';
import { MessageType } from '../../../redux/services/chat';

export default {
  title: 'Chat/TeamChat',
  component: TeamChat,
};

const Messages: MessageProps[] = [
  {
    type: MessageType.Text,
    id: '123',
    senderId: 1,
    outgoing: false,
    text: 'This client may be missing the Identity Verification.',
    thumbnailUrl: 'https://via.placeholder.com/140x185.png',
    createdAt: addHours(new Date(), -4),
    sender: 'James  Corden',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
  },
  {
    type: MessageType.Text,
    id: '123',
    senderId: 1,
    outgoing: false,
    text: '@Rachel Chan Please take care of this account.',
    createdAt: addHours(new Date(), -4),
    sender: 'Wattana Carlson',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
  },
  {
    type: MessageType.Text,
    id: '123',
    senderId: 1,
    outgoing: true,
    text: '@John Carlson I’m on this.',
    createdAt: addHours(new Date(), -4),
    sender: 'Rachel Chan',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
  },
  {
    type: MessageType.Text,
    id: '123',
    senderId: 1,
    outgoing: false,
    text: '@John Carlson I’m on this.',
    createdAt: addHours(new Date(), -4),
    sender: 'Wattana Carlson',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
  },
  {
    type: MessageType.Image,
    id: '123',
    senderId: 1,
    outgoing: false,
    text: 'Here you go!',
    thumbnailUrl: 'https://via.placeholder.com/140x185.png',
    createdAt: addHours(new Date(), -4),
    sender: 'Wattana Carlson',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
  },
];

export const Primary = () => {
  return (
    <Flex justifyContent="flex-end">
      <TeamChat messages={Messages} />
    </Flex>
  );
};
