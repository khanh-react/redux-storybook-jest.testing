import { addHours } from 'date-fns';
import React from 'react';

import { Flex } from '../../common/Flex';
import TeamChatStarredMsg from './TeamChatStarredMsg';
import { StarredMessageProps } from './StarredMessage';

export default {
  title: 'Chat/TeamChatStarredMsg',
  component: TeamChatStarredMsg,
};
const Messages: StarredMessageProps[] = [
  {
    type: 'text',
    outgoing: false,
    text: 'Pw: BH2@3u8*091#u8J0',
    thumbnailUrl: 'https://via.placeholder.com/140x185.png',
    createdAt: addHours(new Date(), -4),
    sender: 'Wattana Carlson',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
  },
  {
    type: 'text',
    outgoing: true,
    text: 'Account id: 627821279492222',
    createdAt: addHours(new Date(), -4),
    sender: 'Rachel Chan',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
  },
  {
    type: 'text',
    outgoing: false,
    text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ullamcorper vulputate neque, id malesuada odio tempus non',
    createdAt: addHours(new Date(), -4),
    sender: 'Rachel Chan',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
  },
  {
    type: 'image',
    outgoing: false,
    text: 'Here you go!',
    thumbnailUrl: 'https://via.placeholder.com/140x185.png',
    createdAt: addHours(new Date(), -4),
    sender: 'Wattana Carlson',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
  },
  {
    type: 'file',
    outgoing: true,
    text: 'Product List-2020.pdf',
    thumbnailUrl: '/static/Pdf.svg',
    createdAt: new Date(),
    sender: 'Rachel Chan',
    avatar: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
    size: '11.67kb',
  },
];
export const Primary = () => {
  return (
    <Flex justifyContent="flex-end">
      <TeamChatStarredMsg messages={Messages} />
    </Flex>
  );
};
