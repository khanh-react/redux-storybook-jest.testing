import { Box, Button, TextField, Typography } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { useScrolling } from '../../../utils/useScrolling';
import { Flex, PointerFlex } from '../../common/Flex';
import InputTag from '../../common/InputTag';
import { addParticipants } from '../../../redux/action/chat';
import { CONTACTS } from '../../../sampleData/contact';
import { useStyles } from './Styles';
import Contact, { ContactListProps } from './Contact';

const StyledBottomButton = styled(Button)`
  .MuiButton-label {
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 26px;
    text-transform: none;
  }
  margin-left: 21.5px;
  border-radius: 8px;
  min-height: 38px;
  width: 145px;
  box-shadow: none;
`;

type Props = {
  onBack?: () => void;
  handleSelect: (user: { id: string; name: string; phoneNumber: string }) => void;
  onRemove: (user: string) => void;
  onSelect: { id: string; name: string; phoneNumber: string }[];
  currentChatId: string;
};

const ParticipantsList = ({ onBack, handleSelect, onRemove, onSelect, currentChatId }: Props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [hidden, setHidden] = useState(true);
  const scrollRef = useRef(null);
  const scrolling = useScrolling(scrollRef);
  const [listContact, setListContact] = useState<{ [key: string]: ContactListProps[] }>({});
  const handleMouseEnter = () => {
    setHidden(false);
  };
  const handleMouseLeave = () => {
    setHidden(true);
  };

  useEffect(() => {
    scrolling ? setHidden(false) : setHidden(true);
  }, [scrolling]);

  useEffect(() => {
    const group = CONTACTS.sort((a, b) => a.name.localeCompare(b.name)).reduce<{ [key: string]: ContactListProps[] }>(
      (acc, contact) => {
        const key = contact.name[0];
        const contacts = acc[key] ?? [];
        contacts.push(contact);
        return { ...acc, [key]: contacts };
      },
      {},
    );
    setListContact(group);
  }, []);

  const filterContact = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const group = CONTACTS.filter((fill) =>
      fill.name.toLocaleLowerCase().trim().includes(value.toLocaleLowerCase().trim()),
    )
      .sort((a, b) => a.name.localeCompare(b.name))
      .reduce<{ [key: string]: ContactListProps[] }>((acc, contact) => {
        const key = contact.name[0];
        const contacts = acc[key] ?? [];
        contacts.push(contact);
        return { ...acc, [key]: contacts };
      }, {});
    setListContact(group);
  };

  const alphabets = [];
  for (let i = 0; i < 26; i++) {
    alphabets.push((i + 10).toString(36).toUpperCase());
  }
  function scrollList(props: string) {
    const list = document.getElementById('scrollList');
    const targetLi = document.getElementById(props);
    if (list != null && targetLi != null) {
      list.scrollTop = targetLi.offsetTop - 300;
    }
  }
  return (
    <>
      <Flex alignItems="center" height={68} borderBottom="1px solid #EEEEEE" px="10px">
        <PointerFlex onClick={onBack} fontSize={22}>
          <ArrowBackIcon fontSize="inherit" />
        </PointerFlex>
        <Flex alignItems="center" justifyContent="space-between" flex={1}>
          <Flex fontWeight="bold" fontSize={16} lineHeight="20px" ml="19px" color="0F0F10" flex={1}>
            Add Participant
          </Flex>
        </Flex>
      </Flex>
      <Flex flexDirection="column" alignItems="center" justifyContent="space-between" pl="15px" width="300px">
        <Flex flexDirection="column" ml={-1} alignItems="center">
          <Flex flexWrap="wrap" ml="10px" alignSelf="flex-start">
            <Typography className={classes.Typography1}>Add Group Participants</Typography>
          </Flex>
          <Flex flexWrap="wrap" ml="25px">
            {onSelect.map((tag) => {
              return (
                <Flex m="5px" key={tag.id}>
                  <InputTag hasCloseIcon label={tag.phoneNumber} id={tag.id} onClose={onRemove} />
                </Flex>
              );
            })}
          </Flex>
          <form noValidate autoComplete="off">
            <TextField
              className={classes.textField}
              id="standard-basic"
              placeholder="Type contact name"
              InputProps={{
                classes: {
                  input: classes.textField,
                  underline: classes.textField,
                },
              }}
              onChange={filterContact}
            />
          </form>
          <Flex position="relative">
            <ul
              ref={scrollRef}
              id="scrollList"
              className={classes.ulHideScrollbar}
              style={{
                height: '600px',
                listStyleType: 'none',
                paddingInlineStart: 0,
                marginTop: 30,
                overflow: 'hidden',
                overflowY: 'scroll',
              }}
            >
              {Object.entries(listContact).map(([key, value]) => {
                return (
                  <Box key={key} id={key} ml="20px">
                    <Typography className={classes.Typography4}>{key}</Typography>
                    {value.map((item) => (
                      <Flex key={item.id}>
                        <Contact
                          id={item.id}
                          name={item.name}
                          image={item.image}
                          phoneNumber={item.phoneNumber}
                          source={item.source}
                          status={!onSelect.every((itemSelect) => itemSelect.id !== item.id) ? 'accept' : 'accepted'}
                          handleSelect={handleSelect}
                        />
                      </Flex>
                    ))}
                  </Box>
                );
              })}
            </ul>
            <Flex
              className="side-bar"
              onMouseEnter={handleMouseEnter}
              onMouseLeave={handleMouseLeave}
              width="25px"
              height="80vh"
              overflow="hidden"
              position="absolute"
              right={30}
              top={0}
            >
              {!hidden && (
                <Flex flexDirection="column" alignItems="center" justifyContent="center" mt="-50px">
                  {alphabets.map((alphabet) => {
                    return (
                      <li
                        key={alphabet}
                        className={classes.sidebarStyle}
                        onClick={() => scrollList(alphabet)}
                        role="menuitem"
                      >
                        {alphabet}
                      </li>
                    );
                  })}
                </Flex>
              )}
            </Flex>
          </Flex>
        </Flex>
      </Flex>
      <Flex alignItems="center" justifyContent="center">
        <Flex mr="12px">
          <StyledBottomButton
            color="secondary"
            variant="contained"
            startIcon={<ControlPointIcon />}
            onClick={() => {
              currentChatId &&
                dispatch(
                  addParticipants(
                    Number(currentChatId),
                    onSelect.map((contact) => contact.phoneNumber),
                  ),
                );
            }}
          >
            Participants
          </StyledBottomButton>
        </Flex>
      </Flex>
    </>
  );
};
export default ParticipantsList;
