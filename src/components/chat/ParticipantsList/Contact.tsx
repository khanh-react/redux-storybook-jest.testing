import React from 'react';
import { Avatar, Box, IconButton, Typography } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import Icon from '../../common/Icon';
import { useStyles } from './Styles';

export type ContactListProps = {
  id: string;
  name: string;
  phoneNumber: string;
  image: string;
  source: 'line' | 'whatsapp';
  status: 'accept' | 'accepted';
  handleSelect?: (user: { id: string; name: string; phoneNumber: string }) => void;
};

const Contact = (props: ContactListProps) => {
  const classes = useStyles();

  return (
    <li
      style={{
        overflow: 'hidden',
        borderBottom: '1px solid #ECEFF1',
        marginBottom: 10,
        paddingBottom: 15,
        width: 300,
        cursor: 'pointer',
      }}
    >
      <Box
        className={classes.avatar}
        display="flex"
        alignItems="flex-start"
        onClick={
          () =>
            props.handleSelect && props.handleSelect({ id: props.id, name: props.name, phoneNumber: props.phoneNumber })
          // eslint-disable-next-line react/jsx-curly-newline
        }
      >
        <Box position="relative">
          <Avatar alt={props.name} src={props.image} />
          <Box
            bottom={-6}
            right={0}
            position="absolute"
            display="flex"
            borderRadius={10}
            boxShadow="0px 0px 4px rgba(0, 0, 0, 0.233419)"
          >
            <Icon name={props.source} />
          </Box>
        </Box>
        <div style={{ marginLeft: 15 }}>
          <Typography className={classes.Typography2}>{props.name}</Typography>
          <Typography className={classes.Typography3}>{props.phoneNumber}</Typography>
        </div>
        <div style={{ marginLeft: 'auto', marginTop: 10, marginRight: 25 }}>
          {props.status === 'accept' ? (
            <Box display="flex" alignItems="center" justifyContent="center">
              <IconButton className={classes.CheckedIcon}>
                <CheckIcon />
              </IconButton>
            </Box>
          ) : (
            <Box />
          )}
        </div>
      </Box>
    </li>
  );
};
export default Contact;
