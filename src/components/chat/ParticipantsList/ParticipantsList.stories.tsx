import React from 'react';

import { Flex } from '../../common/Flex';
import ParticipantsList from './ParticipantsList';

export default {
  title: 'Chat/ParticipantsList',
  component: ParticipantsList,
};

export const Primary = () => {
  return (
    <Flex height="80vh" justifyContent="flex-end" width="100%">
      <ParticipantsList
        onBack={() => {
          return null;
        }}
        onRemove={() => {
          return null;
        }}
        onSelect={[{ id: '1', name: '2', phoneNumber: '124324343' }]}
        handleSelect={() => {
          return null;
        }}
        currentChatId="999"
      />
    </Flex>
  );
};
