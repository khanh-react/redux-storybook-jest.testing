import React from 'react';
import { useTheme } from '@material-ui/styles';
import { Theme } from '@material-ui/core';
import { Flex } from '../../common/Flex';

const Count = (props: { countUnread: number }) => {
  const theme = useTheme<Theme>();
  return (
    <Flex
      bgcolor="error.main"
      borderRadius={10}
      height={20}
      width={20}
      color="white !important"
      fontWeight="bold"
      fontSize={12}
      lineHeight="20px"
      fontFamily={theme.typography.fontFamily}
      alignItems="center"
      justifyContent="center"
    >
      {props.countUnread !== 0 && props.countUnread}
    </Flex>
  );
};

export default Count;
