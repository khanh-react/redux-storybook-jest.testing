import { createMount } from '@material-ui/core/test-utils';
import toJson from 'enzyme-to-json';
import React from 'react';

import { MockedTheme } from '../../../TestContext';
import * as Stories from './ChatList.stories';

describe('<IbMenu/>', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let mount: any;

  beforeEach(() => {
    mount = createMount();
  });

  afterEach(() => {
    mount.cleanUp();
  });

  it('should work', () => {
    const wrapper = mount(
      <MockedTheme>
        <Stories.Default />
      </MockedTheme>,
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
