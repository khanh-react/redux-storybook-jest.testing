import { Box } from '@material-ui/core';
import React from 'react';
import { ChatPlatform, MessageStatus } from '../../../redux/services/chat';
import { ChatList } from '../Container/ChatList';
import ChatListItem, { ChatListItemProps } from './ChatListItem';

export default {
  title: 'Chat/ChatList',
  component: ChatList,
};
const data: ChatListItemProps[] = [
  {
    id: '1',
    name: 'Michael N.',
    lastMessage: 'Hi, there.',
    description: 'Incoming chat request',
    status: 'incoming',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
    platform: ChatPlatform.WhatsApp,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    unreadCount: 0,
  },
  {
    id: '2',
    name: 'Kate Halligan',
    lastMessage: 'Thanks, I will check it for you now.',
    lastMessageStatus: MessageStatus.Read,
    description: '+85251115111',
    unreadCount: 3,
    status: 'assigned',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=KH',
    platform: ChatPlatform.WhatsApp,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
  },
  {
    id: '3',
    name: 'Julian Gruber',
    description: 'GT Group fb',
    lastMessage: 'That’s great! Thank you so much!',
    lastMessageStatus: MessageStatus.Read,
    status: 'assigned',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=JG',
    platform: ChatPlatform.Facebook,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    unreadCount: 0,
  },
  {
    id: '4',
    name: 'Johnny Yeung ',
    description: 'KB Shop',
    lastMessage: 'Product List_2938910.pdf',
    lastMessageStatus: MessageStatus.Newly,
    status: 'unassigned',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=JG',
    platform: ChatPlatform.Line,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    unreadCount: 0,
  },
  {
    id: '5',
    name: 'Kate Tsui',
    description: '+852  5377 8888',
    lastMessage: 'Ok. Thanks!',
    unreadCount: 1,
    status: 'warning',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=JG',
    platform: ChatPlatform.Line,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
  },
  {
    id: '6',
    name: 'William Leung',
    description: 'GT Group fb',
    lastMessage: 'newproduct_8292681315.jpg',
    lastMessageStatus: MessageStatus.Failed,
    status: 'assigned',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=WL',
    platform: ChatPlatform.Facebook,
    unreadCount: 0,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
  },
  {
    id: '11',
    name: 'Michael N.',
    lastMessage: 'Hi, there.',
    lastMessageStatus: MessageStatus.Sent,
    description: 'Incoming chat request',
    status: 'unassigned',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=MN',
    platform: ChatPlatform.WhatsApp,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    unreadCount: 0,
  },
  {
    id: '12',
    name: 'Kate Halligan',
    lastMessage: 'Thanks, I will check it for you now.',
    lastMessageStatus: MessageStatus.Read,
    description: '+85251115111',
    unreadCount: 3,
    status: 'assigned',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=KH',
    platform: ChatPlatform.WhatsApp,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
  },
  {
    id: '13',
    name: 'Julian Gruber',
    description: 'GT Group fb',
    lastMessage: 'That’s great! Thank you so much!',
    lastMessageStatus: MessageStatus.Read,
    status: 'assigned',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=JG',
    platform: ChatPlatform.Facebook,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    unreadCount: 0,
  },
  {
    id: '14',
    name: 'Johnny Yeung ',
    description: 'KB Shop',
    lastMessage: 'Product List_2938910.pdf',
    lastMessageStatus: MessageStatus.Newly,
    status: 'unassigned',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=G',
    platform: ChatPlatform.Line,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    unreadCount: 0,
  },
  {
    id: '15',
    name: 'Kate Tsui',
    description: '+852  5377 8888',
    lastMessage: 'Ok. Thanks!',
    unreadCount: 1,
    status: 'warning',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=JG',
    platform: ChatPlatform.Line,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
  },
  {
    id: '16',
    name: 'William Leung',
    description: 'GT Group fb',
    lastMessage: 'newproduct_8292681315.jpg',
    lastMessageStatus: MessageStatus.Failed,
    status: 'assigned',
    image: 'https://via.placeholder.com/150/858585/FFFFFF/?text=WL',
    platform: ChatPlatform.Facebook,
    updatedAt: new Date('2020-09-04T15:40:57.640Z'),
    unreadCount: 0,
  },
];

export const Default = () => {
  return (
    <Box width={348}>
      {data.map((props) => (
        <ChatListItem {...props} key={props.id} />
      ))}
    </Box>
  );
};
