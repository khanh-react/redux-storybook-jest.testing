import React from 'react';
import { format } from 'date-fns';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Avatar, Box, Theme, Typography } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import { IbMenu, IbMenuItem, useIbMenu } from '../../common/Menu';
import { createTheme } from '../../../theme';
import Icon from '../../common/Icon';
import { Flex } from '../../common/Flex';
import Count from './Count';
import { BasicUser, ChatPlatform, MessageStatus } from '../../../redux/services/chat';
import { CHAT_PLATFORM_TO_ICON_NAME, MESSAGE_STATUS_TO_ICON_NAME } from '../../../redux/type/chat/iconChat';

export const useStyles = makeStyles<Theme>(
  () => ({
    root: {
      height: 95,
      textTransform: 'none',
      overflow: 'hidden',
      '& .more': {
        display: 'none',
      },
      '&:hover .more, & .more__active': {
        display: 'block',
      },
    },
    active: {
      backgroundColor: '#FFF8DA',
      '& .source': {
        borderRadius: 10,
        boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.233419)',
      },
    },
    avatar: {
      marginRight: '18px',
    },
    incomingAction: {
      '&>*': {
        margin: 5,
      },
      height: '100%',
      paddingRight: 17,
      borderRight: '#F16F63 5px solid',
      marginLeft: '-50px',
    },
  }),
  { defaultTheme: createTheme() },
);

export type ChatListItemProps = {
  id: string;
  name: string;
  description: string;
  image: string;
  unreadCount: number;
  platform: ChatPlatform;
  lastMessage?: string;
  updatedAt?: Date;
  lastMessageStatus?: MessageStatus;
  // status: 'assigned' | 'unassigned' | 'incoming' | 'warning';
  status: string;
  assignedUser?: BasicUser;
  // chatTicket?: ChatTicket;
  active?: boolean;
  archived?: boolean;
  onAction?: (action: string, chat_id: string) => void;
  read?: number;
};

const ChatListItem = (props: ChatListItemProps) => {
  const styles = useStyles();
  const theme = useTheme<Theme>();
  const { anchorEl, handleClick, handleClose } = useIbMenu();
  const handleAction = (action: string) => () => {
    if (props.onAction) {
      props.onAction(action, props.id);
    }
    handleClose();
  };

  return (
    <Box
      display="flex"
      flexDirection="row"
      alignItems="stretch"
      textAlign="left"
      width="100%"
      className={`${styles.root} ${props.active ? styles.active : ''}`}
      padding="0 18px"
    >
      <Box className={styles.avatar} display="flex" alignItems="center" justifyContent="center">
        <Box position="relative">
          <Avatar alt={props.name} src={props.image} />
          <Box bottom={-8} right={-4} position="absolute" display="flex">
            <Icon name={CHAT_PLATFORM_TO_ICON_NAME[props.platform] || 'whatsapp'} size={24} />
          </Box>
        </Box>
      </Box>
      <Box
        flex={1}
        display="flex"
        flexDirection="column"
        alignItems="stretch"
        justifyContent="flex-start"
        width="calc(100% - 58px)"
      >
        <Box display="flex" alignItems="flex-start" justifyContent="space-between" mt="15px" mb="-3px">
          <Typography
            style={{
              width: 'calc(100% - 70px)',
              display: 'block',
              marginRight: '10px',
              whiteSpace: 'nowrap',
              textOverflow: 'ellipsis',
              overflow: 'hidden',
            }}
          >
            {props.name}&nbsp;
          </Typography>
          {props.updatedAt && props.status !== 'incoming' && (
            <Typography variant="subtitle1" style={{ width: '60px', display: 'block' }}>
              {format(props.updatedAt, 'iiii')}
            </Typography>
          )}
        </Box>
        <Box display="flex" alignItems="center">
          {!!props.lastMessageStatus && MESSAGE_STATUS_TO_ICON_NAME[props.lastMessageStatus] && (
            <Box mr={0.5} display="flex">
              {/* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */}
              <Icon name={MESSAGE_STATUS_TO_ICON_NAME[props.lastMessageStatus]!} />
            </Box>
          )}
          <Flex justifyContent="space-between" flex={1}>
            <Flex minWidth={0} width={170}>
              <Box
                fontSize={14}
                color="#363638"
                whiteSpace="nowrap"
                overflow="hidden"
                textOverflow="ellipsis"
                position="sticky"
              >
                {props.lastMessage}&nbsp;
              </Box>
            </Flex>
            <Box display="flex" alignItems="center">
              {props.status === 'warning' && (
                <Box mr="5px" display="flex">
                  <Icon name="warning" />
                </Box>
              )}
              {props.unreadCount > 0 && <Count countUnread={props.unreadCount} />}
              {props.read === 0 && !props.unreadCount && <Count countUnread={props.unreadCount} />}
              {props.status !== 'incoming' && (
                <>
                  <Box
                    className={`more ${anchorEl ? 'more__active' : ''}`}
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    onClick={handleClick}
                    mb="-10px"
                  >
                    <ExpandMoreIcon />
                  </Box>
                  <IbMenu
                    id={`action-menu-${props.id}`}
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                  >
                    {props.archived ? (
                      <IbMenuItem onClick={handleAction('unArchive')}>Unarchive chat</IbMenuItem>
                    ) : (
                      <IbMenuItem onClick={handleAction('archive')}>Archive</IbMenuItem>
                    )}
                    {props.read === 0 ? (
                      <IbMenuItem onClick={handleAction('markRead')}>Mark As Read</IbMenuItem>
                    ) : (
                      <IbMenuItem onClick={handleAction('markUnRead')}>Mark as Unread</IbMenuItem>
                    )}
                    <IbMenuItem onClick={handleAction('delete')}>Delete</IbMenuItem>
                  </IbMenu>
                </>
              )}
            </Box>
          </Flex>
        </Box>
        <Box display="flex" justifyContent="space-between" alignItems="flex-start">
          <Typography variant="subtitle1">{props.description}</Typography>
          <Flex flexDirection="row">
            {!props.assignedUser && (props.status === 'Open' || props.status === '') && (
              <Box
                border="1px #FF5F51 solid"
                color="#FF5F51"
                fontSize={12}
                lineHeight="20px"
                borderRadius={10}
                fontWeight={600}
                fontFamily={theme.typography.fontFamily}
                px={1}
              >
                Unassign
              </Box>
            )}
            {props.archived && (
              <Box
                border="1px #98999F solid"
                color="#98999F"
                fontSize={12}
                lineHeight="20px"
                borderRadius={10}
                fontWeight={600}
                fontFamily={theme.typography.fontFamily}
                px={1}
                marginLeft="3px"
              >
                Archived
              </Box>
            )}
          </Flex>
        </Box>
      </Box>
      <Box>
        {props.status === 'incoming' ? (
          <Box className={styles.incomingAction} display="flex" alignItems="center" justifyContent="center">
            <Box onClick={handleAction('accept')}>
              <Icon name="correct" />
            </Box>
            <Box onClick={handleAction('reject')}>
              <Icon name="cancel" />
            </Box>
          </Box>
        ) : (
          <Box />
        )}
      </Box>
    </Box>
  );
};

export default ChatListItem;
