import { Box, Button, Card, CardContent, Link, TextField, Theme, Typography } from '@material-ui/core';
import { Formik, FormikHelpers } from 'formik';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import styled from 'styled-components';
import * as Yup from 'yup';

import { AppState } from '../../redux/store';
import { loginRequest } from '../../redux/action/login';

const StyledLoginCard = styled(Box)`
  display: flex;
  flex-direction: column;
`;

const HeaderText = styled(Typography)`
  ${({ theme }: { theme: Theme }) => `
    margin-top: ${theme.spacing(8)}px;
  `}
`;

const SignupHint = styled.span`
  ${({ theme }: { theme: Theme }) => `
    color: ${theme.palette.info.main};
  `}
`;

const TextAlignCenterBox = styled(Box)`
  text-align: center;
`;

const validationSchema = Yup.object({
  email: Yup.string().email('Enter a valid email').required('Email is required'),
  password: Yup.string().min(8, 'Password must contain at least 8 characters').required('Enter your password'),
});

interface Values {
  email: string;
  password: string;
}

const LoginForm = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const loggedIn = useSelector((state: AppState) => state.auth.loaded);

  React.useEffect(() => {
    if (loggedIn) {
      // history.push('/cms');
      history.go(0);
    }
  }, [loggedIn, history]);
  return (
    <Formik
      initialValues={{
        email: '',
        password: '',
      }}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting }: FormikHelpers<Values>) => {
        dispatch(loginRequest(values));
        setSubmitting(false);
      }}
    >
      {(props) => {
        const { values, touched, errors, isSubmitting, handleChange, handleBlur, handleSubmit } = props;
        return (
          <Box mx={8} width={0.8} display="flex" flexDirection="column">
            <form onSubmit={handleSubmit}>
              <HeaderText variant="h1" color="textPrimary">
                Sign in
              </HeaderText>
              <Box mt={6}>
                <TextField
                  fullWidth
                  error={Boolean(errors.email)}
                  label="Email"
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={errors.email && touched.email && errors.email}
                  margin="normal"
                  variant="outlined"
                  color="secondary"
                />
                <TextField
                  fullWidth
                  label="Password"
                  margin="normal"
                  name="password"
                  type="password"
                  variant="outlined"
                  color="secondary"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  helperText={errors.password && touched.password && errors.password}
                />
                <Box mt={1} display="flex" flexDirection="column" alignItems="flex-end">
                  <Link component={RouterLink} to="/forgetPassword" variant="body2" color="textSecondary">
                    Forgot password?
                  </Link>
                </Box>
              </Box>
              <Box mt={5}>
                <Button fullWidth variant="contained" color="secondary" type="submit" disabled={isSubmitting}>
                  Submit
                </Button>
              </Box>
              <TextAlignCenterBox mt={8}>
                <Link component={RouterLink} to="/register" variant="body2" color="textSecondary">
                  Don&apos;t have an account?
                  <SignupHint>Sign up</SignupHint>
                </Link>
              </TextAlignCenterBox>
            </form>
          </Box>
        );
      }}
    </Formik>
  );
};

const LoginCard = () => {
  return (
    <StyledLoginCard width={[1, 0.8, 0.6, 0.5]} component={Card} color="white">
      <CardContent>
        <LoginForm />
      </CardContent>
    </StyledLoginCard>
  );
};

export default LoginCard;
