import jwtDecode, { JwtPayload } from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';

import { readRecord } from '../../utils/localStorageService';

const PrivateRoute = (props: { path: string }) => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  useEffect(() => {
    const token = readRecord('accessToken');
    if (token) {
      const tokenExpiration = jwtDecode<JwtPayload>(token).exp;
      const dateNow = new Date();
      if (tokenExpiration && tokenExpiration < dateNow.getTime() / 1000) {
        setIsAuthenticated(false);
      } else {
        setIsAuthenticated(true);
      }
    } else {
      setIsAuthenticated(false);
    }
  }, [isAuthenticated]);
  return isAuthenticated ? <Redirect to={props.path} /> : <Redirect to="/login" />;
};

export default PrivateRoute;
