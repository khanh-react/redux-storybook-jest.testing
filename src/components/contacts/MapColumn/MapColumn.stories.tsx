import React from 'react';

import { Flex } from '../../common/Flex';
import MapColumn from './MapColumn';

export default {
  title: 'Contacts/MapColumn',
  component: MapColumn,
};

export const Primary = () => {
  return (
    <Flex flexDirection="row" justifyContent="flex-end" height="90vh">
      <MapColumn />
    </Flex>
  );
};
