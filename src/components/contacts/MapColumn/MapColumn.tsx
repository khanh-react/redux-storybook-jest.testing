import { Button, Checkbox, FormControlLabel, Theme, useTheme } from '@material-ui/core';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import React from 'react';
import styled from 'styled-components';

import { Flex, PointerFlex } from '../../common/Flex';
import { IbTextField, StyledMenuItem } from '../../common/InputField';
import { Nav } from '../../common/Nav';

const StyledArrowForward = styled(ArrowForwardIosIcon)`
  &:hover {
    cursor: pointer;
  }
  font-size: 18px;
`;
const StyledBottomButton = styled(Button)`
  ${({ theme }) => `
.MuiButton-label {
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: 600;
font-size: 15px;
line-height: 26px;
text-transform: none;
}
margin-left: 21.5px;
border-radius: 8px;
min-height: 38px;
box-shadow: none;
`}
`;
const StyledTitle = styled(Flex)`
  ${({ theme }) => `
align-items: center;  
margin-bottom: 20px;
& > span {
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: 600;
font-size: 14px;
line-height: 28px;
color: #19191A;
}

  `}
`;

const StyledSelectField = styled(IbTextField)`
  .MuiOutlinedInput-root {
    border-radius: 8px;
    width: 200px;
    height: 34px;
    &.Mui-focused fieldset {
      border-color: black;
    }
  }
`;
const DATA = [
  {
    id: 1,
    FirstName: ['Chen', '1stName1', '1stName2', '1stName3'],
  },
  {
    id: 2,
    LastName: ['LastName1', 'LastName2', 'LastName3', 'LastName4'],
  },
  {
    id: 3,
    FirstName: ['Chen', '1stName1', '1stName2', '1stName3'],
  },
  {
    id: 4,
    LastName: ['LastName1', 'LastName2', 'LastName3', 'LastName4'],
  },
  {
    id: 5,
    FirstName: ['Chen', '1stName1', '1stName2', '1stName3'],
  },
];

const FileRow = (props: { id: number; FirstName?: string[]; LastName?: string[] }) => {
  const theme = useTheme<Theme>();
  const columnName = Object.keys(props)[1];
  const firstRow = props.FirstName ? props.FirstName[0] : props.LastName && props.LastName[0];
  const [state, setState] = React.useState(false);
  const [value, setValue] = React.useState('none');
  const handleSelectChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setValue(event.target.value as string);
    setState(!state);
  };
  const handleChecked = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!state) {
      setValue(event.target.name);
      setState(!state);
    } else {
      setValue('none');
      setState(!state);
    }
  };
  return (
    <Flex justifyContent="space-between" flex={1} p="15px 10px 15px 25px" borderBottom="1px solid #EEEEEE">
      <Flex flex={1}>
        <FormControlLabel control={<Checkbox checked={state} onChange={handleChecked} />} label="" name={columnName} />
        <Flex flexDirection="column">
          <Flex
            fontFamily={theme.typography.fontFamily}
            fontWeight="600"
            fontSize={14}
            lineHeight="20px"
            color="#0F0F10"
          >
            {columnName}
          </Flex>
          <Flex fontFamily={theme.typography.fontFamily} fontSize={14} lineHeight="20px" color="#6C6C72">
            {firstRow}
          </Flex>
        </Flex>
      </Flex>
      <Flex flex={1} alignItems="center">
        <ArrowForwardIcon />
        <Flex flex={1} ml="25px">
          <StyledSelectField
            label=""
            readOnly={false}
            required={false}
            error={false}
            disabled={false}
            onChange={handleSelectChange}
            value={value}
            select
          >
            <StyledMenuItem value="none">—None—</StyledMenuItem>
            <StyledMenuItem key={props.id} value={columnName}>
              {columnName}
            </StyledMenuItem>
          </StyledSelectField>
        </Flex>
      </Flex>
    </Flex>
  );
};

const FileRows = () => {
  return (
    <Flex flexDirection="column">
      {DATA.map((props) => {
        return <FileRow key={props.id} {...props} />;
      })}
    </Flex>
  );
};

const FileColumn = () => {
  const theme = useTheme<Theme>();
  return (
    <Flex flexDirection="column" m="23px 31px">
      <StyledTitle>
        <span>Map columns in your file to your file to fields in Imbee.</span>
      </StyledTitle>
      <Flex
        fontFamily={theme.typography.fontFamily}
        fontWeight="bold"
        fontSize={14}
        lineHeight="22px"
        color="#263238"
        mt="25px"
        bgcolor="#F8F8F8"
        justifyContent="space-between"
        py="15px"
      >
        <Flex flexDirection="column" flex={1} ml="70px">
          FILE COLUMN
        </Flex>
        <Flex flexDirection="column" flex={1} ml="25px">
          IMBEE PROPERTY
        </Flex>
      </Flex>
      <FileRows />
    </Flex>
  );
};
const MapColumn = () => {
  return (
    <Flex flexDirection="column" justifyContent="space-between" border="1px solid #E9E9F2">
      <Flex flexDirection="column" width={562} borderBottom="1px solid #E9E9F2" height="100%">
        <Nav drawer title="Map columns to fields">
          <PointerFlex alignItems="center" p="20px">
            <StyledArrowForward />
          </PointerFlex>
        </Nav>
        <FileColumn />
      </Flex>
      <Flex justifyContent="flex-end" mr="32px" mt="15px" mb="20px">
        <StyledBottomButton variant="outlined">Back</StyledBottomButton>
        <StyledBottomButton variant="contained" color="secondary">
          Import
        </StyledBottomButton>
      </Flex>
    </Flex>
  );
};

export default MapColumn;
