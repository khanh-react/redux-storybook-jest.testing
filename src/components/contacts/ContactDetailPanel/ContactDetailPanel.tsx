import { Box, Select, Tab } from '@material-ui/core';
import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { updateContactDataQuery } from '../../../redux/action/contact';
import { AppState } from '../../../redux/store';
import { Contact, FieldHeader } from '../../../redux/type/contact/state';
import { Flex } from '../../common/Flex';
import { IbTextField, StyledMenuItem } from '../../common/InputField';
import { IbTabs } from '../../common/Tabs';

export interface IFields extends FieldHeader {
  value: string | string[] | { [field: string]: number };
}

export const ContactDetailPanel = (props: { initialTab: number; Fields: IFields[]; info: Partial<Contact> }) => {
  const { initialTab, Fields, info } = props;
  const [selectedTab, setSelectedTab] = React.useState(initialTab);
  const handleChangeTab = (event: React.ChangeEvent<unknown>, newValue: number) => {
    setSelectedTab(newValue);
  };
  const [fields, setFields] = React.useState(Fields);
  useEffect(() => {
    if (Fields) {
      setFields(Fields);
    }
  }, [Fields]);
  const initFormData = useMemo(() => info, [info]);
  const [formData, setFormData] = React.useState(initFormData);
  const dispatch = useDispatch();
  const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>, index: number, name: string) => {
    const newArr = [...fields];
    newArr[index].value = e.target.value;
    setFields(newArr);
    if (name === 'name') {
      setFormData({
        ...formData,
        name: newArr[index].value as string,
        fields: {
          ...formData.fields,
        },
      });
    } else if (name === 'tel') {
      setFormData({
        ...formData,
        tel: newArr[index].value as string,
        fields: {
          ...formData.fields,
        },
      });
    } else {
      setFormData({
        ...formData,
        fields: {
          ...formData.fields,
          [name]: e.target.value,
        },
      });
    }
  };
  // const [checkBoxes, setCheckBoxes] = React.useState<string[]>([]);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>, name: string) => {
    // setCheckBoxes(event.target.value as string[]);
    setFormData({
      ...formData,
      fields: {
        ...formData.fields,
        [name]: Object.fromEntries((event.target.value as string[]).map((m) => [m, 1])),
      },
    });
  };
  useEffect(() => {
    dispatch(updateContactDataQuery(formData));
  }, [dispatch, formData]);
  const contactFieldInfo = useSelector((s: AppState) => s.subdomain.subdomainSetting?.contactField);
  return (
    <Flex
      borderRadius="10px"
      flexDirection="column"
      boxShadow=" 0 0 0 1px rgba(63, 63, 68, 0.05)"
      bgcolor="white"
      pb="10px"
      width="100%"
      height="calc(85vh - 80px)"
    >
      <Box mt="-15px">
        <IbTabs value={selectedTab} onChange={handleChangeTab} variant="fullWidth">
          <Tab label="INFO" />
          <Tab label="TIMELINE" />
          <Tab label="DEALS" />
          <Tab label="EVENT" />
        </IbTabs>
      </Box>
      {selectedTab === 0 && (
        <Flex p="40px" overflow="auto" flexWrap="wrap" alignItems="center" justifyContent="center">
          <Flex fontSize={15} fontWeight="bold" color="#bbbbc5" width="100%" mx="20px" mb="15px" mt="-5px">
            Contact
          </Flex>
          {fields.map((field, idx) => {
            if (field.type === 'select' || field.type === 'radio')
              return (
                // eslint-disable-next-line react/no-array-index-key
                <Flex py="15px" key={idx} flex="1 1 50%" px="20px">
                  <IbTextField
                    name={field.label}
                    label={field.label}
                    placeholder={field.label}
                    select
                    value={field.value || ''}
                    variant="outlined"
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      handleTextChange(e, idx, field.name);
                    }}
                    disabled={field.name === 'field_36'}
                  >
                    {contactFieldInfo &&
                      contactFieldInfo.map((c) => {
                        if (c.name === field.name)
                          return c.option_list.map((o, i) => (
                            // eslint-disable-next-line react/no-array-index-key
                            <StyledMenuItem key={i} value={o}>
                              {o}
                            </StyledMenuItem>
                          ));
                        return null;
                      })}
                  </IbTextField>
                </Flex>
              );
            if (field.type === 'checkbox') {
              const result =
                contactFieldInfo &&
                contactFieldInfo.filter((c) => {
                  return c.name === field.name;
                });
              // setCheckBoxes(Object.keys(field.value));

              return (
                // eslint-disable-next-line react/no-array-index-key
                <Flex py="15px" key={idx} flex="1 1 50%" px="20px">
                  <Select
                    multiple
                    // value={checkBoxes}
                    defaultValue={Object.keys(field.value)}
                    onChange={(e) => {
                      handleChange(e, field.name);
                    }}
                    MenuProps={{
                      anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'left',
                      },
                      getContentAnchorEl: null,
                    }}
                    input={
                      <IbTextField
                        name={field.label}
                        label={field.label}
                        placeholder={field.label}
                        variant="outlined"
                        select
                      />
                    }
                  >
                    {result &&
                      result[0].option_list.map((o, i) => (
                        // eslint-disable-next-line react/no-array-index-key
                        <StyledMenuItem key={i} value={o}>
                          {o}
                        </StyledMenuItem>
                      ))}
                  </Select>
                </Flex>
              );
            }
            if (field.type === 'textarea') {
              const result =
                contactFieldInfo &&
                contactFieldInfo.filter((c) => {
                  return c.name === field.name;
                });

              return (
                // eslint-disable-next-line react/no-array-index-key
                <Flex py="15px" key={idx} flex="1 1 50%" px="20px">
                  <IbTextField
                    type="textarea"
                    name={field.label}
                    label={field.label}
                    placeholder={field.label}
                    value={result && result[0].option_list}
                    variant="outlined"
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      handleTextChange(e, idx, field.name);
                    }}
                    disabled={field.name === 'field_36'}
                  />
                </Flex>
              );
            }
            if (field.type === 'date' || field.type === 'time') {
              return (
                // eslint-disable-next-line react/no-array-index-key
                <Flex py="15px" key={idx} flex="1 1 50%" px="20px">
                  <IbTextField
                    type={field.type}
                    name={field.label}
                    label={field.label}
                    placeholder={field.label}
                    value={field.value || ''}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    variant="outlined"
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      handleTextChange(e, idx, field.name);
                    }}
                    disabled={field.name === 'field_36'}
                  />
                </Flex>
              );
            }
            return (
              // eslint-disable-next-line react/no-array-index-key
              <Flex py="15px" key={idx} flex="1 1 50%" px="20px">
                <IbTextField
                  name={field.label}
                  label={field.label}
                  placeholder={field.label}
                  type="text"
                  value={field.value || ''}
                  variant="outlined"
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    handleTextChange(e, idx, field.name);
                  }}
                  disabled={field.name === 'field_36'}
                />
              </Flex>
            );
          })}
        </Flex>
      )}
      {selectedTab === 1 && <Flex>TIMELINE</Flex>}
      {selectedTab === 2 && <Flex>DEALS</Flex>}
      {selectedTab === 3 && <Flex>EVENT</Flex>}
    </Flex>
  );
};
