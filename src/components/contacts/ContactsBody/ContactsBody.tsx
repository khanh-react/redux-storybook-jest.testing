import { Box, BoxProps, Button, Checkbox, TableBody, Tooltip } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableCell, { TableCellProps } from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import _ from 'lodash';
import React, { useCallback, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import styled from 'styled-components';

import { bulkDelete } from '../../../redux/action/contact';
import { ContactField, Order, Row } from '../../../redux/type/contact/Contact';
import { Flex } from '../../common/Flex';
import Icon from '../../common/Icon';

/* eslint-disable @typescript-eslint/no-unused-vars */
interface ImTableHeadProps<RowType extends Row> {
  numSelected: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof RowType) => void;
  onSelectAllClick?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: keyof RowType;
  rowCount: number;
  scrolledDown?: boolean;
  scrolledRight?: boolean;
  fields: ContactField<RowType>[];
  handleDelete?: () => void;
}

const ImTableCell = styled(
  ({
    stickyLeft,
    stickyTop,
    shadowDown,
    shadowRight,
    ...props
  }: TableCellProps & { stickyLeft?: number; stickyTop?: number; shadowDown?: boolean; shadowRight?: boolean }) => (
    <TableCell {...props} />
  ),
)`
  height: 65px;
  box-sizing: border-box;
  padding: 10px;
  width: 1px;
  white-space: nowrap;
  background-color: #ffffff;
  min-width: 125px;
  max-width: 150px;

  &.checkbox {
    min-width: 80px;
  }
  &.profile {
    min-width: 215px;
  }

  svg.MuiTableSortLabel-icon {
    opacity: 0 !important;
  }
  .sort-icons {
    &.up {
      color: unset !important;
    }
    &.down {
      color: 'unset !important';
    }
    .up {
      color: ${({ sortDirection }) => (sortDirection === 'desc' ? '#263238' : '#bac4da')};
    }
    .down {
      color: ${({ sortDirection }) => (sortDirection === 'asc' ? '#263238' : '#bac4da')};
      margin-top: -15px;
    }
  }

  ${({ stickyLeft }) =>
    stickyLeft === undefined
      ? ``
      : `
        left: ${stickyLeft}px;
   `};

  ${({ stickyTop }) =>
    stickyTop === undefined
      ? ``
      : `
        top: ${stickyTop}px;
   `};
  ${({ stickyLeft, stickyTop }) => `
    z-index: ${(stickyLeft !== undefined ? 10 : 0) + (stickyTop !== undefined ? 10 : 0)};
    position: sticky;
  `};
  ${({ shadowDown, shadowRight }) =>
    shadowDown || shadowRight
      ? `
        box-shadow:rgba(0,0,0,0.2) ${shadowRight ? 6 : 0}px ${shadowDown ? 6 : 0}px 6px -6px;
      `
      : ''};
`;

const StyledBox = styled(({ title, ...props }: BoxProps & { title?: string }) => <Box {...props} />)`
  ${({ title }) =>
    title === 'ID' &&
    `
    :hover {
    text-decoration: underline;
    font-weight: bold;
    cursor: pointer;
}    
   `};
  word-wrap: 'break-word';
`;

const StyledButton = styled(Button)`
  .MuiButton-label {
    font-family: ${({ theme }) => theme.typography};
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    text-transform: capitalize;
    margin-left: -5px;
  }
  border-radius: 6px;
  border: 1px solid #dcdce4;
  height: 30px;
`;

const ImTableHead = <RowType extends Row>(props: ImTableHeadProps<RowType>) => {
  const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort, handleDelete } = props;
  const createSortHandler = (property: keyof RowType) => {
    if (property === 'id' || property === 'tel' || property === 'name') {
      return (event: React.MouseEvent<unknown>) => {
        onRequestSort(event, property);
      };
    }
    return (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, `field.${property}`);
    };
  };

  const setChecked = rowCount > 0 && numSelected === rowCount;
  const setIndeterminate = numSelected > 0 && numSelected < rowCount;
  const colSpan = Object.keys(props.fields).length - 1;

  return (
    <TableHead>
      <TableRow>
        {props.fields.map((field) => {
          switch (field.type) {
            case 'id':
              return (
                <ImTableCell
                  padding="checkbox"
                  className="checkbox"
                  stickyLeft={0}
                  stickyTop={0}
                  shadowDown={props.scrolledDown}
                  key={field.key as string}
                >
                  <Checkbox
                    indeterminate={setIndeterminate}
                    indeterminateIcon={<CheckBoxIcon />}
                    checked={setChecked}
                    onChange={onSelectAllClick}
                    inputProps={{ 'aria-label': 'select all desserts' }}
                  />
                </ImTableCell>
              );
            default:
              if (setChecked || setIndeterminate) {
                return null;
              }
              return (
                <ImTableCell
                  key={field.key as string}
                  align="left"
                  className={field.key as string}
                  stickyLeft={field.title === 'ID' ? 80 : undefined}
                  shadowDown={props.scrolledDown}
                  shadowRight={props.scrolledRight && field.title === 'ID'}
                  sortDirection={orderBy === field.key ? order : false}
                  stickyTop={0}
                >
                  <Flex onClick={createSortHandler(field.key)} alignItems="center" css={{ cursor: 'pointer' }}>
                    <Flex flexDirection="column" className="sort-icons">
                      <ArrowDropUpIcon className="up" />
                      <ArrowDropDownIcon className="down" />
                    </Flex>
                    <Tooltip title={field.title} arrow>
                      <Box textOverflow="ellipsis" overflow="hidden" whiteSpace="nowrap">
                        {field.title}
                      </Box>
                    </Tooltip>
                  </Flex>
                </ImTableCell>
              );
          }
        })}
        {(setChecked || setIndeterminate) && (
          <ImTableCell
            align="left"
            stickyLeft={80}
            stickyTop={0}
            shadowRight={props.scrolledRight}
            shadowDown={props.scrolledDown}
          >
            <Flex alignItems="center">
              <StyledButton onClick={handleDelete}>
                <Icon name="delete" /> Delete
              </StyledButton>
            </Flex>
          </ImTableCell>
        )}
        {(setChecked || setIndeterminate) &&
          _.times(colSpan - 1, () => {
            return (
              <ImTableCell align="left" stickyTop={0}>
                <Flex>
                  <></>
                </Flex>
              </ImTableCell>
            );
          })}
      </TableRow>
    </TableHead>
  );
};

const ImTableContainer = styled(TableContainer)`
  height: 100%;
  width: 100%;
  overflow: auto;
  table-layout: fixed;
`;

export type ImTableProps<RowType extends Row> = {
  fields: ContactField<RowType>[];
  rows: RowType[];
  order?: Order;
  orderBy?: keyof RowType;
  onOrderChanged?: (order: Order, orderBy: keyof RowType) => void;
};
export const removeByValue = (value: boolean, obj: { [key: string]: boolean }) => {
  const newKeys = Object.keys(obj).filter((key) => obj[key] !== value);
  const newObj: { [key: string]: boolean } = {};
  newKeys.forEach((key) => {
    newObj[key] = obj[key];
  });
  return Object.keys(newObj).map(Number);
};
export default function ImTable<RowType extends Row>(props: ImTableProps<RowType>) {
  const dispatch = useDispatch();
  const { order = 'asc', orderBy = 'id' } = props;
  const [selected, setSelected] = React.useState<{ [key: string]: boolean }>({});
  const [deleteList, setDeleteList] = React.useState<number[]>([]);
  const [page] = React.useState(0);

  const [scrolledDown, setScrolledDown] = React.useState<boolean>(false);
  const [scrolledRight, setScrolledRight] = React.useState<boolean>(false);
  const handleRequestSort = useCallback(
    (event: React.MouseEvent<unknown>, property: keyof RowType) => {
      const isDesc = orderBy === property && order === 'asc';
      props.onOrderChanged && props.onOrderChanged(isDesc ? 'desc' : 'asc', property);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [order, orderBy, props],
  );

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      setSelected(
        _(props.rows)
          .map((r): [string, boolean] => [r.id, true])
          .fromPairs()
          .value(),
      );
      setDeleteList(props.rows.map((row) => Number(row.id)));
    } else {
      setSelected({});
      setDeleteList([]);
    }
  };
  const handleClick = (id: string) => {
    setSelected({ ...selected, [id]: !selected[id] });
    setDeleteList(removeByValue(false, { ...selected, [id]: !selected[id] }));
  };
  const handleDelete = () => {
    dispatch(bulkDelete(deleteList));
    setSelected({});
    setDeleteList([]);
  };
  // const [contactDetail, ShowContactDetail] = React.useState(false);
  // const [contactId, setContactId] = React.useState('');

  const numSelected = useMemo(() => Object.values(selected).filter(_.identity).length, [selected]);
  const history = useHistory();
  // const contactFieldInfo = useSelector((s: AppState) => s.subdomain.subdomainSetting?.contactField);
  // const checkBoxContent = () => {
  //   if (field.type === 'checkbox') {
  //     const result =
  //       contactFieldInfo &&
  //       contactFieldInfo.filter((c) => {
  //         return c.name === field.name;
  //       });

  //     return (
  //       // eslint-disable-next-line react/no-array-index-key
  //       <Flex py="15px" key={idx} flex="1 1 50%" px="20px">
  //         <IbTextField
  //           name={field.label}
  //           label={field.label}
  //           placeholder={field.label}
  //           value={result && result[0].option_list}
  //           variant="outlined"
  //           onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
  //             handleTextChange(e, idx, field.name);
  //           }}
  //           disabled={field.name === 'field_36'}
  //         />
  //       </Flex>
  //     );
  //   }
  // };

  // useEffect(() => {
  //   dispatch(updateContactDetailId(contactId));
  // }, [dispatch, contactId]);
  return (
    <ImTableContainer
      onScroll={(e) => {
        setScrolledDown(e.currentTarget.scrollTop > 0);
        setScrolledRight(e.currentTarget.scrollLeft > 0);
      }}
    >
      <Table stickyHeader aria-label="sticky table">
        <ImTableHead
          numSelected={numSelected}
          order={order}
          orderBy={orderBy}
          onSelectAllClick={handleSelectAllClick}
          onRequestSort={handleRequestSort}
          rowCount={props.rows.length}
          scrolledRight={scrolledRight}
          scrolledDown={scrolledDown}
          fields={props.fields}
          handleDelete={handleDelete}
        />
        <TableBody>
          {props.rows.map((row) => {
            const isItemSelected = !!selected[row.id];
            // const labelCheckboxID = `enhanced-table-checkbox-${index}`;
            return (
              <TableRow
                role="checkbox"
                aria-checked={isItemSelected}
                tabIndex={-1}
                key={row.id}
                selected={isItemSelected}
              >
                {props.fields.map((field) => {
                  switch (field.type) {
                    case 'id':
                      return (
                        <ImTableCell className="checkbox" key="id" stickyLeft={0}>
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{ 'aria-labelledby': 'check' }}
                            onClick={() => {
                              handleClick(row.id);
                            }}
                          />
                        </ImTableCell>
                      );
                    case 'select':
                      return (
                        <ImTableCell className="checkbox" key={field.key as string}>
                          {/* [PENDING SELECT TYPE] */}
                          <StyledBox component="div" whiteSpace="normal" title={field.title} ml="10px">
                            {typeof row[field.key] === 'string' ? row[field.key] : '[EMPTY STRING]'}
                          </StyledBox>
                        </ImTableCell>
                      );
                    case 'checkbox': {
                      return (
                        <ImTableCell key={field.key as string}>
                          {/* [PENDING SELECT TYPE] */}
                          <StyledBox component="div" whiteSpace="normal" title={field.title} ml="10px">
                            {/* {typeof row[field.key] === 'string' ? row[field.key] : '[EMPTY STRING]'} */}
                            {typeof row[field.key] === 'object'
                              ? Object.keys(JSON.parse(JSON.stringify(row[field.key])))
                                  .filter((item) => {
                                    return JSON.parse(JSON.stringify(row[field.key]))[item] === 1;
                                  })
                                  .toString()
                              : '[EMPTY STRING]'}
                          </StyledBox>
                        </ImTableCell>
                      );
                    }
                    // case 'user':
                    //   return (
                    //     <ImTableCell
                    //       className={field.key as string}
                    //       key={field.key as string}
                    //       scope="row"
                    //       padding="none"
                    //       shadowRight={scrolledRight}
                    //       stickyLeft={field.title === 'ID' ? 80 : undefined}
                    //     >
                    //       <Flex>
                    //         {/* <Avatar src={row.profile} /> */}
                    //         <Flex flex={1} flexDirection="column" ml={1}>
                    //           {/* <Typography>{row[field.key] || '[EMPTY STRING]'}</Typography> */}
                    //           {/* <Typography variant="subtitle1">[MISSING FIELD]</Typography> */}
                    //           <StyledBox
                    //             component="div"
                    //             whiteSpace="normal"
                    //             title={field.title}
                    //             onClick={() => {
                    //               history.push(`/cms/contacts/contact-detail/${row.id}`);
                    //             }}
                    //           >
                    //             {row.tel}
                    //           </StyledBox>
                    //         </Flex>
                    //       </Flex>
                    //     </ImTableCell>
                    //   );
                    default:
                      return (
                        <ImTableCell
                          className={field.key as string}
                          align="left"
                          key={field.key as string}
                          shadowRight={field.title === 'ID' && scrolledRight}
                          stickyLeft={field.title === 'ID' ? 80 : undefined}
                        >
                          <StyledBox
                            component="div"
                            whiteSpace="normal"
                            title={field.title}
                            ml="10px"
                            onClick={
                              field.title === 'ID'
                                ? () => {
                                    history.push(`/cms/contacts/contact-detail/${row.id}`);
                                  }
                                : undefined
                            }
                          >
                            {/* eslint-disable-next-line no-nested-ternary */}
                            {field.title === 'ID'
                              ? row.tel
                              : typeof row[field.key] === 'string'
                              ? row[field.key]
                              : '[EMPTY STRING]'}
                          </StyledBox>
                        </ImTableCell>
                      );
                  }
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </ImTableContainer>
  );
}
