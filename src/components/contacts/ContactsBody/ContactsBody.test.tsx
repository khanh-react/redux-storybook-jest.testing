import { removeByValue } from './ContactsBody';

describe('removeFunction', () => {
  test('it should delete property in obj by value', () => {
    const input = { '1': true, '2': false };
    const output = [1];

    expect(removeByValue(false, input)).toEqual(output);
  });
});
