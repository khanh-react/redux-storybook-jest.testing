import { Box } from '@material-ui/core';
import React from 'react';

import { ALL_COLUMNS, ContactRow } from '../../../redux/type/contact/Contact';
import { SAMPLE_ROWS } from '../../../sampleData/contact';
import ImTable from './ContactsBody';

export default {
  title: 'Contacts/Body',
  component: ImTable,
};

export const Primary = () => {
  return (
    <Box position="relative" height="50vh" width="60vw">
      <ImTable<ContactRow> fields={ALL_COLUMNS} rows={SAMPLE_ROWS} />
    </Box>
  );
};
