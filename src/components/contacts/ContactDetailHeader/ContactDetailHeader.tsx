import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';

import { deleteSingleContact, updateContactDetail } from '../../../redux/action/contact';
import { Flex, PointerFlex } from '../../common/Flex';
import StyledButton from '../../common/StyledButton';

export const ContactDetailHeader = (props: { contactId: string }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <Flex>
      <Flex flexDirection="column">
        <Flex fontSize={24} fontWeight={600} color="#3b3b3b" lineHeight="28px">
          Contact Detail
        </Flex>
        <Flex fontSize={12} lineHeight="28px" mt="8px">
          <PointerFlex color="#2b71f3" onClick={() => history.goBack()}>
            CONTACTS/&nbsp;
          </PointerFlex>

          <Flex color="#757575"> CUSTOMER DETAIL</Flex>
        </Flex>
      </Flex>
      <Flex flex={1} justifyContent="flex-end">
        <Flex color="white">
          <StyledButton
            startIcon={<DeleteOutlineOutlinedIcon />}
            bgColor="#f16f63"
            color="inherit"
            variant="contained"
            onClick={() => {
              dispatch(deleteSingleContact(props.contactId));
              history.goBack();
            }}
          >
            Delete
          </StyledButton>
          <Flex ml="20px">
            <StyledButton variant="outlined" onClick={() => history.goBack()}>
              Cancel
            </StyledButton>
          </Flex>
          <Flex ml="20px">
            <StyledButton
              bgColor="black"
              color="inherit"
              variant="contained"
              disableElevation
              onClick={() => {
                dispatch(updateContactDetail());
                history.goBack();
              }}
            >
              Update
            </StyledButton>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};
