import React, { useState } from 'react';

import {
  ALL_COLUMNS,
  ALWAYS_VISIBLE_COLUMNS,
  ContactRow,
  DEFAULT_VISIBLE_COLUMNS,
} from '../../../redux/type/contact/Contact';
import { SAMPLE_ROWS } from '../../../sampleData/contact';
import ImTable from '../ContactsBody/ContactsBody';
import { EditColumnDrawer } from './ContactsEditColumns';

export const Primary = () => {
  const [visibleColumns, setVisibleColumns] = useState(DEFAULT_VISIBLE_COLUMNS);
  const [show, setShow] = useState(true);
  return (
    <div>
      <ImTable<ContactRow> fields={ALL_COLUMNS.filter((c) => visibleColumns[c.key])} rows={SAMPLE_ROWS} />
      {show && (
        <EditColumnDrawer
          allColumns={ALL_COLUMNS}
          visibleColumns={visibleColumns}
          alwaysVisibleColumns={ALWAYS_VISIBLE_COLUMNS}
          onUpdate={(u) => {
            setVisibleColumns(u);
            setShow(false);
          }}
          onCancel={() => setShow(false)}
        />
      )}
    </div>
  );
};

export default {
  title: 'Contacts/EditColumns',
  component: EditColumnDrawer,
};
