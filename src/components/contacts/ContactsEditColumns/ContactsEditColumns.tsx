import Checkbox from '@material-ui/core/Checkbox';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/styles';
import React, { useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';

import { editContact } from '../../../redux/action/contact';
import { ContactField, Row, VisibleColumns } from '../../../redux/type/contact/Contact';
import Button from '../../common/Button';
import FilterModal from '../../common/FilterModal';
import { Flex, PointerFlex } from '../../common/Flex';
import { Nav } from '../../common/Nav';
import Text from '../../common/Text';

const useStyle = makeStyles({
  root: () => ({
    '& .head': {
      height: 72,
      alignItems: 'center',
      borderBottom: '1px solid #eeeeee',
      '& .title': {
        display: 'flex',
        alignItems: 'center',
        marginLeft: 25,
      },
      '& .close': {
        position: 'absolute',
        right: 10,
        cursor: 'pointer',
        opacity: 0.8,
      },
    },
    '& .body': {
      height: '100%',
      '& #visible-column, & #hidden-column': {
        flexDirection: 'column',
        width: '50%',
        padding: 20,
        '& .column': {
          borderBottom: '1px solid #eeeeee',
        },
        '& .title': {
          paddingLeft: 10,
        },
      },
      '& #visible-column': {
        borderRight: '1px solid #eeeeee',
      },
      '& #hidden-column': {
        background: '#F6F7F8',
        '& .title': {
          opacity: 0.6,
        },
      },
    },
    '& .foot': {
      width: '100%',
      height: 70,
      borderTop: '1px solid #eeeeee',
      background: '#ffffff',
      position: 'absolute',
      bottom: 0,
      right: 0,
    },
  }),
});

export const EditColumnDrawer = <RowType extends Row>(props: {
  visibleColumns: VisibleColumns<RowType>;
  allColumns: ContactField<RowType>[];
  alwaysVisibleColumns: VisibleColumns<RowType>;
  onCancel?: () => void;
  onUpdate?: (visibleColumns: VisibleColumns<RowType>) => void;
}) => {
  const [visibleColumnsDraft, setVisibleColumnsDraft] = useState(props.visibleColumns);
  const showingColumns = props.allColumns.filter((c) => visibleColumnsDraft[c.key]);
  const hidingColumns = props.allColumns.filter((c) => !visibleColumnsDraft[c.key]);
  const classes = useStyle();
  const dispatch = useDispatch();
  const editData = useMemo(() => {
    return [
      ...showingColumns.map((c) => ({
        name: c.key,
        label: c.title,
        type: c.type,
      })),
    ].slice(1);
  }, [showingColumns]);
  return (
    <FilterModal className={classes.root}>
      <Nav drawer title="Choose Your Columns">
        <PointerFlex p={2} onClick={props.onCancel}>
          <CloseIcon className="close" fontSize="default" />
        </PointerFlex>
      </Nav>
      <Flex className="body">
        <Flex id="visible-column">
          <Flex className="title" height={50} alignItems="center">
            <Text value="Visible columns" weight="bold" />
          </Flex>
          {showingColumns.map((c) => (
            <Flex className="column" height={50} alignItems="center" key={c.key as string}>
              <Checkbox
                id={c.title}
                checked={visibleColumnsDraft[c.key]}
                disabled={props.alwaysVisibleColumns[c.key]}
                onChange={(_, checked) => setVisibleColumnsDraft({ ...visibleColumnsDraft, [c.key]: checked })}
              />
              {c.title}
            </Flex>
          ))}
        </Flex>
        <Flex id="hidden-column">
          <Flex className="title" height={50} alignItems="center">
            <Text value="Hidden fields" weight="bold" />
          </Flex>
          {hidingColumns.map((c) => (
            <Flex className="column" height={50} alignItems="center" key={c.key as string}>
              <Checkbox
                id={c.title}
                checked={visibleColumnsDraft[c.key]}
                onChange={(_, checked) => setVisibleColumnsDraft({ ...visibleColumnsDraft, [c.key]: checked })}
              />
              {c.title}
            </Flex>
          ))}
        </Flex>
      </Flex>
      <Flex className="foot" alignItems="center" justifyContent="flex-end">
        <Button kind="default" onClick={props.onCancel}>
          Cancel
        </Button>
        <Button
          kind="dark"
          onClick={() => {
            props.onUpdate && props.onUpdate(visibleColumnsDraft);
            dispatch(editContact(editData));
          }}
        >
          Apply
        </Button>
      </Flex>
    </FilterModal>
  );
};
