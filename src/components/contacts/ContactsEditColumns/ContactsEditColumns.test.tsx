import '@testing-library/jest-dom/extend-expect';

import { fireEvent, queryByAttribute, render, screen } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';

import { configureStore } from '../../../redux/store';
import { ALL_COLUMNS, ALWAYS_VISIBLE_COLUMNS, DEFAULT_VISIBLE_COLUMNS } from '../../../redux/type/contact/Contact';
import { EditColumnDrawer } from './ContactsEditColumns';

const store = configureStore();
const getById = queryByAttribute.bind(null, 'id');
describe('ContactsEditColumn', () => {
  const onUpdate = jest.fn();
  it('should call onUpdate when click Apply ', () => {
    render(
      <Provider store={store}>
        <EditColumnDrawer
          onUpdate={onUpdate}
          allColumns={ALL_COLUMNS}
          visibleColumns={DEFAULT_VISIBLE_COLUMNS}
          alwaysVisibleColumns={ALWAYS_VISIBLE_COLUMNS}
        />
      </Provider>,
    );

    fireEvent.click(screen.getByText(/apply/i));
    expect(onUpdate).toBeCalled();
  });
  it('should be match true/false value', () => {
    const { container } = render(
      <Provider store={store}>
        <EditColumnDrawer
          onUpdate={onUpdate}
          allColumns={ALL_COLUMNS}
          visibleColumns={DEFAULT_VISIBLE_COLUMNS}
          alwaysVisibleColumns={ALWAYS_VISIBLE_COLUMNS}
        />
      </Provider>,
    );
    const checkbox = getById(container, 'Name') as HTMLInputElement;
    expect(checkbox).toBeChecked();
    fireEvent.click(checkbox);
    const checkbox1 = getById(container, 'Name') as HTMLInputElement;
    expect(checkbox1).not.toBeChecked();
    fireEvent.click(checkbox1);
    expect(checkbox).toBeChecked();
  });
});
