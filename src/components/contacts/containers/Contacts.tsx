/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { updateContactQuery } from '../../../redux/action/contact';
import { AppState } from '../../../redux/store';
import {
  ALL_COLUMNS,
  ALWAYS_VISIBLE_COLUMNS,
  ContactField,
  DEFAULT_VISIBLE_COLUMNS,
  Order,
  Row,
} from '../../../redux/type/contact/Contact';
import { DEFAULT_PAGE_SIZE } from '../../../redux/type/contact/state';
import { Flex } from '../../common/Flex';
import AddContact from '../AddContact/AddContact';
import ImTable from '../ContactsBody/ContactsBody';
import { EditColumnDrawer } from '../ContactsEditColumns/ContactsEditColumns';
import ExportButton from '../ExportButton/ExportButton';
import Filters from '../Filters/Filters';
import { ContactHeader } from './ContactsHeader';

export default function Contacts() {
  const [visibleColumns, setVisibleColumns] = useState(DEFAULT_VISIBLE_COLUMNS);
  const [show, setShow] = useState(false);
  const dispatch = useDispatch();
  const query = useSelector((state: AppState) => state.contacts.query);
  const currentPage = useSelector((state: AppState) => state.contacts.currentPage);
  const fieldsPage = useSelector((state: AppState) => state.contacts.fields);
  const delResult = useSelector((state: AppState) => state.contacts.delResult);
  const delSingleResult = useSelector((state: AppState) => state.contacts.delSingleResult);
  const updateContactResult = useSelector((state: AppState) => state.contacts.updateContactResult);

  const fields: undefined | ContactField<Row>[] = useMemo(() => {
    if (fieldsPage && fieldsPage.find((field) => field.label !== 'Select'))
      return [
        { type: 'id', key: 'id', title: 'Select' },
        ...fieldsPage.map(
          (f): ContactField<Row> => ({
            type: f.type,
            key: f.name,
            title: f.label,
          }),
        ),
      ];
    return undefined;
  }, [fieldsPage]);
  const rows: undefined | Row[] = useMemo(() => {
    return currentPage?.data.map((r): Row => ({ id: r.id, name: r.name, tel: r.tel, ...r.fields }));
  }, [currentPage?.data]);

  useEffect(() => {
    dispatch(updateContactQuery({ page: 1, pageSize: DEFAULT_PAGE_SIZE }));
  }, [delResult, delSingleResult, updateContactResult]);

  const onOrderChanged = useCallback((order: Order, orderBy: string) => {
    dispatch(updateContactQuery({ sortOrder: order, sortKey: orderBy }));
  }, []);
  const [addContact, ShowAddContact] = useState(false);
  const [filters, ShowFilters] = useState(false);
  return (
    <Flex flexDirection="column" height="100%" width="100%" overflow="hidden">
      <ExportButton handleAddContact={() => ShowAddContact(true)} />
      <Flex bgcolor="white" flexDirection="column" flex={1}>
        <Flex px="39px">
          <ContactHeader handleEditClick={() => setShow(true)} handleFilterClick={() => ShowFilters(true)} />
        </Flex>
        <Flex flex={1} height="calc(100vh - 249px)">
          {fields && rows && query && (
            <ImTable<Row>
              fields={fields}
              rows={rows}
              order={query.sortOrder}
              orderBy={query.sortKey}
              onOrderChanged={onOrderChanged}
            />
          )}
        </Flex>
      </Flex>
      {show && fields && (
        <EditColumnDrawer
          allColumns={ALL_COLUMNS}
          visibleColumns={visibleColumns}
          alwaysVisibleColumns={ALWAYS_VISIBLE_COLUMNS}
          onUpdate={(u) => {
            setVisibleColumns(u);
            setShow(false);
          }}
          onCancel={() => setShow(false)}
        />
      )}
      {addContact && <AddContact onSave={() => ShowAddContact(false)} onCancel={() => ShowAddContact(false)} />}
      {filters && <Filters onSave={() => ShowFilters(false)} onCancel={() => ShowFilters(false)} />}
    </Flex>
  );
}
