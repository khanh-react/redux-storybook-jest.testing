import CircularProgress from '@material-ui/core/CircularProgress';
import { get } from 'lodash';
import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

import { getContactDetail } from '../../../redux/action/contact';
import { AppState } from '../../../redux/store';
import { Flex } from '../../common/Flex';
import { ContactDetailHeader } from '../ContactDetailHeader/ContactDetailHeader';
import { ContactDetailPanel, IFields } from '../ContactDetailPanel/ContactDetailPanel';
import { ContactDetailSummary } from '../ContactDetailSummary/ContactDetailSummary';

const ContactDetail = () => {
  const dispatch = useDispatch();
  const contactDetail = useSelector((state: AppState) => state.contacts.contactDetail);

  const { id } = useParams<{ id: string }>();
  useEffect(() => {
    dispatch(getContactDetail(id));
  }, [id, dispatch]);

  const contactInfo = useMemo(() => {
    return contactDetail?.contact;
  }, [contactDetail]);

  const contactField: IFields[] | undefined = useMemo(() => {
    if (contactInfo && contactDetail) {
      return [
        {
          name: 'tel',
          label: 'Phone Number',
          type: 'text',
          ordering: 0,
          value: contactInfo.tel,
        },
        ...contactDetail.fieldHeaders.map((field) => ({
          ...field,
          value: field.name === 'name' ? contactInfo.name : get(contactInfo.fields, [field.name]),
        })),
      ];
    }
    return undefined;
  }, [contactInfo, contactDetail]);

  return (
    <Flex height="100vh" width="100vw" alignItems="stretch">
      <Flex flexDirection="column" alignItems="stretch" flex={1}>
        <Flex flexDirection="column" bgcolor="#f6f7f8" pt="40px" px="40px" flex={1}>
          <ContactDetailHeader contactId={id} />
          <Flex mt="23px">
            {contactInfo ? (
              <ContactDetailSummary contactInfo={contactInfo} />
            ) : (
              <Flex width="307px" height="100vh" justifyContent="center" alignItems="center">
                <CircularProgress />
              </Flex>
            )}
            {contactField && contactInfo ? (
              <Flex width="100%" height="100vh">
                <ContactDetailPanel initialTab={0} Fields={contactField} info={contactInfo} />
              </Flex>
            ) : (
              <Flex flex={1} height="100vh" justifyContent="center" alignItems="center">
                <CircularProgress />
              </Flex>
            )}
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};
export default ContactDetail;
