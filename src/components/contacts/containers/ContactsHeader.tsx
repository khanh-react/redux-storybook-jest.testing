import {
  Button,
  IconButton,
  InputAdornment,
  makeStyles,
  Select,
  SvgIcon,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { AppState } from '../../../redux/store';
import { CenterFlex, Flex } from '../../common/Flex';
import Icon from '../../common/Icon';
import { IbTextField } from '../../common/InputField';
import { IbMenuItem } from '../../common/Menu';
import { updateContactQuery } from '../../../redux/action/contact';

// export default {
//   title: 'Contacts/Header',
//   // component: ContactsHeader,
// };

const useStyles = makeStyles(() => ({
  button: {
    padding: '5px 48px',
    fontWeight: 600,
    fontSize: 14,
    marginLeft: 10,
    textTransform: 'unset',
  },
}));
const OutlineInput = styled(IbTextField)`
  .MuiInputBase-input {
    font-size: 14px;
    font-weight: 600;
    line-height: 16px;
    color: #2e2e32;
  }
  .MuiOutlinedInput-root {
    height: 36px;
    &.Mui-focused fieldset {
      border-color: black;
    }
    & fieldset {
      border-color: rgba(0, 0, 0, 0.23);
      border-radius: 4px;
    }
    &:hover fieldset {
      border-color: black;
    }
  }
`;
export const ContactHeader = (props: { handleEditClick?: () => void; handleFilterClick?: () => void }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const page = useSelector((state: AppState) => state.contacts.currentPage);
  const query = useSelector((state: AppState) => state.contacts.query);
  const pageSize = useSelector((state: AppState) => state.contacts.query?.pageSize);

  const onPageSizeChanged = useCallback(
    (e: React.ChangeEvent<{ value: unknown }>) => {
      dispatch(updateContactQuery({ pageSize: Number(e.target.value) }));
    },
    [dispatch],
  );

  const pageCount = pageSize && page ? Math.floor((page.total - 1) / pageSize) + 1 : undefined;

  const onNextPage = useCallback(() => {
    if (page && pageCount && page.page < pageCount) {
      dispatch(updateContactQuery({ page: page.page + 1 }));
    }
  }, [page, dispatch, pageCount]);
  const onPreviousPage = useCallback(() => {
    if (page && page.page > 1) {
      dispatch(updateContactQuery({ page: page.page - 1 }));
    }
  }, [page, dispatch]);
  const onFilterChanged = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(updateContactQuery({ filter: e.target.value }));
    },
    [dispatch],
  );
  // Test filters
  // React.useEffect(() => {
  //   const filters = {
  //     'filters[tel]': '85290786185',
  //     'filters[field_32]': 'Female',
  //   };
  //   dispatch(updateContactQuery({ filters }));
  // }, [dispatch]);
  const adornment = (
    <InputAdornment position="start">
      <Icon name="search" />
    </InputAdornment>
  );
  return (
    <Flex alignItems="center" width="100%" justifyContent="space-between" height={88} boxSizing="border-box">
      <Flex alignItems="center" justifyContent="space-between" width={687}>
        <OutlineInput
          placeholder="Search"
          fullWidth
          InputProps={{
            startAdornment: adornment,
          }}
          value={query?.filter ?? ''}
          onChange={onFilterChanged}
        />
        <Button variant="outlined" className={classes.button} onClick={props.handleEditClick}>
          <CenterFlex>
            <Icon name="pen" />
            Edit&nbsp;Column
          </CenterFlex>
        </Button>
        <Button variant="outlined" className={classes.button} onClick={props.handleFilterClick}>
          <CenterFlex>
            <Icon name="filter" />
            Filter
          </CenterFlex>
        </Button>
      </Flex>
      {!!pageSize && !!page?.total && (
        <Flex width={370} justifyContent="space-around" alignItems="center">
          <Typography>Rows per page :</Typography>

          <Select
            labelId="demo-simple-select-filled-label"
            id="demo-simple-select-filled"
            value={pageSize}
            onChange={onPageSizeChanged}
          >
            <IbMenuItem value={10}>10</IbMenuItem>
            <IbMenuItem value={20}>20</IbMenuItem>
            <IbMenuItem value={50}>50</IbMenuItem>
          </Select>
          {page && (
            <>
              <Typography>
                {(page.page - 1) * pageSize + 1}-{Math.min(page.page * pageSize, page.total)} of {page.total}
              </Typography>
              <Tooltip title="Previous">
                <IconButton color="inherit" onClick={onPreviousPage}>
                  <SvgIcon fontSize="small">
                    <ChevronLeft />
                  </SvgIcon>
                </IconButton>
              </Tooltip>
              <Tooltip title="Next">
                <IconButton color="inherit" onClick={onNextPage}>
                  <SvgIcon fontSize="small">
                    <ChevronRight />
                  </SvgIcon>
                </IconButton>
              </Tooltip>
            </>
          )}
        </Flex>
      )}
    </Flex>
  );
};
