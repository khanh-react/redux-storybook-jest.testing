import {
  Box,
  Button,
  Icon,
  IconButton,
  LinearProgress,
  MenuItem,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Theme,
  useTheme,
} from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import React from 'react';
import styled from 'styled-components';

import { Flex } from '../../common/Flex';

const StyledButton = styled(Button)`
  ${({ theme }) => `
margin-left: 15px;
.MuiButton-label {
  font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 24px;
  text-transform: capitalize;
}
border-radius: 8px;
`}
`;
const StyledSpan = styled.span`
  ${({ theme }) => `
font-family: ${theme.typography.fontFamily};
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 24px;
  color: #263238;
`}
`;
const StyledProgressBar = styled(LinearProgress)`
  width: 100%;
  border-radius: 2px;
  .MuiLinearProgress-colorPrimary {
    background-color: #ffdb36;
  }
`;
const StyledNumber = styled(Flex)`
  ${({ theme }) => `
font-family: ${theme.typography.fontFamily};
flex: 1; 
flex-direction:column;
align-items:center;
border-right: 1px solid  #ECEFF1;
& .record {
font-style: normal;
font-weight: 600;
font-size: 12px;
line-height: 31px;
color: #6C6C72
}
& .number {
font-style: normal;
font-weight: 600;
font-size: 29px;
line-height: 32px;
color: #0F0F10;
}
`}
`;
const StyledDownloadButton = styled(Button)`
  ${({ theme }) => `
.MuiButton-label {
align-items:center;
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: 600;
font-size: 14px;
line-height: 24px;
text-transform: capitalize;
color: #2C87D8;
}
`}
`;
const StyledSelect = styled(Select)`
  ${({ theme }) => `
.MuiInputBase-input {
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: normal;
font-size: 12px;
line-height: 15px;
}
`}
`;
const StyledTableCell = styled(TableCell)`
  ${({ theme }) => `
& .table-cell-default {
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 20px;
}
& .table-cell-date {
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: normal;
font-size: 12px;
line-height: 20px;
color: #6C6C72;
}
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: 600;
font-size: 14px;
line-height: 22px;
}
`}
`;
const StyledTag = styled(Box)`
  display: inline-flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  border-radius: 8px;
  height: 19px;
`;
const StyledIconButton = styled(IconButton)`
  padding: 0;
  margin-top: -7px;
  margin-left: 20px;
`;
const StyledTableHead = styled(TableHead)`
  box-shadow: inset 0px -1px 0px #eeeeee;
`;
const ColorTag = ({ label, color, bgcolor }: { label: string; color: string; bgcolor: string }) => {
  const theme = useTheme<Theme>();
  return (
    <StyledTag
      bgcolor={bgcolor}
      color={color}
      fontFamily={theme.typography.fontFamily}
      fontWeight="500"
      fontSize={12}
      lineHeight="12px"
    >
      <Flex pt="4px" pl="7px" pr="11px" pb="3px">
        {label.toUpperCase()}
      </Flex>
    </StyledTag>
  );
};
const ImportIcon = () => {
  return (
    <Icon style={{ fill: 'green' }}>
      <img alt="import" src="/static/import.svg" style={{ fill: 'red' }} />
    </Icon>
  );
};
const DownloadIcon = () => {
  return (
    <Icon>
      <img alt="export" src="/static/download.svg" />
    </Icon>
  );
};
export function LinearDeterminate() {
  // const [progress, setProgress] = React.useState(0);
  const progress = 50;
  const theme = useTheme<Theme>();
  // React.useEffect(() => {
  //   const timer = setInterval(() => {
  //     setProgress((oldProgress) => {
  //       if (oldProgress === 100) {
  //         return 0;
  //       }
  //       const diff = Math.random() * 10;
  //       return Math.min(oldProgress + diff, 100);
  //     });
  //   }, 500);

  //   return () => {
  //     clearInterval(timer);
  //   };
  // }, []);

  return (
    <Flex flexDirection="column" flex={1}>
      <StyledProgressBar variant="determinate" value={50} />
      <Flex
        fontFamily={theme.typography.fontFamily}
        fontWeight="600"
        fontSize={14}
        lineHeight="22px"
        color="#0F0F10"
        mt="10px"
      >
        {`${Math.round(progress)}% Complete`}
      </Flex>
    </Flex>
  );
}
const RecordsData = {
  recordsCreated: '1,832',
  recordsUpdated: '136',
  recordsError: '32',
};
const ProfileProgress = (props: { recordsCreated: string; recordsUpdated: string; recordsError: string }) => {
  const theme = useTheme<Theme>();
  return (
    <Flex
      flexDirection="column"
      boxShadow="0px 0px 0px 1px rgba(63, 63, 68, 0.05), 0px 1px 2px rgba(63, 63, 68, 0.15)"
      bgcolor="white"
      mt="25px"
    >
      <Flex
        fontFamily={theme.typography.fontFamily}
        fontWeight="600"
        fontSize={16}
        lineHeight="20px"
        color="#263238"
        p="16px 0px 20px 16px"
        borderBottom="1px solid #EEEEEE"
      >
        Profile Progress
      </Flex>
      <Flex flexDirection="column" m="25px 30px">
        <Flex flex={1}>
          <LinearDeterminate />
        </Flex>
        <Flex mt="25px">
          <StyledNumber>
            <span className="number">{props.recordsCreated}</span>
            <span className="record">RECORDS CREATED</span>
          </StyledNumber>
          <StyledNumber>
            <span className="number">{props.recordsUpdated}</span>
            <span className="record">RECORDS UPDATED</span>
          </StyledNumber>
          <StyledNumber>
            <span className="number">{props.recordsError}</span>
            <span className="record">RECORDS ERROR</span>
          </StyledNumber>
          <Flex flex={1} justifyContent="center" alignItems="center">
            <StyledDownloadButton startIcon={<DownloadIcon />}>Download</StyledDownloadButton>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};

const rows = [
  {
    id: '1',
    rowName: 'Contact_data_01/05/2020',
    recordsCreated: '2390',
    recordsUpdated: '0',
    recordsError: '57',
    imported: {
      name: 'Rachel',
      date: '9 days ago',
    },
    status: 'completed',
  },
  {
    id: '2',
    rowName: 'Contact_data_01/05/2020',
    recordsCreated: '2390',
    recordsUpdated: '0',
    recordsError: '57',
    imported: {
      name: 'Rachel',
      date: '9 days ago',
    },
    status: 'fail',
  },
  {
    id: '3',
    rowName: 'Contact_data_01/05/2020',
    recordsCreated: '2390',
    recordsUpdated: '0',
    recordsError: '57',
    imported: {
      name: 'Rachel',
      date: '9 days ago',
    },
    status: 'completed',
  },
  {
    id: '4',
    rowName: 'Contact_data_01/05/2020',
    recordsCreated: '2390',
    recordsUpdated: '0',
    recordsError: '57',
    imported: {
      name: 'Rachel',
      date: '9 days ago',
    },
    status: 'fail',
  },
  {
    id: '5',
    rowName: 'Contact_data_01/05/2020',
    recordsCreated: '2390',
    recordsUpdated: '0',
    recordsError: '57',
    imported: {
      name: 'Rachel',
      date: '9 days ago',
    },
    status: 'completed',
  },
  {
    id: '6',
    rowName: 'Contact_data_01/05/2020',
    recordsCreated: '2390',
    recordsUpdated: '0',
    recordsError: '57',
    imported: {
      name: 'Rachel',
      date: '9 days ago',
    },
    status: 'fail',
  },
];

export function RecordsTable() {
  const theme = useTheme<Theme>();
  const [rowQuantity, setRowQuantity] = React.useState('1');
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setRowQuantity(event.target.value as string);
  };
  return (
    <Flex
      mt="20px"
      flexDirection="column"
      borderRadius="4px"
      bgcolor="white"
      boxShadow="0px 0px 0px 1px rgba(63, 63, 68, 0.05), 0px 1px 2px rgba(63, 63, 68, 0.15)"
    >
      <TableContainer>
        <Table>
          <StyledTableHead>
            <TableRow>
              <StyledTableCell align="left">NAME</StyledTableCell>
              <StyledTableCell align="left">RECORDS CREATED</StyledTableCell>
              <StyledTableCell align="left">RECORDS UPDATED</StyledTableCell>
              <StyledTableCell align="left">RECORDS ERROR</StyledTableCell>
              <StyledTableCell align="left">IMPORTED BY</StyledTableCell>
              <StyledTableCell align="left">STATUS</StyledTableCell>
            </TableRow>
          </StyledTableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.id}>
                <TableCell align="left">
                  <Flex className="table-cell-default" pr="100px">
                    {row.rowName}
                  </Flex>
                </TableCell>
                <StyledTableCell align="left">
                  <Flex className="table-cell-default" alignItems="center">
                    {row.recordsCreated}
                    <StyledIconButton>
                      <DownloadIcon />
                    </StyledIconButton>
                  </Flex>
                </StyledTableCell>
                <StyledTableCell align="left">
                  <Flex className="table-cell-default">{row.recordsUpdated}</Flex>
                </StyledTableCell>
                <StyledTableCell align="left">
                  <Flex className="table-cell-default" alignItems="center">
                    {row.recordsError}
                    <StyledIconButton>
                      <DownloadIcon />
                    </StyledIconButton>
                  </Flex>
                </StyledTableCell>
                <StyledTableCell align="left">
                  <Flex className="table-cell-default">{row.imported.name}</Flex>
                  <Flex className="table-cell-date">{row.imported.date}</Flex>
                </StyledTableCell>
                <StyledTableCell align="center">
                  <Flex alignItems="center">
                    <Flex alignItems="center" flex={1}>
                      <ColorTag
                        label={row.status}
                        bgcolor={row.status === 'completed' ? '#E8F5E9' : '#FEEBEE'}
                        color={row.status === 'completed' ? '#388E3C' : '#F16F63'}
                      />
                    </Flex>
                    <IconButton size="small">
                      <ArrowForwardIcon />
                    </IconButton>
                  </Flex>
                </StyledTableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Flex
        justifyContent="flex-end"
        flex={1}
        fontFamily={theme.typography.fontFamily}
        fontWeight="600"
        fontSize={12}
        lineHeight="15px"
        color="#6C6C72"
        alignItems="center"
        my="18px"
      >
        <Flex mr="20px">Rows per page:</Flex>
        <StyledSelect onChange={handleChange} disableUnderline value={rowQuantity}>
          {rows.map((row) => {
            return (
              <MenuItem key={row.id} value={row.id}>
                {row.id}
              </MenuItem>
            );
          })}
        </StyledSelect>
        <Flex fontSize={12} fontWeight="normal">{`1-${rowQuantity}`}</Flex>
        &nbsp;
        <Flex fontSize={12} fontWeight="normal">{`of ${rows.length}`}</Flex>
        <Flex mr="22px" ml="50px">
          <IconButton size="small">
            <ArrowBackIosIcon />
          </IconButton>
          <Flex ml="23px">
            <IconButton size="small">
              <ArrowForwardIosIcon />
            </IconButton>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
}

const RecentImport = () => {
  const theme = useTheme<Theme>();
  return (
    <Flex justifyContent="center" flex={1} p="35px 35px 20px 40px" flexDirection="column" width="1287px">
      <Flex justifyContent="space-between">
        <Box fontFamily={theme.typography.fontFamily} fontWeight="600" fontSize={24} lineHeight="28px" color="#263238">
          Recent Imports
        </Box>
        <Flex>
          <StyledButton startIcon={<ImportIcon />}>
            <StyledSpan>IMPORT</StyledSpan>
          </StyledButton>
        </Flex>
      </Flex>
      <ProfileProgress {...RecordsData} />
      <RecordsTable />
    </Flex>
  );
};

export default RecentImport;
