import React from 'react';

import { Flex } from '../../common/Flex';
import RecentImport from './RecentImport';

export default {
  title: 'Contacts/RecentImport',
  component: RecentImport,
};

export const Primary = () => {
  return (
    <Flex flexDirection="row">
      <RecentImport />
    </Flex>
  );
};
