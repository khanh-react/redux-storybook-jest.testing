import React from 'react';
import * as Yup from 'yup';

import { Flex } from '../../common/Flex';
import { DynamicForm } from './AddContact';

export default {
  title: 'Contacts/AddContact',
  component: DynamicForm,
};
const fields = [
  { label: 'Name', type: 'text', name: 'name', value: '', option_list: [], required: 1 },
  { label: 'Phone Number', type: 'text', name: 'tel', value: '', option_list: [], required: 1 },
  { label: 'Gender', type: 'text', name: 'address', value: '', option_list: [], required: 0 },
  { label: 'City', type: 'text', name: 'city', value: '', option_list: [], required: 0 },
  { label: 'Select', type: 'select', name: 'select', value: '', option_list: ['1', '2', '3'], required: 0 },
];

const validation = Yup.object().shape({
  name: Yup.string().required('required').max(35),
});
export const Primary = () => {
  return (
    <Flex flexDirection="row">
      <DynamicForm fields={fields} validation={validation} />
    </Flex>
  );
};
