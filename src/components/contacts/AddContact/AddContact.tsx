import { Button, Select } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { Field, FieldInputProps, Form, Formik, FormikProps } from 'formik';
import { identity, pickBy } from 'lodash';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import * as Yup from 'yup';

import { addContact } from '../../../redux/action/contact';
import { AppState } from '../../../redux/store';
import { Contact } from '../../../redux/type/contact/state';
import FilterModal from '../../common/FilterModal';
import { Flex, PointerFlex } from '../../common/Flex';
import { IbTextField, StyledMenuItem } from '../../common/InputField';
import { Nav } from '../../common/Nav';

type formsProps = {
  label: string;
  type: string;
  name: string;
  value: string;
  option_list: string[];
  ordering?: number;
  required: number;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface FieldProps<V = any> {
  field: FieldInputProps<V>;
  form: FormikProps<V>;
}
const getInitialValues = (inputs: formsProps[]) => {
  const initialValues: Record<string, string> = {};

  inputs.forEach((field) => {
    if (!initialValues[field.name]) {
      initialValues[field.name] = field.value;
    }
  });

  return initialValues;
};
const StyledBottomButton = styled(Button)`
  .MuiButton-label {
    font-family: ${({ theme }) => theme.typography};
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 26px;
    text-transform: none;
  }
  margin-left: 21.5px;
  border-radius: 8px;
  min-height: 38px;
  width: 90px;
  box-shadow: none;
`;
const FlexForm = styled(Form)`
  display: flex;
  flex-direction: column;
  height: 100%;
`;
const StyledIbTextField = styled(IbTextField)`
  .MuiInputBase-root {
    height: 50px;
  }
`;

export function merge(data: Record<string, string>) {
  const arr = { ...data };
  delete arr.name;
  delete arr.tel;
  return Object.getOwnPropertyNames(arr).reduce((acc: { [field: string]: Record<string, string> }, key) => {
    const prefix = key.split('_')[0];
    acc[prefix] = { ...arr };
    return acc;
  }, {});
}
export const DynamicForm = (props: {
  fields: formsProps[];
  validation: unknown;
  onCancel?: () => void;
  onSave?: () => void;
}) => {
  const initialValues = getInitialValues(props.fields);
  const dispatch = useDispatch();
  const [formValues, setFormValues] = React.useState(initialValues);
  const [formData, setFormData] = React.useState<Partial<Contact>>({});
  const [checkBoxes, setCheckBoxes] = React.useState<string[]>([]);
  const handleCancel = () => {
    return props.onCancel && props.onCancel();
  };
  const handleSave = () => {
    dispatch(addContact(formData));
    return props.onSave && props.onSave();
  };
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetEl = e.target;
    const fieldName = targetEl.name;
    setFormValues({
      ...formValues,
      [fieldName]: targetEl.value,
    });
  };

  useEffect(() => {
    setFormData({
      name: formValues.name,
      tel: formValues.field_owner,
      fields: { ...pickBy(merge(formValues).field, identity) },
    });
  }, [formValues]);

  const handleCheckBox = (event: React.ChangeEvent<{ value: unknown }>, name: string) => {
    setCheckBoxes(event.target.value as string[]);
    setFormData({
      ...formData,
      fields: {
        ...formData.fields,
        [name]: Object.fromEntries((event.target.value as string[]).map((m) => [m, 1])),
      },
    });
  };

  const renderSelect = (input: formsProps) => {
    const options = input.option_list.map((i) => (
      <StyledMenuItem key={i} value={i}>
        {i}
      </StyledMenuItem>
    ));
    return (
      <Flex mb="30px" key={input.name}>
        <Field name={input.name}>
          {({ field, form }: FieldProps) => {
            const { errors, touched, handleChange, handleBlur } = form;
            const hasError = errors[input.name] && touched[input.name] ? 'hasError' : '';
            const onChangeField = (e: React.ChangeEvent<HTMLInputElement>) => {
              onChange(e);
              handleChange(e);
            };
            return (
              <StyledIbTextField
                {...field}
                label={input.label}
                placeholder={input.label}
                select
                error={Boolean(hasError)}
                helperText={errors[input.name]}
                onChange={onChangeField}
                onBlur={handleBlur}
                required={input.label === 'Name' || input.label === 'Phone Number' || input.required === 1}
              >
                {options}
              </StyledIbTextField>
            );
          }}
        </Field>
      </Flex>
    );
  };

  const renderCheckBoxSelect = (input: formsProps) => {
    const options = input.option_list.map((i) => (
      <StyledMenuItem key={i} value={i}>
        {i}
      </StyledMenuItem>
    ));

    return (
      <Flex mb="30px" key={input.name}>
        <Field name={input.name}>
          {({ field, form }: FieldProps) => {
            const { handleBlur } = form;
            // const hasError = errors[input.name] && touched[input.name] ? 'hasError' : '';

            return (
              <Select
                multiple
                value={checkBoxes}
                onChange={(e) => {
                  handleCheckBox(e, input.name);
                }}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
                input={
                  <StyledIbTextField
                    {...field}
                    name={input.label}
                    label={input.label}
                    placeholder={input.label}
                    variant="outlined"
                    select
                    onBlur={handleBlur}
                  />
                }
              >
                {options}
              </Select>
            );
          }}
        </Field>
      </Flex>
    );
  };
  const renderFields = (inputs: formsProps[]) => {
    return inputs.map((input) => {
      if (input.type === 'select' || input.type === 'radio') {
        return renderSelect(input);
      }
      if (input.type === 'checkbox') {
        return renderCheckBoxSelect(input);
      }
      return (
        <Flex mb="30px" key={input.name}>
          <Field name={input.name}>
            {({ field, form }: FieldProps) => {
              const { errors, touched, handleChange, handleBlur } = form;
              const hasError = errors[input.name] && touched[input.name] ? 'hasError' : '';
              const onChangeField = (e: React.ChangeEvent<HTMLInputElement>) => {
                onChange(e);
                handleChange(e);
              };
              return (
                <StyledIbTextField
                  {...field}
                  label={input.label}
                  placeholder={input.label}
                  type="text"
                  error={Boolean(hasError)}
                  helperText={errors[input.name]}
                  onChange={onChangeField}
                  onBlur={handleBlur}
                  multiline
                  required={input.label === 'Name' || input.label === 'Phone Number' || input.required === 1}
                />
              );
            }}
          </Field>
        </Flex>
      );
    });
  };

  return (
    <FilterModal size="medium">
      <Formik
        onSubmit={handleSave}
        validationSchema={props.validation}
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
      >
        {(form) => {
          return (
            <FlexForm onSubmit={form.handleSubmit} noValidate>
              <Flex boxShadow="0px -5px 10px 0px rgba(0,0,0,0.3)" flexDirection="column">
                <Nav drawer title="Add Contact">
                  <PointerFlex alignItems="center" p="20px" color="#2E2E32" onClick={handleCancel}>
                    <CloseIcon />
                  </PointerFlex>
                </Nav>
              </Flex>
              <Flex fontStyle="normal" fontWeight={600} fontSize={14} lineHeight="28px" px="40px" pt="25px">
                Map columns in your file to fields in Imbee.
              </Flex>
              <Flex flex={1} overflow="auto" flexDirection="column" p="30px 40px">
                <Flex flexDirection="column" width="480px">
                  <Flex mb="30px" width="100%" flexDirection="column">
                    {renderFields(props.fields)}
                  </Flex>
                </Flex>
              </Flex>
              <Flex
                justifyContent="flex-end"
                pr="40px"
                pt="15px"
                pb="20px"
                width="100%"
                height="70px"
                bgcolor="white"
                borderTop="1px solid #E9E9F2"
                boxShadow="0px 5px 10px 0px rgba(0,0,0,0.3)"
              >
                <StyledBottomButton variant="outlined" onClick={handleCancel}>
                  Cancel
                </StyledBottomButton>
                <StyledBottomButton type="submit" variant="contained" color="secondary">
                  Save
                </StyledBottomButton>
              </Flex>
            </FlexForm>
          );
        }}
        {/* {console.log(formData)} */}
      </Formik>
    </FilterModal>
  );
};
// const phoneRegExp = /^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/;
// const validation = Yup.object().shape({
//   field_owner: Yup.string().required('Required'),
// });

const AddContact = (props: { onCancel?: () => void; onSave?: () => void }) => {
  const fields = useSelector((state: AppState) => state.contacts.fields) as formsProps[];
  // const item = { label: 'Phone Number', type: 'text', name: 'tel', value: '', option_list: [], required: 1 };
  const fieldsData = [
    ...fields.map((field) => {
      return {
        label: field.label,
        type: field.type,
        name: field.name,
        value: '',
        option_list: field.option_list,
        required: field.required,
      };
    }),
  ];
  // fieldsData.splice(1, 0, item);

  const mapRequireField = fieldsData.map((field) => {
    if (field.required === 1)
      return {
        [field.name]: Yup.string().required('Required'),
      };
    return {};
  });

  const validation = mapRequireField.reduce((acc, val) => {
    Object.assign(acc, val);
    return acc;
  }, {});

  return (
    <DynamicForm
      fields={fieldsData}
      validation={Yup.object().shape(validation)}
      onCancel={props.onCancel}
      onSave={props.onSave}
    />
  );
};
export default AddContact;
