import '@testing-library/jest-dom/extend-expect';

import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import { act } from 'react-dom/test-utils';
import { useSelector } from 'react-redux';

import AddContact, { merge } from './AddContact';

type TestElement = Document | Element | Window | Node;

function hasInputValue(e: TestElement, inputValue: string) {
  return screen.getByDisplayValue(inputValue) === e;
}
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(),
  useSelector: jest.fn(),
}));
const onSave = jest.fn();
const onCancel = jest.fn();
describe('AddContact', () => {
  beforeEach(() => {
    (useSelector as jest.Mock).mockImplementation((selector) =>
      selector({
        contacts: {
          fields: [
            { name: 'name', label: 'Name', type: 'text' },
            {
              name: 'field_32',
              label: 'Gender',
              type: 'text',
            },
          ],
        },
      }),
    );
  });
  afterEach(() => {
    (useSelector as jest.Mock).mockClear();
  });
  it('should call onCancel when click Cancel ', () => {
    render(<AddContact onCancel={onCancel} />);

    fireEvent.click(screen.getByText(/cancel/i));
    expect(onCancel).toBeCalled();
  });
  it('should changeValue ', async () => {
    render(<AddContact onSave={onSave} />);

    const inputName = screen.getByPlaceholderText(/name/i);
    const inputGender = screen.getByPlaceholderText(/gender/i);

    await act(async () => {
      fireEvent.change(inputName, { target: { value: 'Test' } });
      fireEvent.change(inputGender, { target: { value: 'M' } });
    });

    expect(hasInputValue(inputName, 'Test')).toBe(true);
    expect(hasInputValue(inputGender, 'M')).toBe(true);
  });

  it.skip('should call onSave when click Save ', async () => {
    render(<AddContact onSave={onSave} />);

    const inputName = screen.getByPlaceholderText(/name/i);
    const button = screen.getByText('Save');

    await act(async () => {
      fireEvent.change(inputName, { target: { value: 'a' } });
    });
    await act(async () => {
      fireEvent.click(button);
    });

    expect(onSave).toBeCalled();
  });
});

describe('Merge function', () => {
  it('it should match output', () => {
    const input = { name: 'test', tel: '123', field_32: 'Test', field_33: 'Test' };
    const output = { field: { field_32: 'Test', field_33: 'Test' } };
    expect(merge(input)).toEqual(output);
  });
});
