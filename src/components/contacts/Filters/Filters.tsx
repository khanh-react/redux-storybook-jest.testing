/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Button,
  Checkbox,
  createStyles,
  FormControl,
  FormControlLabel,
  FormGroup,
  makeStyles,
  TextField,
  Theme,
  useTheme,
} from '@material-ui/core';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import CloseIcon from '@material-ui/icons/Close';
import Autocomplete from '@material-ui/lab/Autocomplete';
import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { AppState } from '../../../redux/store';
import FilterModal from '../../common/FilterModal';
import { Flex } from '../../common/Flex';
import { IbTextField } from '../../common/InputField';
import InputTag from '../../common/InputTag';
import { Nav } from '../../common/Nav';
import { AddFilter } from '../AddFilter/AddFilter';

export interface InputTagProps {
  id: string;
  label: string;
  hasCloseIcon: boolean;
  onClose?: () => void;
}
const StyledButton = styled(Button)`
  ${({ theme }) => `
  margin-right: 32px;
  .MuiButton-label {
    font-family: ${theme.typography.fontFamily};
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    text-transform: capitalize;
  }
  border-radius: 8px;
  `}
`;
const BottomButton = styled(Button)`
  ${({ theme }) => `
  margin-left: 10px;
  .MuiButton-label {
    font-family: ${theme.typography.fontFamily};
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 26px;
    text-transform: capitalize;
  }
  border-radius: 8px;
  `}
`;

const StyledTextField = styled(TextField)`
  ${({ theme }) => `
  .MuiOutlinedInput-root {
    border-radius: 8px;
    width: 100%;
    height: 50px;
    &.Mui-focused fieldset {
      border-color: black;
    }
  }
  .MuiInputLabel-outlined {
    font-family: ${theme.typography.fontFamily};
    font-size: 12px;
    line-height: 12px;
    letter-spacing: 0.4px;
    color: #0F0F10;
  }
  `}
`;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      boxShadow: 'none',
      margin: 0,
      color: '#0d0d0e',
      fontSize: 14,
      background: 'white',
    },
    option: {
      minHeight: 'auto',
      alignItems: 'flex-start',
      '&[data-focus="true"]': {
        backgroundColor: '#fff8da',
      },
    },
  }),
);

const StyledCloseButton = styled(CloseIcon)`
  &:hover {
    cursor: pointer;
  }
  font-size: 20px;
`;
const EmailProps = [
  { id: '1', hasCloseIcon: true, label: 'novak@email.com' },
  { id: '2', hasCloseIcon: true, label: 'adelman1234568@email.com' },
];
const LastNameProps = [
  { id: '1', hasCloseIcon: true, label: 'Novak' },
  { id: '2', hasCloseIcon: true, label: 'Adelman' },
  { id: '3', hasCloseIcon: true, label: 'Lum' },
];

const FilterTagsItem = ({ data }: { data: InputTagProps[] }) => {
  return (
    <Flex flexDirection="column" p="20px 32px 45px" borderBottom="1px solid #EEEEEE">
      <Flex justifyContent="flex-end" mb="25px" mr="3px">
        <StyledCloseButton style={{ fontSize: 20 }} />
      </Flex>
      <Flex flexDirection="column" mr="10px" flex={1}>
        <StyledTextField variant="outlined" label="Last Name" />
      </Flex>
      <Flex flexWrap="wrap" mt="20px">
        {data.map((prop) => {
          return (
            <Flex key={prop.id} mr="10px">
              <InputTag hasCloseIcon={prop.hasCloseIcon} label={prop.label} />
            </Flex>
          );
        })}
      </Flex>
    </Flex>
  );
};
const FilterCheckBoxItem = () => {
  const theme = useTheme<Theme>();
  return (
    <Flex flexDirection="column" p="20px 15px 23px" borderBottom="1px solid #EEEEEE">
      <Flex justifyContent="space-between" alignItems="center" mb="15px">
        <Flex
          fontFamily={theme.typography.fontFamily}
          fontWeight="bold"
          fontSize={14}
          lineHeight="28px"
          color="#19191A"
          ml="20px"
        >
          Sales owner
        </Flex>
        <Flex justifyContent="flex-end" mr="19px">
          <StyledCloseButton style={{ fontSize: 20 }} />
        </Flex>
      </Flex>
      <FormControl component="fieldset">
        <FormGroup>
          <Flex>
            <Flex flexDirection="column" flexBasis="50%">
              <Flex borderBottom="1px solid #EEEEEE" pl="20px">
                <FormControlLabel control={<Checkbox />} label="Me" />
              </Flex>
              <Flex borderBottom="1px solid #EEEEEE" pl="20px">
                <FormControlLabel control={<Checkbox />} label="Unassigned" />
              </Flex>
              <Flex borderBottom="1px solid #EEEEEE" pl="20px">
                <FormControlLabel control={<Checkbox />} label="Rachel" />
              </Flex>
              <Flex borderBottom="1px solid #EEEEEE" pl="20px">
                <FormControlLabel control={<Checkbox />} label="Johnny" />
              </Flex>
            </Flex>
          </Flex>
        </FormGroup>
      </FormControl>
    </Flex>
  );
};
const StyledFilterModal = styled(FilterModal)`
  .wrapper {
    width: 453px !important;
  }
`;
export const Filters = (props: { onCancel?: () => void; onSave?: () => void }) => {
  const classes = useStyles();
  const fields = useSelector((state: AppState) => state.contacts.fields) || [];
  const filters = useSelector((state: AppState) => state.contacts.filters) || [];
  const subDomainUsers = useSelector((s: AppState) => s.subdomain.subdomainUser) || [];
  const handleCancel = () => {
    return props.onCancel && props.onCancel();
  };
  const handleSave = () => {
    return props.onSave && props.onSave();
  };
  const [addFilter, ShowAddFilter] = React.useState(false);
  return (
    <StyledFilterModal>
      {!addFilter ? (
        <Flex
          flexDirection="column"
          width={453}
          border="1px solid #E9E9F2"
          justifyContent="space-between"
          height="100%"
        >
          <Flex flexDirection="column">
            <Nav drawer title="Filters">
              <StyledButton
                variant="contained"
                color="secondary"
                startIcon={<AddCircleOutlineIcon />}
                onClick={() => ShowAddFilter(true)}
              >
                Add field
              </StyledButton>
            </Nav>
            {/* <FilterTagsItem data={LastNameProps} />
            <FilterTagsItem data={EmailProps} />
            <FilterCheckBoxItem /> */}
            {/* WIP bellow */}
          </Flex>
          <Flex flex={1} overflow="auto" flexDirection="column">
            {filters && filters.length > 0 ? (
              filters.map((filter) => {
                if (filter.type === 'user' && filter.label === 'Owner')
                  return (
                    <Autocomplete
                      classes={{
                        paper: classes.paper,
                        option: classes.option,
                      }}
                      options={subDomainUsers}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => <IbTextField {...params} label={filter.label} />}
                    />
                  );
                if (filter.type === 'date')
                  return (
                    <IbTextField
                      type="date"
                      label={filter.label}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  );
                return <IbTextField key={filter.name} placeholder={filter.type} />;
              })
            ) : (
              <Flex fontStyle="normal" fontWeight={600} fontSize={14} lineHeight="28px" px="40px" pt="25px">
                There is no filter currently. Try to add some filters first
              </Flex>
            )}
          </Flex>

          <Flex justifyContent="flex-end" mr="32px" mt="15px" mb="20px">
            <BottomButton variant="outlined" onClick={handleCancel}>
              Cancel
            </BottomButton>
            <BottomButton variant="contained" color="secondary" onClick={handleSave}>
              Apply
            </BottomButton>
          </Flex>
        </Flex>
      ) : (
        <AddFilter onBack={() => ShowAddFilter(false)} fields={fields} filters={filters} />
      )}
    </StyledFilterModal>
  );
};

export default Filters;
