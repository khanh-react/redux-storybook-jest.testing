import React from 'react';

import { Flex } from '../../common/Flex';
import Filters from './Filters';

export default {
  title: 'Contacts/Filters',
  component: Filters,
};

export const Primary = () => {
  return (
    <Flex flexDirection="row" justifyContent="flex-end">
      <Filters />
    </Flex>
  );
};
