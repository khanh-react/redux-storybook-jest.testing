import React from 'react';

import { Flex } from '../../common/Flex';
import ExportButton from './ExportButton';

export default {
  title: 'Contacts/ExportButton',
  component: ExportButton,
};

export const Primary = () => {
  return (
    <Flex flexDirection="row">
      <ExportButton />
    </Flex>
  );
};
