import { Box, Button, Icon, Theme, useTheme } from '@material-ui/core';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import React from 'react';
import styled from 'styled-components';

import { Flex } from '../../common/Flex';

const StyledButton = styled(Button)`
  margin-left: 15px;
  .MuiButton-label {
    font-family: ${({ theme }) => theme.typography.fontFamily};
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    text-transform: capitalize;
  }
  border-radius: 8px;
`;
const StyledSpan = styled.span`
  font-family: ${({ theme }) => theme.typography.fontFamily};
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 24px;
  color: #263238;
`;

const ExportIcon = () => {
  return (
    <Icon>
      <img alt="export" src="/static/export.svg" />
    </Icon>
  );
};

const ImportIcon = () => {
  return (
    <Icon>
      <img alt="import" src="/static/import.svg" />
    </Icon>
  );
};
const ExportButton = (props: { handleAddContact?: () => void }) => {
  const theme = useTheme<Theme>();
  return (
    <Flex justifyContent="space-between" alignItems="center" p="35px 35px 20px 40px" height={91} boxSizing="border-box">
      <Box fontFamily={theme.typography.fontFamily} fontWeight="600" fontSize={24} lineHeight="28px" color="#263238">
        Contacts
      </Box>
      <Flex>
        <StyledButton theme={theme} startIcon={<ExportIcon />}>
          <StyledSpan>EXPORT</StyledSpan>
        </StyledButton>
        <StyledButton theme={theme} startIcon={<ImportIcon />}>
          <StyledSpan>IMPORT</StyledSpan>
        </StyledButton>
        <StyledButton
          theme={theme}
          variant="contained"
          color="secondary"
          startIcon={<AddCircleOutlineIcon />}
          onClick={props.handleAddContact}
        >
          Add Contact
        </StyledButton>
      </Flex>
    </Flex>
  );
};

export default ExportButton;
