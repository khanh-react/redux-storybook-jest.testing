import { Avatar } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';

import { Contact } from '../../../redux/type/contact/state';
import firstChars from '../../../utils/firstChars';
import { Flex } from '../../common/Flex';
import Icon from '../../common/Icon';
import { IbTextField, StyledMenuItem } from '../../common/InputField';

const StyledAvatar = styled(Avatar)`
  width: 60px;
  height: 60px;
`;
const StyledOwnerAva = styled(Avatar)`
  width: 20px;
  height: 20px;
`;
const StyledSelect = styled(IbTextField)`
  .MuiOutlinedInput-root {
    height: 31px;
    min-width: 75px;
    border-radius: 17px;
    background-color: #fff8da;
    &.Mui-focused fieldset {
      border-color: transparent;
    }
    & fieldset {
      border-color: transparent;
    }
    &:hover fieldset {
      border-color: transparent;
    }
  }
  .MuiInputBase-input {
    font-size: 14px;
    font-weight: 600;
    line-height: 24px;
    color: #0f0f10;
    background: transparent;
    text-align: center;
    margin-right: 10px;
  }

  min-width: 75px;
  max-width: 100px;
`;
const StyledOwnerSelect = styled(IbTextField)`
  .MuiOutlinedInput-root {
    height: 31px;
    min-width: 75px;
    border-radius: 17px;
    border: 1px solid #dcdce4;
    & fieldset {
      border-color: transparent;
    }
  }
  .MuiInputBase-input {
    font-size: 14px;
    font-weight: 600;
    line-height: 24px;
    color: #0f0f10;
    background: transparent;
  }
  max-width: 150px;
`;
const StyledBox = styled(Flex)`
  ${({ theme }) => `
align-items: center;
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 18px;
color: #0F0F10;
margin-bottom: 15px;
> * {
      &${Flex} {
         margin-left: 10px;
      }
    }
`}
`;

export const ContactDetailSummary = (props: { contactInfo: Partial<Contact> }) => {
  const { contactInfo } = props;
  const [status, setStatus] = React.useState('VIP');
  const [owner, setOwner] = React.useState('Rachel');
  const handleChangeStatus = (event: React.ChangeEvent<{ value: unknown }>) => {
    setStatus(event.target.value as string);
  };
  const handleChangeOwner = (event: React.ChangeEvent<{ value: unknown }>) => {
    setOwner(event.target.value as string);
  };

  return (
    <Flex
      flexDirection="column"
      width="307px"
      mr="20px"
      bgcolor="white"
      pt="42px"
      px="23px"
      borderRadius="8px"
      boxShadow="0 2px 8px 0 rgba(217, 217, 217, 0.45)"
      height="calc(80vh - 80px)"
      overflow="auto"
    >
      <Flex justifyContent="center" flexDirection="column" pb="27px">
        <Flex justifyContent="center">
          {contactInfo.name && <StyledAvatar alt="" src={firstChars(contactInfo.name)} />}
        </Flex>
        <Flex fontSize={18} fontWeight={600} color="#0f0f10" mt="10px" justifyContent="center">
          {contactInfo.name}
        </Flex>
        <Flex fontSize={14} fontStyle="italic" color="#98999f" justifyContent="center">
          Role
        </Flex>
        <Flex mt="13px" justifyContent="center">
          <StyledSelect select value={status} onChange={handleChangeStatus}>
            <StyledMenuItem value="VIP">VIP</StyledMenuItem>
            <StyledMenuItem value="Status 2">Status 2</StyledMenuItem>
          </StyledSelect>
        </Flex>
      </Flex>
      <Flex flexDirection="column" px="5px" pt="10px" borderTop="1px dashed #dcdce4" pb="15px">
        <Flex fontWeight="bold" fontSize={12} lineHeight="15px" color="#bbbbc5" pb="28px">
          CONTACT
        </Flex>
        <StyledBox>
          <Icon name="phone" />
          <Flex>{contactInfo.tel}</Flex>
        </StyledBox>
        <StyledBox>
          <Icon name="email" />
          <Flex>@email</Flex>
        </StyledBox>
        <StyledBox>
          <Icon name="home" />
          <Flex>Schummtown</Flex>
        </StyledBox>
      </Flex>

      {/* <Flex flexDirection="column" px="5px" pt="10px" borderTop="1px dashed #dcdce4" pb="30px">
        <Flex fontWeight="bold" fontSize={12} lineHeight="15px" color="#bbbbc5" pb="10px">
          GENERAL
        </Flex>
        <Flex flexWrap="wrap">
          <Flex mr="5px" pt="10px">
            <Tag color="blue" name="Signed" outline />
          </Flex>
          <Flex mr="5px" pt="10px">
            <Tag color="blue" name="Signed" outline />
          </Flex>
          <Flex mr="5px" pt="10px">
            <Tag color="blue" name="Signed" outline />
          </Flex>
          <Flex mr="5px" pt="10px">
            <Tag color="blue" name="Signed" outline />
          </Flex>
        </Flex>
      </Flex> */}
      <Flex flexDirection="column" px="5px" pt="10px" borderTop="1px dashed #dcdce4">
        <Flex fontWeight="bold" fontSize={12} lineHeight="15px" color="#bbbbc5" pb="25px">
          CHANNEL
        </Flex>
        <Flex pb="15px">
          <Icon name="fb" />
          <Flex fontSize={14} lineHeight="18px" color="#2E2E32" ml="10px">
            Name
          </Flex>
        </Flex>
        <Flex>
          <Icon name="whatsapp" />
          <Flex fontSize={14} lineHeight="18px" color="#2E2E32" ml="10px">
            Phone
          </Flex>
        </Flex>
        <Flex mt="50px" mb="30px" alignItems="center">
          <Flex fontWeight="bold" fontSize={12} lineHeight="15px" color="#bbbbc5" mr="5px">
            OWNER:
          </Flex>
          <StyledOwnerSelect select value={owner} onChange={handleChangeOwner}>
            <StyledMenuItem value="Rachel">
              <Flex alignItems="center">
                <StyledOwnerAva alt="" src="http://placekitten.com/60/60" />
                <Flex ml="10px">Rachel</Flex>
              </Flex>
            </StyledMenuItem>
            <StyledMenuItem value="P2">P2</StyledMenuItem>
          </StyledOwnerSelect>
        </Flex>
      </Flex>
    </Flex>
  );
};
