/* eslint-disable @typescript-eslint/no-empty-function */
import React from 'react';

import { FieldHeader } from '../../../redux/type/contact/state';
import { Flex } from '../../common/Flex';
import AddFilter from './AddFilter';

export default {
  title: 'Contacts/AddFilter',
  component: AddFilter,
};
const fieldData: FieldHeader[] = [
  { ordering: 1, name: 'Last Name', label: 'Last Name', type: 'text' },
  { ordering: 2, name: 'First Name', label: 'First Name', type: 'text' },
  { ordering: 3, name: 'Mobile Number', label: 'Mobile Number', type: 'text' },
  { ordering: 4, name: 'Business number', label: 'Business number', type: 'text' },
  { ordering: 5, name: 'Email', label: 'Email', type: 'text' },
  { ordering: 6, name: 'Work email', label: 'Work email', type: 'text' },
  { ordering: 7, name: 'Address', label: 'Address', type: 'text' },
  { ordering: 8, name: 'Company Name', label: 'Company Name', type: 'text' },
  { ordering: 9, name: 'Company Phone', label: 'Company Phone', type: 'text' },
];
export const Primary = () => {
  return (
    <Flex flexDirection="row" justifyContent="flex-end">
      <AddFilter onBack={() => {}} fields={fieldData} filters={fieldData} />
    </Flex>
  );
};
