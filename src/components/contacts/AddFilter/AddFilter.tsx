import { InputAdornment, TextField, Theme, useTheme } from '@material-ui/core';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import CheckIcon from '@material-ui/icons/Check';
import { findIndex } from 'lodash';
import React from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';

import { updateFilters } from '../../../redux/action/contact';
import { FieldHeader } from '../../../redux/type/contact/state';
import { Flex, PointerFlex } from '../../common/Flex';
import Icon from '../../common/Icon';
import { Nav } from '../../common/Nav';

const StyledTextField = styled(TextField)`
  .MuiOutlinedInput-root {
    border-radius: 8px;
    width: 100%;
    height: 50px;
    &.Mui-focused fieldset {
      border-color: black;
    }
  }
`;

const StyledFieldData = styled(Flex)`
  &:hover {
    background-color: #fff8da;
  }
  & > span {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 18px;
    color: #19191a;
    margin: 10px 20px;
  }
`;
const StyledArrowForward = styled(ArrowForwardIosIcon)`
  &:hover {
    cursor: pointer;
  }
  font-size: 18px;
`;

/* export const Nav = (props: { title: string }) => {
  const theme = useTheme<Theme>();
  return (
    <Flex alignItems="center" height={72} borderBottom="1px solid #EEEEEE" justifyContent="space-between">
      <Flex
        fontFamily={theme.typography.fontFamily}
        fontWeight="bold"
        fontSize={16}
        lineHeight="20px"
        ml="32px"
        my="25px"
        color="0F0F10"
      >
        {props.title}
      </Flex>
    </Flex>
  );
}; */

export const AddField = (props: { fields: FieldHeader[]; filters: Partial<FieldHeader>[] }) => {
  const theme = useTheme<Theme>();
  const dispatch = useDispatch();
  // const [onSelect, setOnSelect] = useState<Partial<FieldHeader>[]>([]);
  const handleSelect = (field: Partial<FieldHeader>) => {
    const index = findIndex(props.filters, (o) => {
      return o.name === field.name;
    });
    const temp = [...props.filters];
    if (index === -1) {
      temp.push(field);
    } else {
      temp.splice(index, 1);
    }
    dispatch(updateFilters(temp));
  };

  return (
    <Flex flexDirection="column" m="0px 40px 70px 30px">
      <Flex
        fontFamily={theme.typography.fontFamily}
        fontWeight="bold"
        fontSize={16}
        lineHeight="20px"
        color="#19191A"
        m="25px 20px"
      >
        Add a field to filter
      </Flex>
      <StyledTextField
        variant="outlined"
        placeholder="Search"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <Icon name="search" />
            </InputAdornment>
          ),
        }}
      />
      {props.fields &&
        props.fields.map((field) => {
          return (
            <StyledFieldData
              fontFamily={theme.typography.fontFamily}
              key={field.name}
              onClick={() => handleSelect(field)}
              justifyContent="space-between"
            >
              <span>{field.label}</span>
              {!props.filters.every((itemSelect) => itemSelect.name !== field.name) && (
                <Flex alignItems="center" justifyContent="center" mr="15px">
                  <CheckIcon />
                </Flex>
              )}
            </StyledFieldData>
          );
        })}
    </Flex>
  );
};

export const AddFilter = (props: { onBack: () => void; fields: FieldHeader[]; filters: Partial<FieldHeader>[] }) => {
  return (
    <Flex flexDirection="column" width={453} border="1px solid #E9E9F2">
      <Nav drawer title="Add Filter">
        <PointerFlex alignItems="center" p="20px" onClick={props.onBack}>
          <StyledArrowForward />
        </PointerFlex>
      </Nav>
      <AddField fields={props.fields} filters={props.filters} />
    </Flex>
  );
};

export default AddFilter;
