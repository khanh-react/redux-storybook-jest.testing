import { BoxProps, Button, Checkbox, FormControlLabel, Icon, Theme, useTheme } from '@material-ui/core';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import InfoIcon from '@material-ui/icons/Info';
import React from 'react';
import styled from 'styled-components';

import { Flex, PointerFlex } from '../../common/Flex';
import { IbTextField, StyledMenuItem } from '../../common/InputField';
import { Nav } from '../../common/Nav';

const StyledArrowForward = styled(ArrowForwardIosIcon)`
  &:hover {
    cursor: pointer;
  }
  font-size: 18px;
`;
const StyledNote = styled(Flex)`
  ${({ theme }) => `
align-items: center;
margin-bottom: 20px;
& > span {
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: 600;
font-size: 14px;
line-height: 28px;
color: #19191A;
}
.MuiSvgIcon-root {
    font-size: 18px;
}
  `}
`;
const StyledIcon = styled(Icon)`
  height: 45px;
  width: auto;
`;
const StyledButton = styled(Button)`
  ${({ theme }) => `
.MuiButton-label {
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: 600;
font-size: 14px;
line-height: 24px;
text-transform: none;
color: white;
}
background: #2C87D8;
margin-top: 20px;
border-radius: 8px;
min-height: 36px;
box-shadow: none;
`}
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const StyledDragAndDrop = styled(({ highlighted, ...props }: BoxProps & { highlighted: boolean }) => (
  <Flex {...props} />
))`
  background-color: ${({ highlighted }) => (highlighted ? 'lightblue' : '#f6f7f8')};
  flex-direction: column;
  height: 235px;
  justify-content: center;
  align-items: center;
  border: 1px dashed #bbbbc5;
  border-radius: 4px;
`;

const StyledBottomButton = styled(Button)`
  ${({ theme }) => `
.MuiButton-label {
font-family: ${theme.typography.fontFamily};
font-style: normal;
font-weight: 600;
font-size: 15px;
line-height: 26px;
text-transform: none;
}
margin-left: 21.5px;
border-radius: 8px;
min-height: 38px;
width: 90px;
box-shadow: none;
`}
`;
const SaleOwners = [
  { id: '1', label: 'Rachel' },
  { id: '2', label: 'P2' },
  { id: '3', label: 'P3' },
];
export function DragAndDrop() {
  const theme = useTheme<Theme>();
  const [highlighted, setHighlighted] = React.useState(false);
  const [value, setValue] = React.useState('');
  const [, setUploadingFile] = React.useState<File>();
  const inputFile = React.useRef<HTMLInputElement>(null);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };
  const onButtonClick = () => {
    inputFile.current && inputFile.current.click();
  };

  const onDropUpload = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();
    const file = event.dataTransfer.files[0];
    setUploadingFile(file);
    setHighlighted(false);
  };

  const onUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.currentTarget.files) {
      const file = event.currentTarget.files[0];
      setUploadingFile(file);
      // console.log(uploadingFile);
    }
  };
  return (
    <Flex flexDirection="column" m="23px 31px">
      <StyledNote>
        <span>
          Import your leads using your own file, or use our&nbsp;
          <a href="../../demo.xlsx">sample CSV</a>
          {'. '}
        </span>
        <InfoIcon />
      </StyledNote>
      <StyledDragAndDrop
        onDragEnter={(event: React.DragEvent<HTMLDivElement>) => {
          event.preventDefault();
          event.stopPropagation();
          setHighlighted(true);
        }}
        onDragLeave={(event: React.DragEvent<HTMLDivElement>) => {
          event.preventDefault();
          event.stopPropagation();
          setHighlighted(false);
        }}
        onDragOver={(event: React.DragEvent<HTMLDivElement>) => {
          event.preventDefault();
          event.stopPropagation();
        }}
        onDrop={onDropUpload}
        highlighted={highlighted}
      >
        <StyledIcon>
          <img alt="Xlsx" src="/static/Xlsx.svg" />
        </StyledIcon>
        <Flex
          fontFamily={theme.typography.fontFamily}
          fontWeight="600"
          fontSize={14}
          lineHeight="18px"
          color="#0F0F10"
          mt="18px"
        >
          Drop or upload your file here
        </Flex>
        <Flex
          fontFamily={theme.typography.fontFamily}
          fontStyle="italic"
          fontSize={12}
          lineHeight="15px"
          color="#6C6C72"
          mt="5px"
        >
          (.csz, .xlsx formats supported)
        </Flex>
        <input
          type="file"
          accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          id="file"
          ref={inputFile}
          style={{ display: 'none' }}
          onChange={onUpload}
        />
        <StyledButton variant="contained" startIcon={<AddCircleOutlineIcon />} onClick={onButtonClick}>
          Select Files to upload
        </StyledButton>
      </StyledDragAndDrop>
      <Flex pl="20px" mt="30px">
        <FormControlLabel control={<Checkbox />} label="Update existing contacts (with overwrite)" />
      </Flex>
      <Flex
        fontFamily={theme.typography.fontFamily}
        fontWeight="600"
        fontSize={14}
        lineHeight="18px"
        color="#0F0F10"
        mt="15px"
      >
        Manage owner
      </Flex>
      <Flex flexDirection="column" width="272px" mt="15px" ml="20px">
        <IbTextField
          required={false}
          disabled={false}
          error={false}
          readOnly={false}
          select
          label="Sale owner"
          onChange={handleChange}
          value={value}
        >
          {SaleOwners.map((props) => {
            return (
              <StyledMenuItem key={props.id} value={props.label}>
                {props.label}
              </StyledMenuItem>
            );
          })}
        </IbTextField>
      </Flex>
      <Flex
        fontFamily={theme.typography.fontFamily}
        fontStyle="italic"
        fontSize={12}
        lineHeight="15px"
        color="#BBBBC5"
        mt="15px"
        ml="30px"
      >
        Sales owner will be mapped to all unassigned contacts
      </Flex>
    </Flex>
  );
}
const ImportData = () => {
  return (
    <Flex flexDirection="column" justifyContent="space-between" border="1px solid #E9E9F2">
      <Flex flexDirection="column" width={562} borderBottom="1px solid #E9E9F2" height="100%">
        <Nav drawer title="Import Contacts">
          <PointerFlex alignItems="center" p="20px">
            <StyledArrowForward />
          </PointerFlex>
        </Nav>
        <DragAndDrop />
      </Flex>
      <Flex justifyContent="flex-end" mr="32px" mt="15px" mb="20px">
        <StyledBottomButton variant="outlined">Cancel</StyledBottomButton>
        <StyledBottomButton variant="contained" color="secondary">
          Next
        </StyledBottomButton>
      </Flex>
    </Flex>
  );
};

export default ImportData;
