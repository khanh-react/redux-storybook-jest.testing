import React from 'react';

import { Flex } from '../../common/Flex';
import ImportData from './ImportData';

export default {
  title: 'Contacts/ImportData',
  component: ImportData,
};

export const Primary = () => {
  return (
    <Flex flexDirection="row" justifyContent="flex-end" height="90vh">
      <ImportData />
    </Flex>
  );
};
