import React from 'react';

import { Index } from './paymentFailed';

export default {
  title: 'Account/Subscription/Payment Failed',
  component: Index,
};

export const Default = () => {
  return <Index />;
};
