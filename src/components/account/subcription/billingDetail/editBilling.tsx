import React, { useState } from 'react';
import Wrapper, { StyledTextTitle } from './style';
import Text from '../../../common/Text';
import Button from '../../../common/Button';
import { Flex, CenterFlex } from '../../../common/Flex';
import BillingInfo from '../components/billing';

export const Index = () => {
  const [validate, setValidator] = useState(false);
  const handleClick = () => {
    setValidator(true);
  };
  return (
    <Wrapper>
      <div className="header">
        <StyledTextTitle value="Billing Info" />
        <Flex className="options" right="25% !important">
          <Button kind="default">
            <CenterFlex>
              <Text className="btn-text" color="#2e2e32" value="Cancel" />
            </CenterFlex>
          </Button>
          <Button onClick={handleClick} kind="dark">
            <CenterFlex>
              <Text className="btn-text" color="#FFF" value="Save" />
            </CenterFlex>
          </Button>
        </Flex>
      </div>
      <Flex justifyContent="center" alignItems="center" marginTop="50px" flex={1} flexDirection="column">
        <BillingInfo info validate={validate} />
      </Flex>
    </Wrapper>
  );
};

export default Index;
