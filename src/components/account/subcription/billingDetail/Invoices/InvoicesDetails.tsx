import React from 'react';
import { createStyles, Theme, withStyles, WithStyles, makeStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Wrapper, { StyledDialogTitle, StyledInvoces, StyledDialogInvoices } from '../../freeTrial/style';
import { ExampleDataTable } from '../../components/types';
import Table from './table';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(2),
      top: theme.spacing(2),
      color: 'black',
    },
  });

const useStyles = makeStyles(() =>
  createStyles({
    Content: {
      fontSize: '12px',
      fontStretch: 'normal',
      fontStyle: 'normal',
      letterSpacing: 'normal',
      color: '#2e2e32',
      marginBottom: '10px',
    },
    height: {
      height: '620px',
      background: '#fff !important',
    },
  }),
);

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <StyledDialogTitle>{children}</StyledDialogTitle>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

export const Home = ({ isShowModal, HandleOpen }: { isShowModal: boolean; HandleOpen?(): void }) => {
  const classes = useStyles();

  const handleClose = () => {
    HandleOpen && HandleOpen();
  };
  return (
    <Wrapper>
      <StyledInvoces
        onClose={handleClose}
        className={classes.height}
        aria-labelledby="customized-dialog-title"
        open={isShowModal}
        fullWidth
        maxWidth="md"
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Invoices
        </DialogTitle>
        <StyledDialogInvoices>
          <Table data={ExampleDataTable} />
        </StyledDialogInvoices>
      </StyledInvoces>
    </Wrapper>
  );
};

export default Home;
