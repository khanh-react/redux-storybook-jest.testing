import React from 'react';
import styled from 'styled-components';
import { Box, TablePagination, TableContainer, TableCell, TableBody, TableHead, TableRow } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import Text from '../../../../common/Text';
import { Flex } from '../../../../common/Flex';
import IconStories from '../../../../common/Icon';

const TableWrapper = styled.div`
  background-color: #fff;
  border-radius: 10px;
  margin: 0px 0px 10px;
  table {
    td {
      border-bottom: unset;
    }
  }
`;

const StyledStatus = styled(Box)`
  margin: auto;
  width: 80px;
  height: 19px;
  border-radius: 4px;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 19px;
  letter-spacing: 0.83px;
  text-transform: uppercase;
  text-align: center;
`;

const StyledTablePagination = styled(Flex)`
  float: right;
  p {
    font-size: 13px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 0.3px;
    text-align: right;
    color: #6c6c72;
  }
`;

const StyledDate = styled.span`
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #77777a;
`;

const StyledIcon = styled(Box)`
  svg {
    border-radius: 50%;
    background: #097af4;
    width: 16px;
    height: 16px;
    path {
      fill: #fff;
    }
  }
`;

type Props = {
  date: string;
  status: string;
  const: number;
};

export default function EnhancedTable({ data }: { data: Props[] }) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(8);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

  return (
    <TableWrapper>
      <TableContainer>
        <Table aria-labelledby="tableTitle" aria-label="enhanced table">
          <TableHead>
            <TableRow>
              <TableCell align="left">
                <Text color="#98999f" weight="bold" value="DATE" />
              </TableCell>
              <TableCell align="center">
                <Text color="#98999f" weight="bold" value="STATUS" />
              </TableCell>
              <TableCell align="center">
                <Text color="#98999f" weight="bold" value="COST" />
              </TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((value) => {
              return (
                <TableRow key={`${value.const}_${Math.floor(Math.random() * 101)}`}>
                  <TableCell align="left">
                    <StyledDate>{value.date}</StyledDate>
                  </TableCell>
                  <TableCell align="center">
                    <StyledStatus
                      bgcolor={value.status === 'paid' ? 'rgba(0, 198, 146, 0.12)' : '#feebee'}
                      color={value.status === 'paid' ? '#00c692' : '#e54545'}
                    >
                      <span>{value.status}</span>
                    </StyledStatus>
                  </TableCell>
                  <TableCell align="center">${value.const}</TableCell>
                  <TableCell>
                    <StyledIcon>
                      <IconStories name="triangle-down" />
                    </StyledIcon>
                  </TableCell>
                </TableRow>
              );
            })}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <StyledTablePagination>
        <TablePagination
          component="div"
          rowsPerPageOptions={[8, 10, 25]}
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </StyledTablePagination>
    </TableWrapper>
  );
}
