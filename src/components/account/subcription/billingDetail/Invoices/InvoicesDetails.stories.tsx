import React from 'react';

import { Home } from './InvoicesDetails';

export default {
  title: 'Account/Subscription/components/Invoices Details',
  component: Home,
};

export const Default = () => {
  return <Home isShowModal />;
};
