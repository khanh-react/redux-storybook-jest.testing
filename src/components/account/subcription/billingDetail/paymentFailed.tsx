import React from 'react';
import styled from 'styled-components';
import { StyledContainerFail, StyledIconERR } from './style';
import Text from '../../../common/Text';
import Button from '../../../common/Button';

const StyledTitleErr = styled(Text)`
  margin-top: 40px;
  font-size: 32px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #2e2e32;
`;

const StyledContentErr = styled(Text)`
  margin-top: 10px;
  width: 63%;
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.44;
  letter-spacing: normal;
  text-align: center;
  color: #77777a;
`;

const StyledButton = styled(Button)`
  margin: 60px 0 85px 0;
  width: 500px;
  height: 40px;
  font-size: 14px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #ffffff;
  background: #0f0f10;
`;

export const Index = () => {
  return (
    <StyledContainerFail flexDirection="column" justifyContent="center" alignItems="center">
      <StyledIconERR name="cancel" />
      <StyledTitleErr value="Oh no,  your payment failed" />
      <StyledContentErr value="Unfortunately something went wrong during payment. Do you want to go back to the order summary to retry?" />
      <StyledButton kind="dark">Back to Order Summy</StyledButton>
    </StyledContainerFail>
  );
};

export default Index;
