import React from 'react';

import { Index } from './billingDetail';

export default {
  title: 'Account/Subscription/Billing Detail',
  component: Index,
};

export const Default = () => {
  return <Index />;
};
