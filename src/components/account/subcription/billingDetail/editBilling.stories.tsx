import React from 'react';

import { Index } from './editBilling';

export default {
  title: 'Account/Subscription/Edit Billing',
  component: Index,
};

export const Default = () => {
  return <Index />;
};
