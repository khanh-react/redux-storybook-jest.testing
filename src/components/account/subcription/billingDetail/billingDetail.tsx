import React from 'react';
import Collapse from '@material-ui/core/Collapse';
import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';
import Button from '../../../common/Button';
import PaymentDetail from '../components/paymentDetail';
import Invoices from '../components/invoices';
import Wrapper, {
  Container,
  StyledTitleCard,
  StyledTitleText,
  StyledPaid,
  StyledPaidTotal,
  StyledSub,
  StyledSubTotal,
  StyledTextCount,
  StyledTextPaid,
  StyledFlexSum,
  StyledButtonCheckout,
  StyledCalcular,
  TitlePlan,
  TotalMoney,
  StyledFlexTotalPaid,
  StyledAlert,
  StyledTextTotal,
} from './style';
import Alert from '../../../common/Alert';
import { listPaid } from '../components/types';

type ISummary = {
  listData: DataPaid[];
  updatePlan(): void;
};
type DataPaid = {
  count: number;
  paid: number;
  nameApp: string;
  subName: string;
};
export const Summary = ({ listData, updatePlan }: ISummary) => {
  const formatMoney = (number: number) => {
    return new Intl.NumberFormat().format(number);
  };
  return (
    <Container flexDirection="column">
      <StyledTitleCard>
        <StyledTitleText value="Your imbee plan " />
      </StyledTitleCard>
      <StyledCalcular flexDirection="column">
        <StyledPaid flexDirection="column">
          <StyledTextPaid value="Monthly" />
          <TitlePlan value="Pro*" />
        </StyledPaid>
        {listData.map((value) => {
          return (
            <Flex flexDirection="column">
              <StyledPaid flexDirection="row" justifyContent="space-between">
                <StyledTextCount value={`${value.nameApp}`} />
                <StyledTextCount value={`${value.count}`} />
              </StyledPaid>
              <StyledSub flexDirection="row" justifyContent="space-between">
                <StyledTextPaid value={`${value.subName}`} />
                <StyledTextPaid value={`$ ${formatMoney(value.paid)}`} />
              </StyledSub>
            </Flex>
          );
        })}
      </StyledCalcular>
      <StyledFlexSum flexDirection="row" justifyContent="space-between">
        <StyledSubTotal flexDirection="row">
          <StyledButtonCheckout justifyContent="center">
            <Button kind="primary" onClick={() => updatePlan()}>
              Update Plan
            </Button>
          </StyledButtonCheckout>
        </StyledSubTotal>
        <StyledPaidTotal flexDirection="column">
          <Flex flexDirection="row">
            <StyledFlexTotalPaid flexDirection="column">
              <StyledTextTotal value="Total" />
              <StyledTextPaid value="per month" />
            </StyledFlexTotalPaid>
            <TotalMoney value="$98" />
          </Flex>
        </StyledPaidTotal>
      </StyledFlexSum>
    </Container>
  );
};

export const Index = () => {
  const [open, setOpen] = React.useState(false);
  return (
    <Wrapper>
      <Flex flexDirection="row" marginLeft="20px">
        <div className="header">
          <Text size="xl" weight="bold" value="Subscription" />
        </div>
        <Collapse in={open}>
          <StyledAlert zIndex={10} width="100%">
            <Alert
              type="success"
              onClose={() => {
                setOpen(false);
              }}
            >
              This is a success alert-
              <a href="https://material-ui.com/" target="blank">
                checkit out!
              </a>
            </Alert>
          </StyledAlert>
        </Collapse>
      </Flex>
      <Summary listData={listPaid} updatePlan={() => setOpen(!open)} />
      <Flex>
        <PaymentDetail />
        <Invoices />
      </Flex>
    </Wrapper>
  );
};

export default Index;
