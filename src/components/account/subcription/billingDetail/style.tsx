import styled from 'styled-components';
import { Flex } from '../../../common/Flex';
import Icon from '../../../common/Icon';
import Text from '../../../common/Text';

export default styled.div`
  width: calc(100% - 60px);
  padding: 30px;
  background-color: #f0f0f0;
  position: relative;

  .header {
    display: flex;
    flex-direction: column;
    height: 40px;
  }

  .options {
    position: absolute;
    right: 30px;
    top: 20px;
  }

  .slash {
    margin-left: 5px;
    margin-right: 5px;
  }

  .btn-text {
    margin-left: 5px;
  }

  .cell-text {
    font-family: Helvetica !important;
  }
`;

export const Container = styled(Flex)`
  margin-left: 20px;
  width: 850px;
  border-radius: 8px;
  box-shadow: 0 2px 8px 0 #e2e2e2;
  background-color: #ffffff;
`;

export const StyledTitleCard = styled(Flex)`
  height: 55px;
  margin: 0 0 18px;
  padding: 17px 0px 18px 30px;
  border-radius: 4px;
  border-bottom: 1px solid #eeeeee;
`;

export const StyledTitleText = styled(Text)`
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.05px;
  color: #263238;
`;

export const StyledPaid = styled(Flex)`
  margin: 30px 30px 0px 30px;
`;

export const StyledPaidTotal = styled(Flex)`
  margin: 0px 30px 0px 30px;
`;

export const StyledSub = styled(Flex)`
  margin: 0px 30px 10px 30px;
`;
export const StyledCalcular = styled(Flex)`
  border-bottom: 1px solid #e3e3e3;
  padding-bottom: 30px;
`;
export const StyledSubTotal = styled(Flex)`
  margin: 0px 30px 0px 30px;
`;

export const StyledTextCount = styled(Text)`
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  color: #19191a;
`;

export const StyledTextPaid = styled(Text)`
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  color: #6c6c72;
`;

export const StyledFlexSum = styled(Flex)`
  margin-top: 25px;
`;

export const StyledButtonCheckout = styled(Flex)`
  margin: 10px 0px 85px 0px;
  button {
    width: 160px;
    height: 38px;
    line-height: unset;
  }
`;

export const TitlePlan = styled(Text)`
  font-size: 24px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: rgba(0, 0, 0, 0.85);
`;

export const TotalMoney = styled(Text)`
  width: 90px;
  height: 33px;
  font-size: 30px;
  align-self: center;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: right;
  color: rgba(0, 0, 0, 0.85);
`;

export const StyledFlexTotalPaid = styled(Flex)`
  margin-top: 10px;
`;

export const StyledAlert = styled(Flex)`
  margin: -15px 0 30px 50px;
  width: 500px;
  height: 58px;
  border-radius: 6px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
`;

export const StyledContainerFail = styled(Flex)`
  border-radius: 12px;
  box-shadow: 0 3px 8px 0 rgba(187, 187, 197, 0.3);
  background-color: #ffffff;
  width: 790px;
`;

export const StyledIconERR = styled(Icon)`
  margin-top: 75px;
  svg {
    width: 76px;
    height: 76px;
    fill: #ed6e62;
    ellipse {
      stroke: #fff;
    }
    path {
      stroke: #fff;
    }
  }
`;

export const StyledTextTotal = styled(Text)`
  font-size: 16px;
  font-weight: 600;
  font-stretch: normal;
  text-align: right;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: right;
  color: rgba(0, 0, 0, 0.85);
`;

export const StyledTextTitle = styled(Text)`
  margin-left: 24%;
  font-size: 24px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.17;
  letter-spacing: normal;
  color: #3b3b3b;
`;
