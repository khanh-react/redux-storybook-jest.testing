import React, { useState } from 'react';
import { KeyboardArrowRight, KeyboardArrowDown } from '@material-ui/icons';
import { Box, ButtonGroup, Button } from '@material-ui/core';
import styled from 'styled-components';
import Wrapper from '../freeTrial/style';
import { Flex } from '../../../common/Flex';
import TaskFeature from '../components/features';
import TableFeature from '../components/table';
import Plan from '../components/plan';
import Summary from '../components/summary';
import BillInfo from '../components/billing';
import {
  dataListFeature,
  dataListFeatureAunal,
  headerFeature,
  dataBodyTable,
  dumpDataOrder,
  listPaid,
} from '../components/types';
import Text from '../../../common/Text';

const StyledLineStep = styled(Box)`
  ${({ display }) => `
  width: 30px;
  height: 4px;
  border-radius: 2px;
  background-color: ${display ? '#ffe063' : '#e8e8eb'};
`}
`;
const StyledLineEndStep = styled(Box)`
  ${({ itemScope }) => `
  width: 130px;
  height: 4px;
  border-radius: 2px;
  background-color: ${itemScope ? '#ffe063' : '#e8e8eb'};
`}
`;

const StyledButtonAnual = styled(Button)`
  ${({ itemID }) => `
  width: 127px;
  height: 38px;
  border-radius: 8px;
  border: solid 1px #19191a;
  background-color: ${itemID === 'month' ? '#2e2e32' : '#f6f7f8'};
  span {
    text-transform: capitalize;
    font-size: 15px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: ${itemID === 'month' ? '#ffffff' : '#2e2e32'};
  }
`}
`;
const StyledButtonMonth = styled(Button)`
  ${({ itemID }) => `
  width: 127px;
  height: 38px;
  border-radius: 8px;
  border: solid 1px #19191a;
  background-color: ${itemID === 'month' ? '#f6f7f8' : '#2e2e32'};
  span {
    text-transform: capitalize;
    font-size: 15px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: ${itemID === 'month' ? '#2e2e32' : '#ffffff'};
  }
`}
`;

const StyledOval = styled(Flex)`
  ${({ itemScope }) => `
  margin: -15px 4px;
  width: 33px;
  height: 33px;
  border-radius: 50px;
  background-color: ${itemScope ? '#ffe063' : '#e8e8eb'};
  span {
    margin: auto;
    font-size: 11px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.18;
    letter-spacing: normal;
    text-align: center;
    color: #0f0f10;
  }
  img {
    margin: auto;
  }
`}
`;

const StyledGroupButton = styled(Flex)`
  margin-top: 65px;
  margin-right: 2%;
`;

const StyledListFeature = styled(Flex)`
  margin-right: 15px;
`;

const StyleButtonHide = styled(Flex)`
  margin-top: 42px;
  cursor: pointer;
  #text {
    font-size: 20px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #255cfe;
  }
  #icon {
    margin-top: 2px;
    width: 14px;
    height: 8px;
    color: #255cfe;
  }
`;

const StyledContainerBill = styled(Flex)`
  width: 700px;
  border-radius: 8px;
  box-shadow: 0 2px 8px 0 #d6d6d6;
  background-color: #ffffff;
`;

const StyledTitleCard = styled(Flex)`
  height: 55px;
  margin: 0 0 18px;
  padding: 17px 0px 18px 30px;
  border-radius: 4px;
  border-bottom: 1px solid #eeeeee;
`;

const StyledTitleText = styled(Text)`
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.05px;
  color: #263238;
`;

export const Stepper = ({
  handleStepActive,
  stepActive,
}: {
  handleStepActive(step: number): void;
  stepActive?: number;
}) => {
  /* eslint-disable no-unneeded-ternary */
  const [activeStep, setActiveStep] = useState(stepActive ? stepActive : 1);
  const handleStep = (step: number) => () => {
    if (step !== activeStep) {
      setActiveStep(step);
      handleStepActive(step);
    }
  };
  /* eslint-disable consistent-return */
  const getNumber = (item: number) => {
    switch (item) {
      case 1:
        return activeStep > 1 ? <img src="static/check.svg" alt="check" /> : <span>{item}</span>;
      case 2:
        return <span>{item}</span>;
      case 3:
        return <span>{item}</span>;
      default:
        break;
    }
  };

  return (
    <Flex flexDirection="row" justifyContent="center">
      {[1, 2, 3].map((item) => {
        const count = item;
        return (
          <Flex key={item}>
            {[1].includes(count) ? <StyledLineStep display={activeStep > count || activeStep === count} /> : ''}
            <StyledOval itemScope={activeStep === count || activeStep > count} onClick={handleStep(count)}>
              {getNumber(item)}
            </StyledOval>
            {count === 3 ? (
              <StyledLineStep display={activeStep > count} />
            ) : (
              <StyledLineEndStep itemScope={activeStep > count || (activeStep === count && activeStep < 2)} />
            )}
          </Flex>
        );
      })}
    </Flex>
  );
};

export const Home = ({ activeStep }: { activeStep?: number }) => {
  /* eslint-disable no-unneeded-ternary */
  const [active, setActive] = useState(activeStep ? activeStep : 1);
  const [color, setColor] = useState('month');
  const [showAll, setShowAll] = useState(false);
  const [listFeature, setListFeature] = useState(dataListFeature);
  const handleStepActive = (step: number) => {
    setActive(step);
  };

  const handleSelect = (step: string) => {
    const newList = listFeature.splice(0, listFeature.length);
    /* eslint-disable no-param-reassign */
    newList.forEach((label) => {
      if (label.title === step) {
        label.isSelected = true;
      } else {
        label.isSelected = false;
      }
    });
    setListFeature(newList);
  };
  /* eslint-disable consistent-return */
  const tabStep = () => {
    switch (active) {
      case 1:
        return listFeature.map((dataLite) => {
          return (
            <TaskFeature
              selected={(step: string) => handleSelect(step)}
              key={dataLite.title}
              title={dataLite.title}
              subTitle={dataLite.subTitlle}
              money={dataLite.money}
              listFeature={dataLite.listFeature}
              icon={dataLite.icon}
              index={dataLite.index}
              isSelected={dataLite.isSelected}
            />
          );
        });
      case 2:
        return (
          <Flex>
            <StyledContainerBill flexDirection="column">
              <StyledTitleCard>
                <StyledTitleText value="Customize" />
              </StyledTitleCard>
              {dumpDataOrder.map((value) => {
                return (
                  <Plan
                    bgColor={value.bgColor}
                    subTitle={value.subTitle}
                    isShowMonth={value.isShowMonth}
                    paid={value.paid}
                    nameApp={value.nameApp}
                    requestMinutes={value.requestMinutes}
                    amount={value.amount}
                    unit={value.unit}
                    icon={value.icon}
                  />
                );
              })}
            </StyledContainerBill>
            <Summary listData={listPaid} />
          </Flex>
        );
      case 3:
        return (
          <Flex>
            <BillInfo />
            <Summary listData={listPaid} />
          </Flex>
        );
      default:
        break;
    }
  };

  const changeFeature = (type: string) => {
    let changeNewList = listFeature;
    if (type === 'month') {
      changeNewList = dataListFeature;
    } else {
      changeNewList = dataListFeatureAunal;
    }
    setColor(type);
    setListFeature(changeNewList);
  };

  const handleShowAllFeature = () => {
    setShowAll(!showAll);
  };

  return (
    <Wrapper>
      <Stepper handleStepActive={(step: number) => handleStepActive(step)} stepActive={activeStep} />
      <StyledGroupButton justifyContent="center">
        {active === 1 ? (
          <ButtonGroup>
            <StyledButtonMonth itemID={color} onClick={() => changeFeature('month')}>
              <span>Monthly</span>
            </StyledButtonMonth>
            <StyledButtonAnual itemID={color} onClick={() => changeFeature('anual')}>
              <span>Annual</span>
            </StyledButtonAnual>
          </ButtonGroup>
        ) : (
          ''
        )}
      </StyledGroupButton>
      <StyledListFeature flexDirection="row" justifyContent="center">
        {tabStep()}
      </StyledListFeature>
      {active === 1 ? (
        <Flex justifyContent="center">
          <StyleButtonHide onClick={handleShowAllFeature}>
            <span id="text">Hide all features</span>{' '}
            <span id="icon">{showAll ? <KeyboardArrowDown /> : <KeyboardArrowRight />}</span>
          </StyleButtonHide>
        </Flex>
      ) : (
        ''
      )}
      {showAll && active === 1 ? <TableFeature headerTable={headerFeature} bodyTable={dataBodyTable} /> : ''}
    </Wrapper>
  );
};

export default Home;
