import React from 'react';

import { Home } from './feature';

export default {
  title: 'Account/Subscription/Feature',
  component: Home,
};

export const Default = () => {
  return <Home />;
};
