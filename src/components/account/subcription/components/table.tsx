import React, { Fragment } from 'react';
import styled from 'styled-components';
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { Flex } from '../../../common/Flex';
import Icon from '../../../common/Icon';
import Text from '../../../common/Text';

const StyledContainer = styled(Flex)`
  margin-top: 65px;
  border-radius: 16px;
  box-shadow: 0 2px 9px 0 rgba(138, 138, 138, 0.5);
  background-color: #ffffff;
  .MuiTableContainer-root {
    overflow-x: unset;
  }
`;

const StyleTable = styled(Table)`
  td {
    text-align: center;
  }
  #money {
    font-size: 16px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #000000;
    margin: 0;
  }
  #name {
    margin: 0 0 3px 0;
  }
  border-radius: 16px;
  box-shadow: 0 2px 9px 0 rgba(138, 138, 138, 0.5);
  background-color: #ffffff;
  th {
    border-right: solid 1px #faf3d7;
    background-color: #ffe063;
    height: 70px;
    font-size: 24px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #000000;
  }
  tr:last-child {
    td {
      padding-bottom: 67px;
    }
  }
  th:nth-child(1) {
    width: 350px;
    background-color: #fcfaf1;
    border-top-left-radius: 16px;
  }
  th:last-child {
    border-top-right-radius: 16px;
    border-right: unset;
  }
  .MuiTableCell-root {
    border-bottom: unset;
    border-right: solid 1px #faf3d7;
  }
`;

const StyledCellLeft = styled(TableCell)`
  color: #4d4d4d;
  text-align: left !important;
  padding-left: 65px;
`;

const StyleHeaderBody = styled(TableCell)`
  text-align: left !important;
  padding-left: 65px;
  font-size: 15px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #000000;
`;

const StyledSizeIcon = styled(Icon)`
  svg {
    width: 14px;
    height: 11px;
  }
`;

const StyledTextAll = styled(Text)`
  font-size: 15px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #fbb835;
`;

type IHeaderTable = {
  name: string;
  money: number;
};
type IBodyTable = {
  nameHead: string;
  listCell: IListCell[];
};

type IListCell = {
  nameCell: string;
  lite: string;
  grow: string;
  pro: string;
  enterprise: string;
};

export const TableFeature = ({ headerTable, bodyTable }: { headerTable: IHeaderTable[]; bodyTable: IBodyTable[] }) => {
  /* eslint-disable consistent-return */
  const renderLabelCell = (item: string) => {
    switch (item) {
      case 'check':
        return <StyledSizeIcon name="sent" color="#fbb835" />;
      case 'all':
        return <StyledTextAll value="All" />;
      case 'none':
        return '';
      case '∞':
        return <StyledTextAll value="∞" />;
      default:
        return <StyledTextAll value={item} />;
    }
  };
  /* eslint-disable consistent-return */
  const renderHeadCell = (item: string) => {
    switch (item) {
      case 'Reporting & Statistic':
        return <StyledTextAll value="Advanced" />;
      case 'Integration (zapier, shopify etc)':
        return <StyledSizeIcon name="sent" color="#fbb835" />;
      default:
        return '';
    }
  };

  return (
    <StyledContainer>
      <TableContainer>
        <StyleTable aria-labelledby="tableTitle" aria-label="enhanced table">
          <TableHead>
            <TableRow>
              {headerTable.map((label) => {
                return (
                  <TableCell align="center" key={label.name}>
                    <p id="name">{label.name}</p>
                    <p id="money">{label.money > 0 ? `$${label.money}/mo` : ''}</p>
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {bodyTable.map((value) => {
              const count = Math.floor(Math.random() * 10) + 1;
              return (
                <Fragment key={`${value.nameHead}_${count}`}>
                  <TableRow>
                    <StyleHeaderBody>{value.nameHead}</StyleHeaderBody>
                    <TableCell> </TableCell>
                    <TableCell>
                      {value.nameHead === 'Reporting & Statistic' ? <StyledTextAll value="Basic" /> : ''}
                    </TableCell>
                    <TableCell>{renderHeadCell(value.nameHead)}</TableCell>
                    <TableCell>{renderHeadCell(value.nameHead)}</TableCell>
                  </TableRow>
                  {value.listCell.map((label) => {
                    return (
                      <TableRow key={`${label.nameCell}_${count}`}>
                        <StyledCellLeft>{label.nameCell}</StyledCellLeft>
                        <TableCell>{renderLabelCell(label.lite)}</TableCell>
                        <TableCell>{renderLabelCell(label.grow)}</TableCell>
                        <TableCell>{renderLabelCell(label.pro)}</TableCell>
                        <TableCell>{renderLabelCell(label.enterprise)}</TableCell>
                      </TableRow>
                    );
                  })}
                </Fragment>
              );
            })}
          </TableBody>
        </StyleTable>
      </TableContainer>
    </StyledContainer>
  );
};

export default TableFeature;
