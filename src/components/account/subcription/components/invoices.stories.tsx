import React from 'react';

import { Invoices } from './invoices';

export default {
  title: 'Account/Subscription/components/invoices',
  component: Invoices,
};

export const Default = () => {
  return <Invoices />;
};
