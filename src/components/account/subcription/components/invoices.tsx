import React from 'react';
import styled from 'styled-components';
import { Box, Button } from '@material-ui/core';
import { Flex } from '../../../common/Flex';
import { StyledTitleText, StyledTitleCard } from '../billingDetail/style';
import Text from '../../../common/Text';
import IconStories from '../../../common/Icon';
import InvoicesModal from '../billingDetail/Invoices/InvoicesDetails';

const Container = styled(Flex)`
  margin: 30px 0 0 0;
  width: 411px;
  height: 310px;
  border-radius: 8px;
  box-shadow: 0 2px 14px 0 #ebebeb;
  background-color: #ffffff;
`;

const StyledTitile = styled(StyledTitleCard)`
  width: 100%;
`;

const StyledExpired = styled(Text)`
  width: 100%;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #98999f;
  margin: 5px 0 0 16px;
`;

const StyledIcon = styled(Box)`
  svg {
    border-radius: 50%;
    background: #097af4;
    width: 16px;
    height: 16px;
    path {
      fill: #fff;
    }
  }
`;

const StyledTextMoney = styled(Text)`
  margin-right: 10px;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: right;
  color: #2e2e32;
`;

const StyledFlexBody = styled(Flex)`
  margin-right: 10px;
`;
const StyledContent = styled(Flex)`
  margin-bottom: 20px;
`;

const StyledButton = styled(Button)`
  text-transform: capitalize;
  margin-top: 25px;
  font-size: 14px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.11px;
  text-align: center;
  color: #097af4;
`;

const exampleData = [
  {
    date: 'December 02, 2020',
    paid: '$49',
  },
  {
    date: 'Nov 02, 2020',
    paid: '$49',
  },
  {
    date: 'Oct 02, 2020',
    paid: '$49',
  },
  {
    date: 'Sep 02, 2020',
    paid: '$49',
  },
];

export const Invoices = () => {
  const [open, setOpen] = React.useState(false);
  const HandleOpen = () => {
    setOpen(!open);
  };
  return (
    <Flex flexDirection="column">
      <Container flexDirection="column">
        <StyledTitile>
          <StyledTitleText value="Invoices" />
        </StyledTitile>
        {exampleData.map((value) => {
          return (
            <StyledContent key={value.date} flexDirection="row" justifyContent="space-between">
              <StyledExpired value={`${value.date}`} />
              <StyledFlexBody flexDirection="row">
                <StyledTextMoney value={`${value.paid}`} />
                <StyledIcon>
                  <IconStories name="triangle-down" />
                </StyledIcon>
              </StyledFlexBody>
            </StyledContent>
          );
        })}
        <StyledButton onClick={HandleOpen} href="" color="primary">
          View All
        </StyledButton>
      </Container>
      <InvoicesModal isShowModal={open} HandleOpen={HandleOpen} />
    </Flex>
  );
};

export default Invoices;
