import React from 'react';

import { Billing } from './billing';

export default {
  title: 'Account/Subscription/components/billing',
  component: Billing,
};

export const Default = () => {
  return <Billing />;
};
