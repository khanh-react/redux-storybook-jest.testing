import React from 'react';
import styled from 'styled-components';
import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';
import Button from '../../../common/Button';

const Container = styled(Flex)`
  margin-left: 20px;
  width: 356px;
  border-radius: 8px;
  box-shadow: 0 2px 8px 0 #e2e2e2;
  background-color: #ffffff;
`;

const StyledTitleCard = styled(Flex)`
  height: 55px;
  margin: 0 0 18px;
  padding: 17px 0px 18px 30px;
  border-radius: 4px;
  border-bottom: 1px solid #eeeeee;
`;

const StyledTitleText = styled(Text)`
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.05px;
  color: #263238;
`;

const StyledPaid = styled(Flex)`
  margin: 30px 30px 0px 30px;
`;

const StyledPaidTotal = styled(Flex)`
  margin: 0px 30px 0px 30px;
`;

const StyledSub = styled(Flex)`
  margin: 0px 30px 10px 30px;
`;
const StyledCalcular = styled(Flex)`
  border-bottom: 1px solid #e3e3e3;
  padding-bottom: 30px;
`;
const StyledSubTotal = styled(Flex)`
  margin: 0px 30px 0px 30px;
`;

const StyledTextCount = styled(Text)`
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 2;
  letter-spacing: normal;
  color: #19191a;
`;

const StyledTextPaid = styled(Text)`
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 2;
  letter-spacing: normal;
  color: #6c6c72;
`;

const StyledFlexSum = styled(Flex)`
  margin-top: 25px;
`;

const StyledButtonCheckout = styled(Flex)`
  margin: 30px 0px 85px 0px;
  button {
    width: 302px;
    height: 38px;
    line-height: unset;
  }
`;

type ISummary = {
  listData: DataPaid[];
};
type DataPaid = {
  count: number;
  paid: number;
  nameApp: string;
  subName: string;
};
export const Summary = ({ listData }: ISummary) => {
  const formatMoney = (number: number) => {
    return new Intl.NumberFormat().format(number);
  };
  return (
    <Container flexDirection="column">
      <StyledTitleCard>
        <StyledTitleText value="Summary" />
      </StyledTitleCard>
      <StyledCalcular flexDirection="column">
        {listData.map((value) => {
          return (
            <Flex flexDirection="column">
              <StyledPaid flexDirection="row" justifyContent="space-between">
                <StyledTextCount value={`${value.nameApp}`} />
                <StyledTextCount value={`${value.count}`} />
              </StyledPaid>
              <StyledSub flexDirection="row" justifyContent="space-between">
                <StyledTextPaid value={`${value.subName}`} />
                <StyledTextPaid value={`$ ${formatMoney(value.paid)}`} />
              </StyledSub>
            </Flex>
          );
        })}
      </StyledCalcular>
      <StyledFlexSum flexDirection="column">
        <StyledSubTotal flexDirection="row" justifyContent="space-between">
          <StyledTextPaid value="Sub-total" />
          <StyledTextPaid value="$6,610" />
        </StyledSubTotal>
        <StyledPaidTotal flexDirection="row" justifyContent="space-between">
          <StyledTextCount value="Total" />
          <StyledTextCount value="$6,610" />
        </StyledPaidTotal>
        <StyledButtonCheckout justifyContent="center">
          <Button kind="dark">Checkout</Button>
        </StyledButtonCheckout>
      </StyledFlexSum>
    </Container>
  );
};

export default Summary;
