import React from 'react';
import styled from 'styled-components';
import { Box, Button } from '@material-ui/core';
import { Flex } from '../../../common/Flex';
import { StyledTitleText, StyledTitleCard } from '../billingDetail/style';
import Text from '../../../common/Text';

const Container = styled(Flex)`
  margin: 30px 30px 0 20px;
  width: 411px;
  height: 310px;
  border-radius: 8px;
  box-shadow: 0 2px 14px 0 #ebebeb;
  background-color: #ffffff;
`;

const StyledTitile = styled(StyledTitleCard)`
  width: 100%;
`;
const StyledIconVisa = styled(Box)`
  text-align: center;
  width: 75px;
  height: 50px;
  border: solid 1px #e4e4e4;
  margin: 40px 0px 0px 35px;
  border-radius: 6px;
`;
const StyledImageVisa = styled.img`
  width: 100%;
`;

const StyledFlexDot = styled(Flex)`
  margin: 5% 0 0 0;
  img {
    margin-left: 20px;
    width: 32px;
  }
`;
const StyledTextFooter = styled(Flex)`
  margin: 36px 0 0 16px;
`;

const StyledFlexCardNumber = styled(Flex)`
  align-items: center;
  margin: 5% 0 0 20px;
`;

const StyledExpired = styled(Text)`
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #98999f;
  margin: 5px 0 0 16px;
`;

const StyledButton = styled(Button)`
  text-transform: capitalize;
  margin-top: 25px;
  font-size: 14px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.11px;
  text-align: center;
  color: #097af4;
`;

export const Home = () => {
  return (
    <Container flexDirection="column">
      <StyledTitile>
        <StyledTitleText value="Payment details" />
      </StyledTitile>
      <Flex flexDirection="row">
        <StyledIconVisa>
          <StyledImageVisa alt="visa" src="static/visa.svg" />
        </StyledIconVisa>
        <StyledFlexDot flexDirection="column" justifyContent="center">
          <Flex flexDirection="row">
            <StyledFlexDot>
              {/* eslint-disable jsx-a11y/alt-text */}
              <img src="static/dot.png" />
            </StyledFlexDot>
            <StyledFlexDot>
              {/* eslint-disable jsx-a11y/alt-text */}
              <img src="static/dot.png" />
            </StyledFlexDot>
            <StyledFlexDot>
              {/* eslint-disable jsx-a11y/alt-text */}
              <img src="static/dot.png" />
            </StyledFlexDot>
            <StyledFlexCardNumber>
              <Text value="9" />
              <Text value="9" />
              <Text value="9" />
              <Text value="9" />
            </StyledFlexCardNumber>
          </Flex>
          <StyledExpired value="Visa Card - Expires 02/2024" />
        </StyledFlexDot>
      </Flex>
      <StyledTextFooter flexDirection="column">
        <StyledExpired value="You are billed monthly." />
        <StyledExpired value="Next billing on January 28, 2021" />
      </StyledTextFooter>
      <StyledButton href="#" color="primary">
        Update Payment Method
      </StyledButton>
    </Container>
  );
};

export default Home;
