import React from 'react';

import { Summary } from './summary';
import { listPaid } from './types';

export default {
  title: 'Account/Subscription/components/summary',
  component: Summary,
};

export const Default = () => {
  return <Summary listData={listPaid} />;
};
