import React from 'react';

import { Plan } from './plan';

export default {
  title: 'Account/Subscription/components/plan',
  component: Plan,
};

export const Default = () => {
  return (
    <Plan
      bgColor="#fff"
      icon="whatsapp"
      nameApp="PRO*"
      subTitle="Add more number  to your inbox channel."
      amount={0}
      requestMinutes="asd"
      paid={49}
      unit="user"
    />
  );
};
