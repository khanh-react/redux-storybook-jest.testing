import React, { useState } from 'react';
import styled from 'styled-components';
import { Add, Remove } from '@material-ui/icons';
import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';
import { IbTextField } from '../../../common/InputField';
import Icon, { IconName } from '../../../common/Icon';
import Button from '../../../common/Button';

const StyledContainer = styled(Flex)`
  width: 620px;
  height: 160px;
  border-radius: 8px;
  box-shadow: 0 2px 14px 0 #ebebeb;
  margin: 25px;
`;

const StyleTextPlan = styled(Text)`
  font-size: 12px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #6c6c72;
`;

const StyledNameCard = styled(Flex)`
  margin: 35px 0px 35px 35px;
  width: 170px;
`;

const StyledTextPro = styled(Text)`
  margin-top: 5px;
  font-size: 24px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: rgba(0, 0, 0, 0.85);
`;

const StyledTextPaid = styled(Text)`
  ${({ value }) => `
  margin-top: 5px;
  font-size: 12px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: ${value !== 'paid monthly' ? '#6c6c72' : '#2c87d8'};
`}
`;

const StyleIconRemove = styled(Remove)`
  height: 27px;
  margin-right: 5px;
`;

const StyleCountNumber = styled(IbTextField)`
  width: 60px;
  height: 30px;
  border-radius: 4px;
  .MuiOutlinedInput-input {
    padding: 6px 15px 6px;
  }
`;

const StyleIconAdd = styled(Add)`
  height: 27px;
  margin-left: 5px;
`;

const StyleAddFeature = styled(Flex)`
  margin-top: 55px;
`;

const StyleIcon = styled(Icon)`
  svg {
    width: 25px;
    height: 25px;
  }
`;

const StyleMount = styled(Flex)`
  width: 143px;
  margin: 41px 35px 35px 0;
  #unit {
    margin: 13px 10px 0px 0px;
    font-size: 16px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #2e2e32;
  }
  #amount {
    font-size: 50px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: rgba(0, 0, 0, 0.85);
  }
`;

const StyledClassify = styled(Flex)`
  margin: 13px;
  #species {
    font-size: 16px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #2e2e32;
  }
  #user {
    font-size: 12px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #6c6c72;
  }
`;

type IPlan = {
  icon: IconName;
  nameApp: string;
  amount: number;
  paid: number;
  subTitle?: string;
  bgColor: string;
  requestMinutes?: string;
  unit: string;
  isShowMonth?: boolean;
};
export const Plan = ({ bgColor, icon, nameApp, subTitle, amount, paid, unit, requestMinutes, isShowMonth }: IPlan) => {
  const [number, setNumber] = useState(amount);
  const calculator = (type: string) => {
    let count: number = number;
    if (type === 'sub') {
      count--;
    } else {
      count++;
    }
    setNumber(count);
  };

  const addPlan = () => {
    setNumber(1);
  };
  const getValueNumber = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNumber(parseInt(e.target.value, 10));
  };
  return (
    <StyledContainer bgcolor={bgColor} flexDirection="row" justifyContent="space-between">
      <StyledNameCard flexDirection="column">
        {icon !== 'warning' ? (
          <StyleIcon name={icon} width="25px" height="25px" />
        ) : (
          <StyleTextPlan value="Your imbee plan" />
        )}
        <StyledTextPro value={nameApp} />
        <StyledTextPaid value={subTitle !== 'paid monthly' ? subTitle : 'paid monthly'} />
      </StyledNameCard>
      <StyleAddFeature flexDirection="column">
        {number > 0 ? (
          <Flex>
            <StyleIconRemove onClick={() => calculator('sub')} fontSize="small" />
            <StyleCountNumber value={number} onChange={getValueNumber} />
            <StyleIconAdd onClick={() => calculator('plus')} fontSize="small" />
          </Flex>
        ) : (
          <Button kind="dark" onClick={addPlan}>
            Add
          </Button>
        )}
        {requestMinutes ? <StyleTextPlan value={requestMinutes} /> : <StyleTextPlan value={requestMinutes} />}
      </StyleAddFeature>
      <StyleMount flexDirection="column">
        <Flex flexDirection="row">
          <span id="unit">$</span>
          <span id="amount">{paid}</span>
          <StyledClassify flexDirection="column">
            <span id="species">USD</span>
            <span id="user">/ {unit}</span>
          </StyledClassify>
        </Flex>
        {isShowMonth ? <StyleTextPlan value="monthly" /> : ''}
      </StyleMount>
    </StyledContainer>
  );
};

export default Plan;
