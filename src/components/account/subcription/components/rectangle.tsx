import React from 'react';
import styled from 'styled-components';
import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';

const FlagRetangle = styled(Flex)`
  margin: 0 22px 0 auto;
  #flag {
    width: 42px;
    height: 86px;
    box-sizing: content-box;
    padding-top: 15px;
    position: relative;
    background: #ffe063;
    color: white;
    font-size: 11px;
    letter-spacing: 0.2em;
    text-align: center;
    text-transform: uppercase;
  }
  #flag:after {
    content: '';
    position: absolute;
    left: 0;
    bottom: 0;
    width: 0;
    height: 0;
    border-bottom: 15px solid #fff;
    border-left: 20px solid transparent;
    border-right: 20px solid transparent;
  }
`;

const StyledText = styled(Text)`
  font-size: 15px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #0f0f10;
`;

const Transform = styled.div`
  transform: rotate(-270deg);
  margin-top: 6px;
`;

export const Flag = () => {
  return (
    <FlagRetangle>
      <div id="flag">
        <Transform>
          <StyledText value="Popular" />
        </Transform>
      </div>
    </FlagRetangle>
  );
};

export default Flag;
