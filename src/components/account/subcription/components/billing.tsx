import React, { useState } from 'react';
import styled from 'styled-components';
import { Box, FormControlLabel, Checkbox } from '@material-ui/core';
import { Flex } from '../../../common/Flex';
import { IbTextField, StyledMenuItem } from '../../../common/InputField';
import Text from '../../../common/Text';

const Container = styled(Flex)`
  width: 700px;
  border-radius: 8px;
  box-shadow: 0 2px 8px 0 #d6d6d6;
  background-color: #ffffff;
  padding: 25px 0 50px 35px;
`;

const StyledTitle = styled(Text)`
  font-size: 24px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.17px;
  color: rgba(0, 0, 0, 0.85);
`;

const StyledInputEmail = styled(IbTextField)`
  width: 620px;
  border-radius: 8px;
  margin: 30px 40px 0 0;
`;

const StyledInputCity = styled(IbTextField)`
  width: 227px;
`;

const StyledAddressInput = styled(Flex)`
  margin: 30px 40px 30px 0;
  border-bottom: 2px dashed #e3e3e3;
  padding-bottom: 50px;
`;

const StyledZipcode = styled(IbTextField)`
  width: 150px;
`;

const StyledTitleCard = styled(Text)`
  font-size: 24px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.17px;
  color: rgba(0, 0, 0, 0.85);
`;

const StyledNameCard = styled.span`
  font-size: 12px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1;
  letter-spacing: 0.4px;
  color: #0f0f10;
  margin: 25px 0 10px 0;
  span {
    color: red;
  }
`;

const StyledInputCardHolder = styled.input`
  border-radius: 8px;
  width: 600px;
  border: solid 1px #d1d1d1;
  height: 45px;
  padding-left: 20px;
`;

const StyledFormCard = styled(Flex)`
  ${({ color }) => `
  width: 620px;
  border: solid 1px ${color};
  border-radius: 8px;
`}
`;

const StyledHeadForm = styled(Flex)`
  width: 100%;
  position: relative;
`;

const StyledNumberCard = styled(IbTextField)`
  ${({ contextMenu }) => `
  fieldset {
    border: none;
  }
  border-bottom: 1px solid ${contextMenu};
`}
`;

const StyledNumberCardInfo = styled(IbTextField)`
  fieldset {
    border: none;
  }
`;
const StyledNumberCardInfor = styled(IbTextField)`
  ${({ contextMenu }) => `
  border-right: 1px solid ${contextMenu};
  fieldset {
    border: none;
  }
`}
`;

const StyledIconMasterCard = styled(Box)`
  text-align: center;
  width: 36px;
  position: absolute;
  height: 23px;
  right: 100px;
  border: solid 1px #e4e4e4;
  margin: 13px 10px 14px;
  border-radius: 6px;
`;

const StyledImageMaster = styled.img`
  width: 24px;
`;

const StyledImageVisa = styled.img`
  width: 32px;
  margin-top: 1px;
`;

const StyledIconVisa = styled(Box)`
  text-align: center;
  width: 36px;
  position: absolute;
  height: 23px;
  right: 50px;
  border: solid 1px #e4e4e4;
  margin: 13px 10px 14px;
  border-radius: 6px;
`;
const StyledIconEmpress = styled(Box)`
  text-align: center;
  width: 36px;
  position: absolute;
  height: 23px;
  right: 0;
  border: solid 1px #e4e4e4;
  margin: 13px 10px 14px;
  border-radius: 6px;
`;

const StyledFlexCheckBox = styled(Flex)`
  margin: 30px 40px 0 0;
  width: 620px;
  height: 50px;
  border-radius: 6px;
  background-color: rgba(121, 217, 255, 0.1);
`;

const StyledCheckBox = styled(FormControlLabel)`
  margin-left: 10px;
  svg {
    fill: #097af4;
  }
`;

const StyledTextErr = styled.span`
  margin-top: 10px;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.14;
  letter-spacing: normal;
  color: #e65063;
`;

export const Billing = ({ info, validate }: { info?: boolean; validate?: boolean }) => {
  const [value, setValue] = useState('CA');
  const [checked, setChecked] = useState(false);
  const handleChangeValue = (event: React.ChangeEvent<{ value: unknown }>) => {
    setValue(event.target.value as string);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  return (
    <Container flexDirection="column">
      {info ? '' : <StyledTitle value="Billing Info" />}
      <StyledInputEmail defaultValue="johnny.wong@email.com" label="Email" required />
      <StyledInputEmail defaultValue="Johnny Wong" label="FullName" required />
      <StyledInputEmail defaultValue="Apartment 2C" label="Address line 1" required />
      <StyledInputEmail defaultValue=" 213 Derrick Street" label="Address line 2" required={!info} />
      <StyledAddressInput flexDirection="row" justifyContent="space-between">
        <StyledInputCity defaultValue="Los Angeles" label="City" required />
        <StyledInputCity label="State" onChange={handleChangeValue} value={value} required select>
          <StyledMenuItem value="CA">CA</StyledMenuItem>
          <StyledMenuItem value="admin">admin</StyledMenuItem>
          <StyledMenuItem value="client">client</StyledMenuItem>
        </StyledInputCity>
        <StyledZipcode defaultValue="94133" label="Zip Code" required={!info} />
      </StyledAddressInput>
      <StyledTitleCard value="Credit Card" />
      <StyledNameCard>Card Holder</StyledNameCard>
      <StyledInputCardHolder placeholder="John Smith" />
      <StyledNameCard>Card information{info ? <span>*</span> : ''}</StyledNameCard>
      <StyledFormCard color={validate ? '#e75264' : '#d1d1d1'} flexDirection="column">
        <StyledHeadForm flexDirection="row">
          <StyledNumberCard contextMenu={validate ? '#e75264' : '#d1d1d1'} placeholder="0000 0000 0000 0000 " />
          <StyledIconMasterCard>
            <StyledImageMaster alt="master card" src="static/mastercard.svg" />
          </StyledIconMasterCard>
          <StyledIconVisa>
            <StyledImageVisa alt="visa" src="static/visa.svg" />
          </StyledIconVisa>
          <StyledIconEmpress>
            <StyledImageVisa alt="empress" src="static/empress.svg" />
          </StyledIconEmpress>
        </StyledHeadForm>
        <StyledHeadForm flexDirection="row">
          <StyledNumberCardInfor contextMenu={validate ? '#e75264' : '#d1d1d1'} placeholder="MM/YY" />
          <StyledNumberCardInfo placeholder="CVC" />
        </StyledHeadForm>
      </StyledFormCard>
      {validate ? <StyledTextErr>This field is required.</StyledTextErr> : ''}
      {!info ? (
        <StyledFlexCheckBox>
          <StyledCheckBox
            control={<Checkbox checked={checked} onChange={handleChange} name="checkedC" />}
            label={
              <Flex>
                <span>I agree to the imBee for </span>&nbsp;<a href="#">Teams Agreemen</a>.
              </Flex>
            }
          />
        </StyledFlexCheckBox>
      ) : (
        ''
      )}
      {info ? '' : <StyledNameCard>All amounts shown are in US dollars.</StyledNameCard>}
    </Container>
  );
};

export default Billing;
