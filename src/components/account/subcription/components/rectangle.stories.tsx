import React from 'react';

import { Flag } from './rectangle';

export default {
  title: 'Account/Subscription/components/flag',
  component: Flag,
};

export const Default = () => {
  return <Flag />;
};
