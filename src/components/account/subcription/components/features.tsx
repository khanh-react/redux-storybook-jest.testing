import React from 'react';
import styled from 'styled-components';
import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';
import BootstrapButton from '../../../common/Button';
import Flag from './rectangle';

const StyledFlag = styled(Flex)`
  position: absolute;
  right: 0px;
  top: -1px;
`;

const StyledContainer = styled(Flex)`
  ${({ id }) => `
  position: relative;
  margin: 35px 15px 0px 0px;
  border-radius: 10px;
  box-shadow: 0 2px 14px 0 rgba(208, 208, 208, 0.5);
  background-color: #ffffff;
  width: 254px;
  height: 730px;
  border: ${id ? 'solid 1.5px #ffe063' : 'unset'};
`}
`;

const StyledImage = styled.img`
  align-self: center;
  width: 40px;
  height: 40px;
  margin-top: 45px;
`;

const StyledTitle = styled(Text)`
  margin-top: 15px;
  font-size: 24px;
  font-weight: 900;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: rgba(0, 0, 0, 0.85);
`;

const StyledSubTitle = styled(Text)`
  height: 45px;
  padding: 0 30px;
  font-size: 13px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #6c6c72;
`;

const StyledMoney = styled(Flex)`
  margin-top: 45px;
  #unit {
    margin: 13px 10px 0px 0px;
    font-size: 16px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #2e2e32;
  }
  #amount {
    font-size: 50px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: rgba(0, 0, 0, 0.85);
  }
`;

const StyledClassify = styled(Flex)`
  margin: 13px;
  #species {
    font-size: 16px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #2e2e32;
  }
  #user {
    font-size: 12px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #6c6c72;
  }
`;

const StyledTextPaid = styled(Text)`
  cursor: pointer;
  margin-top: 18px;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #2c87d8;
`;

const StyledTextSoon = styled(Text)`
  margin-top: 41px;
  font-size: 16px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #77777a;
`;

const StyledListFeature = styled(Flex)`
  margin-top: auto;
  padding: 30px 25px 50px 25px;
  width: 100%;
  height: 242px;
  border-bottom-right-radius: 10px;
  border-bottom-left-radius: 10px;
  span {
    font-size: 12px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.83;
    letter-spacing: -0.24px;
    text-align: center;
    color: #6c6c72;
  }
`;

const StyledButtonContact = styled(BootstrapButton)`
  margin-top: 125px;
  border-radius: 8px;
  border: solid 1px #19191a;
  background: #fff;
  width: 145px;
  height: 38px;
  align-self: center;
  span {
    font-size: 15px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #0f0f10;
  }
}
`;

const StyledButton = styled(BootstrapButton)`
  margin-top: 35px;
  width: 160px;
  height: 38px;
  background: rgba(0, 0, 0, 0.87);
  align-self: center;
  span {
    font-size: 14px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
  }
`;

type IFeatures = {
  title: string;
  subTitle: string;
  money: number;
  listFeature: string[];
  icon: string;
  index: number;
  selected?(title: string): void;
  isSelected?: boolean;
};

export const Features = ({ title, subTitle, money, listFeature, icon, index, selected, isSelected }: IFeatures) => {
  const isActive = () => {
    selected && selected(title);
  };

  const renderButtonContact = () => {
    if (money > 0) {
      return isSelected ? (
        <StyledButton kind="dark">
          <Text value="Select Plan" />
        </StyledButton>
      ) : (
        <StyledTextSoon value="Comming Soon" />
      );
    }
    return (
      <StyledButtonContact kind="default">
        <Text color="black" value="Contact Us" />
      </StyledButtonContact>
    );
  };
  return (
    <StyledContainer id={isSelected ? 'solid' : ''} flexDirection="column" onClick={isActive}>
      {index === 3 ? (
        <StyledFlag>
          <Flag />
        </StyledFlag>
      ) : (
        ''
      )}
      <StyledImage src={icon} />
      <StyledTitle value={title} />
      <StyledSubTitle value={subTitle} />
      {money > 0 ? (
        <StyledMoney flexDirection="row" justifyContent="center">
          <span id="unit">$</span>
          <span id="amount">{money}</span>
          <StyledClassify flexDirection="column">
            <span id="species">USD</span>
            <span id="user">/ user</span>
          </StyledClassify>
        </StyledMoney>
      ) : (
        ''
      )}
      {money > 0 ? <StyledTextPaid value="paid monthly" /> : ''}
      {renderButtonContact()}
      <StyledListFeature bgcolor="#faf7ee" alignItems="center" flexDirection="column">
        {listFeature.map((item) => {
          return <span key={item}>{item}</span>;
        })}
      </StyledListFeature>
    </StyledContainer>
  );
};

export default Features;
