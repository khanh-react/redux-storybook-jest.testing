import React from 'react';

import { Features } from './features';
import { dataLite } from './types';

export default {
  title: 'Account/Subscription/components/features',
  component: Features,
};

export const Default = () => {
  return (
    <Features
      title={dataLite.title}
      subTitle={dataLite.subTitlle}
      money={dataLite.money}
      listFeature={dataLite.listFeature}
      icon={dataLite.icon}
      index={1}
    />
  );
};
