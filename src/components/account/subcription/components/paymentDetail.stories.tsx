import React from 'react';

import { Home } from './paymentDetail';

export default {
  title: 'Account/Subscription/components/Payment Detail',
  component: Home,
};

export const Default = () => {
  return <Home />;
};
