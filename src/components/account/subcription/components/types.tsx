import { IconName } from '../../../common/Icon';

export const dataLite = {
  title: 'Lite',
  subTitlle: 'Suitable for just starting out',
  money: 19,
  listFeature: [
    'Max 2 Agents',
    'Web Widget',
    'All Channels',
    'IOS& Android',
    'Team Inbox, e.g.internal chat, @tags',
    '200 Campaign & Automated Messages',
  ],
  icon: 'static/subscriptionplan-imge-lite.svg',
};

export const dataListFeature = [
  {
    index: 1,
    isSelected: false,
    title: 'Lite',
    subTitlle: 'Suitable for just starting out',
    money: 16,
    listFeature: [
      'Max 2 Agents',
      'Web Widget',
      'All Channels',
      'IOS& Android',
      'Team Inbox, e.g.internal chat, @tags',
      '200 Campaign & Automated Messages',
    ],
    icon: 'static/subscriptionplan-imge-lite.svg',
  },
  {
    index: 2,
    isSelected: false,
    title: 'Grow',
    subTitlle: 'More scheduled messages, chatbot and analytics.',
    money: 25,
    listFeature: [
      'Everything in Lite +',
      'Max 50 Agents',
      'Unlimited scheduled mesasges',
      'AI Chatbot',
      'Analytics',
      'Role-based Access Control',
      'Custom Field',
    ],
    icon: 'static/subgrow.svg',
  },
  {
    index: 3,
    isSelected: true,
    title: 'Pro',
    subTitlle: 'Suitable for just starting out',
    money: 42,
    listFeature: [
      'Everything in Grow +',
      'Max 150 Agents',
      'All Channels',
      'Advanced Automation',
      'Reporting  Stauts',
      'Public apps integrations',
      'API',
      'Priority Suport',
    ],
    icon: 'static/plan-imge-pro.svg',
  },
  {
    index: 4,
    isSelected: false,
    title: 'Enterprise',
    subTitlle: 'More everything',
    money: 0,
    listFeature: ['Everything unlimited', 'Multiple teams', 'Personal account manager'],
    icon: 'static/enterprise.svg',
  },
];

export const dataListFeatureAunal = [
  {
    index: 1,
    isSelected: false,
    title: 'Lite',
    subTitlle: 'Suitable for just starting out',
    money: 19,
    listFeature: [
      'Max 2 Agents',
      'Web Widget',
      'All Channels',
      'IOS& Android',
      'Team Inbox, e.g.internal chat, @tags',
      '200 Campaign & Automated Messages',
    ],
    icon: 'static/subscriptionplan-imge-lite.svg',
  },
  {
    index: 2,
    isSelected: false,
    title: 'Grow',
    subTitlle: 'More scheduled messages, chatbot and analytics.',
    money: 29,
    listFeature: [
      'Everything in Lite +',
      'Max 50 Agents',
      'Unlimited scheduled mesasges',
      'AI Chatbot',
      'Analytics',
      'Role-based Access Control',
      'Custom Field',
    ],
    icon: 'static/subgrow.svg',
  },
  {
    index: 3,
    isSelected: true,
    title: 'Pro',
    subTitlle: 'Suitable for just starting out',
    money: 49,
    listFeature: [
      'Everything in Grow +',
      'Max 150 Agents',
      'All Channels',
      'Advanced Automation',
      'Reporting  Stauts',
      'Public apps integrations',
      'API',
      'Priority Suport',
    ],
    icon: 'static/plan-imge-pro.svg',
  },
  {
    index: 4,
    isSelected: false,
    title: 'Enterprise',
    subTitlle: 'More everything',
    money: 0,
    listFeature: ['Everything unlimited', 'Multiple teams', 'Personal account manager'],
    icon: 'static/enterprise.svg',
  },
];

export const headerFeature = [
  { name: 'Features', money: 0 },
  { name: 'Lite', money: 19 },
  { name: 'Grow', money: 39 },
  { name: 'Pro', money: 49 },
  { name: 'Enterprise', money: 0 },
];

export const dataBodyTable = [
  {
    nameHead: 'Team Inbox',
    listCell: [
      {
        nameCell: 'Intenal communication',
        lite: 'check',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
      {
        nameCell: 'Web Widget',
        lite: 'check',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
      {
        nameCell: 'Channels',
        lite: 'all',
        grow: 'all',
        pro: 'all',
        enterprise: 'all',
      },
      {
        nameCell: 'iOS & Android',
        lite: 'check',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
      {
        nameCell: 'Ticket',
        lite: 'none',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
      {
        nameCell: 'Assignment chat @peter',
        lite: 'none',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
      {
        nameCell: 'Teams',
        lite: 'none',
        grow: 'none',
        pro: 'none',
        enterprise: 'check',
      },
      {
        nameCell: 'Access Control',
        lite: 'none',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
    ],
  },
  {
    nameHead: 'AI Automation & Scheduled Messages',
    listCell: [
      {
        nameCell: 'Scheduled Messages and Reminder',
        lite: '200/mpnth',
        grow: '∞',
        pro: '∞',
        enterprise: '∞',
      },
      {
        nameCell: 'AI Chatbot Automation',
        lite: 'none',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
    ],
  },
  {
    nameHead: 'Reporting & Statistic',
    listCell: [
      {
        nameCell: 'Custom fields, profile',
        lite: 'none',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
    ],
  },
  {
    nameHead: 'Security',
    listCell: [
      {
        nameCell: 'Advanced Security and Compliance',
        lite: 'none',
        grow: 'none',
        pro: 'check',
        enterprise: 'check',
      },
    ],
  },
  {
    nameHead: 'Add-on',
    listCell: [
      {
        nameCell: 'API',
        lite: 'none',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
      {
        nameCell: 'Whatsapp API (each number, add on)',
        lite: '39',
        grow: '39',
        pro: 'Free',
        enterprise: 'Free',
      },
      {
        nameCell: 'Invoicing',
        lite: 'none',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
      {
        nameCell: 'Coupon',
        lite: 'none',
        grow: 'none',
        pro: 'check',
        enterprise: 'check',
      },
    ],
  },
  {
    nameHead: 'Support',
    listCell: [
      {
        nameCell: 'Chat',
        lite: 'check',
        grow: 'check',
        pro: 'check',
        enterprise: 'check',
      },
      {
        nameCell: 'Priority support',
        lite: 'none',
        grow: 'none',
        pro: 'check',
        enterprise: 'check',
      },
      {
        nameCell: 'Dedicated account manager',
        lite: 'none',
        grow: 'none',
        pro: 'check',
        enterprise: 'check',
      },
    ],
  },
  {
    nameHead: 'Integration (zapier, shopify etc)',
    listCell: [],
  },
];

type IPlan = {
  icon: IconName;
  nameApp: string;
  amount: number;
  paid: number;
  subTitle?: string;
  bgColor: string;
  requestMinutes?: string;
  unit: string;
  isShowMonth?: boolean;
};
export const dumpDataOrder: IPlan[] = [
  {
    bgColor: '#fff',
    nameApp: 'PRO*',
    paid: 49,
    icon: 'warning',
    unit: 'user',
    amount: 2,
    isShowMonth: false,
  },
  {
    bgColor: '#faf7ee',
    icon: 'whatsapp',
    nameApp: 'WhatsApp',
    subTitle: 'Add more number  to your inbox channel.',
    paid: 25,
    unit: 'number',
    amount: 12,
    isShowMonth: true,
  },
  {
    bgColor: '#faf7ee',
    icon: 'whatsapp',
    nameApp: 'WhatsApp',
    subTitle: 'Add more number  to your inbox channel.',
    paid: 20,
    unit: 'number',
    amount: 0,
    isShowMonth: true,
  },
  {
    bgColor: '#faf7ee',
    icon: 'whatsapp',
    nameApp: 'WhatsApp',
    subTitle: 'Add more number  to your inbox channel.',
    paid: 40,
    unit: 'add-on',
    amount: 200,
    requestMinutes: 'Requests Per Minute',
    isShowMonth: true,
  },
];

export const listPaid = [
  {
    count: 2,
    paid: 2230,
    nameApp: 'Pro user licenses',
    subName: '(2 unassigned)',
  },
  {
    count: 12,
    paid: 1100,
    nameApp: 'WhatsApp',
    subName: 'Add whatsapp number',
  },
  {
    count: 12,
    paid: 2830,
    nameApp: 'Raise API limit',
    subName: 'Requests Per Minute',
  },
];

export const ExampleData = [
  {
    title: '1. Debtor Representation',
    content:
      'The DEBTOR hereby represents and warrants that both parties in this agreement have set a payment plan to secure the deficiency in a scheduled manner set herein without further interruption, notwithstanding an additional fees for processing of such scheduling.',
  },
  {
    title: '2. Payment Plan',
    content:
      'The Parties hereby agree to the scheduled payment plan, as to the declaration of its contents found on Exhibit A attached hereto (the "Payment Plan"). The DEBTOR shall conform to the schedule set and shall pay to the CREDITOR before or upon due the amount as indicated on the Payments Schedule table.',
  },
  {
    title: '3. Payment Method',
    content:
      'Payment shall preferrably be made to the CREDITOR in accordance to the mode as indicated in the Payment Plan, but in any case, the DEBTOR may choose his method of payment to his convenience.',
  },
  {
    title: '4. Indemnification',
    content:
      'In consideration for this Agreement, the CREDITOR hereby releases any other claims against the DEBTOR in relation to fees and penalties resulting from the deficiency or any damages prior this Agreement. However, no obligation shall release the DEBTOR from its obligations herein or limit the rights of the CREDITOR in relation to this Agreement.',
  },
  {
    title: '5. Acceleration Clause',
    content:
      'In the occurrence that the DEBTOR fails to render payment upon reaching fifteen (15) days after the scheduled payment plan, the full amount of the deficiency shall become due and demandable. Any further failure shall give rise to the right to the CREDITOR to demand for damages.',
  },
  {
    title: '6. Agreement Modification',
    content:
      'No modification of this Agreement shall be considered valid unless made in writing and agreed upon by both Parties.',
  },
  {
    title: '7. Assignment of Rights',
    content:
      'The CREDITOR may transfer or assign this Agreement to a third party provided that a written notice to the DEBTOR is given. In the event of such assignment, the assignee may amend the schedule of payment found in this Agreement.',
  },
  {
    title: '8. Severability',
    content:
      'Should any provision found in this Agreement be held invalid, illegal, or unenforceable by any competent court, the same shall apply only to the provision and the rest of the remaining provisions hereto shall remain valid and enforceable.',
  },
  {
    title: '9. Applicable Law',
    content:
      'This Agreement shall be governed by and construed in accordance with the laws of the State of California. Subject to the exclusive jurisdiction of United States, California.',
  },
  {
    title: '9. Applicable Law',
    content:
      'This Agreement shall be governed by and construed in accordance with the laws of the State of California. Subject to the exclusive jurisdiction of United States, California.',
  },
];

export const ExampleDataTable = [
  {
    date: 'December 02, 2020',
    status: Math.random() < 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() < 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() < 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() < 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() < 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() > 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() > 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() > 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() > 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() > 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() > 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() > 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() > 1 ? 'paid' : 'refund',
    const: 49,
  },
  {
    date: 'December 02, 2020',
    status: Math.random() < 1 ? 'paid' : 'refund',
    const: 49,
  },
];
