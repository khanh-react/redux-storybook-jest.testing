import React from 'react';
import { headerFeature, dataBodyTable } from './types';
import { TableFeature } from './table';

export default {
  title: 'Account/Subscription/components/Table',
  component: TableFeature,
};

export const Default = () => {
  return <TableFeature headerTable={headerFeature} bodyTable={dataBodyTable} />;
};
