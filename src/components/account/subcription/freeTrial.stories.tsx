import React from 'react';

import { Index } from './freeTrial/freeTrial';

export default {
  title: 'Account/Subscription/FreeTrial',
  component: Index,
};

export const Default = () => {
  return <Index />;
};
