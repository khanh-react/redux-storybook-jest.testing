import React from 'react';
import styled from 'styled-components';
import Wrapper from './style';
import { Flex } from '../../../common/Flex';
import Text from '../../../common/Text';
import BootstrapButton from '../../../common/Button';

const Container = styled(Flex)`
  width: 1070px;
  border-radius: 10px;
  box-shadow: 0 2px 8px 0 rgba(210, 210, 210, 0.5);
  border: solid 1px #dcdcdc;
  background-color: #ffffff;
  margin-top: 20px;
`;

const StyledTitleCard = styled(Flex)`
  height: 55px;
  margin: 0 0 18px;
  padding: 17px 0px 18px 30px;
  border-radius: 4px;
  border-bottom: 1px solid #eeeeee;
`;
const StyledCardEnable = styled(Flex)`
  margin: 0px 30px 10px 30px;
  background-image: url(/static/subscription-calendar.svg);
  width: 150px;
  height: 139px;
  margin: 60px 137px 33px 240px;
`;

const StyledSubContent = styled(Flex)`
  padding: 45px 0px 5px 30px;
`;

const StyledTitleText = styled(Text)`
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.05px;
  color: #263238;
`;

const StyledTextTrial = styled(Text)`
  font-size: 24px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: rgba(0, 0, 0, 0.85);
`;

const StyledTextUserLicen = styled(Text)`
  margin: 3px 0px 40px 0px;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #2e2e32;
`;

const StyledSpanContent = styled.span`
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #2e2e32;
`;

const StyledButton = styled(BootstrapButton)`
  margin: 40px 0px 105px 0px;
  width: 160px;
  height: 38px;
  background: #097af4;
  color: #097af4;
  span {
    font-size: 14px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
  }
`;

const StyledDayTrial = styled.span`
  padding-top: 20px;
  font-size: 52px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #2e2e32;
`;
const StyledDayLeft = styled.span`
  font-size: 14px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #2e2e32;
`;

type IFreeTrial = {
  userLicenses: number;
  dayTrial: number;
};

export const FreeTrial = (User: IFreeTrial) => {
  return (
    <Container flexDirection="column">
      <StyledTitleCard>
        <StyledTitleText value="Your imbee plan" />
      </StyledTitleCard>
      <Flex justifyContent="space-between">
        <StyledSubContent flexDirection="column">
          <StyledTextTrial value="Free Trial" />
          <StyledTextUserLicen value={`${User.userLicenses} User licenses`} />
          <StyledSpanContent>
            You currently have <b>{User.dayTrial}</b> days left in your free trial{' '}
          </StyledSpanContent>
          <StyledButton kind="primary">
            <Text value="Update Plan" />
          </StyledButton>
        </StyledSubContent>
        <StyledCardEnable flexDirection="column" justifyContent="center">
          <StyledDayTrial>{User.dayTrial}</StyledDayTrial>
          <StyledDayLeft>days left</StyledDayLeft>
        </StyledCardEnable>
      </Flex>
    </Container>
  );
};

export const Index = () => {
  return (
    <Wrapper>
      <div className="header">
        <Text size="xl" weight="bold" value="Subscription" />
      </div>
      <FreeTrial dayTrial={27} userLicenses={2} />
    </Wrapper>
  );
};

export default Index;
