import styled from 'styled-components';
import { Dialog, Typography, DialogContent } from '@material-ui/core';

export default styled.div`
  width: calc(100% - 60px);
  padding: 30px;
  background-color: #f0f0f0;
  position: relative;

  .header {
    display: flex;
    flex-direction: column;
    height: 40px;
  }

  .options {
    position: absolute;
    right: 30px;
    top: 20px;
  }

  .slash {
    margin-left: 5px;
    margin-right: 5px;
  }

  .btn-text {
    margin-left: 5px;
  }

  .cell-text {
    font-family: Helvetica !important;
  }
`;

export const StyledDialog = styled(Dialog)`
  .MuiPaper-rounded {
    border-radius: 30px;
  }
`;

export const StyledInvoces = styled(StyledDialog)`
  .MuiDialog-paperScrollPaper {
    max-height: unset;
    height: 670px;
  }
  .MuiPaper-root {
    background: #fff;
    top: 50px;
  }
`;

export const StyledDialogTitle = styled(Typography)`
  margin: 12px 0 0px 25px;
  font-size: 22px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1;
  letter-spacing: normal;
  color: #2e2e32;
`;

export const StyledDialogContent = styled(DialogContent)`
  margin: 0px 42px 56px 32px;
`;

export const StyledDialogInvoices = styled(DialogContent)`
  padding: 30px 50px 56px 50px;
`;
