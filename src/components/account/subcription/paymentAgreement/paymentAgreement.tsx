import React from 'react';
import { createStyles, Theme, withStyles, WithStyles, makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Wrapper, { StyledDialog, StyledDialogTitle, StyledDialogContent } from '../freeTrial/style';
import Feature from '../feature/feature';
import { ExampleData } from '../components/types';
import { Flex } from '../../../common/Flex';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(3),
      top: theme.spacing(2),
      color: 'black',
    },
  });

const useStyles = makeStyles(() =>
  createStyles({
    Content: {
      fontSize: '12px',
      fontStretch: 'normal',
      fontStyle: 'normal',
      letterSpacing: 'normal',
      color: '#2e2e32',
      marginBottom: '10px',
    },
    height: {
      height: '620px',
    },
  }),
);

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <StyledDialogTitle>{children}</StyledDialogTitle>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

export const Home = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Wrapper>
      <Feature activeStep={3} />
      <StyledDialog
        onClose={handleClose}
        className={classes.height}
        aria-labelledby="customized-dialog-title"
        open={open}
        fullWidth
        maxWidth="md"
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Payment Agreement
        </DialogTitle>
        <StyledDialogContent>
          {ExampleData.map((value) => {
            return (
              <Flex key={value.title} flexDirection="column">
                <Typography className={classes.Content}>{value.title}</Typography>
                <Typography className={classes.Content}>{value.content}</Typography>
              </Flex>
            );
          })}
          <Typography className={classes.Content}>
            Name & Signature of Creditor Name & Signature of Debtor Payment Schedule Payment Method Amount Scheduled
            Date Check $2,000 02/13/2020 Cash $500 03/19/2020 Cash $500 04/18/2020 Name & Signature of Creditor Name &
            Signature of Debtor
          </Typography>
        </StyledDialogContent>
      </StyledDialog>
    </Wrapper>
  );
};

export default Home;
