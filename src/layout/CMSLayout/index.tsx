import React from 'react';

import { Flex } from '../../components/common/Flex';
import NavBar from '../../components/common/NavBar';
import TopBar from '../../components/common/TopBar';

type CMSLayoutProps = {
  children: React.ReactNode;
};

const CMSLayout = (props: CMSLayoutProps) => {
  return (
    <Flex height="100vh" width="100vw" alignItems="stretch">
      <NavBar />
      <Flex flexDirection="column" alignItems="stretch" flex={1} style={{ overflowX: 'auto' }}>
        <TopBar />
        <Flex flex={1} bgcolor="#f6f7f8" boxShadow="0px -5px 5px -8px rgba(0,0,0,0.5)">
          {props.children}
        </Flex>
      </Flex>
    </Flex>
  );
};

export default CMSLayout;
