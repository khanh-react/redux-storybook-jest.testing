import React from 'react';

type DefaultLayoutProps = {
  children: React.ReactNode;
};

/**
 * A basic plain background layout
 */
const DefaultLayout = (props: DefaultLayoutProps) => {
  return <>{props.children}</>;
};

// DefaultLayout.propTypes = {
//   children: PropTypes.node
// }

export default DefaultLayout;
