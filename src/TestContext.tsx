import { ThemeProvider } from '@material-ui/core';
import React from 'react';
import { createTheme } from './theme';
import { NoProps } from './utils/types';

export function MockedTheme({ children }: React.PropsWithChildren<NoProps>) {
  return <ThemeProvider theme={createTheme()}>{children}</ThemeProvider>;
}
