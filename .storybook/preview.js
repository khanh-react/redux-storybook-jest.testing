import { MuiThemeProvider, StylesProvider } from '@material-ui/core';
import React, { createElement } from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { configureStore } from '../src/redux/store';
import { createTheme } from '../src/theme';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
};
const theme = createTheme();
const generateClassName = (rule, sheet) => {
  return `${sheet.options.classNamePrefix}-${rule.key}`;
};

const themeDecorator = (storyFn) => (
  <MuiThemeProvider theme={theme}>
    <ThemeProvider theme={theme}>
      {process.env.NODE_ENV === 'test' ? (
        <StylesProvider generateClassName={generateClassName} injectFirst>
          {storyFn()}
        </StylesProvider>
      ) : (
        <StylesProvider injectFirst>{storyFn()}</StylesProvider>
      )}
    </ThemeProvider>
  </MuiThemeProvider>
);

const store = configureStore();
const reduxDecorator = (storyFn) => <Provider store={store}>{storyFn()}</Provider>;

export const decorators = [createElement, themeDecorator, reduxDecorator];
